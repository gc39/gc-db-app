<?

 //llamada
  //para pruebas  
  //    /usr/local/bin/php /home/digitalboard/public_html/sub_dominios/sodexo/app/Cron_jobs/sodexo_alerta_casinos_sin_publicacion.php nosend >/dev/null 2>&1


header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');

$fecha = new DateTime("now", new DateTimeZone('America/Santiago'));
$fecha_hora = $fecha->format('Y-m-j H:i:s');
$fecha_hoy = $fecha->format('Y-m-j');
$numday = $fecha->format("N");



if($numday <= 5 )
{
    $i=1;
    //Incluye datos de Config y valida 
    $includeOK = include_once __DIR__.'/../Config/config.php';
    if ($includeOK != true) {
        echo $tipo = " Error include";
        $comentario = $Tag.": Sin contenido en carpeta [$dst_dir], configurada sin imagen";
        graba_log($id_config_modulos, $tipo, $comentario);
        die();
    }

    //asigna datos desde config en el include anterior
    $server_db  = LOCALSERVER;
    $user_db    = LOCALUSERDB;
    $password_db = LOCALPASSDB;
    $db_db = LOCALDB;

    //Conecta a base de datos
    $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die ("db_error");
    if(!$obj_conexion) {
        echo 'error';
        die();
    }

    $select_qry = "SELECT
        Alerta_SinExhibicionHoy.cliente_sucursal, 
        Alerta_SinExhibicionHoy.nombre, 
        Alerta_SinExhibicionHoy.segmento, 
        Alerta_SinExhibicionHoy.emails
    FROM
        Alerta_SinExhibicionHoy";

    if (!$resultado = $obj_conexion->query($select_qry)) {
        echo 'error';
        die();
    }
    
    if ($resultado->num_rows != 0) {
        while($rows = $resultado->fetch_assoc()){
            $cliente_sucursal = $rows['cliente_sucursal'];
            $nombre = $rows['nombre'];
            $segmento = $rows['segmento'];
            $emails = $rows['emails'];

            $msg = "Nuestro sistema ha detectado que no existe contenido cargado para el d&iacute;a de hoy en la soluci&oacute;n digital de men&uacute; board para {$nombre}.<br><br>Recordamos cargar la minuta con anticipaci&oacute;n e idealmente la planificaci&oacute;n completa de una semana. <br><br>Esto asegurar&aacute; un mejor funcionamiento de la soluci&oacute;n. Si tienes alguna inquietud de c&oacute;mo cargar la minuta en el sistema, favor contacta al equipo de cuentas de Grupo Clan al correo lc@grupoclan.cl <br><br>Si tienes alguna inquietud respecto al funcionamiento de la pantalla, favor contacta a la Mesa de Ayuda de Grupo Clan al correo soporte@grupoclan.cl";

            $subject = "Alerta casino sin contenido: '{$nombre}'";

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; // No hace falta $headers .= 'To: <'.$_POST['email'].'>,' . "\r\n";
            $headers .= 'From: <no-reply@grupoclan.cl>,' . "\r\n";


           if($argv[1]=="send")
           //if(1==1)
            {
                //echo $i.' -- '.$nombre.'<br>';
                mail($emails, $subject, $msg, $headers);

                $fecha = new DateTime("now", new DateTimeZone('America/Santiago'));

                //graba reporte
                graba_reporteAlertaSinPublicacion($fecha_hoy, $fecha->format("H:i:s"), $cliente_sucursal, $segmento, $emails);

                //Log del envio
                $logFile = fopen("sendmail_log", 'a') or die("Error creando archivo");
                fwrite($logFile, "\n".$fecha->format("Y-m-j ; H:i:s")." ; $cliente_sucursal ; $emails ") or die("Error escribiendo en el archivo");
                fclose($logFile);

                $comentario ='correos enviados';
            }
            else
            {
              //echo $i.' -- '.$nombre.'--------------> '. $emails .'<br>';
              //mail('aga@grupoclan.cl', $subject, $msg, $headers); die(); 
              $comentario ='correos NO enviados';
            }

            $i++;


        }
        $i--;
        echo ''.$i . ' '. $comentario;
    }

}

//Graba los log reporteAlertaSinPublicacion
function graba_reporteAlertaSinPublicacion($fecha, $hora, $cliente_sucursal, $segmento, $emails){
    $cliente_sucursal_partes = split('_', $cliente_sucursal);
    $cliente = $cliente_sucursal_partes[0];
    $sucursal = $cliente_sucursal_partes[1];
    global $obj_conexion;
    $sql = "INSERT INTO reportes_alerta_sin_publicacion(fecha,hora,segmento,cliente,sucursal,emails,created) VALUES ('$fecha','$hora','$segmento','$cliente','$sucursal','$emails',NOW())";
    $resultado = $obj_conexion->query($sql);
    return $resultado;
}

?>