
<?php

/******************************************************************

- Para ejecutar manualmente desde una consola SSH en produccion
$ /usr/local/bin/php /home/digitalboard/public_html/sub_dominios/sodexo/app/Cron_jobs/sodexo_cronjob_diario.php

- Para ejecutar manualmente desde una consola SSH en desarrollo
$ /usr/local/bin/php /home/dev/public_html/sub_dominios/sodexo/app/Cron_jobs/sodexo_cronjob_diario.php

******************************************************************/

    //Tag para los log
    $Tag='CronJob_Diario';

    //Destinatarios si hay un error
    $destinosSendMAil= "ti@grupoclan.cl,bb@grupoclan.cl,fg@grupoclan.cl";
    $tituloSendMail='SODEXO - Error de Publicacion';
    $serverApiSendMail="https://digitalboard.cl/api/SendMail/";

    //Incluye datos de Config y valida 
    $includeOK = include_once __DIR__.'/../Config/config.php';
    if ($includeOK != true) {
        echo $tipo = " Error include";
        $comentario = $Tag.": Sin contenido en carpeta [$dst_dir], configurada sin imagen";
        graba_log($id_config_modulos, $tipo, $comentario);
        die();
    }

    //asigna datos desde config en el include anterior
    $server_db  = LOCALSERVER;
    $user_db    = LOCALUSERDB;
    $password_db = LOCALPASSDB;
    $db_db = LOCALDB;

    $fecha = new DateTime("now", new DateTimeZone('America/Santiago'));
    $fecha_hoy = $fecha->format('Y/m/j');
    $numday = $fecha->format("N");
    $hoy = $fecha->format("Ymd");

    $current_time = $fecha->format('Y-m-d');
    $date_parts = explode('-', $current_time);
    $date_parts[2] = str_replace("0", "", $date_parts[2]); 


    $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die ("db_error");
    if(!$obj_conexion)
    {
        echo $comentario = $Tag.": Erro de Base de Datos";
        //Envia email de alerta Error
        $mensajeSendMail = $comentario;
        file_get_contents($serverApiSendMail."?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
        die();
    }

    $select_qry = "SELECT
                        config_modulos.id, 
                        config_modulos.cliente_sucursal, 
                        config_modulos.origen, 
                        config_modulos.destino, 
                        config_carpetas.carpeta AS carpeta, 
                        config_carpetas.logo, 
                        config_ftp.Ip, 
                        config_ftp.`Port`, 
                        config_ftp.Usuario, 
                        config_ftp.Clave
                    FROM config_carpetas INNER JOIN config_modulos ON config_carpetas.id_config_modulos = config_modulos.id
                    INNER JOIN config_ftp ON config_modulos.id_config_ftp = config_ftp.id
                    WHERE config_modulos.estado = 1 order by config_ftp.Usuario, config_modulos.origen";


    if (!$resultado = $obj_conexion->query($select_qry)) {
        $comentario = $Tag.": Error en la query";
        //Envia email de alerta Error
        $mensajeSendMail = $comentario;
        file_get_contents($serverApiSendMail."?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
        die();
    }

    $tipo = "Tiempo";
    $comentario= "Diario: INICIO";
    graba_log(0, $tipo, $comentario);


    if ($resultado->num_rows != 0) {
        $LastUsuario = '';

        while($rows = $resultado->fetch_assoc()){
            $Ip = $rows['Ip'];
            $Port = $rows['Port'];
            $Usuario = $rows['Usuario'];
            $Clave = $rows['Clave'];
            $LogoDefault = RUTAORIGENLOGOS.$rows['logo'];

            $origen = RUTAORIGENCARPETASCRONJOB . $rows['origen'];
            $carpeta = $rows['carpeta'];
            $destino = $rows['destino'];
            $id_cronjob_modulos = $rows['id'];

            //si la ip de connexion es la misma no vuelve a conectar para el mismo usuario
            //echo $LastUsuario .'!=='. $Usuario.'<br>';
            if ($LastUsuario !== $Usuario){

                if( $LastUsuario != ''){
                    ftp_close($ftp_conn);
                    $tipo = "Info";
                    $comentario = $Tag.": Desconectado al ftp [".$LastUsuario."]";
                    graba_log($Lastid_config_modulos, $tipo, $comentario);
                }

                //Conecta al FTP
                $ftp_conn = @ftp_connect($Ip, $Port);
                $login_res = ftp_login($ftp_conn, $Usuario, $Clave);

                if ($login_res == 1){
                    //Registro Log
                    $tipo = "Info";
                    $comentario = $Tag.": Conectado al ftp [".$Usuario."]";
                    graba_log($id_config_modulos, $tipo, $comentario);
                }else{
                    //Registro Log
                    $tipo = "Error";
                    $comentario = $Tag.": Error al conectar al ftp [".$Usuario."]";
                    graba_log($id_config_modulos, $tipo, $comentario);

                    //Envia email de alerta Error
                    $mensajeSendMail = $comentario;
                    file_get_contents($serverApiSendMail."?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
                }
            }

            if ($login_res){

                $destinos = explode('/', $destino);
                $root = '/';
                if(count($destinos) > 3){
                    $root = '/'.$destinos[1].'/';
    
                    array_splice($destinos,1,1);
                }
                ftp_chdir($ftp_conn, '/');

                //crea la primera carpeta de destino empresa
                if (ftp_mkdir($ftp_conn, $root.$destinos[1])) {
                    $tipo = "Alerta";
                    $comentario = $Tag.": Crea carpeta  /$destinos[1]";
                    graba_log($id_config_modulos, $tipo, $comentario);
                }

                //crea la segunda carpeta de destino cliente
                if (ftp_mkdir($ftp_conn, $root.$destinos[1] . '/' . $destinos[2])) {
                    $tipo = "Alerta";
                    $comentario = $Tag.": Crea carpeta  /$destinos[1]/$destinos[2]";
                    graba_log($id_config_modulos, $tipo, $comentario);
                }

                //crea la tercera carpeta de destino sucursal
                if (ftp_mkdir($ftp_conn, $root.$destinos[1] . '/' . $destinos[2]. '/' . $destinos[3])) {
                    $tipo = "Alerta";
                    $comentario = $Tag.": Crea carpeta  /$destinos[1]/$destinos[2]/$destinos[3]";
                    graba_log($id_config_modulos, $tipo, $comentario);
                }

                //crea carpeta correspondiente al N de hoy
                if (ftp_mkdir($ftp_conn, $root.$destinos[1] . '/' . $destinos[2]. '/' . $destinos[3] . '/' . $numday)) {
                    $tipo = "Alerta";
                    $comentario = $Tag.": Crea carpeta  /$destinos[1]/$destinos[2]/$destinos[3]/$numday";
                    graba_log($id_config_modulos, $tipo, $comentario);
                }

                //crea carpeta de servicio
                if (ftp_mkdir($ftp_conn, $root.$destinos[1] . '/' . $destinos[2]. '/' . $destinos[3]. '/' . $numday . $carpeta)) {
                    $tipo = "Alerta";
                    $comentario = $Tag.": Crea carpeta  /$destinos[1]/$destinos[2]/$destinos[3]/$numday$carpeta";
                    graba_log($id_config_modulos, $tipo, $comentario);
                }

                $origen = getOrigen($origen, $carpeta, $fecha_hoy);
                $destino = $destino . '/' . $numday . $carpeta;
            
                //Funcion para borrar carpetas
                ftp_delete_folder($ftp_conn, $destino, $id_cronjob_modulos);

                //Funcion para copiar carpetas o poner logo si no existe contenido
                ftp_copy($ftp_conn, $origen, $destino, $LogoDefault, $id_cronjob_modulos);
                
            }
            
            $LastUsuario = $Usuario;
            $Lastid_cronjob_modulos =$id_cronjob_modulos;
            //$LastOrigen = $origen;
        }

        ftp_close($ftp_conn);
        $tipo = "Info";
        $comentario = $Tag.": Desconectado al ftp [".$Usuario."]";
        graba_log($id_config_modulos, $tipo, $comentario);

    }else{

        $id_config_modulos=0;
        $tipo = "Error";
        $comentario = $Tag.": Sin Modulos activos encontrados para [$cliente_sucursal]";
        graba_log($id_config_modulos, $tipo, $comentario);

        //Envia email de alerta Error
        $mensajeSendMail = $comentario;
        file_get_contents($serverApiSendMail."?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
    
    }

    $tipo = "Tiempo";
    $comentario= "Diario: FIN";
    graba_log(0, $tipo, $comentario);

    $obj_conexion->close();


    //Graba los log   
    function graba_log($id_config_modulos, $tipo, $comentario){
        global $obj_conexion;
        $sql = "INSERT INTO config_log (id_config_modulos,fechahora,tipo,comentario) VALUES ($id_config_modulos, NOW(), '$tipo', '$comentario')";
        $resultado = $obj_conexion->query($sql);
        return $resultado;
    }


    //obtiene la ruta de origen ordenada
    function getOrigen($origen, $carpeta, $fecha_hoy){
        //echo $current_time = date('Y-m-d', strtotime("+$numday day"));
        $date_parts = explode('/', $fecha_hoy);
        if (substr($date_parts[2],0,1) === "0"){
            $date_parts[2] = str_replace("0", "", $date_parts[2]); 
        }
        return $origen . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . $carpeta;
    }

    //Copia el contenido al FTP
    function ftp_copy($ftp_conn, $src_dir, $dst_dir, $LogoDefault, $id_config_modulos){
        $cdir = scandir($src_dir);
        if ($cdir == false || (count($cdir) == 2 && $cdir[0]=='.' && $cdir[1]=='..')){
            $explode_src_dir = explode('/', $LogoDefault);
            $archivo =  $explode_src_dir[count($explode_src_dir)-1];
            $resultadocopia = ftp_put($ftp_conn, $dst_dir . '/' . $archivo , $LogoDefault, FTP_BINARY);
            
            if ($resultadocopia ==""){
                $tipo = "Alerta";
                $comentario = $GLOBALS["Tag"].": Sin contenido en carpeta [$dst_dir], configurada sin imagen.. ".$LogoDefault;
                graba_log($id_config_modulos, $tipo, $comentario);
            }else{
                $tipo = "Alerta";
                $comentario = $GLOBALS["Tag"].": Sin contenido en carpeta [$dst_dir], se copia logo sodexo [$LogoDefault]";
                graba_log($id_config_modulos, $tipo, $comentario);
            }

        }
        else {
            foreach ($cdir as $key => $value) {
                if (!in_array($value, array(".", ".."))) {
                    if (is_file($src_dir . '/' . $value)) {
                        $src_dir . '/' . $value . PHP_EOL;
                        $fue_copiado = ftp_put($ftp_conn, $dst_dir . "/" . $value, $src_dir . '/' . $value, FTP_BINARY);

                        $tipo = "Info";
                        $comentario = $GLOBALS["Tag"].": Copia contenido [$value] ==> [$dst_dir]";
                        graba_log($id_config_modulos, $tipo, $comentario);
                    }
                }
            }
        }
    }

    //Borra contenido viejo del FTP
    function ftp_delete_folder($ftp_conn, $dst_dir, $id_config_modulos){
        ftp_chdir($ftp_conn, $dst_dir);
        $files = ftp_nlist($ftp_conn, ".");
        foreach ($files as $file) {
            if ($file != "." && $file != "..") {
                if(substr($file, 0, 4) != 'Gnr_'){
                    $borra_contenido = ftp_delete($ftp_conn, $file);
                    if ($borra_contenido == "1"){
                        $tipo = "Info";
                        $comentario = $GLOBALS["Tag"].": Borra contenido viejo [$file] ==> [$dst_dir] ";
                        graba_log($id_config_modulos, $tipo, $comentario);
                    }else{
                        $tipo = "Error";
                        $comentario = $GLOBALS["Tag"].": Error al borrar contenido viejo [$file] ==> [$dst_dir] ";
                        graba_log($id_config_modulos, $tipo, $comentario);
                    }
                }else{
                    $tipo = "Alerta Gnr";
                    $comentario = $GLOBALS["Tag"].": Archivo Gnr_ agregado manual no borrado [$file] ==> [$dst_dir] ";
                    graba_log($id_config_modulos, $tipo, $comentario);

                }
            }
        }
    }
