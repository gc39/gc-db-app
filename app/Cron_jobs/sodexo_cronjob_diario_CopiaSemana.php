
<?php

/******************************************************************

- Para ejecutar manualmente desde una consola SSH en produccion
$ /usr/local/bin/php /home/digitalboard/public_html/sub_dominios/sodexo/app/Cron_jobs/sodexo_cronjob_diario_CopiaSemana.php

- Para ejecutar manualmente desde una consola SSH en desarrollo
$ /usr/local/bin/php /home/dev/public_html/sub_dominios/sodexo/app/Cron_jobs/sodexo_cronjob_diario_CopiaSemana.php

******************************************************************/

//Tag para los log
$Tag='CronJob_Diario';

//Destinatarios si hay un error
$destinosSendMAil= "ti@grupoclan.cl,bb@grupoclan.cl,fg@grupoclan.cl";
$tituloSendMail='SODEXO - Error Job copia Semanal';
$serverApiSendMail="https://digitalboard.cl/api/SendMail/";

//Incluye datos de Config y valida 
$includeOK = include_once __DIR__.'/../Config/config.php';
if ($includeOK != true) {
    echo $tipo = " Error include";
    $comentario = $Tag.": Sin contenido en carpeta [$dst_dir], configurada sin imagen";
    graba_log($id_config_modulos, $tipo, $comentario);
    die();
}

//asigna datos desde config en el include anterior
$server_db = LOCALSERVER;
$user_db = LOCALUSERDB;
$password_db = LOCALPASSDB;
$db_db = LOCALDB;


$fecha = new DateTime("now", new DateTimeZone('America/Santiago'));
$fecha_hoy = $fecha->format('Y/m/j');
$fecha_actual = $fecha->format('Y/m/j');
$numday = $fecha->format("N");
$numdayhoy = $fecha->format("N");
$hoy = $fecha->format("Ymd");

//Para Puebas con un dia X
// $fecha_hoy = '2021/01/22';
// $fecha_actual = '2021/01/22';
// $numday = '5';
// $numdayhoy = '5';
// $hoy = '20210122';



$current_time = $fecha->format('Y-m-d');
$date_parts = explode('-', $current_time);
$date_parts[2] = str_replace("0", "", $date_parts[2]); 




$obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die ("db_error");
if(!$obj_conexion)
{
    echo "Error de Base de Daros";
    $comentario = "Semanal: de Base de Datos";
    //Envia email de alerta Error
    $mensajeSendMail = $comentario;
    file_get_contents($serverApiSendMail."?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
    die();
}

$select_qry = "SELECT
                    config_modulos.id, 
                    config_modulos.cliente_sucursal, 
                    config_modulos.origen, 
                    config_modulos.destino, 
                    config_carpetas.carpeta AS carpeta, 
                    config_carpetas.logo, 
                    config_ftp.Ip, 
                    config_ftp.`Port`, 
                    config_ftp.Usuario, 
                    config_ftp.Clave
                FROM config_carpetas INNER JOIN config_modulos ON config_carpetas.id_config_modulos = config_modulos.id
                INNER JOIN config_ftp ON config_modulos.id_config_ftp = config_ftp.id
                WHERE config_modulos.estado = 1 order by config_ftp.Usuario, config_modulos.origen";

if (!$resultado = $obj_conexion->query($select_qry)) {
    echo "Error en query.";
    $comentario = "Semanal: Error en la query";
    //Envia email de alerta Error
    $mensajeSendMail = $comentario;
    file_get_contents($serverApiSendMail."?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
    die();
}

$tipo = "Tiempo";
$comentario= "Semanal: INICIO";
graba_log(0, $tipo, $comentario);

if ($resultado->num_rows != 0) {
    $LastUsuario = '';
    $LastOrigen  = '';

    while($rows = $resultado->fetch_assoc()){
        $Ip = $rows['Ip'];
        $Port = $rows['Port'];
        $Usuario = $rows['Usuario'];
        $Clave = $rows['Clave'];
        $LogoDefault = RUTAORIGENLOGOS.$rows['logo'];

        
        $ruta_origen = RUTAORIGENCARPETASCRONJOB . $rows['origen'];
        $carpeta = $rows['carpeta'];
        $ruta_destino = $rows['destino'];

        $id_cronjob_modulos = $rows['id'];

        //si la ip de connexion es la misma no vuelve a conectar para el mismo usuario
        if ($LastUsuario !== $Usuario){

            if( $LastUsuario != ''){
                ftp_close($ftp_conn);
                $tipo = "Info";
                $comentario= "Semanal: Desconectado al ftp [".$LastUsuario."]";
                graba_log($Lastid_cronjob_modulos, $tipo, $comentario);
            }


            //Conecta al FTP
            $ftp_conn = @ftp_connect($Ip, $Port);
            $ftp_logged = ftp_login($ftp_conn, $Usuario, $Clave);

            if ($ftp_logged){
                //Registro Log
                $tipo = "Info";
                $comentario= "Semanal: Conectado al ftp [".$Usuario."]";
                graba_log($id_cronjob_modulos, $tipo, $comentario);
            }else{
                //Registro Log
                $tipo = "Error";
                $comentario="Semanal: Error al conectar al ftp [".$Usuario."]";
                graba_log($id_cronjob_modulos, $tipo, $comentario);

                //Envia email de alerta Error
                $mensajeSendMail = $comentario;
                file_get_contents($serverApiSendMail."?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
            }
        }

        if ($ftp_logged){

            $destinos = explode('/', $ruta_destino);
            $root = '/';
            if(count($destinos) > 3){
                $root = '/'.$destinos[1].'/';

                array_splice($destinos,1,1);
            }
            ftp_chdir($ftp_conn, '/');

            for($i=0;$i<7;$i++){
                if ($numday != $numdayhoy){
                    $Publish = '';
                    $Cronjob = '';
                    Publish_Cronjob($Publish, $Cronjob, $ruta_origen);

                    if (($Publish == $Cronjob) && ($Publish == $hoy)){
                        //Registro Log
                        $tipo = "Error";
                        $comentario="Semanal: Cronjob no actualizo, ya pazo hoy";
                        graba_log($id_cronjob_modulos, $tipo, $comentario);

                        //Envia email de alerta Error
                        $mensajeSendMail = $comentario;
                        file_get_contents($serverApiSendMail."?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));

                    }else{

                        if ($Publish>$Cronjob) {
                            
                            //Registro Log
                            $tipo = "Info";
                            $comentario="Semanal: Actualiza [$carpeta] 6 dias, Actializando $i de 6 dias, fecha [$fecha_hoy] carpeta $numday [$carpeta]";
                            graba_log($id_cronjob_modulos, $tipo, $comentario);

                        }else{

                            $i=7;
                            $fechaMasSeisDias = strtotime('+6 day', strtotime($fecha_actual));
                            $numday = date("N", $fechaMasSeisDias);
                            $fecha_hoy = date('Y/m/j', $fechaMasSeisDias);

                            //Registro Log
                            $tipo = "Info";
                            $comentario="Semanal: Actualiza 1 dia, Actializando fecha [$fecha_hoy] carpeta $numday [$carpeta]";
                            graba_log($id_cronjob_modulos, $tipo, $comentario);
                        }

                        //crea la primera carpeta 
                        if (ftp_mkdir($ftp_conn, $root.$destinos[1])) {
                            $tipo = "Alerta";
                            $comentario= "Semanal: Crea carpeta  /$destinos[1]";
                            graba_log($id_cronjob_modulos, $tipo, $comentario);
                        }

                        //si la segunda carpeta existe la crea
                        if(isset($destinos[2])){
                            if (ftp_mkdir($ftp_conn, $root.$destinos[1] . DIRECTORY_SEPARATOR . $destinos[2])) {
                                $tipo = "Alerta";
                                $comentario= "Semanal: Crea carpeta  /$destinos[1]/$destinos[2]";
                                graba_log($id_cronjob_modulos, $tipo, $comentario);
                            }

                            //crea carpeta de hoy
                            if (ftp_mkdir($ftp_conn, $root.$destinos[1] . DIRECTORY_SEPARATOR . $destinos[2] . DIRECTORY_SEPARATOR . $numday)) {
                                $tipo = "Alerta";
                                $comentario= "Semanal: Crea carpeta  /$destinos[1]/$destinos[2]/$numday";
                                graba_log($id_cronjob_modulos, $tipo, $comentario);
                            }

                            //separa carpetas a crear
                            $carpetas = explode('/', $carpeta);

                            //crea primera carpeta
                            if (ftp_mkdir($ftp_conn, $root.$destinos[1] . '/' . $destinos[2]. '/' . $numday .'/' . $carpetas[1])) {
                                $tipo = "Alerta";
                                $comentario= "Semanal: Crea carpeta  /$destinos[1]/$destinos[2]/$numday/$carpetas[1]";
                                graba_log($id_cronjob_modulos, $tipo, $comentario);
                            }

                            //crea segunda carpeta
                            if (ftp_mkdir($ftp_conn, $root.$destinos[1] . '/' . $destinos[2]. '/' . $numday .'/' . $carpetas[1].'/'.$carpetas[2])) {
                                $tipo = "Alerta";
                                $comentario= "Semanal: Crea carpeta  /$destinos[1]/$destinos[2]/$numday/$carpetas[1]/$carpetas[2]";
                                graba_log($id_cronjob_modulos, $tipo, $comentario);
                            }

                            //si la tercera carpeta existe la crea
                            if(isset($carpetas[3])){
                                if (ftp_mkdir($ftp_conn, $root.$destinos[1] . '/' . $destinos[2]. '/' . $numday .'/' . $carpetas[1].'/'.$carpetas[2]).'/'.$carpetas[3]) {
                                    $tipo = "Alerta";
                                    $comentario= "Semanal: Crea carpeta  /$destinos[1]/$destinos[2]/$numday/$carpetas[1]/$carpetas[2]/$carpetas[3]";
                                    graba_log($id_cronjob_modulos, $tipo, $comentario);
                                }
                            }


                        }else{

                            //crea carpeta de hoy
                            if (ftp_mkdir($ftp_conn, $root.$destinos[1] . '/' . $numday)) {
                                $tipo = "Alerta";
                                $comentario = "Semanal: Crea carpeta  /$destinos[1]/$numday";
                                graba_log($id_cronjob_modulos, $tipo, $comentario);
                            }

                            //separa carpetas a crear
                            $carpetas = explode('/', $carpeta);

                            //crea primera carpeta
                            if (ftp_mkdir($ftp_conn, $root.$destinos[1] . '/' . $numday .'/' . $carpetas[1])) {
                                $tipo = "Alerta";
                                $comentario= "Semanal: Crea carpeta  /$destinos[1]/$numday/$carpetas[1]";
                                graba_log($id_cronjob_modulos, $tipo, $comentario);
                            }

                            //crea segunda carpeta
                            if (ftp_mkdir($ftp_conn, $root.$destinos[1] . '/' . $numday .'/'. $carpetas[1] .'/'. $carpetas[2])) {
                                $tipo = "Alerta";
                                $comentario= "Semanal: Crea carpeta  /$destinos[1]/$numday/$carpetas[1]/$carpetas[2]";
                                graba_log($id_cronjob_modulos, $tipo, $comentario);
                            }

                            //si la tercera carpeta existe la crea
                            if(isset($carpetas[3])){
                                if (ftp_mkdir($ftp_conn, $root.$destinos[1] . '/' . $numday .'/' . $carpetas[1].'/'.$carpetas[2]).'/'.$carpetas[3]) {
                                    $tipo = "Alerta";
                                    $comentario= "Semanal: Crea carpeta  /$destinos[1]/$numday/$carpetas[1]/$carpetas[2]/$carpetas[3]";
                                    graba_log($id_cronjob_modulos, $tipo, $comentario);
                                }
                            }
                        }


                        $origen = getOrigen($ruta_origen, $carpeta, $fecha_hoy);
                        $destino = getDestino($ruta_destino, $carpeta , $numday);
                    

                        //Funcion para borrar carpetas
                        ftp_delete_folder($ftp_conn, $destino, $id_cronjob_modulos);

                        //Funcion para copiar carpetas o poner logo si no existe contenido
                        ftp_copy($ftp_conn, $origen, $destino, $LogoDefault, $id_cronjob_modulos);

                        //echo $numday .'-->'. $fecha_hoy.'<br>';

                        $date_time = new DateTime("now", new DateTimeZone('America/Santiago'));
                        echo $date_time->format("Y-m-d H:i:s").' -- '.$origen .' -- '. $destino.'<br>';
                        //echo $origen .'<br>';

                    }   


                }
                
                
                $fechaMasUno = strtotime('+1 day', strtotime($fecha_hoy));
                $fechaInicial = strtotime($fecha_actual);
                $fechaMasSieteDias = strtotime('+7 day', strtotime($fecha_actual));
                $fecha_hoy = ($fechaMasSieteDias > $fechaMasUno)? date('Y/m/j', $fechaMasUno) : date('Y/m/j',$fechaInicial) ;
                $numday = ($numday == 7) ? 1 : $numday + 1;

            }
            
        }

        //echo '<br><br>'.$LastOrigen .'<br>'. $ruta_origen.'<br><br>';
        if($LastOrigen !== '' && $LastOrigen !== $ruta_origen){
                deleteCronjobFile($LastOrigen);
                // Crear archivo marca
                $marca_hoy = $LastOrigen . DIRECTORY_SEPARATOR .  'Cronjob_' . $hoy . '.txt';
                $fileHandler = fopen($marca_hoy, 'w+');
                fclose($fileHandler);
        }

        $LastUsuario = $Usuario;
        $LastOrigen = $ruta_origen;
        $Lastid_cronjob_modulos =$id_cronjob_modulos;
    }

    ftp_close($ftp_conn);
    $tipo = "Info";
    $comentario= "Semanal: Desconectado al ftp [".$Usuario."]";
    graba_log($id_cronjob_modulos, $tipo, $comentario);




}else{
    $id_cronjob_modulos="00";
    $tipo = "Error";
    $comentario= "Semanal: Sin Modulos Activados";
    graba_log($id_cronjob_modulos, $tipo, $comentario);

    //Envia email de alerta Error
    $mensajeSendMail = $comentario;
    file_get_contents($serverApiSendMail."?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
}
$tipo = "Tiempo";
$comentario= "Semanal: FIN";
graba_log(0, $tipo, $comentario);

$obj_conexion->close();



function graba_log($id_cronjob_modulos, $tipo, $comentario){
    global $obj_conexion;
    $sql = "INSERT INTO cronjob_log (id_cronjob_modulos,fechahora,tipo,comentario) VALUES ($id_cronjob_modulos, NOW(), '$tipo', '$comentario')";
    $resultado = $obj_conexion->query($sql);
    return $resultado;
}

function getOrigen($origen, $carpeta, $fecha_hoy){
    //echo $current_time = date('Y-m-d', strtotime("+$numday day"));
    $date_parts = explode('/', $fecha_hoy);
    if (substr($date_parts[2],0,1) === "0"){
        $date_parts[2] = str_replace("0", "", $date_parts[2]); 
    }
    return $origen . DIRECTORY_SEPARATOR . $date_parts[0] . DIRECTORY_SEPARATOR . $date_parts[1] . DIRECTORY_SEPARATOR . $date_parts[2] . $carpeta;
}

function getDestino($destino, $carpeta, $numday){
    //$numday = date('N', strtotime("+$offset day"));
    return $destino . DIRECTORY_SEPARATOR . $numday . $carpeta;
}


//Copia contenido
function ftp_copy($ftp_conn, $src_dir, $dst_dir, $LogoDefault, $id_cronjob_modulos){
    $cdir = scandir($src_dir);
    if ($cdir == false || (count($cdir) == 2 && $cdir[0]=='.' && $cdir[1]=='..')){
        $explode_src_dir = explode('/', $LogoDefault);
        $archivo =  $explode_src_dir[count($explode_src_dir)-1];
        $resultadocopia = ftp_put($ftp_conn, $dst_dir . DIRECTORY_SEPARATOR . $archivo , $LogoDefault, FTP_BINARY);
        
        if ($resultadocopia ==""){
            $tipo = "Alerta";
            $comentario= "Semanal: Sin contenido en carpeta [$dst_dir], configurada sin imagen";
            graba_log($id_cronjob_modulos, $tipo, $comentario);
        }else{
            $tipo = "Alerta";
            $comentario= "Semanal: Sin contenido en carpeta [$dst_dir], se copia logo sodexo [$LogoDefault]";
            graba_log($id_cronjob_modulos, $tipo, $comentario);
        }

    }
    else {
        foreach ($cdir as $key => $value) {
            if (!in_array($value, array(".", ".."))) {
                if (is_file($src_dir . DIRECTORY_SEPARATOR . $value)) {
                    $src_dir . DIRECTORY_SEPARATOR . $value . PHP_EOL;
                    $fue_copiado = ftp_put($ftp_conn, $dst_dir . "/" . $value, $src_dir . DIRECTORY_SEPARATOR . $value, FTP_BINARY);

                    $tipo = "Info";
                    $comentario= "Semanal: Copia contenido [$value] ==> [$dst_dir]";
                    graba_log($id_cronjob_modulos, $tipo, $comentario);
                }
            }
        }
    }
}

//Borra contenido viejo
function ftp_delete_folder($ftp_conn, $dst_dir, $id_cronjob_modulos){
    ftp_chdir($ftp_conn, $dst_dir);
    $files = ftp_nlist($ftp_conn, ".");
    foreach ($files as $file) {
        if ($file != "." && $file != "..") {
            if(substr($file, 0, 4) != 'Gnr_'){
                $borra_contenido = ftp_delete($ftp_conn, $file);
                if ($borra_contenido == "1"){
                    $tipo = "Info";
                    $comentario= "Semanal: Borra contenido viejo [$file] ==> [$dst_dir] ";
                    graba_log($id_cronjob_modulos, $tipo, $comentario);
                }
            }else{
                $tipo = "Alerta Gnr";
                $comentario= "Semanal: Archivo Gnr_ agregado manual no borrado [$file] ==> [$dst_dir] ";
                graba_log($id_cronjob_modulos, $tipo, $comentario);
            }
        }
    }
}

function Publish_Cronjob(&$Publish, &$Cronjob, $ruta_origen)
    {
        $cdir = scandir($ruta_origen);
        foreach($cdir as $itm){
            if (substr( $itm, 0, 8 ) === "Cronjob_"){
                $Cronjob = substr( $itm, 8, 8);
            }
            if (substr( $itm, 0, 8 ) === "Publish_"){
                $Publish = substr( $itm, 8, 8);
            }
        }
    }

function deleteCronjobFile($directorio){
    $files = glob($directorio . '/*');
    foreach ($files as $file) {
        if (is_file(($file))) {
            if (strpos($file, 'Cronjob_') !== false) {
                unlink($file);
            }
        }
    }
}