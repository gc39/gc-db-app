<?php

class Content extends Model {   
    
    public function getSchema($module) {
        
        $this->useTable = $module;
        return $this->schema();
        
    }    
    
    public function getAllRows($module, $order, $fieldsData, $page, $rows_per_page, &$pages) {
        
        $this->useTable = $module;
        
        $contentCount = $this->find('count');        
        
        $pages= ceil($contentCount/$rows_per_page);
        
        if($page == 1) {    
            $start = 0;
        } else {
            $start = $rows_per_page * ($page - 1);
        }
        
        
        if($order) {
            $content = $this->find('all', array('order' => $order, 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
        } else {
            $content = $this->find('all', array('order' => 'id ASC', 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
        }

        return $content;
        
    }
    
    public function getLiveRelatedContent($module, $conditions, $fieldID, $fieldShow) {
        
        $this->useTable = $module;
        
        $content     = $this->find('all', array('conditions' => $conditions, 'order' => 'id ASC'));
        $returnArray = array();
        
        foreach ($content as $row) {            
            $returnArray[] = array('itemName' => '['.$row['Content'][$fieldID].'] '.$row['Content'][$fieldShow], 'value' => $row['Content'][$fieldID]);
        }

        return $returnArray;
        
    }       
    
    public function validateUnique($module, $id, $field, $value) {
        
        $this->useTable = $module;
        
        $conditions = array('id <> '.$id. ' AND '.$field. ' = '.$value);
        
        return $this->find('all', array('conditions' => $conditions));
        
        
    }       

    public function getDistinct($module, $field) {
        
        $this->useTable = $module;   
        return $this->find('all', array('fields' => array('DISTINCT ('.$field.') AS '.$field) ));
        
    }
    
    public function findAllRows($searchTerm, $module, $order, $fieldsData, $page, $rows_per_page, &$pages) {
        
        $query = false;
        
        $this->useTable = $module;
        
        
        foreach($fieldsData as $field) {   
            
            if($module == 'app_novedades'){
            
                if(isset($field['@db-type']) && (($field['@type'] == 'text') || ($field['@type'] == 'textarea') || ($field['@type'] == 'richtext') || ($field['@type'] == 'date') || (($field['@type'] == 'select') && $field['@key'] != 'send_notification') )) {               
                    $query[] = array($field['@key'].' LIKE' => '%'.$searchTerm.'%');    
                }   
            
            }else{
                if(isset($field['@db-type']) && (($field['@type'] == 'text') || ($field['@type'] == 'textarea') || ($field['@type'] == 'richtext') || ($field['@type'] == 'date') || ($field['@type'] == 'select') )) {               
                    $query[] = array($field['@key'].' LIKE' => '%'.$searchTerm.'%');
                }  
            }        
        }

        if(is_array($query)) {    
            $contentCount = $this->find('count', array('conditions' => array('OR' => $query)));           
        } else {        
            $contentCount = $this->find('count');         
        }
        
        $pages= ceil($contentCount/$rows_per_page);
        
        if($page == 1) {    
            $start = 0;
        } else {
            $start = $rows_per_page * ($page - 1);
        }
        
        
        if(is_array($query)) {
        
            if($order) {
                $content = $this->find('all', array('order' => $order, 'conditions' => array('OR' => $query), 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
            } else {
                $content = $this->find('all', array('order' => 'id ASC', 'conditions' => array('OR' => $query), 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
            }
            
        } else {        
            if($order) {
                $content = $this->find('all', array('order' => $order, 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
            } else {
                $content = $this->find('all', array('order' => 'id ASC', 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
            }
        }

        return $content;
        
    }   
    
    public function getById($module, $id, $fieldsData = false) {
        
        $this->useTable = $module;

        return $this->findById($id);
        
    }          
    
    public function getFieldTypeByKey($key, $fieldsData) {
        
        foreach($fieldsData as $field) {            
            if($field['@key'] == $key) {
                return $field['@type'];
            }
        }        
        
        return false;
        
    }
    
    public function customSave($conditions, $module) {
        
        $this->useTable = $module;
        
        return $this->save($conditions);
                
    }
    
    public function getItemModifications($module, $date, $service, $withService, $date_limit = false) {
        
        $this->useTable = $module.'_menu_diario_reportes';
        
        $shortDate = date('d-m-Y', strtotime($date));
        
       // echo $date_limit . '->' . $date.'<br>';
        
        if($date_limit === false) {
            
            if($withService) {
                $query = $this->find('all',                 
                    array('fields' => array( '*'), 
                        'conditions' => array(       
                            'servicio = "'.$service.'"',
                            'fecha = "'.$shortDate.'"',
                            'modified < TIMESTAMP("'.$date.'")',
                            'created <> modified'
                        )
                    )
                );
            } else {
                $query = $this->find('all',                 
                    array('fields' => array( '*'), 
                        'conditions' => array(                            
                            'fecha = "'.$shortDate.'"',
                            'modified < TIMESTAMP("'.$date.'")',
                            'created <> modified'
                        )
                    )
                );                
            }
            
        } else {
            
            if($withService) {
                $query = $this->find('all',                 
                    array('fields' => array( '*'), 
                        'conditions' => array(
                            'servicio = "'.$service.'"',
                            'fecha = "'.$shortDate.'"',
                            'modified BETWEEN TIMESTAMP("'.$date_limit.'") AND TIMESTAMP("'.$date.'")',
                            'created <> modified'
                        )
                    )
                );
            } else {
                $query = $this->find('all',                 
                    array('fields' => array( '*'), 
                        'conditions' => array(                            
                            'fecha = "'.$shortDate.'"',
                            'modified BETWEEN TIMESTAMP("'.$date_limit.'") AND TIMESTAMP("'.$date.'")',
                            'created <> modified'
                        )
                    )
                );                
            }    
        }
        
        return $query;
    }  
    
    public function getNoPublication($module, $date, $service, $withService, $date_limit = false) {
        
        App::import('Model', 'PublishHistory');

        $PublishHistory = new PublishHistory();
        
        $this->useTable = $module.'_menu_diario_reportes';
        
        $shortDate = date('d-m-Y', strtotime($date));
        
        if($withService) {
            $items = $this->find('all',                 
                array('fields' => array( '*'), 
                    'conditions' => array(
                        'servicio = "'.$service.'"',
                        'fecha = "'.$shortDate.'"'                        
                    )
                )
            );
        } else {
            $items = $this->find('all',                 
                array('fields' => array( '*'), 
                    'conditions' => array(                        
                        'fecha = "'.$shortDate.'"'                        
                    )
                )
            );           
        }

        $nopubitems = 0;
        
        foreach ($items as $item) {                   
            if(!$PublishHistory->havePublicationInPeriod($module.'_menu_diario', $item['Content']['modified'], $date)) {
                $nopubitems++;
            }
        }
        
        return $nopubitems;
    }  
    
    public function getFirstUploadMenuItem($service, $module, $date, $withService) {
        
        $this->useTable = $module;

        if($withService)  {
            return $this->find('first', 
                array('fields' => array( '*'), 
                    'conditions' => array(
                        'fecha = "'.$date.'"',
                        'servicio = "'.$service.'"'
                    ),
                    'order' => array('created ASC'),
                    'limit' => 1
                )
            );
        } else {
            return $this->find('first', 
                array('fields' => array( '*'), 
                    'conditions' => array(
                        'fecha = "'.$date.'"',                  
                    ),
                    'order' => array('created ASC'),
                    'limit' => 1
                )
            );
        }
    }
    
    public function customCreate($module) {
        
        $this->useTable = $module;
        
        $this->create();
        
        return true;
        
    }
    
    public function customDeleteAll($args, $module) {
        
        $this->useTable = $module;
        $this->deleteAll($args);
        
    }

    public function importFromCSV($file_path, $delete_all, $module) {
        
        $this->useTable = $module;
        
        if(empty($file_path)) {
            return 'FILE_NOT_FOUND';
        }
        
        $no_errors     = false;
        $return_status = true;        
        $colums_map    = array('C1' => 'dia', 'C2' => 'mes', 'C3' => 'nombre', 'C4' => 'cargo');        
        $month_names   = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
        
        ini_set("auto_detect_line_endings", true);
        
        if (($gestor = fopen($file_path, "r")) !== FALSE) {
            
            $row = 1;            
            
            while (($datos = fgetcsv($gestor, 1000, ";")) !== FALSE) {
                
                if($row == 1) {

                    $columns = count($datos);                

                    //pr($datos);
                    
                    for ($c = 0; $c < $columns; $c++) {
                        if(strtolower(trim($datos[$c])) == 'mes') { $month_index = $c; }
                        if(strtolower(trim($datos[$c])) == 'dia') { $day_index = $c; }
                        if(strtolower(trim($datos[$c])) == 'nombre') { $name_index = $c; }                        
                        if(strtolower(trim($datos[$c])) == 'cargo') { $position_index = $c; }                        
                    }

                } elseif(count($datos) >= 3) {

                    if(!isset($month_index) || !isset($day_index) || !isset($name_index)) {
                        $return_status = 'MISSING_COLUMNS';
                    } else {
                        
                      // pr($datos);

                        //pr($datos[0]);
                        $month = trim($datos[$month_index]);
                        $day   = trim($datos[$day_index]);
                        $name  = trim($datos[$name_index]);
                        
                        if(!empty($month) && !empty($day) && !empty($name)) {
                            
                            $position  = isset($position_index) ? trim($datos[$position_index]) : '';
                            
                            if(!in_array(strtolower($month), $month_names)) {
                                $return_status = 'INVALID_DATA';
                            } else {
                                if(!is_numeric($day)) {
                                    $return_status = 'INVALID_DATA';
                                } else {
                                    if(empty($name)) {
                                        $return_status = 'INVALID_DATA';
                                    } else {
                                        // todo ok    
                                        $no_errors = true;
                                        
                                    }

                                }
                            }
                        } 
                    }

                }


                $row++;
            }
            
            fclose($gestor);

            if($no_errors) {
                
                if($delete_all) {                    
                    $this->customDeleteAll(array('id <> 0'), $module);
                }
                
                $gestor = fopen($file_path, "r");
                $row    = 1;
                
                while (($datos = fgetcsv($gestor, 1000, ";")) !== FALSE) {

                    if($row != 1 && count($datos) >= 3) {

                        $month = trim($datos[$month_index]);
                        $day   = trim($datos[$day_index]);
                        $name  = trim($datos[$name_index]);
                        
                        if (!preg_match('!!u', $name)) {
                            $name  = utf8_encode($name);                           
                        }
                                                
                        if(!empty($month) && !empty($day) && !empty($name)) {
                            
                            $position  = isset($position_index) ? trim($datos[$position_index]) : '';
                            
                            if (!preg_match('!!u', $position)) {
                                $position  = utf8_encode($position);                           
                            }                        
                        
                            $id_row = $this->generateID($module);

                            $this->create();
                            $this->save(array('id_row' => $id_row, 'key' => 'C1', 'value' => $day));
                            $this->create();
                            $this->save(array('id_row' => $id_row, 'key' => 'C2', 'value' => ucwords($month)));
                            $this->create();
                            $this->save(array('id_row' => $id_row, 'key' => 'C3', 'value' => ucwords($name)));
                            $this->create();
                            $this->save(array('id_row' => $id_row, 'key' => 'C4', 'value' => ucwords($position)));
                        }
                    }

                    $row++;
                    
                } 
                
                fclose($gestor);
                
            }
            
            
        } else {
            $return_status = 'FILE_NOT_FOUND';
        }
        
        return $return_status;
    }
    
    public function importFromCSVGeneric($file_path, $delete_all, $module, $map, $separator = ';', $module, $segmento, $cliente, $sucursal, $usuario) {
        $jsonFileMaestroProductos       = file_get_contents(BASE_TEMPLATE . '/templates/json/prod/maestro_productos-data.json');
        $jsonFileMaestroProductos = json_decode($jsonFileMaestroProductos);
        $jsonMaestro = array();
        foreach($jsonFileMaestroProductos as $item){
            $jsonMaestro[$item->codigo]=$item;
        }

        $fecha_exhibicion = '';

        $date_array = array();
        
        $this->useTable = $module;
        
        if(empty($file_path)) {
            return 'FILE_NOT_FOUND';
        }                      
        
        $no_errors       = true;
        $return_status   = true;        
        $missing_columns = false;
        
        foreach ($map['map'] as $mapItem) {
            
            $mapColumnsIndexs[strtolower($mapItem['@column'])] = '';
            $mapColumnsDB[strtolower($mapItem['@column'])]     = $mapItem['@field'];
            
            if($mapItem['@optional'] == 'no') {
                $requiredColumns[strtolower($mapItem['@column'])] = strtolower($mapItem['@column']);
            }            
        }
        
        
        ini_set("auto_detect_line_endings", true);
        
        if (($gestor = fopen($file_path, "r")) !== FALSE) {
            
            $row = 1;            
            
            while (($datos = fgetcsv($gestor, 3000, $separator)) !== FALSE) {
                
                if($row == 1) {

                    $columns = count($datos);                

                    if($columns == 1) {
                        $missing_columns = true;
                        $no_errors       = false;   
                        $return_status = 'MISSING_COLUMNS_SEPARATOR';  
                    }
                    
                    for ($c = 0; $c < $columns; $c++) {
                        if(isset($mapColumnsIndexs[strtolower(trim($datos[$c]))])) { 
                            $mapColumnsIndexs[strtolower(trim($datos[$c]))] = $c;                             
                        }                   
                    }
                    
                    foreach ($requiredColumns as $requiredColumn) {
                        if(trim($mapColumnsIndexs[$requiredColumn]) == '') {
                            $missing_columns = true;
                            $no_errors       = false;                                   
                        }
                    }
                        
                    
                    
                } elseif(count($datos) > 1) {

                    if($missing_columns) {                        
                        $return_status = 'MISSING_COLUMNS';                           
                    } else {                                             
                        
                        if($columns != count($datos)) {   
                            $return_status = 'BAD_COLUMN_NUMBERS';     
                            $no_errors     = false;                                     
                        }    
                    }
                } 
                
                $row++;
                

            }
            
            fclose($gestor);  
            
            $codigos_inexistentes = array();

            if($no_errors) {
                
                if($delete_all) {                    
                    $this->customDeleteAll(array('id <> 0'), $module);
                }
                
                $gestor = fopen($file_path, "r");
                $row    = 1;
                
                while (($datos = fgetcsv($gestor, 1000, $separator)) !== FALSE) {

                    if($row != 1) {

                        $saveArray = array();
                                
                        foreach ($mapColumnsDB as $column => $keyDB) {
                            
                            if( isset($datos[$mapColumnsIndexs[$column]])) {       
                            
                                $value = trim($datos[$mapColumnsIndexs[$column]]) == '' ? ' ' : trim($datos[$mapColumnsIndexs[$column]]);                                
                                
                            } else {
                                $value = '';
                            }  

                            if(!preg_match('!!u', $value)) {
                                $value = utf8_encode($value);                           
                            }     
                            
                            if($column == '_db_order') {
                                $value = $row - 1;
                            }                                                       

                            foreach ($map['map'] as $field) {
                                
                                if((isset($field['@replace']) && isset($field['@replace-with'])) && ($keyDB == $field['@field'])) {
                                    $saveArray[$keyDB] = str_replace($field['@replace'], $field['@replace-with'], $value);
                                } else {
                                    $saveArray[$keyDB] = $value;
                                }
                                
                            }
                        
                        }   
                        
                        $this->create();
                        $this->save($saveArray);

                        // evaluar si existe el codigo del plato
                        $codigo = $saveArray['codigo'];

                        $splt = explode('-', $saveArray["fecha"]);
                        if(count($splt) > 1){
							$date_array[] = $splt[2].'-'.$splt[1].'-'.$splt[0];
                            $fecha_exh = $splt[2].'-'.$splt[1].'-'.$splt[0];
						}

                        if (!isset($jsonMaestro[$codigo])){
                            $codigos_inexistentes[] = $codigo.':'.$fecha_exh;
                        }
                        
                        // fecha exhibicion
                        if ($saveArray["fecha"] != $fecha_exhibicion){
                            $fecha_exhibicion = $saveArray["fecha"];

                            $splt = explode('-', $saveArray["fecha"]);
                            if(count($splt) > 1){
                                $fecha_exh = $splt[2].'-'.$splt[1].'-'.$splt[0];
                            }

                            $this->registrarEventoImportacion($usuario, $segmento, $cliente, $sucursal, $fecha_exh);
                        }
                    }

                    $row++;
                    
                } 
                
                fclose($gestor);

                $this->guardarCodigosPlatosInexistentes($codigos_inexistentes, $module, $segmento, $cliente, $sucursal, $usuario);

                /*
                // llamar a la api de carga
                if (count($date_array)>0){
					$url = "http://api.digitalboard.cl/sodexo/modules/{$module}/upload";
					$str_dataarray = implode('","', $date_array);
					$data_string = '{"data": ["' . $str_dataarray . '"]}';
					$headers =  array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string)
					);
					// n$response = Content::curl_to_host('POST', $url, $headers, $data_string, $resp_headers);
				}
                */
                
            }
            
        } else {
            $return_status = 'FILE_NOT_FOUND';
        }             
        
        return $return_status;
    }     
    
    public function importFromCSVGeneric2($file_path, $delete_all, $module, $map, $separator = ';') {
        
        $this->useTable = $module;
        
        if(empty($file_path)) {
            return 'FILE_NOT_FOUND';
        }                      
        
        $no_errors       = true;
        $return_status   = true;        
        $missing_columns = false;
        
        foreach ($map['map'] as $mapItem) {
            
            $mapColumnsIndexs[strtolower($mapItem['@column'])] = '';
            $mapColumnsDB[strtolower($mapItem['@column'])] = $mapItem['@field'];
            
            if($mapItem['@optional'] == 'no') {
                $requiredColumns[strtolower($mapItem['@column'])] = strtolower($mapItem['@column']);
            }            
        }
        
        
        ini_set("auto_detect_line_endings", true);
        
        if (($gestor = fopen($file_path, "r")) !== FALSE) {
            
            $row = 1;            
            
            while (($datos = fgetcsv($gestor, 3000, $separator)) !== FALSE) {
                
                if($row == 1) {

                    $columns = count($datos);                

                    if($columns == 1) {
                        $missing_columns = true;
                        $no_errors       = false;   
                        $return_status = 'MISSING_COLUMNS_SEPARATOR';  
                    }
                    
                    for ($c = 0; $c < $columns; $c++) {
                        if(isset($mapColumnsIndexs[strtolower(trim($datos[$c]))])) { 
                            $mapColumnsIndexs[strtolower(trim($datos[$c]))] = $c;                             
                        }                   
                    }
                    
                    foreach ($requiredColumns as $requiredColumn) {
                        if(trim($mapColumnsIndexs[$requiredColumn]) == '') {
                            $missing_columns = true;
                            $no_errors       = false;                                   
                        }
                    }
                        
                    
                    
                } elseif(count($datos) > 1) {

                    if($missing_columns) {                        
                        $return_status = 'MISSING_COLUMNS';                           
                    } else {                
                        if($columns != count($datos)) {   
                            $return_status = 'BAD_COLUMN_NUMBERS';     
                            $no_errors     = false;                                     
                        }    
                    }
                } 
                
                $row++;
                

            }
            
            fclose($gestor);         

            if($no_errors) {
                if($delete_all) {                    
                    $this->customDeleteAll(array('id <> 0'), $module);
                }
                
                $gestor = fopen($file_path, "r");
                $row    = 1;
                
                while (($datos = fgetcsv($gestor, 1000, $separator)) !== FALSE) {

                    if($row != 1) {

                        $saveArray = array();
                                
                        foreach ($mapColumnsDB as $column => $keyDB) {
                            
                            if( isset($datos[$mapColumnsIndexs[$column]])) {       
                            
                                $value = trim($datos[$mapColumnsIndexs[$column]]) == '' ? ' ' : trim($datos[$mapColumnsIndexs[$column]]);
                                        
                                
                                if(isset($map['map'][$mapColumnsIndexs[$column]]['@replace']) && isset($map['map'][$mapColumnsIndexs[$column]]['@replace-with'])) {                        
                                    
                                }
                                
                            } else {
                                $value = '';
                            }  

                            if(!preg_match('!!u', $value)) {
                                $value = utf8_encode($value);                           
                            }     
                            
                            if($column == '_db_order') {
                                $value = $row - 1;
                            }                                                       

                            foreach ($map['map'] as $field) {
                                
                                if((isset($field['@replace']) && isset($field['@replace-with'])) && ($keyDB ==$field['@field'])) {
                                    $saveArray[$keyDB] = str_replace($field['@replace'], $field['@replace-with'], $value);
                                } else {
                                    $saveArray[$keyDB] = $value;
                                }
                                
                            }
                        }   
                        $this->create();
                        $this->save($saveArray);
                        
                    }

                    $row++;
                    
                } 
                
                fclose($gestor);
                
            }
            
        } else {
            $return_status = 'FILE_NOT_FOUND';
        }             
        
        //die($return_status);
        return $return_status;
    }    

    public function compareFields($fields_A, $fields_B) {
        
        if(count($fields_A) != count($fields_B)) {       
            return false;            
        }
        
        foreach ($fields_A as &$field) {
            $maxLength   = isset($field['@maxlength']) ? $field['@maxlength'] : '';
            $compare_A[] = array($field['@key'], $field['@type'], $field['@label'], $maxLength);
        }
        
        foreach ($fields_B as &$field) {
            $maxLength   = isset($field['@maxlength']) ? $field['@maxlength'] : '';
            $compare_B[] = array($field['@key'], $field['@type'], $field['@label'], $maxLength);
        }
        
        if($compare_A == $compare_B) {
            return true;
        } else {
            return false;
        }
        
    }
    
    public function generatePreview($module, $clientFolder, $moduleData) {
        $this->generate($module, $clientFolder, 'test', $moduleData);
    }
    
    public function generatePublish($module, $clientFolder, $idPublish, $moduleData) {
        $this->generate($module, $clientFolder, 'prod', $moduleData , $idPublish);
    }
    
    private function generate($module, $clientFolder, $mode, $moduleData, $idPublish = 0) {
                
        $this->useTable = $module;            
        
        $content = $this->find('all', array('order' => 'id ASC'));       
        
        if($content) {
            $content = Set::classicExtract($content,'{n}.Content');       
            $content = $this->getImagesRelatedContent($content, $module, $moduleData, $clientFolder);
        }
        
        $resources = $content['resources'];
        $content   = json_encode($content['content']);
        
        file_put_contents(WWW_ROOT . 'files' . DS . $clientFolder .DS . 'templates'. DS . 'json' . DS . $mode . DS . $module . '-data.json', $content);
        
        // if($mode == 'prod') {
            
        //     @mkdir(WWW_ROOT . 'files' . DS . $clientFolder .DS . 'publications'. DS . $idPublish);
        //     file_put_contents(WWW_ROOT . 'files' . DS . $clientFolder .DS . 'publications'. DS . $idPublish . DS . $module . '-data.json', $content);
            
        //     foreach ($resources as $image) {                      
        //         copy(WWW_ROOT . 'files' . DS . $clientFolder .DS . $image, WWW_ROOT . 'files' . DS . $clientFolder .DS . 'publications'. DS . $idPublish . DS . $image);
        //     }
        // }
        
    }
    
    function getImagesRelatedContent($content, $module, $moduleData, $clientFolder) {
        
        App::import('Model', 'Image');
        
        $ImageModel = new Image();
        
        $videosFields = array();
        $imagesFields = array();
        $imageList    = array();


        foreach ($moduleData['fields']['field'] as $field) {
            if(isset($field['@type']) && $field['@type'] == 'video-upload') {
                $videosFields[] = $field['@key'];
            }
        }
        
        foreach ($moduleData['fields']['field'] as $field) {
            if(isset($field['@type']) && $field['@type'] == 'image') {
                $imagesFields[] = $field['@key'];
            }
        }

        foreach ($content as &$row) {
            foreach ($imagesFields as $imageField) {
                $filename         = str_replace('/', '', $ImageModel->getImageFilename($row[$imageField]));
                $imageList[]      = $filename == 'no_image.gif' ? '' : $filename;
                $row[$imageField] = $filename == 'no_image.gif' ? '' : $filename;
            }
        }

        foreach ($content as &$row) {
            foreach ($videosFields as $videoField) {
                $uri = $row[$videoField];
            }
        }

        foreach ($moduleData['fields']['field'] as $field) {
            if(isset($field['@type']) && $field['@type'] == 'static-image') {
                $imagesFields[] = $field['@key'];
            }
        }
        
        return array('content' => $content, 'resources' => $imageList);       
    }

    public function getAllMotivos() {
        $this->table = 'config_motivos';
        $this->useTable = 'config_motivos';

        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;
        $tablename = REPORTE_MODIFICADOS_TBL;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
        if (!$obj_conexion)
        {
            echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
            die();
        }

        $select_qry = 'SELECT * FROM config_motivos ';
                
        if (!$resultado = $obj_conexion->query($select_qry))
        {
            echo $select_qry;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
        }
        $motivos = Array();
        while ($registro = $resultado->fetch_assoc()) {
            $tmp_array = array();
            $tmp_array[] = $registro['id'];
            $tmp_array[] = $registro['Descripcion'];

            $motivos[] = $tmp_array;
        }
        $obj_conexion->close();

        return $motivos;
    }

    public function getAllFtpAccounts() {
        $server_db  = LOCALSERVER;
        $user_db    = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die ("db_error");
        if(!$obj_conexion)
        {
            $comentario = ": Error de Base de Datos";
            die();
        }

        $select_qry = "SELECT config_ftp.id, config_ftp.Ip, config_ftp.`Port`, config_ftp.Usuario, config_ftp.Clave FROM config_ftp";

        if (!$resultado = $obj_conexion->query($select_qry)) {
            die();
        }

        //------------------------ Obtiene las cuentas FTP ---------------------------------
        $cuentas_ftp = array();

        if ($resultado->num_rows != 0) {
            while ($rows = $resultado->fetch_assoc()) {
                $row = $rows;

                $id = $rows['id'];
                $ip = $rows['Ip'];
                $port = $rows['Port'];
                $usuario = $rows['Usuario'];
                $clave = $rows['Clave'];

                $cuenta_ftp = array();
                $cuenta_ftp['id'] = $id;
                $cuenta_ftp['name'] = '['.$id.'] '.$usuario;
                $cuentas_ftp[] = $cuenta_ftp;
            }
        }

        $obj_conexion->close();

        return $cuentas_ftp;
    }

    public function borrarPlatosConMotivos($module, $num_registers, $originIDs, $originServices, $fechaExhib, $originDishes, $tempDishes, $tipoPlatos, $txt_motivo, $userLogged, $segmento, $cliente, $sucursal){

        $hubo_codigos_no_encontrados = false;

        $segmento = strtolower($segmento);

        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;
        $tablename = REPORTE_MODIFICADOS_TBL;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
        if (!$obj_conexion)
        {
            echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
            die();
        }

        for($index = 0; $index < $num_registers; $index++){
            $cod_modificado = NULL;
            $accion = 'plato eliminado';
            
            $date_array = explode('-',$fechaExhib[$index]);
            $fecha_exhib_plato = $date_array[2] . '-' . $date_array[1] . '-' . $date_array[0];

            $servicio = $originServices[$index];
            $nombre_plato = $originDishes[$index];
            $nombre_plato_temporal = $tempDishes[$index];
            $sourceID = $originIDs[$index];
            $tipoPlato = $tipoPlatos[$index];

            // extraemos el codigo del plato que se encuentra entre [] usando expresion regular \[(.*?)\]
            if (preg_match('/\[(.*?)\]/', $nombre_plato, $match) == 1) {
                $codigo_plato = $match[1];
                $nombre_plato = substr($nombre_plato, strpos($nombre_plato, "]") + 2);
            }

            if ($nombre_plato != 'No Encontrado' || $nombre_plato_temporal != ''){

                $qryinsert = "INSERT INTO $tablename(cod_modificado, accion, motivo, fecha_mod, fecha_exh, servicio, tipo_plato, nombre_plato, Segmento, cliente, sucursal, usuario, created, modified) VALUES ('$codigo_plato', '$accion', '$txt_motivo', NOW(), '$fecha_exhib_plato', '$servicio', '$tipoPlato', '$nombre_plato', '$segmento', '$cliente', '$sucursal', '$userLogged', NOW(), NOW())";
                
                if (!$resultado = $obj_conexion->query($qryinsert))
                {
                    echo ".Lo sentimos, este sitio web está experimentando problemas.";
                    $obj_conexion->close();
                    die();
                }

                $obj_conexion->query('DELETE FROM ' . $module . ' WHERE ' . $module . '.id = ' . $sourceID);
            } else{
                $hubo_codigos_no_encontrados = true;
            }
        }

        $obj_conexion->close();

        return $hubo_codigos_no_encontrados;
    }

    public function guardarCodigosPlatosInexistentes($codigos_inexistentes, $module, $segmento, $cliente, $sucursal, $usuario){
        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;
        $tablename = REPORTE_MODIFICADOS_TBL;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
        if (!$obj_conexion)
        {
            echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
            die();
        }

        $accion = 'codigo no existe';
        $motivo = 'codigo no existe en maestro producto';

        foreach($codigos_inexistentes as $codigo){
            $splt = explode(':', $codigo);
            $codigo = $splt[0];
            $fecha_exh = $splt[1];

            $servicio = '';
            $tipoPlato = '';

            $qryinsert = "INSERT INTO $tablename(cod_modificado, accion, motivo, fecha_mod, fecha_exh, servicio, tipo_plato, Segmento, cliente, sucursal, usuario, created, modified) VALUES ('$codigo', '$accion', '$motivo', NOW(), '$fecha_exh', '$servicio', '$tipoPlato', '$segmento', '$cliente', '$sucursal', '$usuario', NOW(), NOW())";

            if (!$resultado = $obj_conexion->query($qryinsert))
            {
                echo ".Lo sentimos, este sitio web está experimentando problemas.";
                $obj_conexion->close();
                die();
            }
        }

        $obj_conexion->close();
    }

    public function registrarEventoImportacion($userLogged, $segmento, $cliente, $sucursal, $fecha_exhibicion){

        $segmento = strtolower($segmento);

        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;
        $tablename = REPORTE_MODIFICADOS_TBL;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
        if (!$obj_conexion)
        {
            echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
            die();
        }

        $accion = 'upload';

        $qryinsert = "INSERT INTO $tablename(cod_modificado, accion, motivo, fecha_mod, fecha_exh, servicio, tipo_plato, nombre_plato, Segmento, cliente, sucursal, usuario, created, modified) VALUES ('', '$accion', '', NOW(), '$fecha_exhibicion', '', '', '', '$segmento', '$cliente', '$sucursal', '$userLogged', NOW(), NOW())";
        
        if (!$resultado = $obj_conexion->query($qryinsert))
        {
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            $obj_conexion->close();
            die();
        }

        $obj_conexion->close();
    }

    public function updateDish($segmento,$cod_modificado,$accion,$fecha_exhib,$servicio,$codigo_plato,$nombre_plato,$tipo_plato,$cliente,$sucursal,$usuario,$codigo_temporal,$txt_motivo)
    {
        $date_array = explode('/',$fecha_exhib);
        $fecha_exhib = $date_array[2] . '-' . $date_array[1] . '-' . $date_array[0];
        $segmento = strtolower($segmento);

        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;
        $tablename = REPORTE_MODIFICADOS_TBL;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
        if (!$obj_conexion)
        {
            echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
            die();
        }

        $qryinsert = "INSERT INTO $tablename (cod_modificado, accion, fecha_mod, fecha_exh, servicio, tipo_plato, nombre_plato, Segmento, cliente, sucursal, usuario, nuevo_codigo, motivo, created, modified) VALUES ('$cod_modificado', '$accion', NOW(), '$fecha_exhib', '$servicio', '$tipo_plato', '$nombre_plato', '$segmento', '$cliente', '$sucursal', '$usuario', '$codigo_temporal', '$txt_motivo', NOW(), NOW())";

        if (!$resultado = $obj_conexion->query($qryinsert))
        {
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
        }

        $obj_conexion->close();
    }

    public function procesarReportesDeModificacionCorporativo(){
        // listado de ids para copiar
        $ids = array();

        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;
        $tablename = REPORTE_MODIFICADOS_TBL;
        $tablename_dest = REPORTE_MODIFICADOS_CORP_TBL;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die ("db_error");
        if(!$obj_conexion)
        {
            echo "No se ha podido conectar a la base de datos." . PHP_EOL;
            die();
        }

        // buscamos los grupos cliente sucursal
        $clientes_sucursales = array();
        $select_qry = "SELECT DISTINCT cliente, sucursal FROM $tablename WHERE segmento = 'corporativo'";
        if (!$resultado = $obj_conexion->query($select_qry)) {
            echo "Lo sentimos, este sitio web está experimentando problemas." . PHP_EOL;
            die();
        }
        if ($resultado->num_rows != 0) {
            while ($row = $resultado->fetch_assoc()) {
                $clientes_sucursales[] = $row;
            }
        }

        // por cada cliente sucursal se hace el proceso de buscar el rango upload - publish
        foreach($clientes_sucursales as $cliente_sucursal){
            $cliente = $cliente_sucursal['cliente'];
            $sucursal = $cliente_sucursal['sucursal'];
            $id_first_upload = 0;
            $id_last_publish = 0;

            // obtener las fechas de exhibicion
            $fecha_exh_array = array();
            $select_qry = "SELECT DISTINCT fecha_exh FROM reportes_modificacion WHERE cliente = '$cliente' AND sucursal = '$sucursal'";
            if (!$resultado = $obj_conexion->query($select_qry))
            {
                echo $select_qry;
                echo ".Lo sentimos, este sitio web está experimentando problemas.";
                die();
            }

            while ($registro = $resultado->fetch_assoc()) {
                $fecha_exh_array[] = $registro['fecha_exh'];
            }

            foreach($fecha_exh_array as $fecha_exh){
                if ($fecha_exh == '2021-12-20'){
                    $atento = 1;
                }

                // por cada cliente sucursal se hace el proceso de buscar el rango upload - publish
                $select_qry = "SELECT id, accion, fecha_exh FROM reportes_modificacion WHERE cliente = '$cliente' AND sucursal = '$sucursal' AND fecha_exh = '$fecha_exh' ORDER BY id DESC";

                if (!$resultado = $obj_conexion->query($select_qry))
                {
                    echo $select_qry;
                    echo ".Lo sentimos, este sitio web está experimentando problemas.";
                    die();
                }

                // obtener la primera fecha de exhibicion
                $accion_publicar = false;
                $id_publish = 0;
                $id_first_fecha_exh = 0;
                $first_fecha_exh = '';
                while ($registro = $resultado->fetch_assoc()) {
                    $tmp_array = array();
                    $tmp_array['id'] = $registro['id'];
                    $tmp_array['accion'] = $registro['accion'];

                    if ($accion_publicar && $registro['accion'] != 'Publicar'){
                        $id_first_fecha_exh = $registro['id'];
                        break;
                    }

                    if ($registro['accion'] == 'Publicar'){
                        $first_fecha_exh = $registro['fecha_exh'];
                    }

                    if ($registro['accion'] == 'Publicar' && !$accion_publicar){
                        $accion_publicar = true;
                        $id_publish = $registro['id'];
                    }
                }

                // buscar el upload de la primera fecha de exhibicion
                $select_qry = "SELECT id, accion FROM reportes_modificacion WHERE accion = 'upload' AND cliente = '$cliente' AND sucursal = '$sucursal' AND fecha_exh = '$first_fecha_exh' ORDER BY id DESC";
                if (!$resultado = $obj_conexion->query($select_qry))
                {
                    echo $select_qry;
                    echo ".Lo sentimos, este sitio web está experimentando problemas.";

                    die();
                }
                $registro = $resultado->fetch_assoc();
                $id_upload = $registro['id'];

                // listado de id entre el rango de upload y publish
                $select_qry = "SELECT id FROM reportes_modificacion WHERE id >= $id_upload AND id <= $id_publish AND cliente = '$cliente' AND sucursal = '$sucursal' AND fecha_exh = '$first_fecha_exh'";
                if (!$resultado = $obj_conexion->query($select_qry))
                {
                    //echo $select_qry;
                    //echo ".Lo sentimos, este sitio web está experimentando problemas.";

                    //die();
                }
                else
                while ($registro = $resultado->fetch_assoc()) {
                    $ids[] = $registro['id'];
                }
            }
        }
        sort($ids);

        // borrar los registros viejos
        $query_delete = "delete from $tablename_dest";
        $obj_conexion->query($query_delete);

        // copia los registros correspondientes
        for($i = 0; $i < count($ids); $i++){
            $id = $ids[$i];
            $query_insert = "insert into $tablename_dest SELECT * FROM $tablename where id = $id";

            $obj_conexion->query($query_insert);
        }

        $obj_conexion->close();
    }

    public function procesarReportesDeModificacionMineria(){
        // listado de ids para copiar
        $ids = array();

        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;
        $tablename = REPORTE_MODIFICADOS_TBL;
        $tablename_dest = REPORTE_MODIFICADOS_MINERIA_TBL;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die ("db_error");
        if(!$obj_conexion)
        {
            echo "No se ha podido conectar a la base de datos." . PHP_EOL;
            die();
        }

        // buscamos los grupos cliente sucursal
        $clientes_sucursales = array();
        $select_qry = "SELECT DISTINCT cliente, sucursal FROM $tablename WHERE segmento = 'mineria'";
        if (!$resultado = $obj_conexion->query($select_qry)) {
            echo "Lo sentimos, este sitio web está experimentando problemas." . PHP_EOL;
            die();
        }
        if ($resultado->num_rows != 0) {
            while ($row = $resultado->fetch_assoc()) {
                $clientes_sucursales[] = $row;
            }
        }

        // por cada cliente sucursal se hace el proceso de buscar el rango upload - publish
        foreach($clientes_sucursales as $cliente_sucursal){
            $cliente = $cliente_sucursal['cliente'];
            $sucursal = $cliente_sucursal['sucursal'];
            $id_first_upload = 0;
            $id_last_publish = 0;

            // obtener las fechas de exhibicion
            $fecha_exh_array = array();
            $select_qry = "SELECT DISTINCT fecha_exh FROM reportes_modificacion WHERE cliente = '$cliente' AND sucursal = '$sucursal'";
            if (!$resultado = $obj_conexion->query($select_qry))
            {
                echo $select_qry;
                echo ".Lo sentimos, este sitio web está experimentando problemas.";
                die();
            }

            while ($registro = $resultado->fetch_assoc()) {
                $fecha_exh_array[] = $registro['fecha_exh'];
            }

            foreach($fecha_exh_array as $fecha_exh){
                if ($fecha_exh == '2021-12-20'){
                    $atento = 1;
                }

                // por cada cliente sucursal se hace el proceso de buscar el rango upload - publish
                $select_qry = "SELECT id, accion, fecha_exh FROM reportes_modificacion WHERE cliente = '$cliente' AND sucursal = '$sucursal' AND fecha_exh = '$fecha_exh' ORDER BY id DESC";

                if (!$resultado = $obj_conexion->query($select_qry))
                {
                    echo $select_qry;
                    echo ".Lo sentimos, este sitio web está experimentando problemas.";
                    die();
                }

                // obtener la primera fecha de exhibicion
                $accion_publicar = false;
                $id_publish = 0;
                $id_first_fecha_exh = 0;
                $first_fecha_exh = '';
                while ($registro = $resultado->fetch_assoc()) {
                    $tmp_array = array();
                    $tmp_array['id'] = $registro['id'];
                    $tmp_array['accion'] = $registro['accion'];

                    if ($accion_publicar && $registro['accion'] != 'Publicar'){
                        $id_first_fecha_exh = $registro['id'];
                        break;
                    }

                    if ($registro['accion'] == 'Publicar'){
                        $first_fecha_exh = $registro['fecha_exh'];
                    }

                    if ($registro['accion'] == 'Publicar' && !$accion_publicar){
                        $accion_publicar = true;
                        $id_publish = $registro['id'];
                    }
                }

                // buscar el upload de la primera fecha de exhibicion
                $select_qry = "SELECT id, accion FROM reportes_modificacion WHERE accion = 'upload' AND cliente = '$cliente' AND sucursal = '$sucursal' AND fecha_exh = '$first_fecha_exh' ORDER BY id DESC";
                if (!$resultado = $obj_conexion->query($select_qry))
                {
                    echo $select_qry;
                    echo ".Lo sentimos, este sitio web está experimentando problemas.";
                    die();
                }
                $registro = $resultado->fetch_assoc();
                $id_upload = $registro['id'];

                // listado de id entre el rango de upload y publish
                $select_qry = "SELECT id FROM reportes_modificacion WHERE id >= $id_upload AND id <= $id_publish AND cliente = '$cliente' AND sucursal = '$sucursal' AND fecha_exh = '$first_fecha_exh'";
                if (!$resultado = $obj_conexion->query($select_qry))
                {
                    echo $select_qry;
                    echo ".Lo sentimos, este sitio web está experimentando problemas.";
                    die();
                }
                while ($registro = $resultado->fetch_assoc()) {
                    $ids[] = $registro['id'];
                }
            }
        }
        sort($ids);

        // borrar los registros viejos
        $query_delete = "delete from $tablename_dest";
        $obj_conexion->query($query_delete);

        // copia los registros correspondientes
        for($i = 0; $i < count($ids); $i++){
            $id = $ids[$i];
            $query_insert = "insert into $tablename_dest SELECT * FROM $tablename where id = $id";

            $obj_conexion->query($query_insert);
        }

        $obj_conexion->close();
    }

    public function procesarReportesDeModificacionSalud(){
        // listado de ids para copiar
        $ids = array();

        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;
        $tablename = REPORTE_MODIFICADOS_TBL;
        $tablename_dest = REPORTE_MODIFICADOS_SALUD_TBL;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die ("db_error");
        if(!$obj_conexion)
        {
            echo "No se ha podido conectar a la base de datos." . PHP_EOL;
            die();
        }

        // buscamos los grupos cliente sucursal
        $clientes_sucursales = array();
        $select_qry = "SELECT DISTINCT cliente, sucursal FROM $tablename WHERE segmento = 'salud'";
        if (!$resultado = $obj_conexion->query($select_qry)) {
            echo "Lo sentimos, este sitio web está experimentando problemas." . PHP_EOL;
            die();
        }
        if ($resultado->num_rows != 0) {
            while ($row = $resultado->fetch_assoc()) {
                $clientes_sucursales[] = $row;
            }
        }

        // por cada cliente sucursal se hace el proceso de buscar el rango upload - publish
        foreach($clientes_sucursales as $cliente_sucursal){
            $cliente = $cliente_sucursal['cliente'];
            $sucursal = $cliente_sucursal['sucursal'];
            $id_first_upload = 0;
            $id_last_publish = 0;

            // obtener las fechas de exhibicion
            $fecha_exh_array = array();
            $select_qry = "SELECT DISTINCT fecha_exh FROM reportes_modificacion WHERE cliente = '$cliente' AND sucursal = '$sucursal'";
            if (!$resultado = $obj_conexion->query($select_qry))
            {
                echo $select_qry;
                echo ".Lo sentimos, este sitio web está experimentando problemas.";
                die();
            }

            while ($registro = $resultado->fetch_assoc()) {
                $fecha_exh_array[] = $registro['fecha_exh'];
            }

            foreach($fecha_exh_array as $fecha_exh){
                if ($fecha_exh == '2021-12-20'){
                    $atento = 1;
                }

                // por cada cliente sucursal se hace el proceso de buscar el rango upload - publish
                $select_qry = "SELECT id, accion, fecha_exh FROM reportes_modificacion WHERE cliente = '$cliente' AND sucursal = '$sucursal' AND fecha_exh = '$fecha_exh' ORDER BY id DESC";

                if (!$resultado = $obj_conexion->query($select_qry))
                {
                    echo $select_qry;
                    echo ".Lo sentimos, este sitio web está experimentando problemas.";
                    die();
                }

                // obtener la primera fecha de exhibicion
                $accion_publicar = false;
                $id_publish = 0;
                $id_first_fecha_exh = 0;
                $first_fecha_exh = '';
                while ($registro = $resultado->fetch_assoc()) {
                    $tmp_array = array();
                    $tmp_array['id'] = $registro['id'];
                    $tmp_array['accion'] = $registro['accion'];

                    if ($accion_publicar && $registro['accion'] != 'Publicar'){
                        $id_first_fecha_exh = $registro['id'];
                        break;
                    }

                    if ($registro['accion'] == 'Publicar'){
                        $first_fecha_exh = $registro['fecha_exh'];
                    }

                    if ($registro['accion'] == 'Publicar' && !$accion_publicar){
                        $accion_publicar = true;
                        $id_publish = $registro['id'];
                    }
                }

                // buscar el upload de la primera fecha de exhibicion
                $select_qry = "SELECT id, accion FROM reportes_modificacion WHERE accion = 'upload' AND cliente = '$cliente' AND sucursal = '$sucursal' AND fecha_exh = '$first_fecha_exh' ORDER BY id DESC";
                if (!$resultado = $obj_conexion->query($select_qry))
                {
                    echo $select_qry;
                    echo ".Lo sentimos, este sitio web está experimentando problemas.";
                    die();
                }
                $registro = $resultado->fetch_assoc();
                $id_upload = $registro['id'];

                // listado de id entre el rango de upload y publish
                $select_qry = "SELECT id FROM reportes_modificacion WHERE id >= $id_upload AND id <= $id_publish AND cliente = '$cliente' AND sucursal = '$sucursal' AND fecha_exh = '$first_fecha_exh'";
                if (!$resultado = $obj_conexion->query($select_qry))
                {
                    echo $select_qry;
                    echo ".Lo sentimos, este sitio web está experimentando problemas.";
                    die();
                }
                while ($registro = $resultado->fetch_assoc()) {
                    $ids[] = $registro['id'];
                }
            }
        }
        sort($ids);

        // borrar los registros viejos
        $query_delete = "delete from $tablename_dest";
        $obj_conexion->query($query_delete);

        // copia los registros correspondientes
        for($i = 0; $i < count($ids); $i++){
            $id = $ids[$i];
            $query_insert = "insert into $tablename_dest SELECT * FROM $tablename where id = $id";

            $obj_conexion->query($query_insert);
        }

        $obj_conexion->close();
    }

    public function procesarReportesDeModificacion(){
        $this->procesarReportesDeModificacionCorporativo();
        $this->procesarReportesDeModificacionMineria();
        $this->procesarReportesDeModificacionSalud();
    }
    
    // curl en php
	static public function curl_to_host($method, $url, $headers, $data, &$resp_headers){
		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
		curl_setopt($ch, CURLOPT_HEADER, 1);

		if ($method=='POST'){
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}

		$rtn=curl_exec($ch);
		$error_msg = curl_error($ch);
		curl_close($ch);

		$rtn=explode("\r\n\r\nHTTP/", $rtn, 2);
		$rtn=(count($rtn)>1 ? 'HTTP/' : '').array_pop($rtn);

		list($str_resp_headers, $rtn)=explode("\r\n\r\n", $rtn, 2);

		$str_resp_headers=explode("\r\n", $str_resp_headers);
		array_shift($str_resp_headers);
		$resp_headers=array();
		foreach ($str_resp_headers as $k=>$v){
			$v=explode(': ', $v, 2);
			$resp_headers[$v[0]]=$v[1];
		}

		return $rtn;
	}
}