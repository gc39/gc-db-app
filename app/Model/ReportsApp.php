<?php

class ReportsApp extends Model {   
    
    public function getSchema($module) {
               
        $this->useTable = $module;
        return $this->schema();
        
    }    

    public function getAllOrder($module, $order, $page, $rows_per_page, &$pages) {
               
        $this->useTable = $module;
        
        $query[]= array('fecha > DATE_FORMAT(NOW(),"%Y-%m-%d 00:00:00")');
        $contentCount = $this->find('count', array('conditions' => array('OR' => $query)));           

        $pages= ceil($contentCount/$rows_per_page);
        
        if($page == 1) {    
            $start = 0;
        } else {
            $start = $rows_per_page * ($page - 1);
        }
       
        
        if($order) {
            $content = $this->find('all', array('order' => $order, 'conditions' => array('OR' => $query), 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
        } else {
            $content = $this->find('all', array('order' => 'id ASC', 'conditions' => array('OR' => $query), 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
        }
        return $content;
    }
    
     public function findAllRows($searchTerm, $module, $order, $fieldsData, $page, $rows_per_page, &$pages) {
        
        $query = false;
        $this->useTable = $module;
        
        
        foreach($fieldsData as $field) {                         
            $query[] = array($field.' LIKE' => '%'.$searchTerm.'%');         
        }

        $query[]= array('fecha > DATE_FORMAT(NOW(),"%Y-%m-%d 00:00:00")');
        
        if(is_array($query)) {    
            $contentCount = $this->find('count', array('conditions' => array('OR' => $query)));           
        } else {        
            $contentCount = $this->find('count');         
        }
        
        $pages= ceil($contentCount/$rows_per_page);
        
        if($page == 1) {    
            $start = 0;
        } else {
            $start = $rows_per_page * ($page - 1);
        }
        
        if(is_array($query)) {
            if($order) {
                $content = $this->find('all', array('order' => $order, 'conditions' => array('OR' => $query), 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
            } else {
                $content = $this->find('all', array('order' => 'id ASC', 'conditions' => array('OR' => $query), 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
            }
        } else {        
            if($order) {
                $content = $this->find('all', array('order' => $order, 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
            } else {
                $content = $this->find('all', array('order' => 'id ASC', 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
            }
        }

        return $content;
    } 
    
    public function findAllRowsCount($searchTerm, $module, $order, $fieldsData, $page, $rows_per_page, &$pages) {
        
        $query = false;
        $this->useTable = $module;
        
        if(is_array($fieldsData)){
            foreach($fieldsData as $field) {                         
                $query[] = array($field.' LIKE' => '%'.$searchTerm.'%');         
            }  
        }
        
        if(is_array($query)) {    
            $contentCount = $this->find('count',array('conditions' => array('OR' => $query, 'fechaPedido > DATE_FORMAT(NOW(),"%Y-%m-%d 00:00:00")'), 'group' => 'plato'));
        } else {        
            $contentCount = $this->find('count',array('conditions' => array('fechaPedido > DATE_FORMAT(NOW(),"%Y-%m-%d 00:00:00")'), 'group' => 'plato'));
        }
        
        $pages= ceil($contentCount/$rows_per_page);
        
        if($page == 1) {    
            $start = 0;
        } else {
            $start = $rows_per_page * ($page - 1);
        }
        
        if(is_array($query)) {
    
          //  $content = $this->find('all', array('order' => 'id ASC', 'conditions' => array('OR' => $query, 'fechaPedido > DATE_FORMAT(NOW(),"%Y-%m-%d 00:00:00")'), 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
     
               $content = $this->find('all',array(
                'fields' =>array('ReportsApp.tipo', 'ReportsApp.plato', 'COUNT(1) as cantidad' ),
                'conditions' => array('OR' => $query, 'fechaPedido > DATE_FORMAT(NOW(),"%Y-%m-%d 00:00:00")'),
                'limit' => $rows_per_page, 
                'page' => $page,
                'offset' => $start,
                'group' => 'ReportsApp.plato'
            ));

        } else {        

            //$content = $this->find('all', array('order' => 'id ASC', 'conditions' => array('fechaPedido > DATE_FORMAT(NOW(),"%Y-%m-%d 00:00:00")'), 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));

            $content = $this->find('all',array(
                'fields' =>array('ReportsApp.tipo', 'ReportsApp.plato', 'COUNT(1) as cantidad' ),
                'conditions' => array('fechaPedido > DATE_FORMAT(NOW(),"%Y-%m-%d 00:00:00")'),
                'limit' => $rows_per_page, 
                'page' => $page,
                'offset' => $start,
                'group' => 'ReportsApp.plato'
            ));

        }

        return $content;
        
    }
    
    
     public function getAll($module, $date, $type) {
               
        $this->useTable = $module;
        
        $query[]= array('fechaPedido > DATE_FORMAT("'.$date.'","%Y-%m-%d 00:00:00")');
        
        if($type != 'count'){
            $content = $this->find('all', array('order' => 'id ASC', 'conditions' => array('OR' => $query)));
        }else{
            $content = $this->find('all',array(
                'fields' =>array('ReportsApp.tipo', 'ReportsApp.plato', 'COUNT(1) as cantidad' ),
                'conditions' => array('fechaPedido > DATE_FORMAT("'.$date.'","%Y-%m-%d 00:00:00")'),
                'group' => 'ReportsApp.plato'
            ));
        }
        

        return $content;
        
    }
    
    
    
}