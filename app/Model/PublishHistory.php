<?php

class PublishHistory extends Model {   
     
    public $useTable = 'publish_history';    
 
    public function getModifModule($modules, $condition) {
          
        //$this->useTable = $modules;
         $this->table = $modules;
           
        $content = $this->find('all', array(            
            'fields' => array('*'),
            'conditions' => $condition
        ));     
        
      
        return $content;
        
    }
   
    public function havePublicationInPeriod($module, $dateFrom, $dateTo) {         
           
        //echo 'send_date BETWEEN TIMESTAMP("'.$dateFrom.'") AND TIMESTAMP("'.$dateTo.'") <br>';
        
        $content = $this->find('all', array(            
            'fields' => array('*'),
            'conditions' => array(
                'module' => $module,
                'send_date BETWEEN TIMESTAMP("'.$dateFrom.'") AND TIMESTAMP("'.$dateTo.'")')
        ));     
              
      //  print_r($content);
        return count($content) > 0 ? true : false;
        
    }
    
    public function getPublishWeek($condition){
       
        $publications = $this->find('all', 
                array('fields' => array( 'PublishHistory.*','User.id','User.fullname','User.email','User.rol'), 
                   'joins' => array(
                    array(
                        'table' => 'user',
                        'alias' => 'User',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'User.id = PublishHistory.user_id'
                        )
                    )),
                    'conditions' => $condition,
                    'order' => array('PublishHistory.send_date DESC', 'PublishHistory.module')
                )
        );
        return $publications;
    }
    
    public function getCountPublishWeek(){
        $publicationsCount = $this->find('count', 
                array('fields' => array( 'PublishHistory.*','User.id','User.fullname','User.email','User.rol'), 
                   'order' => 'send_date DESC',
                   'joins' => array(
                    array(
                        'table' => 'user',
                        'alias' => 'User',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'User.id = PublishHistory.user_id'
                        )
                    )),
                    'conditions' => array(
                        'PublishHistory.send_date BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY)  AND NOW()', 'NOT' => array('module LIKE "%maestro%"' , 'module LIKE "%bloque%"')
                    ),
                    'order' => array('PublishHistory.send_date DESC')
                )
        );
        return $publicationsCount;
    }
    
   
    public function publishTodayForModule($module){
        $query = $this->find('all', 
                array('fields' => array( 'PublishHistory.*','User.id','User.fullname','User.email','User.rol'), 
                   'joins' => array(
                    array(
                        'table' => 'user',
                        'alias' => 'User',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'User.id = PublishHistory.user_id'
                        )
                    )),
                    'conditions' => array(
                        'DATE(NOW()) = DATE(PublishHistory.send_date) AND PublishHistory.module LIKE "%'.$module.'%"'
                    ),
                    'order' => array('PublishHistory.send_date DESC')
                )
        );
        return $query;
        
    }
    
    public function getFirstBeforeHour($module, $date) {
        

            
        $query = $this->find('first', 
            array('fields' => array( 'PublishHistory.*, TIMESTAMPDIFF(MINUTE, "'.$date.'", PublishHistory.send_date) AS diff'), 

                'conditions' => array(
                    'PublishHistory.send_date < "'.$date.'" AND module LIKE "%'.$module.'%"'
                ),
                'order' => array('PublishHistory.send_date DESC'),
                'limit' => 1
            )
        );
            
      
        
//        echo '<pre>';
//        echo $date.'<br>';
//        print_r($query);
//        echo '</pre>';
//        
//        die();
        
        return $query;        
        
    }
    
    public function getFirstAfterHour($module, $date, $serviceDate, $withService) {
                    
        $query = $this->find('first', 
            array('fields' => array( 'PublishHistory.*, TIMESTAMPDIFF(MINUTE, "'.$serviceDate.'", PublishHistory.send_date) AS diff'), 

                'conditions' => array(
                    'PublishHistory.send_date > "'.$date.'" AND module = "'.$module.'"'
                ),
                'order' => array('PublishHistory.send_date ASC'),
                'limit' => 1
            )
        );
            
      
        
//        echo '<pre>';        
//        print_r($query);
//        echo '</pre>';
//        
//        die();
        
        return $query;
        
        
    }
    
     
    
}