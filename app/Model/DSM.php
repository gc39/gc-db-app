<?php

class DSM extends Model {   
     
    public $useTable = false;
    
    public function getPlayerStatus($serial) {                
        if(!is_numeric($serial)) { return false; }        	               
        return $this->sendRequest('/JSON1/player/'.$serial.'/status');	               
    }       
         
    public function getPlayerName($serial) {                
        if(!is_numeric($serial)) { return false; }        	               
        $return = $this->sendRequest('/JSON1/player/'.$serial.'/status');	               
        
        return isset($return->player_name) ? $return->player_name : '';
    }       
         
    
    public function generateScreencap($playerSerial) {                
        
        if(!is_numeric($playerSerial)) { return false; }  
        
        $this->sendRequest('/player/'.$playerSerial.'.remote/status/screen_snap_shot');
        $imageData = $this->sendRequest('/player/'.$playerSerial.'.remote/data/config/screen_snap_shot.png', true);

        return $imageData;
                
    }
    
    private function sendRequest($params, $rawFormat = false) {                
        
        $request = curl_init(DSM_HOST . $params);	        
                   curl_setopt($request, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
                   curl_setopt($request, CURLOPT_USERPWD, DSM_USER.':'.DSM_PASSWORD);
                   curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
                   curl_setopt($request, CURLOPT_POST, false);
                   
        $response   = curl_exec($request);	
        $statusCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
        
        curl_close($request);
        
        if($statusCode == 200) {                   
            return $rawFormat ? $response : json_decode($response);            
        } else {
            return false;
        }
        
    }
    
}