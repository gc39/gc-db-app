<?php
    
    define('CLIENT', 'sodexo');
    define('ENABLESENDMAIL', false);

    $CLIENT = CLIENT;

    define('STANDALONE_MODE', false);
    
    define('MODULES_CONFIG_PATH', ROOT . DIRECTORY_SEPARATOR . APP_DIR . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR);

    define('ROWS_PER_PAGE', 3000);
    define('ROWS_PER_PAGE_IN_MEDIA', 32);
    define('ROWS_PER_PAGE_IN_MEDIA_POPUP', 8);
    define('SESSION_TIME_MINS_MODULE', 120);

    define('RUTAIMAGES', "/home/dev/public_html/sub_dominios/$CLIENT/app/webroot/files/$CLIENT/");
    define('CARPETA_SHELL_EXEC', "/home/dev/public_html/sub_dominios/$CLIENT/app/Shell_exec/");
    define('CARPETA_CRON_JOBS', "/home/dev/public_html/sub_dominios/$CLIENT/app/Cron_jobs/");
    define('PHP_RUTA', 'php');

    define('URLAPI', 'http://sodexo.dev.digitalboard.app/noapi'); //  http://sodexo.digitalboard.app/api/publica.php?cliente_sucursal=Cliente_Sucursal&fecha_exhibicion=2021-08-18

    define('LOCALSERVER', 'localhost');
    define('LOCALUSERDB', 'dev');
    define('LOCALPASSDB', 'qazplmqazplm');
    define('LOCALDB', "dev_$CLIENT");

    // tabla que almacena el reporte de los platos modificados
    define('REPORTE_MODIFICADOS_TBL', 'reportes_modificacion');

    // tabla que almacena el reporte de los platos modificados de corporativo
    define('REPORTE_MODIFICADOS_CORP_TBL', 'reportes_corp');

    // tabla que almacena el reporte de los platos modificados de mineria
    define('REPORTE_MODIFICADOS_MINERIA_TBL', 'reportes_mineria');

    // tabla que almacena el reporte de los platos modificados de salud
    define('REPORTE_MODIFICADOS_SALUD_TBL', 'reportes_salud');

    define('BASE_TEMPLATE', WWW_ROOT . 'files' . '/' . $CLIENT);
    define('BASE_URL', "https://sodexo.dev.digitalboard.app");

    define('RUTAMODULOS', "/home/dev/public_html/sub_dominios/$CLIENT/app/modules/$CLIENT");
    define('RUTAORIGENCARPETASCRONJOB', "/home/dev/public_html/sub_dominios/$CLIENT/app/webroot/files/$CLIENT/templates_images");
    define('RUTAORIGENLOGOS', "/home/dev/public_html/sub_dominios/$CLIENT/app/webroot/files/$CLIENT/templates/images/");

    define('RUTAORIGENLOGOSSODEXO', RUTAORIGENLOGOS);

    define('RUTAFONDOS',"/home/dev/public_html/sub_dominios/$CLIENT/app/webroot/files/$CLIENT/templates/images/fondos");


    // Indica la direccion Ip y el Puerto de la API de MagicInfo para crear las carpetas remota
    define('MISODEXOSERVER', '190.151.100.11');
    define('MISODEXOSERVERPORT', 9502);
    define('MISODEXOFTP', '201.217.243.182');
    define('MISODEXOPORT', 21);
    define('MISODEXOFTPUSERNAME', 'ClientesDb@dev.digitalboard.app');
    define('MISODEXOFTPPASS', 'qazplmqazplm');

    // Debido a que todos los casinos en el magicinfo deben crearse dentro de la carpeta de sodexo hay que indicar cual el Id de la Carpeta padre
    // En este caso la carpeta SODEXO tiene un Id de 370
    define('MISODEXOFOLDERID', 370);

    define('IMAGENSODEXOPORDEFECTO','sodexo_logo_1080x1920.jpg');

    define('CONFIG_CASINOS', 'config_casinos');