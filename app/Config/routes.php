<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */


    Router::connect('/', array('controller' => 'dashboard', 'action' => 'index'));
    Router::connect('/program', array('controller' => 'program', 'action' => 'index'));
    Router::connect('/playlists/*', array('controller' => 'playlists', 'action' => 'index'));
    Router::connect('/playlists', array('controller' => 'playlists', 'action' => 'index'));
    Router::connect('/publications', array('controller' => 'publications', 'action' => 'index'));
    Router::connect('/publications/planification/*', array('controller' => 'publications', 'action' => 'planification'));
    Router::connect('/publications/changes/*', array('controller' => 'publications', 'action' => 'changes'));
    Router::connect('/publications/reports/*', array('controller' => 'publications', 'action' => 'reports'));
    Router::connect('/publications/no-publication/*', array('controller' => 'publications', 'action' => 'noPublication'));
    Router::connect('/publications/reportsDownload', array('controller' => 'publications', 'action' => 'download'));
    Router::connect('/publications/publicationsReviewImages', array('controller' => 'publications', 'action' => 'publicationsReviewImages'));
    Router::connect('/publications/publicationsReviewImages/tablePublicationsReviewAjax/*', array('controller' => 'publications', 'action' => 'tablePublicationsReviewAjax'));
    Router::connect('/publications/generateFolder', array('controller' => 'publications', 'action' => 'generateFolder'));
    Router::connect('/publications/generateYearFolder', array('controller' => 'publications', 'action' => 'generateFolderYears'));
    Router::connect('/tools/templatesURLs', array('controller' => 'tools', 'action' => 'templatesUrls'));
    Router::connect('/tools/offlineTools', array('controller' => 'tools', 'action' => 'offlineTools'));
    Router::connect('/tools/reset', array('controller' => 'tools', 'action' => 'reset'));
    Router::connect('/tools', array('controller' => 'tools', 'action' => 'index'));
    Router::connect('/monitor', array('controller' => 'monitor', 'action' => 'index'));
    Router::connect('/gallery', array('controller' => 'gallery', 'action' => 'all'));
    Router::connect('/gallery/cropModal/:id/:fieldKey/:expectedSize/:moduleKey', array('controller' => 'gallery', 'action' => 'cropModal'));
    Router::connect('/gallery/:action/*', array('controller' => 'gallery'));
    Router::connect('/gallery/*', array('controller' => 'gallery'));
    Router::connect('/users/delete/*', array('controller' => 'users', 'action' => 'delete'));
    Router::connect('/users/edit/*', array('controller' => 'users', 'action' => 'edit'));
    Router::connect('/users/add', array('controller' => 'users', 'action' => 'add'));
    Router::connect('/users/login', array('controller' => 'users', 'action' => 'login'));
    Router::connect('/users/forgot', array('controller' => 'users', 'action' => 'forgot'));
    Router::connect('/users/updatePassword', array('controller' => 'users', 'action' => 'updatePassword'));

    //revisar despues//

    Router::connect('/users/*', array('controller' => 'users'));

    //revisar
    
    Router::connect('/notifications/unread', array('controller' => 'notifications', 'action' => 'unread'));
    Router::connect('/notifications/box', array('controller' => 'notifications', 'action' => 'box'));
    Router::connect('/notifications/markas/*', array('controller' => 'notifications', 'action' => 'markAs'));
    Router::connect('/notifications/*', array('controller' => 'notifications'));
    Router::connect('/downloads', array('controller' => 'downloads', 'action' => 'index'));

    // dinamic modules

    Router::connect('/:module/createCasino', array('controller' => 'content', 'action' => 'createCasino'));
    Router::connect('/:module/createCasinoPost', array('controller' => 'content', 'action' => 'createCasinoPost'));
    Router::connect('/:module/updateCasino/:id', array('controller' => 'content', 'action' => 'updateCasino'));
    Router::connect('/:module/save', array('controller' => 'content', 'action' => 'save'));
    Router::connect('/:module/delete', array('controller' => 'content', 'action' => 'delete'));
    Router::connect('/:module/makePreview', array('controller' => 'content', 'action' => 'makePreview'));
    Router::connect('/:module/makePublish', array('controller' => 'content', 'action' => 'makePublish'));
    Router::connect('/:module/makeProcess', array('controller' => 'content', 'action' => 'makeProcess'));
    Router::connect('/:module/makeProcessCorporativo', array('controller' => 'content', 'action' => 'makeProcessCorporativo'));
    Router::connect('/:module/makeProcessMineria', array('controller' => 'content', 'action' => 'makeProcessMineria'));
    Router::connect('/:module/makeProcessSalud', array('controller' => 'content', 'action' => 'makeProcessSalud'));
    Router::connect('/:module/makeSync', array('controller' => 'content', 'action' => 'makeSync'));
    Router::connect('/:module/makePublishImageMode', array('controller' => 'content', 'action' => 'makePublishImageMode'));
    Router::connect('/:module/edit/:id', array('controller' => 'content', 'action' => 'edit'));
    Router::connect('/:module/editcasino/:id', array('controller' => 'content', 'action' => 'editCasino'));
    Router::connect('/:module/getLiveRelatedContent/:displayFields', array('controller' => 'content', 'action' => 'getLiveRelatedContent'));
    Router::connect('/:module/validateUnique', array('controller' => 'content', 'action' => 'validateUnique'));
    Router::connect('/:module/uploadLocal', array('controller' => 'content', 'action' => 'uploadLocal'));
    Router::connect('/:module/sendNotifications', array('controller' => 'content', 'action' => 'sendNotifications'));
    Router::connect('/:module/downloadCSV', array('controller' => 'content', 'action' => 'downloadCSV'));
    Router::connect('/:module/createTmpDish', array('controller' => 'content', 'action' => 'createTmpDish'));
    Router::connect('/:module/checkcode', array('controller' => 'content', 'action' => 'checkcode'));

    Router::connect('/:module/downloadReportsCount/:date', array('controller' => 'reportsApp', 'action' => 'downloadReportsCount'));
    Router::connect('/:module/downloadReports/:date', array('controller' => 'reportsApp', 'action' => 'downloadReports'));
    Router::connect('/:module/downloadReportsApp/:type', array('controller' => 'reportsApp', 'action' => 'downloadReportsApp'));
    Router::connect('/:module/downloadReportCSV', array('controller' => 'reportsApp', 'action' => 'downloadReportCSV'));

    Router::connect('/:module/reportsApp/:page', array('controller' => 'reportsApp'));
    Router::connect('/:module/reportsAppCount/:page', array('controller' => 'reportsApp', 'action' => 'reportsAppCount'));
    Router::connect('/:module/index-ajax', array('controller' => 'reportsApp', 'action' => 'indexAjax'));
    Router::connect('/*', array('controller' => 'content'));

    CakePlugin::routes();
