<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

//JSON
//$app->before(function (Request $request) {
//    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
//        $data = json_decode($request->getContent(), true);
//        $request->request->replace(is_array($data) ? $data : array());
//    }
//});

$app->error(function (\Exception $e, $code) use ($app) {
    $app['monolog']->addError($e->getMessage());
    //$app['monolog']->addError($e->getTraceAsString());

    return new JsonResponse(
        array(
            //'message' => $e->getMessage()
            'statusCode' => $code,
            //'stacktrace' => $e->getTraceAsString(),
        )
    );
});

return $app;
