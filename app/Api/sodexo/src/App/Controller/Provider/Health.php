<?php
namespace App\Controller\Provider;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Firebase\JWT\JWT;

class Health implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $health = $app['controllers_factory'];
/*
        $health->before(function (Request $request) use ($app) {
            // bearer
            $rawHeader = $request->headers->get('Authorization');
        });
*/
        $health->get('/', 'App\\Controller\\HealthController::ready');

        return $health;
    }
}
