<?php
namespace App\Controller\Provider;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Firebase\JWT\JWT;

class Job implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $job = $app['controllers_factory'];
        $job ->get('/{module}', 'App\\Controller\\JobController::index');

        return $job ;
    }
}
