<?php
namespace App\Controller\Provider;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Firebase\JWT\JWT;

class Display implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $display = $app['controllers_factory'];

        $display ->before(function (Request $request) use ($app) {
            // bearer
            $rawHeader = $request->headers->get('Authorization');
            if ($rawHeader) {
                if (strpos($rawHeader, 'Bearer ') === false) {
                    return new JsonResponse(
                        array('message' => 'Unauthorized', 'stacktrace' => ''),
                        401
                    );
                }

                $jwt = str_replace('Bearer ', '', $rawHeader);
                $secretKey = base64_decode($app['secret']);

                try {
                    $token = JWT::decode($jwt, $secretKey, [$app['algorithm']]);
                } catch (Exception $e) {
                    return new JsonResponse(
                        array('message' => 'Unauthorized', 'stacktrace' => ''),
                        401
                    );
                }
            } else {
                return new JsonResponse(
                    array('message' => 'Bad Request', 'stacktrace' => ''),
                    400
                );
            }
        });

        $display ->get('/{module}/macaddress', 'App\\Controller\\DisplayController::getDisplay');
        $display->post('/{module}/macaddress/{value}', 'App\\Controller\\DisplayController::insertDisplay');
        $display->put('/{module}/macaddress/{value}', 'App\\Controller\\DisplayController::updateDisplay');
        
        return $display ;
    }
}
