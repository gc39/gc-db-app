<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Silex\Application;

class PublicationController
{
    public function index(Application $app, Request $request)
    {
        $result = '';

        return new JsonResponse(
            $result,
            200
        );
    }

    public function lastModulePublications(Application $app, Request $request, $module){
      $url = "http://sodexo.digitalboard.cl/files/sodexo/templates/json/prod/$module-data.json";

      $contents = file_get_contents($url);
      $json = json_decode($contents);

      $min_date = PublicationController::datetotime('31-12-2100', 'DD-MM-YYYY');
      $max_date = PublicationController::datetotime('1-1-1900', 'DD-MM-YYYY');

      foreach ($json as $val) {
          if ($val->fecha != '0000-00-00'){
            $tmp = PublicationController::datetotime($val->fecha, 'DD-MM-YYYY');
            if ($tmp < $min_date)
                $min_date = $tmp;
            if ($tmp > $max_date)
                $max_date = $tmp;
          }
      }

      return new JsonResponse(
          date('d-m-Y', $min_date) . ' / ' . date('d-m-Y', $max_date),
          200
      );
    }

    public function lastPublications(Application $app, Request $request)
    {

      $sql_select = implode( ' ', ['SELECT ',
      'publish_history.module AS module,',
      'publish_history.send_date AS date,',
      '`user`.fullname AS username,',
      '`user`.email AS email,',
      'publish_history.`status` AS `status`,',
      'publicaciones_pantallas.desde AS `from`,',
      'publicaciones_pantallas.hasta AS `to`,',
      'monitoreo_pantallas.mac_pantalla AS `pantalla`, ',
      'monitoreo_pantallas.cliente,',
      'monitoreo_pantallas.casino,',
      'monitoreo_pantallas.segmento ',
      'FROM ',
      '((publish_history ',
      'JOIN `user` ON ((publish_history.user_id = `user`.id))) ',
      'LEFT JOIN publicaciones_pantallas ON (((publish_history.module = convert(`publicaciones_pantallas`.`modulo` using utf8)) AND (publish_history.send_date = publicaciones_pantallas.send_date)))) ',
      'INNER JOIN monitoreo_pantallas ON monitoreo_pantallas.modulo = publish_history.module ',
      'order by `publish_history`.`send_date` desc ',
      'limit 200']);


      /*
        $sql_select = implode( ' ', ['SELECT ',
	        '`publish_history`.`module` AS `module`,',
	        '`publish_history`.`send_date` AS `date`,',
	        '`user`.`fullname` AS `username`,',
	        '`user`.`email` AS `email`,',
	        '`publish_history`.`status` AS `status`, ',
          '`monitoreo_pantallas`.`mac_pantalla` AS `pantalla`, ',
          '`publicaciones_pantallas`.`desde` AS `from`, ',
          '`publicaciones_pantallas`.`hasta` AS `to` ',
        'FROM ',
  	      '(((',
  		          '`publish_history`',
  		          'JOIN `user` ON((`publish_history`.`user_id` = `user`.`id`))) ',
  		          'LEFT JOIN `monitoreo_pantallas` ON((`publish_history`.`module` = CONVERT(`monitoreo_pantallas`.`modulo` USING utf8)))) ',
  		          'LEFT JOIN `publicaciones_pantallas` ON(((`publish_history`.`module` = CONVERT(`publicaciones_pantallas`.`modulo` USING utf8)) ',
                'AND(`publish_history`.`send_date` = `publicaciones_pantallas`.`send_date` ',
          ')))) ',
  	        'ORDER BY `publish_history`.`send_date` DESC ',
            'LIMIT 200']);
            */

/*
        $sql_select = implode( ' ', ['SELECT ',
  	        '`publish_history`.`module` AS `module`,',
  	        '`publish_history`.`send_date` AS `date`,',
  	        '`user`.`fullname` AS `username`,',
  	        '`user`.`email` AS `email`,',
  	        '`publish_history`.`status` AS `status`, ',
            '`monitoreo_pantallas`.`mac_pantalla` AS `pantalla` ',
          'FROM ',
    	            '(',
    		'`publish_history`',
    		'JOIN `user` ON(',
    		    '(',
    		        '`publish_history`.`user_id` = `user`.`id`',
    		    ')',
    		')',
    	        ') ',
            'LEFT JOIN monitoreo_pantallas ON publish_history.module = monitoreo_pantallas.modulo ',
            'ORDER BY',
    	        '`publish_history`.`send_date` DESC',
            'LIMIT 200']);
            */

        //$sql_select = "SELECT nombre FROM casinos WHERE idCasino = {$casino}";
        $result = $app['db']->fetchAll($sql_select);

        return new JsonResponse(
            $result,
            200
        );
    }



    static public function datetotime ($date, $format = 'YYYY-MM-DD') {
        if ($format == 'YYYY-MM-DD') list($year, $month, $day) = explode('-', $date);
        if ($format == 'YYYY/MM/DD') list($year, $month, $day) = explode('/', $date);
        if ($format == 'YYYY.MM.DD') list($year, $month, $day) = explode('.', $date);

        if ($format == 'DD-MM-YYYY') list($day, $month, $year) = explode('-', $date);
        if ($format == 'DD/MM/YYYY') list($day, $month, $year) = explode('/', $date);
        if ($format == 'DD.MM.YYYY') list($day, $month, $year) = explode('.', $date);

        if ($format == 'MM-DD-YYYY') list($month, $day, $year) = explode('-', $date);
        if ($format == 'MM/DD/YYYY') list($month, $day, $year) = explode('/', $date);
        if ($format == 'MM.DD.YYYY') list($month, $day, $year) = explode('.', $date);

        return mktime(0, 0, 0, intval($month), intval($day), intval($year));
    }
}
