<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Silex\Application;

class JobController
{
    public function index(Application $app, Request $request, $module){
      $url = "http://sodexo.digitalboard.cl/files/sodexo/templates/json/prod/$module-data.json";

      $contents = file_get_contents($url);
      $json = json_decode($contents);

      $min_date = PublicationController::datetotime('31-12-2100', 'DD-MM-YYYY');
      $max_date = PublicationController::datetotime('1-1-1900', 'DD-MM-YYYY');

      foreach ($json as $val) {
          if ($val->fecha != '0000-00-00') {
            $tmp = PublicationController::datetotime($val->fecha, 'DD-MM-YYYY');
            if ($tmp < $min_date)
                $min_date = $tmp;
            if ($tmp > $max_date)
                $max_date = $tmp;
          }
      }

      $datefrom = date('Y-m-d', $min_date);
      $dateto = date('Y-m-d', $max_date);

      $sqlSelectLastPublish = "SELECT send_date FROM publish_history WHERE module = '$module' ORDER BY send_date DESC LIMIT 1";
      $lastPublish = $app['db']->fetchAssoc($sqlSelectLastPublish);
      $send_date = $lastPublish['send_date'];

      $sqlInsert = "INSERT INTO publicaciones_pantallas(modulo, desde, hasta, send_date) VALUES ('$module', '$datefrom', '$dateto', '$send_date')";
      $result = $app['db']->executeUpdate($sqlInsert);

      if($result == 0)
        $result = 'not updated';
      else
        $result = 'updated';

      return new JsonResponse(
          $result,
          200
      );
    }
}
