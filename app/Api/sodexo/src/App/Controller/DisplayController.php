<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Silex\Application;

class DisplayController
{
    public function getDisplay(Application $app, Request $request, $module)
    {
        $sql_select = "SELECT monitoreo_pantallas.mac_pantalla FROM monitoreo_pantallas WHERE modulo = '$module'";
        $result = $app['db']->fetchAll($sql_select);

        return new JsonResponse(
            $result,
            200
        );
    }

    public function updateDisplay(Application $app, Request $request, $module, $value)
    {
        //$content = $request->request->all();
        //$mac = $content['mac'];

        $sql = "UPDATE monitoreo_pantallas SET monitoreo_pantallas.mac_pantalla = '{$value}' WHERE modulo = '{$module}'";
        try{
          $result = $app['db']->executeUpdate($sql);
        }
        catch(Exception $e){
          $result = 0;
        }

        return new JsonResponse(
            $result,
            200
        );
    }

    public function insertDisplay(Application $app, Request $request, $module, $value)
    {
        $sql_select = "SELECT monitoreo_pantallas.mac_pantalla FROM monitoreo_pantallas WHERE modulo = '$module'";
        $result = $app['db']->fetchAll($sql_select);
        $recordslength = count($result);

        if ($recordslength == 1){
          $result = 0;
        } else {
          $sql = "INSERT INTO monitoreo_pantallas(modulo, mac_pantalla) VALUES ('$module','$value')";
          $result = $app['db']->executeUpdate($sql);
        }


        return new JsonResponse(
            $result,
            200
        );
    }
}
