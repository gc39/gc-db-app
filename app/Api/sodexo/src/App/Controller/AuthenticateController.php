<?php
namespace App\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Firebase\JWT\JWT;

class AuthenticateController
{
    public function authenticate(Application $app, Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');

        if ($email && $password) {
            $sql = "SELECT `user`.`password` FROM `user` WHERE `user`.email = ?";
            $md5password = $app['db']->fetchAssoc($sql, array($email));

            if (strcmp(md5($password), $md5password['password']) == 0) {
                $tokenId    = base64_encode(rand(1314159,23423423234));
                $issuedAt   = time();
                $notBefore  = $issuedAt;
                $expire     = $notBefore + 604800; // Adding 1 week
                $serverName = $app['serverName']; // Retrieve the server name from config file

                $data = [
                    'iat'  => $issuedAt,         // Issued at: Hora en que fue emitido el token
                    'jti'  => $tokenId,          // Json Token Id: Identificador Unico del Token
                    'iss'  => $serverName,       // Emisor
                    'nbf'  => $notBefore,        // No antes de
                    'exp'  => $expire           // Expiracion
                ];

                $secretKey = base64_decode($app['secret']);
                $algorithm = $app['algorithm'];

                $jwt = JWT::encode(
                    $data,
                    $secretKey,
                    $algorithm
                );

                return new JsonResponse(
                    array('token' => $jwt),
                    200
                );
            } else {
                return new JsonResponse(
                    array('message' => 'Failed to Authenticate'),
                    403
                );
            }
        } else {
            return new JsonResponse(
                array('message' => 'Failed to Authenticate'),
                403
            );
        }
    }
}
