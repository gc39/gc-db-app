<?php
	header('Content-Type: text/html; charset=UTF-8');
	if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip'))
		ob_start("ob_gzhandler");
	else
		ob_start();

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

require __DIR__.'/../app/config/config_prod.php';
require __DIR__.'/../app/config/parameters.php';
require __DIR__.'/../app/routes.php';
require __DIR__.'/../app/providers.php';
require __DIR__.'/../src/index.php';

$app['environment'] = "prod";
$app['debug'] = false;
$app['log.level'] = Monolog\Logger::ERROR;

$app['http_cache']->run();
