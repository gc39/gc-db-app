<?php
$app->match($app['api'].'/login', "\App\Controller\AuthenticateController::authenticate");
$app->mount($app['api'].'/health', new \App\Controller\Provider\Health());
$app->mount($app['api'].'/publication', new \App\Controller\Provider\Publication());
$app->mount($app['api'].'/display', new \App\Controller\Provider\Display());
$app->mount($app['api'].'/job', new \App\Controller\Provider\Job());
