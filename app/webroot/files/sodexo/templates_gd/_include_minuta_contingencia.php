<?php

$includeOK = include_once __DIR__.'/_include_base.php';
if ($includeOK != true) die();

// Preview
if (AMBIENTE == 'test') {

    // URLINDEX para los servicios usados 
    $urlPreview = '';
    for ($ind = 0; $ind < $cantServicios; $ind++) {
        if (URLINDEX == $ind) {
            $urlPreview = $servicios_usados[$ind];
        }
    }

    // recorre el json y genere un objeto con la data
    foreach ($jsonDataMinutaContingencia as &$slide) {
        $date_parts = split('-', $slide->fecha);
        $slide_ts = strtotime($date_parts[1] . '/' . $date_parts[0] . '/' . $date_parts[2]);
        if ($slide_ts == $today_ts) {
            $slide->nombre = $slide->nombre;
            $slide->servicio = $slide->servicio;
            $slides[$slide_ts][] = $slide;
        }
    }

    // Borrado del archivo en directorio preview
    $directorio = BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $cliente . '/' . $sucursal;
    $files = glob($directorio . '/*');
    foreach ($files as $file) {
        foreach ($borrar_archivos_minuta_contingencia as $n_arch) {
            if (is_file(($file))) {
                if (strpos($file, $n_arch) !== false) {
                    unlink($file);
                }
            }
        }
    }

    foreach ($servicios_usados as $servicio_usado) {
        foreach ($slides as $dates) {
            foreach ($dates as $slide) {

                // **********************************************************************************
                // Reglas del negocio
                // **********************************************************************************

                // Si corresponde con el servicio siempre y cuando la sucursal tenga dos o mas servicios
                if ($servicio_usado == $urlPreview && $slide->servicio == $urlPreview) {

                    // Imagen de Fondo
                    imagecopy($imageHandler, $bgImageMinuta, 0, 0, 0, 0, 1080, 1920);

                    // Posicion del plato
                    $plato = makeTextBlock(strtoupper(html_entity_decode($slide->platos)), $tipografia, 80, 1800);
                    $y = 546;

                    foreach ($plato as $index => $valueDet) {
                        $newLine = explode("\r\n", $valueDet);
                        if (is_array($newLine) && count($newLine) > 19) {
                            foreach ($newLine as $indexLine => $line) {
                                if ($indexLine < 19) {
                                    imagefttext($imageHandler, $tamanios['titulo_platos'], 0, 50, $y, $colors['titulo_platos'], $tipografia, $line);
                                    $y = $y + 70;
                                }
                            }
                        } else {
                            if ($index < 19) {
                                imagefttext($imageHandler, $tamanios['titulo_platos'], 0, 50, $y, $colors['titulo_platos'], $tipografia, $valueDet);
                                $y = $y + 70;
                            }
                        }
                    }

                    // Posición de la fecha
                    $date_parts = split('-', $slide->fecha);
                    $fecha_vigencia = $date_parts[2] . $date_parts[1] . $date_parts[0];
                    $stringDate =  $date_parts[0] . ' de ' . $arrayMonth[$date_parts[1] - 1] . ' ' . $date_parts[2];

                    if ($date_parts[0] < 10) {
                        $date_parts[0] = str_replace("0", "", $date_parts[0]);
                    }

                    // Codigo para el centrado de la fecha
                    $puntoInicio = newCenterText(strtoupper($stringDate), $tipografiaFecha, $tamanios['fecha'], $tamanoFondoMinuta[0]);

                    // Titulo Menu del Dia
                    $titulo = 'Menú del Día';

                    // Centrado del titulo Menu del dia
                    $inicioTitulo = newCenterText($titulo, $tipografia, $tamanios['titulo'], 1080);

                    imagettftext($imageHandler, $tamanios['titulo'], 0, $inicioTitulo, 350, $colors['titulo'], $tipografia, $titulo);
                    imagettftext($imageHandler, $tamanios['fecha'], 0, $puntoInicio, 400, $colors['alergenos'], $tipografiaFecha, $stringDate);

                    // **********************************************************************************
                    // Fin de las Reglas de Negocio
                    // **********************************************************************************

                    mkdir(BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $cliente);
                    mkdir(BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $cliente . '/' . $sucursal);

                    imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $cliente . '/' . $sucursal . '/' . '100' . '_' . ServicioUsadoCarpeta($servicio_usado) . '_' . $minuta_contingencia . '.jpg');
                }
            }
        }
    }
}

if (AMBIENTE == 'prod') {

    // api que marca la publicacion en monitoreo
    $casino = basename(__FILE__, '.php');
    file_get_contents(URLAPI . "/sodexo/job/$casino");

    // recorre el json y genere un objeto con la data
    foreach ($jsonDataMinutaContingencia as &$slide) {
        $date_parts = split('-', $slide->fecha);
        $slide_ts = strtotime($date_parts[1] . '/' . $date_parts[0] . '/' . $date_parts[2]);
        if ($slide_ts >= $today_ts) {
            $slide->nombre = $slide->nombre;
            $slide->servicio = $slide->servicio;
            $slides[$slide_ts][] = $slide;
            $slides_service[] = $slide->servicio;
        }
    }

    // Esta variable obtiene los servicios publicados y elimina los duplicados
    $servicios_publicados = array_unique($slides_service);

    // Borra archivos de hoy y crea las carpetas de hoy
    foreach ($servicios_publicados as $servicio_publicado) {
        $date_parts = split('-', $today);
        $date_parts[2] = ($date_parts[2] < 10) ? str_replace("0", "", $date_parts[2]) : $date_parts[2];
        // Borrado del archivo en directorio preview
        $directorio = BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_publicado);
        $files = glob($directorio . '/*');
        foreach ($files as $file) {
            foreach ($borrar_archivos_minuta_contingencia as $n_arch) {
                if (is_file(($file))) {
                    if (strpos($file, $n_arch) !== false) {
                        unlink($file);
                    }
                }
            }
        }
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0]);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1]);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2]);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_publicado));
    }

    // si no hay imagenes deja logo default
    $contador_archivos = 0;
    foreach ($servicios_usados as $servicio_usado) {
        $date_parts = split('-', $today);
        $date_parts[2] = ($date_parts[2] < 10) ? str_replace("0", "", $date_parts[2]) : $date_parts[2];
        // Borrado del archivo en directorio preview
        $directorio = BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado);
        $files = glob($directorio . '/*');
        foreach ($files as $file) {
            $contador_archivos++;
        }
        if ($contador_archivos == 0) {
            copy($defaultLogo, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado) . '/'  . 'sodexo_logo_1080x1920.jpg');
        }
    }

    foreach ($servicios_usados as $servicio_usado) {
        foreach ($slides as $dates) {
            foreach ($dates as $slide) {

                //Ajusta la fecha para crear las carpetas
                $date_parts = split('-', $slide->fecha);
                if ($date_parts[0] < 10) {
                    $date_parts[0] = str_replace("0", "", $date_parts[0]);
                }
                //crea las carpetas
                mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente);
                mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal);
                mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2]);
                mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1]);
                mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0]);
                mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0] . '/' . ServicioUsadoCarpeta($servicio_usado));

                // **********************************************************************************
                // Reglas del negocio
                // **********************************************************************************

                // Si el url preview corresponde con el servicio 
                if ($slide->servicio == $servicio_usado) {

                    // Imagen de Fondo
                    imagecopy($imageHandler, $bgImageMinuta, 0, 0, 0, 0, 1080, 1920);

                    // Posición del plato
                    $plato = makeTextBlock(strtoupper(html_entity_decode($slide->platos)), $tipografia, 80, 1800);
                    $y = 546;
                    
                    foreach ($plato as $index => $valueDet) {
                        $newLine = explode("\r\n", $valueDet);
                        if (is_array($newLine) && count($newLine) > 19) {
                            foreach ($newLine as $indexLine => $line) {
                                if ($indexLine < 19) {
                                    imagefttext($imageHandler, $tamanios['titulo_platos'], 0, 50, $y, $colors['titulo_platos'], $tipografia, $line);
                                    $y = $y + 70;
                                }
                            }
                        } else {
                            if ($index < 19) {
                                imagefttext($imageHandler, $tamanios['titulo_platos'], 0, 50, $y, $colors['titulo_platos'], $tipografia, $valueDet);
                                $y = $y + 70;
                            }
                        }
                    }

                    // Posición de la fecha
                    $date_parts = split('-', $slide->fecha);
                    $fecha_vigencia = $date_parts[2] . $date_parts[1] . $date_parts[0];
                    $stringDate =  $date_parts[0] . ' de ' . $arrayMonth[$date_parts[1] - 1] . ' ' . $date_parts[2];

                    if ($date_parts[0] < 10) {
                        $date_parts[0] = str_replace("0", "", $date_parts[0]);
                    }

                    // Codigo para el centrado de la fecha
                    $puntoInicio = newCenterText(strtoupper($stringDate), $tipografiaFecha, $tamanios['fecha'], $tamanoFondoMinuta[0]);

                    // Titulo Menu del Dia
                    $titulo = 'Menú del Día';

                    // Centrado del titulo Menu del dia
                    $inicioTitulo = newCenterText($titulo, $tipografia, $tamanios['titulo'], 1080);

                    imagettftext($imageHandler, $tamanios['titulo'], 0, $inicioTitulo, 350, $colors['titulo'], $tipografia, $titulo);
                    imagettftext($imageHandler, $tamanios['fecha'], 0, $puntoInicio, 400, $colors['alergenos'], $tipografiaFecha, $stringDate);

                    // **********************************************************************************
                    // Fin de las Reglas de Negocio
                    // **********************************************************************************

                    // Borra 
                    $directorio = BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0] . '/' . ServicioUsadoCarpeta($servicio_usado);
                    $files = glob($directorio . '/*');
                    foreach ($files as $file) {
                        foreach ($borrar_archivos_minuta_contingencia as $n_arch) {
                            if (is_file(($file))) {
                                if (strpos($file, $n_arch) !== false) {
                                    unlink($file);
                                }
                            }
                        }
                    }
                    imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0] . '/' . ServicioUsadoCarpeta($servicio_usado) . '/'  .  '101_' . $minuta_contingencia . '_' . $fecha_hora . '_' . $fecha_vigencia . '_' . $fecha_vigencia . '.jpg');
                    imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0] . '/' . ServicioUsadoCarpeta($servicio_usado) . '/'  .  '201_' . $minuta_contingencia . '_' . $fecha_hora . '_' . $fecha_vigencia . '_' . $fecha_vigencia . '.jpg');
                    imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0] . '/' . ServicioUsadoCarpeta($servicio_usado) . '/'  .  '301_' . $minuta_contingencia . '_' . $fecha_hora . '_' . $fecha_vigencia . '_' . $fecha_vigencia . '.jpg');
                }
            }
        }
    }
    shell_exec(PHP_RUTA . ' ' . CARPETA_SHELL_EXEC . 'copiarArchFTP_Casinos.php ' . $cliente_sucursal . ' > /dev/null 2>&1 &');
}

// libera memoria de sistema
imagedestroy($bgImage);
imagedestroy($imageHandler);
