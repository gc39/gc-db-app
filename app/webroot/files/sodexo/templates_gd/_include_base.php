<?php

// Colores Utilizados
$imageHandler                   = @imagecreatetruecolor(1080, 1920);
$colors['titulo']               = imagecolorallocate($imageHandler, 255, 255, 255);     // Color del Titulo del Menu Diario (Menú del Día)
$colors['titulo_platos']        = imagecolorallocate($imageHandler, 255, 255, 255);     // Color del Titulo de Platos de Minuta y Menu Diario 
$colors['calorias']             = imagecolorallocate($imageHandler, 255, 255, 255);     // Color de las Calorias y de las Kcal (Unidad de Medida) del Menu Diario 
$colors['alergenos']            = imagecolorallocate($imageHandler, 255, 195, 0);       // Color de los Alergenos del Menu Diario 
$colors['aviso_alergenos']      = imagecolorallocate($imageHandler, 255, 195, 0);       // Color del Aviso de los Alergenos del Menu Diario
$colors['simbologia']           = imagecolorallocate($imageHandler, 255, 255, 255);     // Color del Significado de las Simbologias del Menu Diario
$colors['fecha']                = imagecolorallocate($imageHandler, 255, 195, 0);       // Color de la Fecha de Minuta de Contingencia y Menu Diario
$colors['tipo_plato']           = imagecolorallocate($imageHandler, 249, 231, 159);     // Color de los Tipos de Plato en el Menu Diario
$colors['platos_sugerencia']    = imagecolorallocate($imageHandler, 255, 255, 255);     // Color de los platos de Sugerencia del Chef
$colors['calorias_sugerencia']  = imagecolorallocate($imageHandler, 202, 200, 200);     // Color de las Calorias y de las Kcal (Unidad de Medida) en Sugerencia del Chef
$colors['fecha_sugerencia']     = imagecolorallocate($imageHandler, 255, 255, 255);     // Color de la Fecha en Sugerencia del Chef
$colors['cumpleanos']           = imagecolorallocate($imageHandler, 255, 255, 255);     // Color del Mes del Cumpleaños

// Cuando el menu diario esta en ingles
$colors['titulo_platos_espanol']= imagecolorallocate($imageHandler, 255, 195, 0);       // Color del Titulo de Platos de Menu Diario Español cuando ENGLISH es false
$colors['titulo_platos_ingles'] = imagecolorallocate($imageHandler, 255, 255, 255);     // Color del Titulo de Platos de Menu Diario Ingles cuando ENGLISH es true
$colors['calorias_espanol']     = imagecolorallocate($imageHandler, 255, 255, 255);     // Color de las Calorias y de las Kcal (Unidad de Medida) del Menu Diario cuando ENGLISH es false
$colors['calorias_ingles']      = imagecolorallocate($imageHandler, 255, 195, 0);       // Color de las Calorias y de las Kcal (Unidad de Medida) del Menu Diario cuando ENGLISH es true
$colors['alergenos_espanol']    = imagecolorallocate($imageHandler, 255, 195, 0);       // Color de los Alergenos del Menu Diario cuando ENGLISH es false
$colors['alergenos_ingles']     = imagecolorallocate($imageHandler, 255, 255, 255);     // Color de los Alergenos del Menu Diario cuando ENGLISH es true

// Tamaños Utilizados
$tamanios['titulo']                 = 60;       // Tamaño del Titulo del Menu Diario y Minuta de Contingencia (Menú del Día)
$tamanios['titulo_platos']          = 45;       // Tamaño del Titulo de Platos de Minuta y Menu Diario
$tamanios['platos_sugerencia']      = 110;      // Tamaño de los platos de Sugerencia      
$tananios['calorias']               = 40;       // Tamaño de las Calorias del Menu Diario
$tananios['kcal_menu']              = 25;       // Tamaño de las Kcal (Unidad de Medida) del Menu Diario
$tamanios['alergenos']              = 34;       // Tamaño del Alergeno en el Menu Diario
$tamanios['aviso_alergenos']        = 25;       // Tamaño del Aviso de los Alergenos del Menu Diario
$tamanios['simbologia']             = 25;       // Tamaño del Significado de las Simbologias en el Menu Diario
$tamanios['fecha']                  = 35;       // Tamaño de la Fecha de Minuta de Contingencia y Menu Diario
$tamanios['tipo_plato']             = 42;       // Tamaño del Tipo de Plato en el Menu Diario 
$tamanios['calorias_sugerencia']    = 70;       // Tamaño de las Calorias en Sugerencia del Chef
$tamanios['kcal_sugerencia']        = 40;       // Tamaño de las Kcal (Unidad de Medida) en Sugerencia del Chef
$tamanios['fecha_sugerencia']       = 42;       // Tamaño de la Fecha en Sugerencia del Chef
$tamanios['cumpleanos']             = 115;      // Tamaño del Mes del Cumpleaños 

// Tipografia a utilizar
$tipografia         = BASE_TEMPLATE . '/templates/fonts/ttf/BebasNeue-Regular.ttf'; 
$tipografiaFecha    = BASE_TEMPLATE . '/templates/fonts/otf/BebasNeueBold.otf';     // Fechas Menu Diario, Contingencia y Sugerencia del Chef

// Coordenada X donde comienzan los tipos de platos, platos y alergenos en el Menu Diario
$inicio_platos = 85;

// Espacio entre platos cuando empieza un nuevo tipo de plato en el Menu Diario
$espacio_platos = 90;

// Coordenada "Y" de la imagen de fondo donde comienza a imprimir los titulos y platos en el Menu Diario
$inicio = 450;

// Variable que corresponde a KCAL
$kcal = ' KCAL';

// Titulo Menu del Dia
$titulo = 'Menú del Día';

//limite del nombre de los platos
$limite_nombre_plato = 29;

// Texto de Alergenos Presentes en Preparaciones
$aviso_Alergenos = '* Alérgenos presentes en preparación';

// Coordenadas donde comienza el menu del dia (variable y), fecha (variable y), leyenda de simbologias

$inicio_menu_del_dia_1  = 350;  // Donde comienza el titulo Menu del Dia en el menu diario
$inicio_fecha_1         = 400;  // Donde comienza la fecha en el menu diario
$inicio_simbologia_x_1  = 45;   // Donde comienza la simbologia en la coordenada X
$inicio_simbologia_y_1  = 1775; // Donde comienza la simbologia en la coordenada Y

// Coordenada Y donde comienza el aviso de alergenos

$aviso_alergenos_y     = 1750; // Parametro Y donde esta ubicado el aviso de los alergenos en la imagen

// Datos de Cliente, Sucursal y Segmento
$cliente            = $this->moduleData['gcdb']['cliente'];                     // Es el nombre del casino del cliente 
$sucursal           = $this->moduleData['gcdb']['sucursal'];                    // Es el nombre de la sucursal del cliente 
$cliente_sucursal   = $cliente . '_' . $sucursal;                               // $cliente_$sucursal
$segmento           = $this->moduleData['gcdb']['segmento'];                    // Es el segmento al que pertenece el casino
$ingles             = $this->moduleData['gcdb']['ingles'];                      // Si ingles en el xml es Si el menu tambien estara en ingles
$servicios          = $this->moduleData['gcdb']['servicios']['servicio'];       // Servicios con atributos obtenidos del XML



if ($servicios['@servicio'] != NULL)
{
    $servicios_usados[0] = $servicios['@servicio'];                             // Servicios usados que posee si es un solo servicio
    $paginas[$servicios['@servicio']] = intval($servicios['@paginas']);          // Paginas que tiene asociadas cada servicio si es un solo servicio
    $cantServicios = 1;                                                          // Cantidad de Servicios disponibles

}else{

    foreach($servicios as $servicio)
    {
        $servicios_usados[] = $servicio['@servicio'];                               // Servicios usados que posee cada modulo
        $paginas[$servicio['@servicio']] = intval($servicio['@paginas']);           // Paginas que tiene asociadas cada servicio
    }
    $cantServicios      = count($servicios_usados);                                 // Cantidad de Servicios disponibles
}

// echo '<pre>' . var_export($servicios_usados , true) . '</pre>';//die();
// echo  count($servicios_usados);
// die();

$usuario = $userLogged["User"]["email"];

$cantServicios      = count($servicios_usados);                                 // Cantidad de Servicios disponibles
global $tipos_platos;                                                           
$tipos_platos       = $this->moduleData['gcdb']['tipos_platos']['tipo_plato'];  // Trae los tipos de platos con sus atributos del xml
//echo serialize($tipos_platos);
//die();

// Variables para las FullZona
$nombre_FullZona      = $this->moduleData['gcdb']['nombre_FullZona'];  // Es el nombre del modulo
$indice_FullZona      = $this->moduleData['gcdb']['indice_FullZona'];  // Indice de la Full Zona
$borrar_archivos      = [$nombre_FullZona, 'sodexo_logo'];             // Los archivos que borra ese Modulo

// Variables para menu diario, minuta de contingencia y sugerencia del chef
$menu_diario                            = 'menu_diario';
$minuta_contingencia                    = 'minuta_contingencia';
$sugerencia_chef                        = 'sugerencia_chef';
$borrar_archivos_menu_diario            = ['menu_diario'        , 'minuta_contingencia'    , 'sodexo_logo'];
$borrar_archivos_minuta_contingencia    = ['minuta_contingencia', 'menu_diario', 'branding', 'sodexo_logo'];
$borrar_archivos_sugerencia_chef        = ['sugerencia_chef'                               , 'sodexo_logo'];
$borrar_archivos_branding               = ['branding'           , 'minuta_contingencia'    , 'sodexo_logo'];

// Archivos json a utilizar

// Archivos json para los FullZona
$jsonFile             = file_get_contents(BASE_TEMPLATE . '/templates/json/' . AMBIENTE . '/' . $cliente . '_' . $sucursal . '_' . $nombre_FullZona . '-data.json');
//echo BASE_TEMPLATE . '/templates/json/' . AMBIENTE . '/' . $cliente . '_' . $sucursal . '_' . $nombre_FullZona . '-data.json';
$jsonData             = json_decode($jsonFile);

$jsonFileMenuDiario             = file_get_contents(BASE_TEMPLATE . '/templates/json/' . AMBIENTE . '/' . $cliente . '_' . $sucursal . '_' . $menu_diario . '-data.json');
$jsonDataMenuDiario             = json_decode($jsonFileMenuDiario);

$jsonFileMinutaContingencia     = file_get_contents(BASE_TEMPLATE . '/templates/json/' . AMBIENTE . '/' . $cliente . '_' . $sucursal . '_' . $minuta_contingencia . '-data.json');
$jsonDataMinutaContingencia     = json_decode($jsonFileMinutaContingencia);

$jsonFileSugerenciaChef         = file_get_contents(BASE_TEMPLATE . '/templates/json/' . AMBIENTE . '/' . $cliente . '_' . $sucursal . '_' . $sugerencia_chef . '-data.json');
$jsonDataSugerenciaChef         = json_decode($jsonFileSugerenciaChef);


$jsonFileMaestroProductos       = file_get_contents(BASE_TEMPLATE . '/templates/json/prod/maestro_productos-data.json');

// Formatos de Fecha y hora a utilizar
$fecha          = new DateTime("now", new DateTimeZone('America/Santiago'));
$fecha_hora     = $fecha->format('Ymd-His');
$num_dia        = $fecha->format("N");
$current_time   = $fecha->format('d-m-Y');
$fecha_hoy      = $fecha->format('Y/m/d');
$today          = $fecha->format("Y-m-d");
$fecha_Inicio   = $fecha->format("Ymd");
$fecha_Termino  = $fecha->format("Ymd");
$today_ts       = strtotime($current_time);

// Patrones necesarios para quitar acentos
$patterns[0] = 'á';
$patterns[1] = 'é';
$patterns[2] = 'í';
$patterns[3] = 'ó';
$patterns[4] = 'ú';
$patterns[5] = ' ';

$replacements[0] = 'a';
$replacements[1] = 'e';
$replacements[2] = 'i';
$replacements[3] = 'o';
$replacements[4] = 'u';
$replacements[5] = '';


// Imagenes de Fondo de Cumpleaños , Minuta de Contingencia y Sugerencia del Chef
$segmento = strtolower(str_replace($patterns, $replacements, $segmento));

if($segmento == "corporativo"){
    $RutaFondoMenuDiario1 = RUTAFONDOS . "/corporativo/bg_menu_diario_pagina1_Alm.jpg";
    $RutaFondoMenuDiario2 = RUTAFONDOS . "/corporativo/bg_menu_diario_pagina2_Alm.jpg";
    $RutaFondoMenuDiario3 = RUTAFONDOS . "/corporativo/bg_menu_diario_pagina3_Alm.jpg";

    $rutaImagenCumpleanos   = RUTAFONDOS . "/corporativo/bg_casino_cumpleanos.jpg";
    $bgImageCumpleanos      = imagecreatefromjpeg($rutaImagenCumpleanos);
    $tamanoFondoCumpleanos  = getimagesize($rutaImagenCumpleanos);

    $rutaImagenMinuta       = RUTAFONDOS . "/corporativo/bg_minuta_contingencia.jpg";
    $bgImageMinuta          = imagecreatefromjpeg($rutaImagenMinuta);
    $tamanoFondoMinuta      = getimagesize($rutaImagenMinuta);

    $rutaImagenSugerencia   = RUTAFONDOS . "/corporativo/bg_sugerencia_chef.jpg";
    $bgImageSugerencia      = imagecreatefromjpeg($rutaImagenSugerencia);
    $tamanoFondoSugerencia  = getimagesize($rutaImagenSugerencia);
}

if($segmento == "mineria"){
    $RutaFondoMenuDiario1 = RUTAFONDOS . "/mineria/bg_menu_diario_pagina1_Alm.jpg";
    $RutaFondoMenuDiario2 = RUTAFONDOS . "/mineria/bg_menu_diario_pagina2_Alm.jpg";
    $RutaFondoMenuDiario3 = RUTAFONDOS . "/mineria/bg_menu_diario_pagina3_Alm.jpg";

    $rutaImagenCumpleanos   = RUTAFONDOS . "/mineria/bg_casino_cumpleanos.jpg";
    $bgImageCumpleanos      = imagecreatefromjpeg($rutaImagenCumpleanos);
    $tamanoFondoCumpleanos  = getimagesize($rutaImagenCumpleanos);

    $rutaImagenMinuta       = RUTAFONDOS . "/mineria/bg_minuta_contingencia.jpg";
    $bgImageMinuta          = imagecreatefromjpeg($rutaImagenMinuta);
    $tamanoFondoMinuta      = getimagesize($rutaImagenMinuta);

    $rutaImagenSugerencia   = RUTAFONDOS . "/mineria/bg_sugerencia_chef.jpg";
    $bgImageSugerencia      = imagecreatefromjpeg($rutaImagenSugerencia);
    $tamanoFondoSugerencia  = getimagesize($rutaImagenSugerencia);
}

if($segmento == "salud"){
    $RutaFondoMenuDiario1 = RUTAFONDOS . "/salud/bg_menu_diario_pagina1_Alm.jpg";
    $RutaFondoMenuDiario2 = RUTAFONDOS . "/salud/bg_menu_diario_pagina2_Alm.jpg";
    $RutaFondoMenuDiario3 = RUTAFONDOS . "/salud/bg_menu_diario_pagina3_Alm.jpg";

    $rutaImagenCumpleanos   = RUTAFONDOS . "/salud/bg_casino_cumpleanos.jpg";
    $bgImageCumpleanos      = imagecreatefromjpeg($rutaImagenCumpleanos);
    $tamanoFondoCumpleanos  = getimagesize($rutaImagenCumpleanos);

    $rutaImagenMinuta       = RUTAFONDOS . "/salud/bg_minuta_contingencia.jpg";
    $bgImageMinuta          = imagecreatefromjpeg($rutaImagenMinuta);
    $tamanoFondoMinuta      = getimagesize($rutaImagenMinuta);

    $rutaImagenSugerencia   = RUTAFONDOS . "/salud/bg_sugerencia_chef.jpg";
    $bgImageSugerencia      = imagecreatefromjpeg($rutaImagenSugerencia);
    $tamanoFondoSugerencia  = getimagesize($rutaImagenSugerencia);
}


// Logo default si no hay imagenes en carpeta
$defaultLogo = BASE_TEMPLATE . '/' . 'templates/images/sodexo_logo_1080x1920.jpg';

// Variables creadas para mejorar Sugerencia del Chef
$imageTrans = imagecreatetruecolor(1080, 1920);
$color      = imagecolorallocatealpha($imageTrans, 0, 0, 0, 127); // Color Transparente
imagefill($imageTrans, 0, 0, $color);
imagesavealpha($imageTrans, true);

// Iconos a utilizar para el menu diario
$segmento = strtolower(str_replace($patterns, $replacements, $segmento));

if($segmento == "corporativo"){
    $icoSal            = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/fondos/corporativo/ico_sal.png');
    $icoGluten         = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/fondos/corporativo/ico_gluten.png');
    $icoVegano         = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/fondos/corporativo/ico_vegano.png');
    $icoAzucar         = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/fondos/corporativo/ico_azucar.png');
    $icoVegetariano    = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/fondos/corporativo/ico_vegetariano.png');
}

if($segmento == "mineria"){
    $icoSal            = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/fondos/corporativo/ico_sal.png');
    $icoGluten         = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/fondos/corporativo/ico_gluten.png');
    $icoVegano         = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/fondos/corporativo/ico_vegano.png');
    $icoAzucar         = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/fondos/corporativo/ico_azucar.png');
    $icoVegetariano    = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/fondos/corporativo/ico_vegetariano.png');
}

if($segmento == "salud"){
    $icoSal            = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/fondos/corporativo/ico_sal.png');
    $icoGluten         = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/fondos/corporativo/ico_gluten.png');
    $icoVegano         = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/fondos/corporativo/ico_vegano.png');
    $icoAzucar         = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/fondos/corporativo/ico_azucar.png');
    $icoVegetariano    = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/fondos/corporativo/ico_vegetariano.png');
}

// Arreglos Importantes de Meses y Dias
$arrayMonth     = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
$arrayWeek      = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');

// Variables Iniciales Esenciales
$control    = false; 
$slides     = false;
$a          = 1;
$masterObj;

// Funciones Claves

// Funcion para centrar texto
function centerText($text, $font, $size, $xi)
{
    $box = ImageTTFBBox($size, 0, $font, $text);
    $xr = abs(max($box[2], $box[4]));
    $x = intval(($xi - $xr) / 2);
    return array($x);
}

// Funcion para crear bloque de texto
function makeTextBlock($text, $fontfile, $fontsize, $width)
{
    $words = explode(' ', $text);
    $lines = array($words[0]);
    $currentLine = 0;
    for ($i = 1; $i < count($words); $i++) {
        $lineSize = imagettfbbox($fontsize, 0, $fontfile, $lines[$currentLine] . ' ' . $words[$i]);
        if ($lineSize[2] - $lineSize[0] < $width) {
            $lines[$currentLine] .= ' ' . $words[$i];
        } else {
            $currentLine++;
            $lines[$currentLine] = $words[$i];
        }
    }
    return $lines;
}

// Funcion para insertar imagen
function imagettftextSp($image, $size, $angle, $x, $y, $color, $font, $text, $spacing = 0)
{
    if ($spacing == 0) {
        imagettftext($image, $size, $angle, $x, $y, $color, $font, $text);
    } else {
        $temp_x = $x;
        for ($i = 0; $i < strlen($text); $i++) {
            $bbox = imagettftext($image, $size, $angle, $temp_x, $y, $color, $font, $text[$i]);
            $temp_x += $spacing + ($bbox[2] - $bbox[0]);
        }
    }
}

// Nueva Funcion de Centrado de Texto
function newCenterText($text, $font, $size, $distance)
{
    $dimensionsText = imagettfbbox($size, 0, $font, $text);
    $widthText = (abs($dimensionsText[2]) - abs($dimensionsText[0])) + 6;
    $middlePointText = $widthText / 2;
    $startPointText = ($distance / 2) - $middlePointText;
    return $startPointText;
}

// Funcion para obtener el codigo maestro
// function getMasterCode($code, $jsonFileMaster)
// {
//     $jsonDataMaster = json_decode($jsonFileMaster);
//     foreach ($jsonDataMaster as $master) {
//         if (trim($master->codigo) == $code) {
//             return $master;
//         }
//     }
// }

// Funcion para obtener el codigo maestro desde bd
function getMasterCode($code)
{

    // Si el plato temporal existe entonces asignara los datos por su codigo temporal para que se muestre en la imagen
    if ($code != NULL) {
        //Incluye datos de Config y valida 
        $includeOK = include_once __DIR__ . '/../../../../Config/config.php';
        if ($includeOK != true) die();

        $sql = "SELECT
            maestro_productos.id, 
            maestro_productos.codigo, 
            maestro_productos.nombre_espanol, 
            maestro_productos.nombre_ingles, 
            maestro_productos.calorias, 
            maestro_productos.lactosa, 
            maestro_productos.soya, 
            maestro_productos.frutos_secos, 
            maestro_productos.gluten, 
            maestro_productos.vegano, 
            maestro_productos.vegetariano, 
            maestro_productos.altoazucar, 
            maestro_productos.altosodio
        FROM
            maestro_productos
        WHERE
            maestro_productos.codigo = $code";

        //Conecta a base de datos
        $obj_conexion = new mysqli(LOCALSERVER, LOCALUSERDB, LOCALPASSDB, LOCALDB) or die("db_error");
        if (!$resultado = $obj_conexion->query($sql)) die();

        if ($resultado->num_rows != 0) {
            while ($rows = $resultado->fetch_assoc()) {
                $master->id = $rows['id'];
                $master->codigo = $rows['codigo'];
                $master->nombre_espanol = $rows['nombre_espanol'];
                $master->nombre_ingles = $rows['nombre_ingles'];
                $master->calorias = $rows['calorias'];
                $master->altosodio = strtolower($rows['altosodio']);
                $master->gluten = strtolower($rows['gluten']);
                $master->vegano = strtolower($rows['vegano']);
                $master->vegetariano = strtolower($rows['vegetariano']);
                $master->altoazucar = strtolower($rows['altoazucar']);
                $master->lactosa = $rows['lactosa'];
                $master->soya = $rows['soya'];
                $master->frutos_secos = $rows['frutos_secos'];
            }
        }

        return $master;
    }
}



// Ordena slides por indice
function OrdenaArrayIndice($a,$b)
{
	if ($a->indice==$b->indice) return 0;
	return ($a->indice < $b->indice)?-1:1;
}

// Graba los log publish_history_menu_diario
function PublishHistoryMenuDiario($cliente_sucursal, $fecha_exhibicion)
{
    $obj_conexion = new mysqli(LOCALSERVER, LOCALUSERDB, LOCALPASSDB, LOCALDB) or die("db_error");
    $sql = "INSERT INTO publish_history_menu_diario(cliente_sucursal,fecha_exhibicion,fecha_hora) VALUES ('$cliente_sucursal','$fecha_exhibicion',NOW())";
    $resultado = $obj_conexion->query($sql);
    return $resultado;
}

// Graba resporte al Publicar
function PublishReportesModificacion($today,$fecha_exhibicion,$segmento,$cliente,$sucursal,$usuario)
{
    //Conecta a base de datos
    $obj_conexion = new mysqli(LOCALSERVER, LOCALUSERDB, LOCALPASSDB, LOCALDB) or die("db_error");
    
    $sql = "INSERT INTO reportes_modificacion(accion,fecha_mod,fecha_exh,Segmento,cliente,sucursal,usuario,created,modified) VALUES ('Publicar','$today','$fecha_exhibicion','$segmento','$cliente','$sucursal','$usuario',NOW(),NOW())";
    if (!$resultado = $obj_conexion->query($sql)) die();
    
    return $resultado;
}

// Permite asignar el nombre del servicio usado para el nombre de la carpeta
function LimpiaServicioUsado($servicio_usado)
{
	// Patrones necesarios para quitar acentos
	$patterns[0] = 'á';
	$patterns[1] = 'é';
	$patterns[2] = 'í';
	$patterns[3] = 'ó';
	$patterns[4] = 'ú';
	$patterns[5] = ' ';

	$replacements[0] = 'a';
	$replacements[1] = 'e';
	$replacements[2] = 'i';
	$replacements[3] = 'o';
	$replacements[4] = 'u';
	$replacements[5] = '';

	$servicio_usado = str_replace($patterns, $replacements, $servicio_usado);
	return $servicio_usado;
}

// Permite asignar el nombre del servicio usado para el nombre de la carpeta
function ServicioUsadoCarpeta($servicio_usado_carpeta)
{

    //Consulta por el servicio segun menu
    $sql = "SELECT config_casinos_servicios.carpeta FROM config_casinos_servicios WHERE nombre = '$servicio_usado_carpeta' LIMIT 1";

    //Conecta a base de datos
    $obj_conexion = new mysqli(LOCALSERVER, LOCALUSERDB, LOCALPASSDB, LOCALDB) or die("db_error");
    if (!$resultado = $obj_conexion->query($sql)) die();

    if ($resultado->num_rows != 0) {
        while ($rows = $resultado->fetch_assoc()) {
            $carpeta = $rows['carpeta'];
        }
    }
    $servicio_usado_carpeta =  $carpeta ? $carpeta : $servicio_usado_carpeta;

	// Patrones necesarios para quitar acentos
	$patterns[0] = 'á';
	$patterns[1] = 'é';
	$patterns[2] = 'í';
	$patterns[3] = 'ó';
	$patterns[4] = 'ú';
	$patterns[5] = ' ';

	$replacements[0] = 'a';
	$replacements[1] = 'e';
	$replacements[2] = 'i';
	$replacements[3] = 'o';
	$replacements[4] = 'u';
	$replacements[5] = '';

	$servicio_usado_carpeta = str_replace($patterns, $replacements, $servicio_usado_carpeta);
    
	$servicio_usado_carpeta = (strtolower($servicio_usado_carpeta) == 'almuerzo') 		? 'Alm' 	: $servicio_usado_carpeta;
    $servicio_usado_carpeta = (strtolower($servicio_usado_carpeta) == 'cenatarde') 		? 'CenTar' 	: $servicio_usado_carpeta;
    $servicio_usado_carpeta = (strtolower($servicio_usado_carpeta) == 'cenamadrugada') 	? 'CenMad' 	: $servicio_usado_carpeta;
	$servicio_usado_carpeta = (strtolower($servicio_usado_carpeta) == 'cenanocturna') 	? 'CenNoc' 	: $servicio_usado_carpeta;
    $servicio_usado_carpeta = (strtolower($servicio_usado_carpeta) == 'desayuno') 		? 'Des' 	: $servicio_usado_carpeta;
	$servicio_usado_carpeta = (strtolower($servicio_usado_carpeta) == 'colacion') 		? 'Col' 	: $servicio_usado_carpeta;
    $servicio_usado_carpeta = (strtolower($servicio_usado_carpeta) == 'once') 			? 'Onc' 	: $servicio_usado_carpeta;

	return $servicio_usado_carpeta;
}

// crea conexion global a base de datos
global $obj_conexion;
$obj_conexion = new mysqli(LOCALSERVER, LOCALUSERDB, LOCALPASSDB, LOCALDB) or die("db_error");