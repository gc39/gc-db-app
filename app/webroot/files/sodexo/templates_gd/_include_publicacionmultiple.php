<?php

if (!defined('BASE_TEMPLATE')) {
    define('BASE_TEMPLATE', WWW_ROOT . 'files' . '/' . $clientFolder);
}

//$servicios_usados = ['Alm'];
$servicios_usados = array();

// es el indice de bloque en playlist
$Indice = $this->moduleData['gcdb']['indice'];

//es el nombre del archivo segun el xml
$nombre_FullZona = $this->moduleData['gcdb']['nombre_FullZona'];

//Contenido del nombre el archivo a borrar antes de copiar
$borrar_archivos = [$nombre_FullZona, 'sodexo_logo'];

//Logo default si no hay imagenes en carpeta
$defaultLogo = BASE_TEMPLATE . '/' . 'templates/images/sodexo_logo_1080x1920.jpg';

$jsonFile = file_get_contents(BASE_TEMPLATE . '/templates/json/' . AMBIENTE . '/publicacionmultiple_' . $nombre_FullZona . '-data.json');
$jsonData = json_decode($jsonFile);
$slides = false;

foreach ($jsonData as $itemData) {
    $casinos_corporativos = explode('|#|', $itemData->casinos);
    $itemData->casinos = $casinos_corporativos;

    $ids_pub = explode('|#|', $itemData->ids_pub);
    $itemData->ids_pub = $ids_pub;
}

$fecha = new DateTime("now", new DateTimeZone('America/Santiago'));
$fecha_hora = $fecha->format('Ymd-His');
$num_dia = $fecha->format("N");
$current_time = $fecha->format('d-m-Y');
$fecha_hoy = $fecha->format('Y/m/d');
$today =  $fecha->format("Y-m-d");
$today_ts     = strtotime($current_time);
$a = 1;

$server_db = LOCALSERVER;
$user_db = LOCALUSERDB;
$password_db = LOCALPASSDB;
$db_db = LOCALDB;

$obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
if (!$obj_conexion) {
    echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
    die();
}
$GLOBALS['obj_conexion'] = $obj_conexion;

if (AMBIENTE == 'prod') {

    // recorre el json y genere un objeto con la data
    foreach ($jsonData as &$slide) {

        $date_ini = strtotime($slide->fecha_inicio);
        $date_ter = strtotime($slide->fecha_termino);

        if ($date_ini <= $date_ter) {
            // permite que se pueda publicar para fechas futuras
            if (($today_ts <= $date_ini || $today_ts >= $date_ini)  && $today_ts <= $date_ter) {
                $slides[$a] = $slide;
                $a++;
            }
        }
    }

    foreach ($slides as $slide) {
        $row_id = $slide->id;
        $fechaIni = str_replace("-", "", $slide->fecha_inicio);
        $fechaTer = str_replace("-", "", $slide->fecha_termino);
        $resultado = array_diff($slide->ids_pub, $slide->casinos);
        $resultado = array_merge($resultado, $slide->casinos);
        $resultado = array_filter($resultado);

        
        
        // Borrando contenido 020_PM_ de la lista de borrado
        foreach ($resultado as $modulecasino) {

            // se extrae el cliente y la sucursal a partir del nombre del modulo
            $clientesucursal = getClienteSucursal($modulecasino);

            $cliente = $clientesucursal[0];
            $sucursal = $clientesucursal[1];

            $servicios_usados = getCarpetasPorClienteSucursal($cliente, $sucursal);

            //Borra archivos a partir de hoy
            for ($recorre_fecha = $slide->fecha_inicio; strtotime($recorre_fecha) <= strtotime($slide->fecha_termino); $recorre_fecha = date("Y-m-d", strtotime($recorre_fecha . "+ 1 days"))) {
                $date_parts = split('-', $recorre_fecha);

                $date_parts[2] = ($date_parts[2] < 10) ? str_replace("0", "", $date_parts[2]) : $date_parts[2];
                $slide_ts = strtotime($recorre_fecha);

                

                if ($recorre_fecha >= $today) {
                    foreach ($servicios_usados as $servicio_usado) {

                        

                        // Borra 
                        $directorio = BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . $servicio_usado;

                        if (substr_count($servicio_usado, '/')  > 1) {
                            $servicio_usado_array = explode('/', $servicio_usado);
                            $directorio = BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . $servicio_usado_array[1] . '/' . $servicio_usado_array[2];
                        }

                        $files = glob($directorio . '/*');
                        foreach ($files as $file) {
                            foreach ($borrar_archivos as $n_arch) {
                                if (is_file(($file))) {
                                    if (strpos($file, $n_arch) !== false) {
                                        //echo $file . '<br>' . $n_arch . '<br>';
                                        unlink($file);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        foreach ($slide->casinos as $modulecasino) {
            if ($modulecasino == "")
                break;

            // se extrae el cliente y la sucursal a partir del nombre del modulo
            $clientesucursal = getClienteSucursal($modulecasino);

            $cliente = $clientesucursal[0];
            $sucursal = $clientesucursal[1];

            

            $servicios_usados = getCarpetasPorClienteSucursal($cliente, $sucursal);

            for ($recorre_fecha = $slide->fecha_inicio; strtotime($recorre_fecha) <= strtotime($slide->fecha_termino); $recorre_fecha = date("Y-m-d", strtotime($recorre_fecha . "+ 1 days"))) {
                $date_parts = split('-', $recorre_fecha);

                $date_parts[2] = ($date_parts[2] < 10) ? str_replace("0", "", $date_parts[2]) : $date_parts[2];
                $slide_ts = strtotime($recorre_fecha);

                if ($recorre_fecha >= $today) {
                    foreach ($servicios_usados as $servicio_usado) {

                        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente);
                        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal);
                        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0]);
                        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1]);
                        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2]);

                        // Crea la carpeta del servicio 
                        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . $servicio_usado);

                        // Copia la imagen en la carpeta
                        copy(BASE_TEMPLATE . '/' . $slide->imagen, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . $servicio_usado . '/'  . $Indice . '_' . $nombre_FullZona . '_' . $fecha_hora . '_'  . $fechaIni . '_' . $fechaTer . '.jpg');
                    }
                }
            }
            $cliente_sucursal = $cliente . '_' . $sucursal;
            shell_exec(PHP_RUTA . ' ' . CARPETA_SHELL_EXEC . 'copiarArchFTP_Casinos.php ' . $cliente_sucursal . ' > /dev/null 2>&1 &');
        }

        // copiar el registro casino a el campo idpublicaciones
        updateIdPublicaciones($row_id, $nombre_FullZona);
    }
}

function getClienteSucursal($modulename)
{
    $result = array();

    // subir 4 niveles
    $module_file = WWW_ROOT . 'files' . '/' . '../../modules/sodexo/' . $modulename . '.xml';
    
    $modulefileHandler = fopen($module_file, 'r');
    $contenido = fread($modulefileHandler, filesize($module_file));

    //echo $module_file.'<br>';

    $xml_parsed = new SimpleXMLElement($contenido);
    $result[] = $xml_parsed->cliente;
    $result[] = $xml_parsed->sucursal;

    fclose($modulefileHandler);

    return $result;
}

function getCarpetasPorClienteSucursal($cliente, $sucursal)
{
    $cliente_sucursal = $cliente . '_' . $sucursal;

    $qry = "SELECT `config_modulos`.`cliente_sucursal` AS `cliente_sucursal`, `config_carpetas`.`carpeta` AS `carpeta` FROM ( `config_modulos` JOIN `config_carpetas` ON (( `config_modulos`.`id` = `config_carpetas`.`id_config_modulos` ))) WHERE(`config_modulos`.`cliente_sucursal` = '$cliente_sucursal')";

    $tablename = 'publicacionmultiple_'. $GLOBALS['nombre_FullZona'];
    
    // bandera para saber si aparece el nombre del filtro ej: corportativo_1
    $aparece = false;

    $obj_conexion = $GLOBALS['obj_conexion'];

    if (!$resultado = $obj_conexion->query($qry)) {
        echo "...Lo sentimos, este sitio web está experimentando problemas.";

        die();
    }
    if ($resultado->num_rows >= 0) {
        $carpetas = array();
        while ($registro = $resultado->fetch_assoc()) {
            // filtra por el nombre del archivo php
            if (strpos($registro['carpeta'], $tablename) !== false)
                $aparece = true;
            //if (strpos($registro['carpeta'], $tablename) !== false) {
            $carpetas[] = $registro['carpeta'];
            //}
        }

        $carpetas_tmp = array();

        if ($aparece) {
            foreach ($carpetas as $carpeta) {
                if (strpos($carpeta, $tablename) !== false) {
                    $carpetas_tmp[] = $carpeta;
                }
            }

            $carpetas = $carpetas_tmp;
        }
    }

    return $carpetas;
}

function updateIdPublicaciones($id, $nombre_FullZona)
{
    $tablename = 'publicacionmultiple_'. $nombre_FullZona;
    $obj_conexion = $GLOBALS['obj_conexion'];

    $qry = "UPDATE $tablename SET ids_pub = casinos WHERE id = $id";

    if ($obj_conexion->query($qry) === true) {
        //echo "registro actualizado";

    } else {
        echo "Error: DB <br>" . $obj_conexion->error;
    }
}

$obj_conexion->close();
