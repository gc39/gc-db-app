<?php
$includeOK = include_once __DIR__.'/_include_base.php';
if ($includeOK != true) die();

//echo '<pre>' . var_export($jsonData , true) . '</pre>';//die();
// Preview
if (AMBIENTE == 'test') {

    // recorre el json y genere un objeto con la data
    foreach ($jsonData as &$slide) {
        $date_ini = strtotime($slide->fecha_inicio);
        $date_ter = strtotime($slide->fecha_termino);
        if ($date_ini <= $date_ter) {
            for ($date = $slide->fecha_inicio; strtotime($date) <= strtotime($slide->fecha_termino); $date = date("d-m-Y", strtotime($date . "+ 1 days"))) {
                if (strtotime($date) == $today_ts) {
                    $slides[$a] = $slide;
                    $a++;
                }
            }
        }
    }

    // Borrado del archivo en directorio preview
    $directorio = BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $cliente . '/' . $sucursal;
    $files = glob($directorio . '/*');
    foreach ($files as $file) {
        foreach ($borrar_archivos as $n_arch) {
            if (is_file(($file))) {
                if (strpos($file, $n_arch) !== false) {
                    unlink($file);
                }
            }
        }
    }

    if (!empty($slides) && isset($slides)) {
        foreach ($slides as $dates) {
            $dateSplitStart =  split('-', $dates->fecha_inicio);
            $dateSplitStart = $dateSplitStart[2] . '-' . $dateSplitStart[1] . '-' . $dateSplitStart[0];
            $dateSplitEnd =  split('-', $dates->fecha_termino);
            $dateSplitEnd = $dateSplitEnd[2] . '-' . $dateSplitEnd[1] . '-' . $dateSplitEnd[0];

            // Nuevo codigo para centrar cumpleaños
            imagecopy($imageHandler, $bgImageCumpleanos, 0, 0, 0, 0, 1080, 1920);
            $mes = $dates->Mes;
            $y = 1350;
            $puntoInicio = newCenterText($mes, $tipografia, $tamanios['cumpleanos'], $tamanoFondoCumpleanos[0]);
            imagefttext($imageHandler, $tamanios['cumpleanos'], 0, $puntoInicio, $y, $colors['cumpleanos'], $tipografia, $mes);

            mkdir(BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $cliente);
            mkdir(BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $cliente . '/' . $sucursal);

            imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $cliente . '/' . $sucursal . '/' . $indice_FullZona . '_' . $nombre_FullZona . '.jpg');
        }
    }
}

if (AMBIENTE == 'prod') {
    

    // recorre el json y genere un objeto con la data
    foreach ($jsonData as &$slide) {
        $date_ini = strtotime($slide->fecha_inicio);
        $date_ter = strtotime($slide->fecha_termino);
        if ($date_ini <= $date_ter) {
            if (($today_ts <= $date_ini || $today_ts >= $date_ini)  && $today_ts <= $date_ter) {
                $slides[$a] = $slide;
                $a++;
            }
        }
    }

    //Borra archivos de hoy y crea las carpetas de hoy
    foreach ($servicios_usados as $servicio_usado) {
        $date_parts = split('-', $today);
        $date_parts[2] = ($date_parts[2] < 10) ? str_replace("0", "", $date_parts[2]) : $date_parts[2];
        // Borrado del archivo en directorio preview
        $directorio = BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado);
        $files = glob($directorio . '/*');
        foreach ($files as $file) {
            foreach ($borrar_archivos as $n_arch) {
                if (is_file(($file))) {
                    if (strpos($file, $n_arch) !== false) {
                        unlink($file);
                    }
                }
            }
        }
        //Crea carpetas de hoy si no existen
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0]);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1]);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2]);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado));
    }

    //si no hay imagenes deja logo default
    $contador_archivos = 0;
    foreach ($servicios_usados as $servicio_usado) {
        $date_parts = split('-', $today);
        $date_parts[2] = ($date_parts[2] < 10) ? str_replace("0", "", $date_parts[2]) : $date_parts[2];
        // Borrado del archivo en directorio preview
        $directorio = BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado);
        $files = glob($directorio . '/*');
        foreach ($files as $file) {
            $contador_archivos++;
        }
        if ($contador_archivos == 0) {
            copy($defaultLogo, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado) . '/'  . 'sodexo_logo_1080x1920.jpg');
        }
    }

    if (!empty($slides) && isset($slides)) {
        foreach ($slides as $dates) {
            
            // Les da el formato a las fechas
            $dateSplitStart =  split('-', $dates->fecha_inicio);
            // Variable fecha para usar en el nombre del archivo
            $fechaIni = $dateSplitStart[0] . $dateSplitStart[1] . $dateSplitStart[2];

            $dateSplitEnd =  split('-', $dates->fecha_termino);
            // Variable fecha para usar en el nombre del archivo
            $fechaTer = $dateSplitEnd[0] . $dateSplitEnd[1] . $dateSplitEnd[2];
            
            // Nuevo codigo para centrar cumpleaños
            imagecopy($imageHandler, $bgImageCumpleanos, 0, 0, 0, 0, 1080, 1920);
            $mes = $dates->Mes;
            $y = 1350;
            $puntoInicio = newCenterText($mes, $tipografia, $tamanios['cumpleanos'], $tamanoFondoCumpleanos[0]);
            imagefttext($imageHandler, $tamanios['cumpleanos'], 0, $puntoInicio, $y, $colors['cumpleanos'], $tipografia, $mes);

            for ($recorre_fecha = $dates->fecha_inicio; strtotime($recorre_fecha) <= strtotime($dates->fecha_termino); $recorre_fecha = date("Y-m-d", strtotime($recorre_fecha . "+ 1 days"))) {
                $date_parts = split('-', $recorre_fecha);
                $date_parts[2] = ($date_parts[2] < 10) ? str_replace("0", "", $date_parts[2]) : $date_parts[2];
                $slide_ts = strtotime($recorre_fecha);

                if ($recorre_fecha >= $today) {
                    foreach ($servicios_usados as $servicio_usado) {
                        // Borra 
                        $directorio = BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado);
                        $files = glob($directorio . '/*');
                        foreach ($files as $file) {
                            foreach ($borrar_archivos as $n_arch) {
                                if (is_file(($file))) {
                                    if (strpos($file, $n_arch) !== false) {
                                        unlink($file);
                                    }
                                }
                            }
                        }

                        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente);
                        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal);
                        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0]);
                        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1]);
                        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2]);
                        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado));

                        imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado) . '/'  . $indice_FullZona . '_' . $nombre_FullZona . '_' . $fecha_hora . '_' . $fechaIni . '_' . $fechaTer . '.jpg');
                    }
                }
            }
        }
    }
    shell_exec(PHP_RUTA . ' ' . CARPETA_SHELL_EXEC . 'copiarArchFTP_Casinos.php ' . $cliente_sucursal . ' > /dev/null 2>&1 &');
}