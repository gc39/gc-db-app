<?php
//.app

$includeOK = include_once __DIR__.'/_include_base.php';
if ($includeOK != true) die();

$includeOK = include_once __DIR__.'/_coords.php';
if ($includeOK != true) die();

if($prevent_clean_branding != true) {
    //Conecta a base de datos
    $obj_conexion = new mysqli(LOCALSERVER, LOCALUSERDB, LOCALPASSDB, LOCALDB) or die("db_error");
    $obj_conexion->set_charset("utf8");
    if (!$resultado = $obj_conexion->query($sql)) die();
}

$jsonFileMenuDiario = file_get_contents(BASE_TEMPLATE . '/templates/json/' . AMBIENTE . '/' . $cliente . '_' . $sucursal . '_' . $menu_diario . '-data.json');
$jsonDataMenuDiario = json_decode($jsonFileMenuDiario);

$imageHandler                   = @imagecreatetruecolor(1080, 1920);
$slides = false;

Coordenadas($control);

//***********************************************************
// Se carga el fondo de la imagen de Branding

$RutaFondoBranding = '';
$branding = 'branding';
switch ($current_branding){
    case 'Naturalmente Arma tu ensalada':
        $RutaFondoBranding = RUTAFONDOS . "/corporativo/bg_naturalmente_armatuensalada.jpg";
        break;
    case 'Naturalmente Bowl':
        $RutaFondoBranding = RUTAFONDOS . "/corporativo/bg_naturalmente_bowl.jpg";
        break;
    case 'The Grill':
        $RutaFondoBranding = RUTAFONDOS . "/corporativo/bg_thegrill.jpg";
        break;
    case 'Action Station':
        $RutaFondoBranding = RUTAFONDOS . "/corporativo/bg_actionstation.jpg";
        break;
    case 'Italianissimo':
        $RutaFondoBranding = RUTAFONDOS . "/corporativo/bg_italianissimo.jpg";
        break;
    case 'Italianissimo Arma tu Pasta':
        $RutaFondoBranding = RUTAFONDOS . "/corporativo/bg_italianissimo_armatupasta.jpg";
        break;
    case 'Sopa del Dia':
        $RutaFondoBranding = RUTAFONDOS . "/corporativo/bg_sopadeldia.jpg";
        break;
    case 'Meson Criollo':
        $RutaFondoBranding = RUTAFONDOS . "/corporativo/bg_mesoncriollo.jpg";
        break;
    case 'Sandwich del Dia':
        $RutaFondoBranding = RUTAFONDOS . "/corporativo/bg_sandwichdeldia.jpg";
        break;
    case 'Simply to Go':
        $RutaFondoBranding = RUTAFONDOS . "/corporativo/bg_simplytogo.jpg";
        break;
}

$bgBranding = imagecreatefromjpeg($RutaFondoBranding);
$tamanoBranding = getimagesize($RutaFondoBranding);

//***********************************************************
// se carga de la base de datos de branding
$sql = 'SELECT * FROM config_branding ORDER BY TipoBranding';
if (!$resultado = $obj_conexion->query($sql)) die();

$branding_nombres = array();
while ($rows = $resultado->fetch_assoc()) {
    $tipo = $rows['TipoBranding'];
    $descripcion = $rows['Descripcion'];

    if ($tipo == $current_branding)
        $branding_nombres[] = $descripcion;
}

//******** Mensaje si producto no existe inicio *************


// recorre el json y genere un objeto con la data
foreach ($jsonDataMenuDiario  as $indice => &$slide) {
    $date_parts = explode('-', $slide->fecha);
    $slide_ts = strtotime($date_parts[1] . '/' . $date_parts[0] . '/' . $date_parts[2]);
    $masterObj = getMasterCode(trim($slide->codigo));
    $slide->nombre_espanol = $masterObj->nombre_espanol;
}

// Preview
if (AMBIENTE == 'test') {

    // URLINDEX para los servicios usados
    $urlPreview = '';
    for ($ind = 0; $ind < $cantServicios; $ind++) {
        if (URLINDEX == $ind) {
            $urlPreview = $servicios_usados[$ind];
        }
    }

    // recorre el json y genere un objeto con la data
    foreach ($jsonDataMenuDiario  as &$slide) {
        $date_parts = explode('-', $slide->fecha);
        $slide_ts = strtotime($date_parts[1] . '/' . $date_parts[0] . '/' . $date_parts[2]);
        if ($slide_ts == $today_ts) {
            $masterObj = getMasterCode(trim($slide->codigo));
            $slide->nombre_espanol = $masterObj->nombre_espanol;
            $slide->nombre_ingles = $masterObj->nombre_ingles;
            $slide->calorias = $masterObj->calorias;
            $slide->altosodio = strtolower($masterObj->altosodio);
            $slide->gluten = strtolower($masterObj->gluten);
            $slide->vegano = strtolower($masterObj->vegano);
            $slide->vegetariano = strtolower($masterObj->vegetariano);
            $slide->altoazucar = strtolower($masterObj->altoazucar);
            $slide->lactosa = $masterObj->lactosa;
            $slide->soya = $masterObj->soya;
            $slide->frutos_secos = $masterObj->frutos_secos;
            $slide->servicio = $slide->servicio;
            $slide->tipo = $slide->tipo;
            $slide->codigo_temp = $slide->codigo_temp;

            foreach($GLOBALS["tipos_platos"] as $tipo_plato){
                $slide->indice = $slide->tipo == $tipo_plato['@tipoplato'] ? $control[$slide->servicio][$tipo_plato['@tipoplato']]['indice'] : $slide->indice;
            }

            //$slides[$slide_ts][] = $slide;
            if (in_array($slide->tipo, $branding_nombres))
                $slides[$slide_ts][] = $slide;
        }
    }

    // Ordena slides segun indice asignado por el tipo de plato correspondiente $slide->indice
    $slides_keys = array_keys($slides);
    foreach ($slides_keys as $slides_key) {
        $item = $slides[$slides_key];
        uasort($item, "OrdenaArrayIndice"); //Ordena array del menu por el campo indice
        $slides[$slides_key] = $item;
    }

    // Borrado del archivo en directorio preview
    $directorio = BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $cliente . '/' . $sucursal;
    $files = glob($directorio . '/*');
    if (!$prevent_clean_branding) {
        foreach ($files as $file) {
            foreach ($borrar_archivos_branding as $n_arch) {
                if (is_file(($file))) {
                    if (strpos($file, $n_arch) !== false) {
                        unlink($file);
                    }
                }
            }
        }
    }

    // esta variable sirve para no pisar la image de brandings anteriores en modo test
    $contador = 0;
    $max_branding_index = 0;
    if ($prevent_clean_branding) {
        foreach ($files as $file) {
            foreach ($borrar_archivos_branding as $n_arch) {
                if (is_file(($file))) {
                    if (strpos($file, $n_arch) !== false) {
                        $tmp_filename = end(explode('/', $file));
                        $tmp_filename_index = substr($tmp_filename,2,1);
                        if(intval($tmp_filename_index)>$max_branding_index)
                            $max_branding_index = intval($tmp_filename_index);
                    }
                }
            }
        }
    }

    foreach ($servicios_usados as $servicio_usado) {
        for ($cont = 1; $cont <= $paginas[$servicio_usado]; $cont++) {
            foreach ($slides as $dates) {
                $platosY = $inicio;
                $tipoPlato = '';

                Coordenadas($control);

                // Imagen de Fondo
                $bgImage = imagecreatefromjpeg($RutaFondoBranding);

                imagecopy($imageHandler, $bgImage, 0, 0, 0, 0, 1080, 1920);

                foreach ($dates as $slide) {

                    // Si el plato temporal existe entonces asignara los datos por su codigo temporal para que se muestre en la imagen
                    if ($slide->codigo_temp != NULL) {
                        //Incluye datos de Config y valida
                        $includeOK = include_once __DIR__ . '/../../../../Config/config.php';
                        if ($includeOK != true) die();

                        $sql = "SELECT
							maestro_temporal.nombre_espanol, 
							maestro_temporal.nombre_ingles, 
							maestro_temporal.calorias, 
							maestro_temporal.lactosa, 
							maestro_temporal.soya, 
							maestro_temporal.frutos_secos, 
							maestro_temporal.gluten, 
							maestro_temporal.vegano, 
							maestro_temporal.vegetariano, 
							maestro_temporal.altoazucar, 
							maestro_temporal.altosodio, 
							maestro_temporal.codigo_temporal
						FROM
							maestro_temporal
						WHERE
							maestro_temporal.codigo_temporal = $slide->codigo_temp";

                        if (!$resultado = $obj_conexion->query($sql)) die();

                        if ($resultado->num_rows != 0) {
                            while ($rows = $resultado->fetch_assoc()) {
                                $slide->nombre_espanol = $rows['nombre_espanol'];
                                $slide->nombre_ingles = $rows['nombre_ingles'];
                                $slide->calorias_temp = $rows['calorias'];
                                $slide->altosodio = strtolower($rows['altosodio']);
                                $slide->gluten = strtolower($rows['gluten']);
                                $slide->vegano = strtolower($rows['vegano']);
                                $slide->vegetariano = strtolower($rows['vegetariano']);
                                $slide->altoazucar = strtolower($rows['altoazucar']);
                                $slide->lactosa = $rows['lactosa'];
                                $slide->soya = $rows['soya'];
                                $slide->frutos_secos = $rows['frutos_secos'];
                            }
                        }
                    }

                    //Si el url preview corresponde con el servicio
                    if ($servicio_usado == $urlPreview && $slide->servicio == $urlPreview) {

                        // Toma la fecha del slide
                        $date_parts = explode('-', $slide->fecha);
                        $fecha_comp = $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0];

                        // Si la fecha de hoy corresponde a la fecha de publicacion del plato
                        if ($fecha_hoy == $fecha_comp) {

                            // Si el slide existe y el current es menor al numero de veces (cantidad)
                            if ($servicio_usado == $urlPreview && isset($control[$servicio_usado][$slide->tipo]) && ($control[$servicio_usado][$slide->tipo]['current'] < $control[$servicio_usado][$slide->tipo]['cantidad'])) {

                                // Si la vista del tipo de plato y servicio corresponde a la pagina
                                if ($control[$servicio_usado][$slide->tipo]['pagina'] == $cont) {

                                    // **********************************************************************************
                                    // Reglas del negocio
                                    // **********************************************************************************

                                    // Posicion del Tipo de Plato
                                    if ($tipoPlato != $slide->tipo) {
                                        $platosY += $espacio_platos;
                                    }

                                    // Si $ingles es igual a 'Si' el tipo plato tambien aparecera en ingles sino solo en español

                                    if ($ingles == 'true' && $tipoPlato != $slide->tipo) {
                                        imagefttext($imageHandler, $tamanios['tipo_plato'], 0, $inicio_platos, $platosY, $colors['tipo_plato'], $tipografia, $slide->tipo . ' / ' . $control[$servicio_usado][$slide->tipo]['ingles']);
                                        $tipoPlato = $slide->tipo;
                                        $platosY += 68;
                                    } else if ($ingles != 'true' && $tipoPlato != $slide->tipo) {
                                        imagefttext($imageHandler, $tamanios['tipo_plato'], 0, $inicio_platos, $platosY, $colors['tipo_plato'], $tipografia, $slide->tipo);
                                        $tipoPlato = $slide->tipo;
                                        $platosY += 68;
                                    }

                                    // Posición del plato

                                    // Si $ingles es igual a 'Si' el plato tambien aparecera en ingles sino solo en español
                                    if ($ingles == 'true') {
                                        imagefttext($imageHandler, $tamanios['titulo_platos'], 0, $inicio_platos, $platosY, $colors['titulo_platos_espanol'], $tipografia, substr($slide->nombre_espanol,0,$limite_nombre_plato));
                                        $platosY += 105;
                                        imagefttext($imageHandler, $tamanios['titulo_platos'], 0, $inicio_platos, $platosY, $colors['titulo_platos_ingles'], $tipografia, substr($slide->nombre_ingles,0,$limite_nombre_plato));
                                    } else {
                                        imagefttext($imageHandler, $tamanios['titulo_platos'], 0, $inicio_platos, $platosY, $colors['titulo_platos'], $tipografia, substr($slide->nombre_espanol,0,$limite_nombre_plato));
                                        $platosY += 50;
                                    }

                                    // Semaforo Saludable de los platos

                                    // La variable difSimbolos corrige la distancia del semaforo saludable cuando $ingles es 'Si' o no
                                    $icoXposition = 870;
                                    if ($ingles == 'true') {
                                        $difSimbolos = 145;
                                    } else {
                                        $difSimbolos = 85;
                                    }
                                    if ($slide->gluten == 'si') {
                                        imagecopy($imageHandler, $icoGluten, $icoXposition, $platosY - $difSimbolos, 0, 0, 40, 40);
                                        $icoXposition += 45;
                                    }
                                    if ($slide->vegano == 'si') {
                                        imagecopy($imageHandler, $icoVegano, $icoXposition, $platosY - $difSimbolos, 0, 0, 40, 40);
                                        $icoXposition += 45;
                                    }
                                    if ($slide->vegetariano == 'si') {
                                        imagecopy($imageHandler, $icoVegetariano, $icoXposition, $platosY - $difSimbolos, 0, 0, 40, 40);
                                        $icoXposition += 45;
                                    }
                                    if ($slide->altoazucar == 'si') {
                                        imagecopy($imageHandler, $icoAzucar, $icoXposition, $platosY - $difSimbolos, 0, 0, 40, 40);
                                        $icoXposition += 45;
                                    }
                                    if ($slide->altosodio == 'si' && $slide->altoazucar == 'no') {
                                        imagecopy($imageHandler, $icoSal, $icoXposition, $platosY - $difSimbolos, 0, 0, 40, 40);
                                        $icoXposition += 45;
                                    }

                                    // Lista de alergenos presentes en cada plato

                                    $alergenos 			= array();
                                    $alergenos_ingles 	= array();

                                    // Si $ingles es igual a 'Si' los alergenos tambien se mostraran en ingles sino solo en español
                                    if ($ingles == 'true') {
                                        if ($slide->lactosa == 'Si') {
                                            $alergenos[] = 'Lactosa';
                                            $alergenos_ingles[] = 'Lactose';
                                        }
                                        if ($slide->soya == 'Si') {
                                            $alergenos[] = 'Soya';
                                            $alergenos_ingles[] = 'Soy';
                                        }
                                        if ($slide->frutos_secos == 'Si') {
                                            $alergenos[] = 'Frutos Secos';
                                            $alergenos_ingles[] = 'Nuts';
                                        }
                                    } else {
                                        if ($slide->lactosa == 'Si') {
                                            $alergenos[] = 'Lactosa';
                                        }
                                        if ($slide->soya == 'Si') {
                                            $alergenos[] = 'Soya';
                                        }
                                        if ($slide->frutos_secos == 'Si') {
                                            $alergenos[] = 'Frutos Secos';
                                        }
                                    }

                                    $txtAlergenos 			= implode(" - ", $alergenos);
                                    $txtAlergenos 			= $txtAlergenos != NULL ? $txtAlergenos . '*' : $txtAlergenos;
                                    $txtAlergenos_ingles 	= implode(" - ", $alergenos_ingles);
                                    $txtAlergenos_ingles 	= $txtAlergenos_ingles != NULL ? $txtAlergenos_ingles . '*' : $txtAlergenos_ingles;

                                    // Posicion de las calorias , generalmente al lado del plato

                                    // La variable difCalorias corrige la alineacion de las calorias cuando $ingles es 'Si' o no
                                    if ($ingles == 'true') {
                                        $difCalorias = 110;
                                    } else {
                                        $difCalorias = 50;
                                    }

                                    // Si en el formulario de plato temporal no se colocan calorias entonces no aparecera kcal en la imagen
                                    if ($slide->calorias != '') {
                                        $kcal = ' KCAL';
                                    }else {
                                        if ($slide->calorias_temp == ' ') {
                                            $kcal = '';
                                        }
                                        else {
                                            $kcal = ' KCAL';
                                        }
                                    }

                                    // La variable caloriasX corrige la posicion X dependiendo si calorias tiene 3 o 4 cifras
                                    if ($ingles == 'true') {
                                        if ($slide->calorias_temp != '' && $slide->calorias_temp != ' ') {
                                            if ($slide->calorias_temp < 1000) {
                                                $caloriasX = 730;
                                            } else {
                                                $caloriasX = 710;
                                            }
                                            imagefttext($imageHandler, $tananios['calorias'], 0, $caloriasX, $platosY - $difCalorias, $colors['calorias_ingles'], $tipografia, $slide->calorias_temp);
                                            imagefttext($imageHandler, $tananios['kcal_menu'], 0, 800, $platosY - $difCalorias, $colors['calorias_ingles'], $tipografia, $kcal);
                                        } else {
                                            if ($slide->calorias < 1000) {
                                                $caloriasX = 730;
                                            } else {
                                                $caloriasX = 710;
                                            }
                                            imagefttext($imageHandler, $tananios['calorias'], 0, $caloriasX, $platosY - $difCalorias, $colors['calorias_ingles'], $tipografia, $slide->calorias);
                                            imagefttext($imageHandler, $tananios['kcal_menu'], 0, 800, $platosY - $difCalorias, $colors['calorias_ingles'], $tipografia, $kcal);
                                        }
                                    } else {
                                        if ($slide->calorias_temp != '' && $slide->calorias_temp != ' ') {
                                            if ($slide->calorias_temp < 1000) {
                                                $caloriasX = 730;
                                            } else {
                                                $caloriasX = 710;
                                            }
                                            imagefttext($imageHandler, $tananios['calorias'], 0, $caloriasX, $platosY - $difCalorias, $colors['calorias_espanol'], $tipografia, $slide->calorias_temp);
                                            imagefttext($imageHandler, $tananios['kcal_menu'], 0, 800, $platosY - $difCalorias, $colors['calorias_espanol'], $tipografia, $kcal);
                                        } else {
                                            if ($slide->calorias < 1000) {
                                                $caloriasX = 730;
                                            } else {
                                                $caloriasX = 710;
                                            }
                                            imagefttext($imageHandler, $tananios['calorias'], 0, $caloriasX, $platosY - $difCalorias, $colors['calorias_espanol'], $tipografia, $slide->calorias);
                                            imagefttext($imageHandler, $tananios['kcal_menu'], 0, 800, $platosY - $difCalorias, $colors['calorias_espanol'], $tipografia, $kcal);
                                        }
                                    }

                                    // Posicion de los alergenos, generalmente debajo del plato

                                    if ($ingles == 'true') {
                                        imagefttext($imageHandler, $tamanios['alergenos'], 0, $inicio_platos, $platosY - 65, $colors['alergenos_espanol'], $tipografia, $txtAlergenos);
                                        $platosY += 45;
                                        imagefttext($imageHandler, $tamanios['alergenos'], 0, $inicio_platos, $platosY, $colors['alergenos_ingles'], $tipografia, $txtAlergenos_ingles);
                                        $platosY += 70;
                                    } else {
                                        imagefttext($imageHandler, $tamanios['alergenos'], 0, $inicio_platos, $platosY, $colors['alergenos'], $tipografia, $txtAlergenos);
                                        $platosY += 70;
                                    }

                                    $control[$servicio_usado][$slide->tipo]['current']++;
                                }
                            }
                            // Formatos de las fechas
                            $date_parts = explode('-', $slide->fecha);

                            // Fecha de Vigencia del Plato
                            $fecha_vigencia = $date_parts[2] . $date_parts[1] . $date_parts[0];

                            // Fecha mostrada en el menu
                            $stringDate =  $date_parts[0] . ' de ' . $arrayMonth[$date_parts[1] - 1];

                            if ($date_parts[0] < 10) {
                                $date_parts[0] = str_replace("0", "", $date_parts[0]);
                            }

                            // Centrado de la fecha
                            $puntoInicio = newCenterText($stringDate, $tipografiaFecha, $tamanios['fecha'], 1080);

                            // Centrado del Texto de Alergenos Presentes
                            $inicioAlergenos = newCenterText($aviso_Alergenos, $tipografia, $tamanios['aviso_alergenos'], 1080);

                            // Coordenadas de Fecha y Leyenda de Simbologias
                            imagettftext($imageHandler, $tamanios['fecha'], 0, $puntoInicio, $inicio_fecha_1 , $colors['fecha'], $tipografiaFecha, $stringDate);
                            imagettftext($imageHandler, $tamanios['aviso_alergenos'], 0, $inicioAlergenos, $aviso_alergenos_y , $colors['aviso_alergenos'], $tipografia, $aviso_Alergenos);
                            $simbolosX = $inicio_simbologia_x_1;
                            $simbolosY = $inicio_simbologia_y_1;

                            // Explicacion de Simbologia del Semaforo Saludable

                            imagecopy($imageHandler, $icoSal, $simbolosX, $simbolosY, 0, 0, 40, 40);
                            imagefttext($imageHandler, $tamanios['simbologia'], 0, $simbolosX + 50, $simbolosY + 30, $colors['simbologia'], $tipografia, 'Alto en Sodio');
                            imagecopy($imageHandler, $icoGluten, $simbolosX + 200, $simbolosY, 0, 0, 40, 40);
                            imagefttext($imageHandler, $tamanios['simbologia'], 0, $simbolosX + 245, $simbolosY + 30, $colors['simbologia'], $tipografia, 'Contiene Gluten');
                            imagecopy($imageHandler, $icoVegano, $simbolosX + 428, $simbolosY, 0, 0, 40, 40);
                            imagefttext($imageHandler, $tamanios['simbologia'], 0, $simbolosX + 475, $simbolosY + 30, $colors['simbologia'], $tipografia, 'Vegano');
                            imagecopy($imageHandler, $icoVegetariano, $simbolosX + 575, $simbolosY, 0, 0, 40, 40);
                            imagefttext($imageHandler, $tamanios['simbologia'], 0, $simbolosX + 625, $simbolosY + 30, $colors['simbologia'], $tipografia, 'Vegetariano');
                            imagecopy($imageHandler, $icoAzucar, $simbolosX + 775, $simbolosY, 0, 0, 40, 40);
                            imagefttext($imageHandler, $tamanios['simbologia'], 0, $simbolosX + 825, $simbolosY + 30, $colors['simbologia'], $tipografia, 'Alto en Azucar');

                            // **********************************************************************************
                            // Fin de las Reglas de Negocio
                            // **********************************************************************************

                            mkdir(BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $cliente);
                            mkdir(BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $cliente . '/' . $sucursal);

                            // Guarda la imagen en la carpeta respectiva
                            if ($slide->servicio == $servicio_usado && $platosY > $inicio) {
                                $counter = $cont + $max_branding_index;
                                imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $cliente . '/' . $sucursal . '/' . '12' . $counter . '_' . ServicioUsadoCarpeta($servicio_usado) . '_' . $branding . $counter . '.jpg', 75);
                            }
                        }
                    }
                }
            }
        }
    }
}

// Publicacion, todo lo de publicacion va aca
if (AMBIENTE == 'prod') {

    // recorre el json y genere un objeto con la data
    foreach ($jsonDataMenuDiario  as $indice => &$slide) {
        $date_parts = explode('-', $slide->fecha);
        $slide_ts = strtotime($date_parts[1] . '/' . $date_parts[0] . '/' . $date_parts[2]);
        if ($slide_ts >= $today_ts) {
            $masterObj = getMasterCode(trim($slide->codigo));
            $slide->nombre_espanol = $masterObj->nombre_espanol;
            $slide->nombre_ingles = $masterObj->nombre_ingles;
            $slide->calorias = $masterObj->calorias;
            $slide->altosodio = strtolower($masterObj->altosodio);
            $slide->gluten = strtolower($masterObj->gluten);
            $slide->vegano = strtolower($masterObj->vegano);
            $slide->vegetariano = strtolower($masterObj->vegetariano);
            $slide->altoazucar = strtolower($masterObj->altoazucar);
            $slide->lactosa = $masterObj->lactosa;
            $slide->soya = $masterObj->soya;
            $slide->frutos_secos = $masterObj->frutos_secos;
            $slide->servicio = $slide->servicio;
            $slide->tipo = $slide->tipo;
            $slide->codigo_temp = $slide->codigo_temp;
            foreach($GLOBALS["tipos_platos"] as $tipo_plato){
                // $tipo_plato['@servicio'].' -> '.$tipo_plato['@tipoplato'].' -> '.$tipo_plato['@cantidad'].' -> '.$tipo_plato['@pagina'].' -> '.$tipo_plato['@indice'].'<br>';
                $slide->indice = $slide->tipo == $tipo_plato['@tipoplato'] ? $control[$slide->servicio][$tipo_plato['@tipoplato']]['indice'] : $slide->indice;
            }

            // ojo meter este fernandezajp
            if (in_array($slide->tipo, $branding_nombres))
                $slides[$slide_ts][] = $slide;
        }
    }

    // Ordena slides segun indice asignado por el tipo de plato correspondiente $slide->indice
    $slides_keys = array_keys($slides);
    foreach ($slides_keys as $slides_key) {
        $item = $slides[$slides_key];
        uasort($item, "OrdenaArrayIndice"); //Ordena array del menu por el campo indice
        $slides[$slides_key] = $item;
    }

    //Borra archivos de hoy y crea las carpetas de hoy
    foreach ($servicios_usados as $servicio_usado) {
        $date_parts = explode('-', $today);
        $date_parts[2] = ($date_parts[2] < 10) ? str_replace("0", "", $date_parts[2]) : $date_parts[2];
        // Borrado de todas las imagenes de los servicios
        $directorio = BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado);
        $files = glob($directorio . '/*');
        if (!$prevent_clean_branding) {
            foreach ($files as $file) {
                foreach ($borrar_archivos_branding as $n_arch) {
                    if (is_file(($file))) {
                        if (strpos($file, $n_arch) !== false) {
                            unlink($file);
                        }
                    }
                }
            }
        }
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0]);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1]);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2]);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado));
    }

    //si no hay imagenes deja logo default
    $contador_archivos = 0;
    foreach ($servicios_usados as $servicio_usado) {
        $date_parts = explode('-', $today);
        $date_parts[2] = ($date_parts[2] < 10) ? str_replace("0", "", $date_parts[2]) : $date_parts[2];
        // Borrado del archivo en directorio preview
        $directorio = BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado);
        $files = glob($directorio . '/*');
        foreach ($files as $file) {
            $contador_archivos++;
        }
        if ($contador_archivos == 0) {
            copy($defaultLogo, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado) . '/'  . 'sodexo_logo_1080x1920.jpg');
        }
    }

    foreach ($servicios_usados as $servicio_usado) {
        for ($cont = 1; $cont <= $paginas[$servicio_usado]; $cont++) {
            foreach ($slides as $dates) {

                $platosY = $inicio;
                $tipoPlato = '';

                Coordenadas($control);

                // Imagen de Fondo
                $bgImage = imagecreatefromjpeg($RutaFondoBranding);

                imagecopy($imageHandler, $bgImage, 0, 0, 0, 0, 1080, 1920);

                foreach ($dates as $slide) {

                    // Si el plato temporal existe entonces asignara los datos por su codigo temporal para que se muestre en la imagen
                    if ($slide->codigo_temp != NULL) {
                        //Incluye datos de Config y valida
                        $includeOK = include_once __DIR__ . '/../../../../Config/config.php';
                        if ($includeOK != true) die();

                        $sql = "SELECT
							maestro_temporal.nombre_espanol, 
							maestro_temporal.nombre_ingles, 
							maestro_temporal.calorias, 
							maestro_temporal.lactosa, 
							maestro_temporal.soya, 
							maestro_temporal.frutos_secos, 
							maestro_temporal.gluten, 
							maestro_temporal.vegano, 
							maestro_temporal.vegetariano, 
							maestro_temporal.altoazucar, 
							maestro_temporal.altosodio, 
							maestro_temporal.codigo_temporal
						FROM
							maestro_temporal
						WHERE
							maestro_temporal.codigo_temporal = $slide->codigo_temp";

                        //Conecta a base de datos
                        if (!$resultado = $obj_conexion->query($sql)) die();

                        if ($resultado->num_rows != 0) {
                            while ($rows = $resultado->fetch_assoc()) {
                                $slide->nombre_espanol = $rows['nombre_espanol'];
                                $slide->nombre_ingles = $rows['nombre_ingles'];
                                $slide->calorias_temp = $rows['calorias'];
                                $slide->altosodio = strtolower($rows['altosodio']);
                                $slide->gluten = strtolower($rows['gluten']);
                                $slide->vegano = strtolower($rows['vegano']);
                                $slide->vegetariano = strtolower($rows['vegetariano']);
                                $slide->altoazucar = strtolower($rows['altoazucar']);
                                $slide->lactosa = $rows['lactosa'];
                                $slide->soya = $rows['soya'];
                                $slide->frutos_secos = $rows['frutos_secos'];
                            }
                        }
                    }

                    // Toma la fecha del slide
                    $date_parts = explode('-', $slide->fecha);

                    if ($date_parts[0] < 10) {
                        $date_parts[0] = str_replace("0", "", $date_parts[0]);
                    }

                    // Crea las carpetas
                    mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente);
                    mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal);
                    mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2]);
                    mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1]);
                    mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0]);
                    mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0] . '/' . ServicioUsadoCarpeta($servicio_usado));

                    // Ordena la fecha de exhibicion
                    $fecha_exhibicion = $date_parts[2] . '-' . $date_parts[1] . '-' . $date_parts[0];

                    //array con fechas de exhibicion
                    $fechas_ex[] = $fecha_exhibicion;
                    $fechas_ex = array_unique($fechas_ex);

                    // Si corresponde el url preview con el servicio
                    if ($slide->servicio == $servicio_usado) {

                        // Si el slide existe y el current es menor al numero de veces (cantidad)
                        if (isset($control[$servicio_usado][$slide->tipo]) && ($control[$servicio_usado][$slide->tipo]['current'] < $control[$servicio_usado][$slide->tipo]['cantidad'])) {

                            // Si la vista del tipo de plato y servicio corresponde a la pagina
                            if ($control[$servicio_usado][$slide->tipo]['pagina'] == $cont) {

                                // **********************************************************************************
                                // Reglas del negocio
                                // **********************************************************************************

                                // Posicion del Tipo de Plato
                                if ($tipoPlato != $slide->tipo) {
                                    $platosY += $espacio_platos;
                                }

                                // Si $ingles es igual a 'Si' el tipo plato tambien aparecera en ingles sino solo en español

                                if ($ingles == 'true' && $tipoPlato != $slide->tipo) {
                                    imagefttext($imageHandler, $tamanios['tipo_plato'], 0, $inicio_platos, $platosY, $colors['tipo_plato'], $tipografia, $slide->tipo . ' / ' . $control[$servicio_usado][$slide->tipo]['ingles']);
                                    $tipoPlato = $slide->tipo;
                                    $platosY += 68;
                                } else if ($ingles != 'true' && $tipoPlato != $slide->tipo) {
                                    imagefttext($imageHandler, $tamanios['tipo_plato'], 0, $inicio_platos, $platosY, $colors['tipo_plato'], $tipografia, $slide->tipo);
                                    $tipoPlato = $slide->tipo;
                                    $platosY += 68;
                                }

                                // Posición del plato

                                // Si $ingles es igual a 'Si' el plato tambien aparecera en ingles sino solo en español
                                if ($ingles == 'true') {
                                    imagefttext($imageHandler, $tamanios['titulo_platos'], 0, $inicio_platos, $platosY, $colors['titulo_platos_espanol'], $tipografia, substr($slide->nombre_espanol,0,$limite_nombre_plato));
                                    $platosY += 105;
                                    imagefttext($imageHandler, $tamanios['titulo_platos'], 0, $inicio_platos, $platosY, $colors['titulo_platos_ingles'], $tipografia, substr($slide->nombre_ingles,0,$limite_nombre_plato));
                                } else {
                                    imagefttext($imageHandler, $tamanios['titulo_platos'], 0, $inicio_platos, $platosY, $colors['titulo_platos'], $tipografia, substr($slide->nombre_espanol,0,$limite_nombre_plato));
                                    $platosY += 50;
                                }

                                // Semaforo Saludable de los platos

                                // La variable difSimbolos corrige la distancia del semaforo saludable cuando $ingles es 'Si' o no
                                $icoXposition = 870;
                                if ($ingles == 'true') {
                                    $difSimbolos = 145;
                                } else {
                                    $difSimbolos = 85;
                                }
                                if ($slide->gluten == 'si') {
                                    imagecopy($imageHandler, $icoGluten, $icoXposition, $platosY - $difSimbolos, 0, 0, 40, 40);
                                    $icoXposition += 45;
                                }
                                if ($slide->vegano == 'si') {
                                    imagecopy($imageHandler, $icoVegano, $icoXposition, $platosY - $difSimbolos, 0, 0, 40, 40);
                                    $icoXposition += 45;
                                }
                                if ($slide->vegetariano == 'si') {
                                    imagecopy($imageHandler, $icoVegetariano, $icoXposition, $platosY - $difSimbolos, 0, 0, 40, 40);
                                    $icoXposition += 45;
                                }
                                if ($slide->altoazucar == 'si') {
                                    imagecopy($imageHandler, $icoAzucar, $icoXposition, $platosY - $difSimbolos, 0, 0, 40, 40);
                                    $icoXposition += 45;
                                }
                                if ($slide->altosodio == 'si' && $slide->altoazucar == 'no') {
                                    imagecopy($imageHandler, $icoSal, $icoXposition, $platosY - $difSimbolos, 0, 0, 40, 40);
                                    $icoXposition += 45;
                                }

                                // Lista de alergenos presentes en cada plato

                                $alergenos 			= array();
                                $alergenos_ingles 	= array();

                                // Si $ingles es igual a 'Si' los alergenos tambien se mostraran en ingles sino solo en español
                                if ($ingles == 'true') {
                                    if ($slide->lactosa == 'Si') {
                                        $alergenos[] = 'Lactosa';
                                        $alergenos_ingles[] = 'Lactose';
                                    }
                                    if ($slide->soya == 'Si') {
                                        $alergenos[] = 'Soya';
                                        $alergenos_ingles[] = 'Soy';
                                    }
                                    if ($slide->frutos_secos == 'Si') {
                                        $alergenos[] = 'Frutos Secos';
                                        $alergenos_ingles[] = 'Nuts';
                                    }
                                } else {
                                    if ($slide->lactosa == 'Si') {
                                        $alergenos[] = 'Lactosa';
                                    }
                                    if ($slide->soya == 'Si') {
                                        $alergenos[] = 'Soya';
                                    }
                                    if ($slide->frutos_secos == 'Si') {
                                        $alergenos[] = 'Frutos Secos';
                                    }
                                }

                                $txtAlergenos 			= implode(" - ", $alergenos);
                                $txtAlergenos 			= $txtAlergenos != NULL ? $txtAlergenos . '*' : $txtAlergenos;
                                $txtAlergenos_ingles 	= implode(" - ", $alergenos_ingles);
                                $txtAlergenos_ingles 	= $txtAlergenos_ingles != NULL ? $txtAlergenos_ingles . '*' : $txtAlergenos_ingles;

                                // Posicion de las calorias , generalmente al lado del plato

                                // La variable difCalorias corrige la alineacion de las calorias cuando $ingles es 'Si' o no
                                if ($ingles == 'true') {
                                    $difCalorias = 110;
                                } else {
                                    $difCalorias = 50;
                                }

                                // Si en el formulario de plato temporal no se colocan calorias entonces no aparecera kcal en la imagen
                                if ($slide->calorias != '') {
                                    $kcal = ' KCAL';
                                }else {
                                    if ($slide->calorias_temp == ' ') {
                                        $kcal = '';
                                    }
                                    else {
                                        $kcal = ' KCAL';
                                    }
                                }

                                // La variable caloriasX corrige la posicion X dependiendo si calorias tiene 3 o 4 cifras
                                if ($ingles == 'true') {
                                    if ($slide->calorias_temp != '' && $slide->calorias_temp != ' ') {
                                        if ($slide->calorias_temp < 1000) {
                                            $caloriasX = 730;
                                        } else {
                                            $caloriasX = 710;
                                        }
                                        imagefttext($imageHandler, $tananios['calorias'], 0, $caloriasX, $platosY - $difCalorias, $colors['calorias_ingles'], $tipografia, $slide->calorias_temp);
                                        imagefttext($imageHandler, $tananios['kcal_menu'], 0, 800, $platosY - $difCalorias, $colors['calorias_ingles'], $tipografia, $kcal);
                                    } else {
                                        if ($slide->calorias < 1000) {
                                            $caloriasX = 730;
                                        } else {
                                            $caloriasX = 710;
                                        }
                                        imagefttext($imageHandler, $tananios['calorias'], 0, $caloriasX, $platosY - $difCalorias, $colors['calorias_ingles'], $tipografia, $slide->calorias);
                                        imagefttext($imageHandler, $tananios['kcal_menu'], 0, 800, $platosY - $difCalorias, $colors['calorias_ingles'], $tipografia, $kcal);
                                    }
                                } else {
                                    if ($slide->calorias_temp != '' && $slide->calorias_temp != ' ') {
                                        if ($slide->calorias_temp < 1000) {
                                            $caloriasX = 730;
                                        } else {
                                            $caloriasX = 710;
                                        }
                                        imagefttext($imageHandler, $tananios['calorias'], 0, $caloriasX, $platosY - $difCalorias, $colors['calorias_espanol'], $tipografia, $slide->calorias_temp);
                                        imagefttext($imageHandler, $tananios['kcal_menu'], 0, 800, $platosY - $difCalorias, $colors['calorias_espanol'], $tipografia, $kcal);
                                    } else {
                                        if ($slide->calorias < 1000) {
                                            $caloriasX = 730;
                                        } else {
                                            $caloriasX = 710;
                                        }
                                        imagefttext($imageHandler, $tananios['calorias'], 0, $caloriasX, $platosY - $difCalorias, $colors['calorias_espanol'], $tipografia, $slide->calorias);
                                        imagefttext($imageHandler, $tananios['kcal_menu'], 0, 800, $platosY - $difCalorias, $colors['calorias_espanol'], $tipografia, $kcal);
                                    }
                                }

                                // Posicion de los alergenos, generalmente debajo del plato

                                if ($ingles == 'true') {
                                    imagefttext($imageHandler, $tamanios['alergenos'], 0, $inicio_platos, $platosY - 65, $colors['alergenos_espanol'], $tipografia, $txtAlergenos);
                                    $platosY += 45;
                                    imagefttext($imageHandler, $tamanios['alergenos'], 0, $inicio_platos, $platosY, $colors['alergenos_ingles'], $tipografia, $txtAlergenos_ingles);
                                    $platosY += 70;
                                } else {
                                    imagefttext($imageHandler, $tamanios['alergenos'], 0, $inicio_platos, $platosY, $colors['alergenos'], $tipografia, $txtAlergenos);
                                    $platosY += 70;
                                }

                                $control[$servicio_usado][$slide->tipo]['current']++;
                            }
                        }

                        // Formatos de las fechas
                        $date_parts = explode('-', $slide->fecha);

                        // Fecha de Vigencia del Plato
                        $fecha_vigencia = $date_parts[2] . $date_parts[1] . $date_parts[0];

                        // Fecha mostrada en el menu
                        $stringDate =  $date_parts[0] . ' de ' . $arrayMonth[$date_parts[1] - 1];

                        if ($date_parts[0] < 10) {
                            $date_parts[0] = str_replace("0", "", $date_parts[0]);
                        }

                        // Centrado de la fecha
                        $puntoInicio = newCenterText($stringDate, $tipografiaFecha, $tamanios['fecha'], 1080);

                        // Centrado del Texto de Alergenos Presentes
                        $inicioAlergenos = newCenterText($aviso_Alergenos, $tipografia, $tamanios['aviso_alergenos'], 1080);

                        // Coordenadas de Fecha y Leyenda de Simbologias
                        imagettftext($imageHandler, $tamanios['fecha'], 0, $puntoInicio, $inicio_fecha_1 , $colors['fecha'], $tipografiaFecha, $stringDate);
                        imagettftext($imageHandler, $tamanios['txt_alergenos'], 0, $inicioAlergenos, $aviso_alergenos_y , $colors['aviso_alergenos'], $tipografia, $aviso_Alergenos);
                        $simbolosX = $inicio_simbologia_x_1;
                        $simbolosY = $inicio_simbologia_y_1;

                        // Explicacion de Simbologia del Semaforo Saludable

                        imagecopy($imageHandler, $icoSal, $simbolosX, $simbolosY, 0, 0, 40, 40);
                        imagefttext($imageHandler, $tamanios['simbologia'], 0, $simbolosX + 50, $simbolosY + 30, $colors['simbologia'], $tipografia, 'Alto en Sodio');
                        imagecopy($imageHandler, $icoGluten, $simbolosX + 200, $simbolosY, 0, 0, 40, 40);
                        imagefttext($imageHandler, $tamanios['simbologia'], 0, $simbolosX + 245, $simbolosY + 30, $colors['simbologia'], $tipografia, 'Contiene Gluten');
                        imagecopy($imageHandler, $icoVegano, $simbolosX + 428, $simbolosY, 0, 0, 40, 40);
                        imagefttext($imageHandler, $tamanios['simbologia'], 0, $simbolosX + 475, $simbolosY + 30, $colors['simbologia'], $tipografia, 'Vegano');
                        imagecopy($imageHandler, $icoVegetariano, $simbolosX + 575, $simbolosY, 0, 0, 40, 40);
                        imagefttext($imageHandler, $tamanios['simbologia'], 0, $simbolosX + 625, $simbolosY + 30, $colors['simbologia'], $tipografia, 'Vegetariano');
                        imagecopy($imageHandler, $icoAzucar, $simbolosX + 775, $simbolosY, 0, 0, 40, 40);
                        imagefttext($imageHandler, $tamanios['simbologia'], 0, $simbolosX + 825, $simbolosY + 30, $colors['simbologia'], $tipografia, 'Alto en Azucar');

                        // **********************************************************************************
                        // Fin de las Reglas de Negocio
                        // **********************************************************************************

                        // Elimina logo sodexo cuando crea imagenes

                        if ($cont == 1) {
                            $directorio = BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0] . '/' . ServicioUsadoCarpeta($servicio_usado);
                            $files = glob($directorio . '/*');
                            if (!$prevent_clean_branding) {
                                foreach ($files as $file) {
                                    foreach ($borrar_archivos_branding as $n_arch) {
                                        if (is_file(($file))) {
                                            if (strpos($file, $n_arch) !== false) {
                                                unlink($file);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // Guarda las imagenes en la carpeta respectiva segun servicio
                        // la pregunta $platosY > $inicio permite saber si hay contenido en la pagina actual
                        if ($slide->servicio == $servicio_usado && $platosY > $inicio) {
                            imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0] .  '/' . ServicioUsadoCarpeta($servicio_usado)  . '/' . '12' . ($cont + $paginas[$slide->servicio] * $branding_counter) . '_' . $branding . $cont . '_' . $fecha_hora . '_' . $fecha_vigencia . '_' . $fecha_vigencia . '.jpg');
                            imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0] .  '/' . ServicioUsadoCarpeta($servicio_usado)  . '/' . '22' . ($cont + $paginas[$slide->servicio] * $branding_counter) . '_' . $branding . $cont . '_' . $fecha_hora . '_' . $fecha_vigencia . '_' . $fecha_vigencia . '.jpg');
                            imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0] .  '/' . ServicioUsadoCarpeta($servicio_usado)  . '/' . '32' . ($cont + $paginas[$slide->servicio] * $branding_counter) . '_' . $branding . $cont . '_' . $fecha_hora . '_' . $fecha_vigencia . '_' . $fecha_vigencia . '.jpg');
                        }
                    }
                }
            }
        }
    }

    foreach ($fechas_ex as $fecha_ex) {

        // gusrda el historial de publicaciones (para el reporte de casinos sin publicacion)
        //PublishHistoryBranding($cliente_sucursal, $fecha_ex);

        //guarda en reporte de modificacion el publicar
        //PublishReportesModificacion($today,$fecha_ex,$segmento,$cliente,$sucursal,$usuario);

    }

    //ejecuta archivo en segun plano para copiar en ftp
    //shell_exec(PHP_RUTA . ' ' . CARPETA_SHELL_EXEC . 'copiarArchFTP_Casinos.php ' . $cliente_sucursal . ' > /dev/null 2>&1 &');

}

// libera memoria de sistema
imagedestroy($bgImage);
imagedestroy($imageHandler);

$branding_counter++;