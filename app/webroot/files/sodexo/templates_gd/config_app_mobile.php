<?php


// Publicacion, todo lo de publicacion va aca
if (AMBIENTE == 'prod') {


    
    $jsonFile     = file_get_contents(BASE_TEMPLATE . '/templates/json/' . AMBIENTE . '/config_app_mobile-data.json');
    $jsonData     = json_decode($jsonFile);

    // recorre el json y genere un objeto con la data
    foreach ($jsonData as $key => &$slide) {
        $id=strtolower($slide->id);

        //Consulta si esta activado
        if($slide->estado == "true"){

            //incluye el config
            $includeOK = include_once __DIR__ . '/../../../../Config/config.php';
			if ($includeOK != true) die();

            //Condulta para obtener el nombre del modulo 
            $sql = "SELECT config_modulos.nombre, config_modulos.segmento, config_modulos.estado, config_modulos.id, config_modulos.cliente_sucursal
                    FROM config_modulos
                    WHERE config_modulos.id = $slide->id_config_modulos";

            //Conecta a base de datos
            $obj_conexion = new mysqli(LOCALSERVER, LOCALUSERDB, LOCALPASSDB, LOCALDB) or die("db_error");
            mysqli_set_charset($obj_conexion,"utf8");
            if (!$resultado = $obj_conexion->query($sql)) die();


            if ($resultado->num_rows != 0) {
                while ($rows = $resultado->fetch_assoc()) {
                    $estado= $rows['estado'];
                    $nombre= $rows['nombre'];
                    $segmento= $rows['segmento'];
                    $cliente_sucursal= $rows['cliente_sucursal'];
                }
            }

            //Consulta si está activo el modulo
            if($estado == true){

                $codigo=strtolower($slide->codigo);
                $slides[$codigo]['name'] = $nombre;
                $slides[$codigo]['menu'] = BASE_URL ."/files/sodexo/templates/json/" . AMBIENTE . "/".$cliente_sucursal."_menu_diario-data-conmaestro.json";
                $slides[$codigo]['news'] = BASE_URL ."/files/sodexo/templates/json/" . AMBIENTE . "/app_novedades_corp-data.json";
                $slides[$codigo]['table'] = $cliente_sucursal."_menu_diario";
                $slides[$codigo]['contingencia'] = BASE_URL ."/files/sodexo/templates/json/" . AMBIENTE . "/".$cliente_sucursal."_minuta_contingencia-data.json";
                $slides[$codigo]['sugerencia'] = BASE_URL ."/files/sodexo/templates/json/" . AMBIENTE . "/".$cliente_sucursal."_sugerencia_chef-data.json";
                $slides[$codigo]['multiselect'] = $slide->multiselect;

            }

            
            
        }


    }

    //crea Json para AppWeb
    $fh = fopen(BASE_TEMPLATE . '/templates/json/app/AppMobile.json', 'w');
    fwrite($fh, json_encode($slides));
    fclose($fh);

    $tipo_mensaje= "dos";
    
}


    

?>