<?php

// Coordenadas del Menu Diario
function Coordenadas(&$control)
{
    foreach($GLOBALS["tipos_platos"] as $tipo_plato){
        $control[$tipo_plato['@servicio']][$tipo_plato['@tipoplato']]['current'] = 0;
        $control[$tipo_plato['@servicio']][$tipo_plato['@tipoplato']]['cantidad'] = intval($tipo_plato['@cantidad']);
        $control[$tipo_plato['@servicio']][$tipo_plato['@tipoplato']]['pagina'] = $tipo_plato['@pagina'];
        $control[$tipo_plato['@servicio']][$tipo_plato['@tipoplato']]['ingles'] = $tipo_plato['@ingles'];
        $control[$tipo_plato['@servicio']][$tipo_plato['@tipoplato']]['indice'] = $tipo_plato['@indice'];
    }
}
