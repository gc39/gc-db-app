<?php

$includeOK = include_once __DIR__.'/_include_base.php';
if ($includeOK != true) die();

// Preview
if (AMBIENTE == 'test') {

    // URLINDEX para los servicios usados 
    $urlPreview = '';
    for ($ind = 0; $ind < $cantServicios; $ind++) {
        if (URLINDEX == $ind) {
            $urlPreview = $servicios_usados[$ind];
        }
    }

    // recorre el json y genere un objeto con la data
    foreach ($jsonDataSugerenciaChef as &$slide) {
        $date_parts = split('-', $slide->fecha);
        $slide_ts = strtotime($date_parts[1] . '/' . $date_parts[0] . '/' . $date_parts[2]);
        if ($slide_ts == $today_ts) {
            $slide->nombre = $slide->nombre;
            $slide->servicio = $slide->servicio;
            $slides[$slide_ts][] = $slide;
        }
    }

    // Borrado del archivo en directorio preview
    $directorio = BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $cliente . '/' . $sucursal;
    $files = glob($directorio . '/*');
    foreach ($files as $file) {
        foreach ($borrar_archivos_sugerencia_chef as $n_arch) {
            if (is_file(($file))) {
                if (strpos($file, $n_arch) !== false) {
                    unlink($file);
                }
            }
        }
    }

    foreach ($servicios_usados as $servicio_usado) {
        foreach ($slides as $dates) {
            foreach ($dates as $slide) {

                // **********************************************************************************
                // Reglas del negocio
                // **********************************************************************************

                // Si el url preview corresponde con el servicio 
                if ($servicio_usado == $urlPreview && $slide->servicio == $urlPreview) {

                    // Imagen de fondo
                    imagecopy($imageHandler, $bgImageSugerencia, 0, 0, 0, 0, 1080, 1920);

                    // Posición del plato de Sugerencia del Chef
                    
                    $plato = makeTextBlock(strtoupper(html_entity_decode($slide->plato)), $tipografia, $tamanios['platos_sugerencia'], 650);
                    $y = 965;

                    foreach ($plato as $index => $valueDet) {
                        $imageTrans = imagecreatetruecolor(1080, 1920);
                        $color = imagecolorallocatealpha($imageTrans, 0, 0, 0, 127); //fill transparent back
                        imagefill($imageTrans, 0, 0, $color);
                        imagesavealpha($imageTrans, true);
                        $newLine = explode("\r\n", $valueDet);
                        if (is_array($newLine) && count($newLine) > 3) {
                            foreach ($newLine as $indexLine => $line) {
                                if ($indexLine < 4) {
                                    $line = substr($line, 0, 11);
                                    list($x) = centerText($line, $tipografia, $tamanios['platos_sugerencia'], 800);
                                    imagefttext($imageTrans, $tamanios['platos_sugerencia'], 0, $x + 200, $y + 85, $colors['platos_sugerencia'], $tipografia, $line);
                                    $y = $y + 128;
                                    imagecopyresized($imageHandler, imagerotate($imageTrans, -9, $color), -80, -50, 0, 0, 910, 1750, 1080, 1920);
                                }
                            }
                        } else {
                            if ($index < 4) {
                                $valueDet = substr($valueDet, 0, 11);
                                list($x) = centerText($valueDet, $tipografia, $tamanios['platos_sugerencia'], 800);
                                imagefttext($imageTrans, $tamanios['platos_sugerencia'], 0, $x + 200, $y + 85, $colors['platos_sugerencia'], $tipografia, $valueDet);
                                $y = $y + 128;
                                imagecopyresized($imageHandler, imagerotate($imageTrans, -9, $color), -80, -50, 0, 0, 910, 1750, 1080, 1920);
                            }
                        }
                    }

                    // Posición de las calorias
                    $kcal = makeTextBlock(html_entity_decode($slide->calorias), $tipografia, 55, 100);
                    $y = 1413;

                    foreach ($kcal as $value) {
                        list($x) = centerText($value, $tipografia, 90, 500);

                        // Si la caloria es menor a 1000 posiciona kcal y el valor en una posicion adecuada
                        if ($value < 1000) {
                            imagefttext($imageHandler, $tamanios['calorias_sugerencia'], -10, 40 + $x, $y, $colors['calorias_sugerencia'], $tipografia, $value);
                            imagefttext($imageHandler, $tamanios['kcal_sugerencia'], -10, 147 + $x, $y + 18, $colors['calorias_sugerencia'], $tipografia, ' kcal');
                        }
                        // Si la caloria es mayor o igual a 1000 posiciona kcal y el valor un poco mas a la derecha para adecuar
                        else {
                            imagefttext($imageHandler, $tamanios['calorias_sugerencia'], -10, 70 + $x, $y, $colors['calorias_sugerencia'], $tipografia, $value);
                            imagefttext($imageHandler, $tamanios['kcal_sugerencia'], -10, 217 + $x, $y + 25, $colors['calorias_sugerencia'], $tipografia, ' kcal');
                        }
                        $y = $y + 95;
                    }

                    // Posición de la fecha
                    $date_parts = split('-', $slide->fecha);
                    $fecha_vigencia = $date_parts[2] . $date_parts[1] . $date_parts[0];
                    $stringDate =  $date_parts[0] . ' de ' . $arrayMonth[$date_parts[1] - 1] . ' ' . $date_parts[2];

                    if ($date_parts[0] < 10) {
                        $date_parts[0] = str_replace("0", "", $date_parts[0]);
                    }

                    // Nueva Función de centrado de Fecha 
                    $puntoInicio = newCenterText($stringDate, $tipografiaFecha, $tamanios['fecha_sugerencia'], 1135);
                    imagettftextSp($imageHandler, $tamanios['fecha_sugerencia'], -9, $puntoInicio, 715, $colors['fecha_sugerencia'], $tipografiaFecha, $stringDate, 0);

                    // **********************************************************************************
                    // Fin de las Reglas de Negocio
                    // **********************************************************************************

                    mkdir(BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $cliente);
                    mkdir(BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $cliente . '/' . $sucursal);

                    imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $cliente . '/' . $sucursal . '/' . '110' . '_' . ServicioUsadoCarpeta($servicio_usado) . '_' . $sugerencia_chef . '.jpg');
                }
            }
        }
    }
}

// Publicacion, todo lo de publicacion va aca
if (AMBIENTE == 'prod') {

    // api que marca la publicacion en monitoreo
    $casino = basename(__FILE__, '.php');
    file_get_contents(URLAPI . "/sodexo/job/$casino");

    // recorre el json y genere un objeto con la data
    foreach ($jsonDataSugerenciaChef as &$slide) {
        $date_parts = split('-', $slide->fecha);
        $slide_ts = strtotime($date_parts[1] . '/' . $date_parts[0] . '/' . $date_parts[2]);
        if ($slide_ts >= $today_ts) {
            $slide->nombre = $slide->nombre;
            $slide->servicio = $slide->servicio;
            $slides[$slide_ts][] = $slide;
        }
    }

    //Borra archivos de hoy y crea las carpetas de hoy
    foreach ($servicios_usados as $servicio_usado) {
        $date_parts = split('-', $today);
        $date_parts[2] = ($date_parts[2] < 10) ? str_replace("0", "", $date_parts[2]) : $date_parts[2];
        // Borrado del archivo en directorio preview
        $directorio = BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado);
        $files = glob($directorio . '/*');
        foreach ($files as $file) {
            foreach ($borrar_archivos_sugerencia_chef as $n_arch) {
                if (is_file(($file))) {
                    if (strpos($file, $n_arch) !== false) {
                        unlink($file);
                    }
                }
            }
        }
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0]);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1]);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2]);
        mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado));
    }

    //si no hay imagenes deja logo default
    $contador_archivos = 0;
    foreach ($servicios_usados as $servicio_usado) {
        $date_parts = split('-', $today);
        $date_parts[2] = ($date_parts[2] < 10) ? str_replace("0", "", $date_parts[2]) : $date_parts[2];
        // Borrado del archivo en directorio preview
        $directorio = BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado);
        $files = glob($directorio . '/*');
        foreach ($files as $file) {
            $contador_archivos++;
        }
        if ($contador_archivos == 0) {
            copy($defaultLogo, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[0] . '/' . $date_parts[1] . '/' . $date_parts[2] . '/' . ServicioUsadoCarpeta($servicio_usado) . '/'  . 'sodexo_logo_1080x1920.jpg');
        }
    }

    foreach ($servicios_usados as $servicio_usado) {
        foreach ($slides as $dates) {
            foreach ($dates as $slide) {

                //Ajusta la fecha para crear las carpetas
                $date_parts = split('-', $slide->fecha);

                if ($date_parts[0] < 10) {
                    $date_parts[0] = str_replace("0", "", $date_parts[0]);
                }
                //crea las carpetas
                mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente);
                mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal);
                mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2]);
                mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1]);
                mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0]);
                mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0] . '/' . ServicioUsadoCarpeta($servicio_usado));

                // **********************************************************************************
                // Reglas del negocio
                // **********************************************************************************

                // Si el url preview corresponde con el servicio
                if ($slide->servicio == $servicio_usado) {

                    //Imagen de fondo
                    imagecopy($imageHandler, $bgImageSugerencia, 0, 0, 0, 0, 1080, 1920);

                    // Posición del plato de Sugerencia del Chef
                    $plato = makeTextBlock(strtoupper(html_entity_decode($slide->plato)), $tipografia, $tamanios['platos_sugerencia'], 650);
                    $y = 965;

                    foreach ($plato as $index => $valueDet) {
                        $imageTrans = imagecreatetruecolor(1080, 1920);
                        $color = imagecolorallocatealpha($imageTrans, 0, 0, 0, 127); //fill transparent back
                        imagefill($imageTrans, 0, 0, $color);
                        imagesavealpha($imageTrans, true);
                        $newLine = explode("\r\n", $valueDet);
                        if (is_array($newLine) && count($newLine) > 3) {
                            foreach ($newLine as $indexLine => $line) {
                                if ($indexLine < 4) {
                                    $line = substr($line, 0, 11);
                                    list($x) = centerText($line, $tipografia, $tamanios['platos_sugerencia'], 800);
                                    imagefttext($imageTrans, $tamanios['platos_sugerencia'], 0, $x + 200, $y + 85, $colors['platos_sugerencia'], $tipografia, $line);
                                    $y = $y + 128;
                                    imagecopyresized($imageHandler, imagerotate($imageTrans, -9, $color), -80, -50, 0, 0, 910, 1750, 1080, 1920);
                                }
                            }
                        } else {
                            if ($index < 4) {
                                $valueDet = substr($valueDet, 0, 11);
                                list($x) = centerText($valueDet, $tipografia, $tamanios['platos_sugerencia'], 800);
                                imagefttext($imageTrans, $tamanios['platos_sugerencia'], 0, $x + 200, $y + 85, $colors['platos_sugerencia'], $tipografia, $valueDet);
                                $y = $y + 128;
                                imagecopyresized($imageHandler, imagerotate($imageTrans, -9, $color), -80, -50, 0, 0, 910, 1750, 1080, 1920);
                            }
                        }
                    }

                    // Posición de las calorias
                    $kcal = makeTextBlock(html_entity_decode($slide->calorias), $tipografia, 55, 100);
                    $y = 1413;

                    foreach ($kcal as $value) {
                        list($x) = centerText($value, $tipografia, 90, 500);
                        // Si la caloria es menor a 1000 posiciona kcal y el valor en una posicion adecuada
                        if ($value < 1000) {
                            imagefttext($imageHandler, $tamanios['calorias_sugerencia'], -10, 40 + $x, $y, $colors['calorias_sugerencia'], $tipografia, $value);
                            imagefttext($imageHandler, $tamanios['kcal_sugerencia'], -10, 147 + $x, $y + 18, $colors['calorias_sugerencia'], $tipografia, ' kcal');
                        }
                        // Si la caloria es mayor o igual a 1000 posiciona kcal y el valor un poco mas a la derecha para adecuar
                        else {
                            imagefttext($imageHandler, $tamanios['calorias_sugerencia'], -10, 70 + $x, $y, $colors['calorias_sugerencia'], $tipografia, $value);
                            imagefttext($imageHandler, $tamanios['kcal_sugerencia'], -10, 217 + $x, $y + 25, $colors['calorias_sugerencia'], $tipografia, ' kcal');
                        }
                        $y = $y + 95;
                    }

                    // Posición de la fecha
                    $date_parts = split('-', $slide->fecha);
                    $fecha_vigencia = $date_parts[2] . $date_parts[1] . $date_parts[0];
                    $stringDate =  $date_parts[0] . ' de ' . $arrayMonth[$date_parts[1] - 1] . ' ' . $date_parts[2];

                    if ($date_parts[0] < 10) {
                        $date_parts[0] = str_replace("0", "", $date_parts[0]);
                    }

                    // Nuevo codigo para el centrado de fecha 
                    $puntoInicio = newCenterText($stringDate, $tipografiaFecha, $tamanios['fecha_sugerencia'], 1135);
                    imagettftextSp($imageHandler, $tamanios['fecha_sugerencia'], -9, $puntoInicio, 715, $colors['fecha_sugerencia'], $tipografiaFecha, $stringDate, 0);

                    // **********************************************************************************
                    // Fin de las Reglas de Negocio
                    // **********************************************************************************

                    // Borra 
                    $directorio = BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0] . '/' . ServicioUsadoCarpeta($servicio_usado);
                    $files = glob($directorio . '/*');
                    foreach ($files as $file) {
                        foreach ($borrar_archivos_sugerencia_chef as $n_arch) {
                            if (is_file(($file))) {
                                if (strpos($file, $n_arch) !== false) {
                                    unlink($file);
                                }
                            }
                        }
                    }
                    imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0] . '/' . ServicioUsadoCarpeta($servicio_usado) . '/'  .  '110_' . $sugerencia_chef . '_' . $fecha_hora . '_' . $fecha_vigencia . '_' . $fecha_vigencia . '.jpg');
                    imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0] . '/' . ServicioUsadoCarpeta($servicio_usado) . '/'  .  '210_' . $sugerencia_chef . '_' . $fecha_hora . '_' . $fecha_vigencia . '_' . $fecha_vigencia . '.jpg');
                    imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $cliente . '/' . $sucursal . '/' . $date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0] . '/' . ServicioUsadoCarpeta($servicio_usado) . '/'  .  '310_' . $sugerencia_chef . '_' . $fecha_hora . '_' . $fecha_vigencia . '_' . $fecha_vigencia . '.jpg');
                }
            }
        }
    }
    shell_exec(PHP_RUTA . ' ' . CARPETA_SHELL_EXEC . 'copiarArchFTP_Casinos.php ' . $cliente_sucursal . ' > /dev/null 2>&1 &');
}

// libera memoria de sistema
imagedestroy($bgImage);
imagedestroy($imageHandler);
imagedestroy($imageTrans);