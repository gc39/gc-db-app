<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <META CONTENT="no-cache">
    <title>DigitalBoard - Previsualización</title>
    <meta name="description" content="Previsualizacion" />

    <script>

        function rotar_imagen(){
            var cliente = getParameterByName('cliente');    // Este es el nombre del Cliente 
            var sucursal = getParameterByName('sucursal');  // Nombre de la sucursal del cliente 
            var servicio = getParameterByName('servicio');  // Servicio ['Des', 'Alm', 'CenTar', 'CenNoc', 'CenMad', 'Onc' , 'Col']
            var paginas = getParameterByName('paginas');    // 1,2,3, 4 o 5 paginas
            var criollo = getParameterByName('criollo');    // si el criollo tiene mas de un plato
            var fullzona = getParameterByName('fullzona');  // Nombre de fulzona 000_minuta_contingencia.jpg, 009_sugerencia_chef.jpg, 010_cumpleanos.jpg, 020_equilibrate.jpg, 030_informativo.jpg, 040_campana.jpg
            // console.log('../../../templates_images_test/' + cliente + '/' + sucursal + '/001_' + servicio + '_pagina1_menu_diario.jpg?');
    
            if(paginas == 5 && servicio!= 'null'){
                var tiempo = 5000;//tiempo en milisegundos
                var arrImagenes = [
                    //'../../../templates_images_test/' + cliente + '/' + sucursal + '/001_' + servicio + '_pagina1_menu_diario.jpg?' + Math.random(),
                    //'../../../templates_images_test/' + cliente + '/' + sucursal + '/002_' + servicio + '_pagina2_menu_diario.jpg?' + Math.random(),
                    //'../../../templates_images_test/' + cliente + '/' + sucursal + '/003_' + servicio + '_pagina3_menu_diario.jpg?' + Math.random(),
                    //'../../../templates_images_test/' + cliente + '/' + sucursal + '/004_' + servicio + '_pagina4_menu_diario.jpg?' + Math.random()];
                    '../../../templates_images_test/' + cliente + '/' + sucursal + '/101_' + servicio + '_menu_diario1.jpg?' + Math.random(),
                    '../../../templates_images_test/' + cliente + '/' + sucursal + '/102_' + servicio + '_menu_diario2.jpg?' + Math.random(),
                    '../../../templates_images_test/' + cliente + '/' + sucursal + '/103_' + servicio + '_menu_diario3.jpg?' + Math.random(),
                    '../../../templates_images_test/' + cliente + '/' + sucursal + '/104_' + servicio + '_menu_diario4.jpg?' + Math.random(),
                    '../../../templates_images_test/' + cliente + '/' + sucursal + '/105_' + servicio + '_menu_diario5.jpg?' + Math.random()];
                
                _img = document.getElementById('rotativo');
                 //cargar la 1er imagen
                _img.src = arrImagenes[0];
                var i=1;
                setInterval(function(){
                    _img.src = arrImagenes[i];
                    i = (i == arrImagenes.length-1)? 0 : (i+1);
                }, tiempo);
            }
            
            if(paginas == 4 && servicio!= 'null'){
                var tiempo = 5000;//tiempo en milisegundos
                var arrImagenes = [
                    //'../../../templates_images_test/' + cliente + '/' + sucursal + '/001_' + servicio + '_pagina1_menu_diario.jpg?' + Math.random(),
                    //'../../../templates_images_test/' + cliente + '/' + sucursal + '/002_' + servicio + '_pagina2_menu_diario.jpg?' + Math.random(),
                    //'../../../templates_images_test/' + cliente + '/' + sucursal + '/003_' + servicio + '_pagina3_menu_diario.jpg?' + Math.random(),
                    //'../../../templates_images_test/' + cliente + '/' + sucursal + '/004_' + servicio + '_pagina4_menu_diario.jpg?' + Math.random()];
                    '../../../templates_images_test/' + cliente + '/' + sucursal + '/101_' + servicio + '_menu_diario1.jpg?' + Math.random(),
                    '../../../templates_images_test/' + cliente + '/' + sucursal + '/102_' + servicio + '_menu_diario2.jpg?' + Math.random(),
                    '../../../templates_images_test/' + cliente + '/' + sucursal + '/103_' + servicio + '_menu_diario3.jpg?' + Math.random(),
                    '../../../templates_images_test/' + cliente + '/' + sucursal + '/104_' + servicio + '_menu_diario4.jpg?' + Math.random()];
                
                _img = document.getElementById('rotativo');
                 //cargar la 1er imagen
                _img.src = arrImagenes[0];
                var i=1;
                setInterval(function(){
                    _img.src = arrImagenes[i];
                    i = (i == arrImagenes.length-1)? 0 : (i+1);
                }, tiempo);
            }

            if(paginas == 3 && servicio!= 'null'){
                var tiempo = 5000;//tiempo en milisegundos
                var arrImagenes = [
                    //'../../../templates_images_test/' + cliente + '/' + sucursal + '/001_' + servicio + '_pagina1_menu_diario.jpg?' + Math.random(),
                    //'../../../templates_images_test/' + cliente + '/' + sucursal + '/002_' + servicio + '_pagina2_menu_diario.jpg?' + Math.random(),
                    //'../../../templates_images_test/' + cliente + '/' + sucursal + '/003_' + servicio + '_pagina3_menu_diario.jpg?' + Math.random()];
                    '../../../templates_images_test/' + cliente + '/' + sucursal + '/101_' + servicio + '_menu_diario1.jpg?' + Math.random(),
                    '../../../templates_images_test/' + cliente + '/' + sucursal + '/102_' + servicio + '_menu_diario2.jpg?' + Math.random(),
                    '../../../templates_images_test/' + cliente + '/' + sucursal + '/103_' + servicio + '_menu_diario3.jpg?' + Math.random()];
                
                _img = document.getElementById('rotativo');
                 //cargar la 1er imagen
                _img.src = arrImagenes[0];
                var i=1;
                setInterval(function(){
                    _img.src = arrImagenes[i];
                    i = (i == arrImagenes.length-1)? 0 : (i+1);
                }, tiempo);
                
            }

            if(servicio && paginas == 2 && servicio!= 'null'){
                var tiempo = 5000;//tiempo en milisegundos
                var arrImagenes = [ //'../../../templates_images_test/' + cliente + '/' + sucursal + '/001_' + servicio + '_pagina1_menu_diario.jpg?' + Math.random(),
                                    //'../../../templates_images_test/' + cliente + '/' + sucursal + '/002_' + servicio + '_pagina2_menu_diario.jpg?' + Math.random()];
                                    '../../../templates_images_test/' + cliente + '/' + sucursal + '/101_' + servicio + '_menu_diario1.jpg?' + Math.random(),
                                    '../../../templates_images_test/' + cliente + '/' + sucursal + '/102_' + servicio + '_menu_diario2.jpg?' + Math.random()];
                
                _img = document.getElementById('rotativo');
                 //cargar la 1er imagen
                _img.src = arrImagenes[0];
                var i=1;
                setInterval(function(){
                    _img.src = arrImagenes[i];
                    i = (i == arrImagenes.length-1)? 0 : (i+1);
                }, tiempo);
            }
            
            if(paginas == 1 && servicio!= 'null'){
                //var arrImagenes = ['../../../templates_images_test/' + cliente + '/' + sucursal + '/001_' + servicio + '_pagina1_menu_diario.jpg?' + Math.random()];
                var arrImagenes = ['../../../templates_images_test/' + cliente + '/' + sucursal + '/101_' + servicio + '_menu_diario1.jpg?' + Math.random()];

                _img = document.getElementById('rotativo');
                 //cargar la 1er imagen
                _img.src = arrImagenes[0];
            }



            if(criollo && paginas == 2 && criollo != 'null'){
                var tiempo = 5000;//tiempo en milisegundos
                var arrImagenes = [ '../../../templates_images_test/' + cliente + '/' + sucursal + '/' + criollo + '1.jpg?' + Math.random(),
                                    '../../../templates_images_test/' + cliente + '/' + sucursal + '/' + criollo + '2.jpg?' + Math.random()];
                
                _img = document.getElementById('rotativo');
                 //cargar la 1er imagen
                _img.src = arrImagenes[0];
                var i=1;
                setInterval(function(){
                    _img.src = arrImagenes[i];
                    i = (i == arrImagenes.length-1)? 0 : (i+1);
                }, tiempo);
            }

            if(fullzona && fullzona != 'null'){
                var arrImagenes = ['../../../templates_images_test/' + cliente + '/' + sucursal + '/'+fullzona+'?' + Math.random()];
                
                _img = document.getElementById('rotativo');
                 //cargar la 1er imagen
                _img.src = arrImagenes[0];
                
                
            }
        }



        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
    </script>

    <style>
        html,body {
            margin: 0px;
            background-color: rgb(61, 59, 59);
        }

        img{
            max-width: 100%;
            height: 100vh;
            }
    </style>
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">

</head>
<body onload="rotar_imagen();">
    <img id="rotativo" onerror="this.src='../../../templates/images/sodexo_logo_1080x1920.jpg'">
</body>

</html>