<?php

header('Access-Control-Allow-Origin: *'); 
header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
header("Content-Type: application/json");

if(!isset($_GET['branch_id'])) {
    die('{"error":"BAD_REQUEST"}');
}

$return_branch = false;
$json_branchs  = file_get_contents('./data/branchs.json');
$json_branchs  = json_decode($json_branchs, false);
var_dump($json_branchs);
die();


foreach($json_branchs as $branchID => $branch) {
    
    if($branchID == $_GET['branch_id']) {
        $return_branch = $branch;
    }
}   


if($return_branch === false) {
    die('{"error":"NOT_FOUND"}');
} else {
    echo json_encode($return_branch);
}

?>