<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
header("Content-Type: application/json");

if(!isset($_GET['branch_id'])) {
    die('{"error":"BAD_REQUEST"}');
}

$info_branch = false;
$json_branchs  = file_get_contents('data/branchs.json');
$json_branchs  = json_decode($json_branchs, true);

foreach($json_branchs as $branchID => $branch) {
    
    if($branchID == $_GET['branch_id']) {
        $info_branch = $branch;
    }
}   


if($info_branch === false) {
    die('{error:NOT_FOUND}');
} else {
    
    $json_menu        = isset($info_branch['menu']) ? file_get_contents($info_branch['menu']) : false;
    $json_sugerencia  = isset($info_branch['sugerencia']) ? file_get_contents($info_branch['sugerencia']) : '"NOT_FOUND"';
    
    if($json_menu) {
        echo '{"menu": '.$json_menu.',"sugerencia":'.$json_sugerencia.'}';    
    } else {
        die('{"error":"NOT_FOUND"}');
    }
    
}

?>