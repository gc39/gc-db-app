window.onerror = function (msg, url, lineNo, columnNo, error) {
    //alert(msg + url + lineNo);
}

var branchName = '';
var branchID   = 0;
var $$         = Dom7;
var istoday    = true;

var hayPedidoHoy = false;
var hayPedidoManana = false;

var appF7 = new Framework7({
    root: '#app',
    name: 'Sodexo',
    id: 'com.grupoclan.menudeldiav1',
    panel: {
        swipe: 'left',
    },
    sheet: {
        closeByBackdropClick: false,
    },
    routes: [
        {
            name: 'home',
            path: '/home',
            url: 'index.html',
        },
        {
            name: 'menu',
            path: '/menu',
            url: 'menu.html',
        },
        {
            name: 'menuanticipado',
            path: '/menuanticipado',
            url: 'menuanticipado.html',
        },
        {
            name: 'news',
            path: '/news',
            url: 'news.html',
        },
        {
            name: 'mispedidos',
            path: '/mispedidos',
            url: 'mispedidos.html',
        },
    ],
});

Date.prototype.ddmmyyyy = function() {
          var mm = this.getMonth() + 1; // getMonth() is zero-based
          var dd = this.getDate();
    return [
        (dd>9 ? '' : '0') + dd,'-',
        (mm>9 ? '' : '0') + mm,'-',
        this.getFullYear()
    ].join('');
};

Date.prototype.yyyymmdd = function() {
          var mm = this.getMonth() + 1; // getMonth() is zero-based
          var dd = this.getDate();
    return [
        this.getFullYear(),'-',
        (mm>9 ? '' : '0') + mm,'-',
        (dd>9 ? '' : '0') + dd
    ].join('');
};

function timeNow() {
  var d = new Date(),
    h = (d.getHours()<10?'0':'') + d.getHours(),
    m = (d.getMinutes()<10?'0':'') + d.getMinutes(),
    s = (d.getSeconds()<10?'0':'') + d.getSeconds();
  return h + ':' + m + ':' + s;
}

var getClosest = function (elem, selector) {
    for ( ; elem && elem !== document; elem = elem.parentNode ) {
        if ( elem.matches( selector ) ) return elem;
    }
    return null;
};

mainView = appF7.views.create('.view-main');

function initFlow() {
    setTimeout(function () {
        if (getBranchID() == false) {
            dynamicSheet.open();

            $$('#button-save-branch').off('click');
            $$('#button-save-branch').on('click', function (e) {

                // af@grupoclan.cl
                rut = $$('#input-rut').val();
                if(!validate(rut)){
                  appF7.dialog.alert('RUT inválido.', 'Atención');
                  return;
                }
                window.localStorage["rut"] = rut;
                branchID = $$('#input-branch-id').val();

                Framework7.request.json('http://sodexo.digitalboard.cl/files/sodexo/mobile/get_branch_info.php?branch_id=' + branchID, function (data) {
                    if (typeof data.error !== 'undefined') {
                        appF7.dialog.alert('El código ingresado no pertenece a ningún casino.', 'Atención');
                    } else {
                        //console.log(data);
                        dynamicSheet.close();
                        setBranchInfo(branchID, data.name);
                        // af@grupoclan.cl
                        window.localStorage["codename"] = data.table.substr(0, data.table.indexOf("_menu_diario"));
                        if(data.multiselect===true) {
                          window.localStorage["multiselect"] = true;
                        }
                        else {
                          window.localStorage["multiselect"] = false;
                        }
                        if(data.hasOwnProperty("pedidos")) {
                          window.localStorage["pedidos"] = true;
                        }
                        else {
                          window.localStorage["pedidos"] = false;
                        }
                        $$('#branch-name').html(data.name);

                        mainView.router.navigate('/menu', {reloadAll: true});
                    }

                }, function(xhr, status) {
                    appF7.dialog.alert('Sin conexión a Internet. Por favor conectarse a Internet y reinteintar', 'Sin Conexión');
                });
            });
        } else {
            getBranchID();
            getBranchName();
            mainView.router.navigate('/menu', {reloadAll: true});
        }
    }, 2500);
}

initFlow();

var app = {
    initialize: function () {
        this.bindEvents();
    },

    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },

    onDeviceReady: function () {
        //StatusBar.hide();

        $$('#status-bar').css('background', '#6361d3');
        $$('#status-bar').css('color', '#FFF');

        window.plugins.PushbotsPlugin.initialize("5b99e6e71db2dc604444aa75", {"android":{"sender_id":"678533592100"}});
        window.plugins.PushbotsPlugin.on("registered", function(token){
            if(branchID != 0) {
                window.plugins.PushbotsPlugin.tag("bID-" + branchID);
            }
        });
    },
};

// PAGES EVENTS ************************************************************* //
$$(document).on('page:init', function (e) {
    // Get page data from event data
    var page = e.detail;

    if (page.name === 'menu') {
        istoday = true;
        ////StatusBar.show();
        loadMenu();
      checkDatePedidos(); //verifica si ya hay pedidos para hoy y mañana
      hidePedidoAnticipadoButtonByHour(); // oculta el boton de pedido anticipado si se pasa la hora
    }

    if (page.name === 'menuanticipado') {
        //StatusBar.show();
        loadMenuAnticipado();
      checkDatePedidos(); //verifica si ya hay pedidos para hoy y mañana
      hidePedidoAnticipadoButtonByHour(); // oculta el boton de pedido anticipado si se pasa la hora
    }

    if (page.name === 'news') {
        // Following code will be executed for page with data-page attribute equal to "about"
        loadNews();
        window.plugins.PushbotsPlugin.resetBadge();
    }

    if (page.name === 'mispedidos') {
        //StatusBar.show();
        loadMisPedidos();
    }
});

function loadMenu() {
    $$('#calories-bar').css('background-color', '#00c853');
    $$("#calc-string").html('<strong id="total-items">0 Items</strong>, Total: <strong>&nbsp;<span id="total-kcal">0</span> Calorías</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
    $$('#services-bar .segmented').html('');
    $$('#menu-services-content-tabs').html('');
    $$('#loading-menu').show();

    // limpiar las marcas
    document.querySelectorAll("#menu-services-content-tabs .with-sum-kcal").forEach(function(e) {
      $$(e).removeClass('with-sum-kcal')
    });
    document.querySelectorAll("#menuanticipado-services-content-tabs .with-sum-kcal").forEach(function(e) {
      $$(e).removeClass('with-sum-kcal')
    });

    //window.plugins.PushbotsPlugin.tag("bID-" + branchID);

    Framework7.request.json('http://sodexo.digitalboard.cl/files/sodexo/mobile/get_branch_menu.php?branch_id=' + branchID, function (json_data) {
        var months = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        var days = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");

        date = new Date();
        dateSpanishLong =  days[date.getDay()] + " " + date.getUTCDate() + " de "  + months[date.getUTCMonth()];
        dateSpanish     =  ('0' + date.getDate()).slice(-2) + '-' + ('0' + (date.getMonth()+1)).slice(-2) + '-' + date.getFullYear();

        var today = new Date();
        if (!istoday) {
            today.setDate(today.getDate() + 1);
            dataSpanish = today.ddmmyyyy();
        }

        var todayMenu = getMenuForDate(json_data.menu, dateSpanish);
        var todaySugerencia = getSugerenciaForDate(json_data.sugerencia, dateSpanish);
        //console.log(todaySugerencia);

        var allDaysMenu = json_data.menu;
        if(todayMenu.length == 0) {
            $$('#services-bar .segmented').html('');
            $$('#menu-services-content-tabs').html('<div class="tab tab-active" id="0"><div class="block"><p class="text-align-center no-menu-advice"><img src="img/chef_no_menu.png" ><br /><br />Aún no ha sido publicado el menú para hoy.<br /><a href="/menu" class="link refresh-menu" onclick="loadMenu()">Reintentar</a></p></div></div>');
            $$('#loading-menu').hide();
            return false;
        }

        services = getServices(allDaysMenu);
        //services = services.filter(function(element){
        //  return element=='Almuerzo'
        //});

        var menuServices = _.groupBy(todayMenu, 'servicio');
        if (services === false) {
            types = getTypes(todayMenu);
            menu  = _.groupBy(todayMenu, 'tipo');

            html  = '<br><div class="card service-card-header">';
            html += '<div style="background-image:url(img/services/almuerzo.jpg)" class="card-header align-items-flex-end"><strong>' + dateSpanishLong + '</strong></div>';
            html += '<div class="card-content card-content-padding">';

            items_html = '';
            no_items_in_service = true;

            _.each(types, function (element, index) {
                items_html += '       <li class="item-divider"><strong>' + element + '</strong></li>';

                var typeIndex = index;
                _.each(menu[element], function (element, index) {
                    no_items_in_service = false;
                    if(element.calorias.trim() == '') {
                        items_html += '<li><a href="#" class="item-link item-content type_'+ typeIndex + '"  data-type="' + typeIndex + '"><div class="item-title">' + element.nombre.toUpperCase().trim() + '</div><div class="item-after"></div></a></li>';
                    } else {
                        items_html += '<li><a href="#" onclick="toggleKCal(this)" class="item-link item-content type_'+ typeIndex + '""  data-type="' + typeIndex + '" data-kcal="' + element.calorias.trim() + '"><div class="item-title">' + element.nombre.toUpperCase().trim() + '<br /><span style="font-size:13px">'  + element.calorias.trim() + ' Calorías</span></div></a></li>';
                    }
                })
            });

            sugerencias_html = '';
            _.each(todaySugerencia, function (element, index) {
                sugerencias_html += '<p class="text-align-center" style="font-size: 21px; margin-top: -13px !important;">' + element.plato + '</p>';
            });

            if(no_items_in_service) {
                html += '<p class="text-align-center" style="font-size: 18px";><br />En este momento no hay menú publicado. Revisa mas tarde!<br /></p>';
            } else {
                html += '<div class="list links-list">';
                html += '<ul>';
                html += items_html;
                html += '</ul>';
                html += '</div>';
            }
            html += '</div>';
            html += '</div>';

            if(sugerencias_html !== '') {
                html += '<div class="card sugerencia-card-header" style="background: #2488d9;color: white;">';
                html += '<div style="background-image:url(img/food_bg/7.jpg)" class="card-header align-items-flex-start"><p class="text-align-center"><strong>Sugerencia del Chef</strong></p></div>';
                html += '<div class="card-content card-content-padding"><p class="text-align-center" style="font-size:15px;">HOY</p>';
                html += sugerencias_html;
                html += '</div></div>';
            }

            html += '<p class="text-align-center"><img src="img/logo_black.png" width="44" style="opacity:0.5"></p>';

            $$('#menu-services-content-tabs').html('<div class="tab tab-active" id="0">' + html + '</div>')
            $$('#services-bar .segmented').html('<a href="#0" class="button  tab-link-active tab-link">HOY</a>');
            $$('#loading-menu').hide();
        } else {
            _.each(services, function (element, serviceIndex) {
                var serviceIndex = serviceIndex;
                var foodImages = ['desayuno', 'almuerzo', 'once']

                types = getTypes(menuServices[element]);
                menu  = _.groupBy(menuServices[element], 'tipo');

                html  = '<br><div class="card service-card-header">';
                html += '<div style="background-image:url(img/services/' + foodImages[serviceIndex] + '.jpg)" class="card-header align-items-flex-end"><strong>' + element + ', ' + dateSpanishLong + '</strong></div>';
                html += '<div class="card-content card-content-padding">';

                items_html = '';
                no_items_in_service = true;

                _.each(types, function (element, index) {
                    items_html += '       <li class="item-divider"><strong>' + element + '</strong></li>';

                    var typeIndex = index;
                    _.each(menu[element], function (element, index) {
                        no_items_in_service = false;
                        if((element.calorias == null) || (element.calorias.trim() == '')) {
                            if (element.nombre == null)
                                element.nombre = "";
                            items_html += '<li><a href="#" class="item-link item-content type_'  + serviceIndex + '_' + typeIndex + '"" data-type="' + serviceIndex + '_' + typeIndex + '"><div class="item-title">' + element.nombre.toUpperCase().trim() + '</div><div class="item-after"></div></a></li>';
                        } else {
                            items_html += '<li><a href="#" onclick="toggleKCal(this)" class="item-link item-content type_'  + serviceIndex + '_' + typeIndex + '""  data-type="' + serviceIndex + '_' + typeIndex  + '" data-kcal="' + element.calorias.trim() + '"><div class="item-title">' + element.nombre.toUpperCase().trim() + '<br /><span style="font-size:13px">'  + element.calorias.trim() + ' Calorías</span></div></a></li>';
                        }
                    })
                });

                sugerenciasForService = _.where(todaySugerencia, {servicio: element});
                sugerencias_html = '';
                _.each(sugerenciasForService, function (element, index) {
                    sugerencias_html += '<p class="text-align-center" style="font-size: 21px;  margin-top: -13px !important;">' + element.plato + '</p>';
                });

                if(no_items_in_service) {
                    html += '<p class="text-align-center" style="font-size: 18px;"><br />En este momento no hay menú publicado para este servicio. Revisa mas tarde!<br /></p>';
                } else {
                    html += '<div class="list links-list">';
                    html += '<ul>';
                    html += items_html;
                    html += '</ul>';
                    html += '</div>';
                }

                html += '</div>';
                html += '</div>';

                if(sugerencias_html !== '') {
                    html += '<div class="card sugerencia-card-header" style="background: #2488d9;color: white;text-shadow: -1px -1px 11px #709ec5;">';
                    html += '<div style="background-image:url(img/food_bg/7.jpg)" class="card-header align-items-flex-start"><p class="text-align-center"><strong>Sugerencia del Chef</strong></p></div>';
                    html += '<div class="card-content card-content-padding"><p class="text-align-center" style="font-size:15px;">HOY</p>';
                    html += sugerencias_html;
                    html += '</div></div>';
                }

                html += '<p class="text-align-center"><img src="img/logo_black.png" width="44" style="opacity:0.5"></p>';
                activeClass = serviceIndex == 0 ? 'tab-link-active' : '';

                $$('#services-bar .segmented').append('<a href="#' + element + '" class="button ' + activeClass + ' tab-link">' + element + '</a>');
                activeClass = serviceIndex == 0 ? 'tab-active' : '';
                $$('#menu-services-content-tabs').append('<div class="tab ' + activeClass + '" id="' + element + '">' + html + '</div>')
            })

            $$('#menu-services-content-tabs').css('margin-top', '0px');
            $$('#services-bar').show();
            $$('#loading-menu').hide();
            $$('.page.page-current').addClass('page-with-subnavbar');
        }

    }, function(xhr, status) {
            $$('#services-bar .segmented').html('');
            $$('#menu-services-content-tabs').html('<div class="tab tab-active" id="0"><div class="block"><p class="text-align-center no-menu-advice"><img src="img/no_internet.png" ><br /><br />Sin conexión a Internet.<br> Por favor conectarse a Internet y <a href="/menu" class="link refresh-menu" onclick="loadMenu()">reintentar</a>.<br /></p></div></div>');
            $$('#loading-menu').hide();
    });

    if($$('#news-tab').hasClass('finished') == false) {
        setTimeout(function() {
            $$('#news-tab').css('width', '290px');
        }, 1900);

        setTimeout(function() {
            $$('#news-tab img').show();
        }, 2100);

        setTimeout(function() {
            $$('#news-tab').css('width', '114px');
            $$('#news-tab img').hide();
            $$('#news-tab').addClass('finished');
        }, 3900);
    }

    var $ptrContentMenu = $$('.ptr-content.ptr-menu');

    $ptrContentMenu.off('ptr:refresh');
    $ptrContentMenu.on('ptr:refresh', function (e) {
        setTimeout(function() {
            loadMenu();
            appF7.ptr.done();
            setKCalTotal();
        }, 1200);
    });
}

function loadMenuAnticipado() {
  var months = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
  var days = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");

  date = new Date();
  date.setDate(date.getDate() + 1);
  dateSpanishLong =  days[date.getDay()] + " " + date.getUTCDate() + " de "  + months[date.getUTCMonth()];
  dataSpanish = date.ddmmyyyy();

  $$('#calories-bar-menuanticipado').css('background-color', '#00c853');
  $$('#menuanticipado-content').html('contenido del menu anticipado');
  //$$('#loading-menuanticipado').show();
  $$('#menuanticipado-services-content-tabs').html('');
  $$('#loading-menu').show();

  // limpiar las marcas
  document.querySelectorAll("#menu-services-content-tabs .with-sum-kcal").forEach(function(e) {
    $$(e).removeClass('with-sum-kcal')
  });
  document.querySelectorAll("#menuanticipado-services-content-tabs .with-sum-kcal").forEach(function(e) {
    $$(e).removeClass('with-sum-kcal')
  });

  // af@grupoclan.cl
  $$("#btnHacerPedidoAnticipado").on('click', function () {
    var date = new Date;
    var currHour = date.getHours();
    var currMin = date.getMinutes();
    if (currHour >= 15 && currMin > 0)
    {
      swal({
        text: 'Lo sentimos se ha cerrado el proceso de recepcion de pedido, hora limite 15 horas'
      });
      return;
    }

    var tableCasino = window.localStorage["codename"];
    var codeCasino = window.localStorage["branch"];
    var rut = window.localStorage["rut"];

    var payload = [];

    // items chequeados
    document.querySelectorAll("#menuanticipado-services-content-tabs .with-sum-kcal").forEach(function(element) {
      item = element.innerHTML.split('<br>')[0].split('<div class="item-title">')[1];
      tipoplato = getTipoPlato(element);

      var parentTab = getClosest(element, '.tab');
      var servicio = parentTab.id.split('_')[0]
      if (servicio == "0")
        servicio = "Para la fecha";

        var comidaItem = {};
        comidaItem["rut"] = rut;
        comidaItem["casino"] = tableCasino;
        comidaItem["plato"] = item;
        comidaItem["tipo"] = tipoplato;
        comidaItem["servicio"] = servicio;
        comidaItem["fechahora"] = dataSpanish + " 09:00:00";
        payload.push(comidaItem);
      });

    // se realiza una agrupacion
    var servicioGroup = _.groupBy(payload, 'servicio');

    var result = [];
    _.map(servicioGroup, item => {
        _.map(item, detail => {
                result.push({
                    "servicio": detail.servicio,
                    "plato": detail.plato
                });
            });
        });

    var lastTxtItem = "";
    var textContent="";
    _.map(result, item=> {
        if (item.servicio!=lastTxtItem) {
            lastTxtItem = item.servicio;
            textContent+="\n"+lastTxtItem+":\n";
        }
        textContent+=item.plato+"\n";
    });

    textContent+="\n";
    textContent+="Su pedido puede ser modificado hasta las 15 horas, para ello acceda al menú Ver mis pedidos y cancele para crear su nueva selección.\n";

    swal({
      title: 'Su Pedido para el ' + dateSpanishLong,
      text: textContent,
      confirmButtonColor: '#2196f3',
      buttons: [
        'No',
        'Si'
      ]
    }).then(function(isConfirm) {
      if (isConfirm) {
        xhttp.send(data);
      } else {
        swal("Pedido Cancelado", "", "");
      }
    });

    var xhttp = new XMLHttpRequest();
    //xhttp.open("POST", "http://digitalboardoff.cl/app/webroot/files/api/web/pedidoanticipado/", true);
    xhttp.open("POST", "http://sodexo.digitalboard.cl/files/api/web/pedidoanticipado/", true);
    xhttp.setRequestHeader("Content-type", "application/json;charset=UTF-8");
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            swal({
                title: "Pedido realizado",
                icon: "success",
            });
            checkDatePedidos();
            // limpiar las marcas
            document.querySelectorAll("#menuanticipado-services-content-tabs .with-sum-kcal").forEach(function(e) {
              $$(e).removeClass('with-sum-kcal')
            });
        }
        if (xhttp.readyState == 2 && xhttp.status == 500) {
            swal({
                text: "Hubo un error al realizar su pedido",
                icon: "error",
            });
            console.log(xhttp.responseText);
        }
    }
    var data = JSON.stringify(payload);
});

Framework7.request.json('http://sodexo.digitalboard.cl/files/sodexo/mobile/get_branch_menu.php?branch_id=' + branchID, function (json_data) {
    var todayMenu = getMenuForDate(json_data.menu, dateSpanish);
    var todaySugerencia = getSugerenciaForDate(json_data.sugerencia, dateSpanish);

    var allDaysMenu = json_data.menu;
    if(todayMenu.length == 0) {
      $$('#services-bar-menuanticipado .segmented').html('');
      $$('#menuanticipado-services-content-tabs').html('<div class="tab tab-active" id="0"><div class="block"><p class="text-align-center no-menu-advice"><img src="img/chef_no_menu.png" ><br /><br />Aún no ha sido publicado el menú para hoy.<br /><a href="/menu" class="link refresh-menu" onclick="loadMenu()">Reintentar</a></p></div></div>');
      $$('#loading-menuanticipado').hide();
      return false;
    }

    // desayuno almuerzo y cena
    services = getServices(allDaysMenu);
    services = services.filter(function(element){
      return element=='Almuerzo'
    });

    var menuServices = _.groupBy(todayMenu, 'servicio');

    if (services === false) {
      // Si estan definidos servicios

      types = getTypes(todayMenu);
      menu  = _.groupBy(todayMenu, 'tipo');

      html  = '<br><div class="card service-card-header">';
      html += '<div style="background-image:url(img/services/almuerzo.jpg)" class="card-header align-items-flex-end"><strong>' + dateSpanishLong + '</strong></div>';
      html += '<div class="card-content card-content-padding">';

      items_html = '';
      no_items_in_service = true;


      // revisar
      _.each(types, function (element, index) {
          items_html += '       <li class="item-divider"><strong>' + element + '</strong></li>';
          var typeIndex = index;
          _.each(menu[element], function (element, index) {
              no_items_in_service = false;
              if(element.calorias.trim() == '') {
                  items_html += '<li><a href="#" class="item-link item-content type_'+ typeIndex + '"  data-type="' + typeIndex + '"><div class="item-title">' + element.nombre.toUpperCase().trim() + '</div><div class="item-after"></div></a></li>';
              } else {
                  items_html += '<li><a href="#" onclick="toggleKCal(this)" class="item-link item-content type_'+ typeIndex + '""  data-type="' + typeIndex + '" data-kcal="' + element.calorias.trim() + '"><div class="item-title">' + element.nombre.toUpperCase().trim() + '<br /><span style="font-size:13px">'  + element.calorias.trim() + ' Calorías</span></div></a></li>';
              }
          })
      });

      sugerencias_html = '';

      _.each(todaySugerencia, function (element, index) {
          sugerencias_html += '<p class="text-align-center" style="font-size: 21px; margin-top: -13px !important;">' + element.plato + '</p>';
      });

      if(no_items_in_service) {
          html += '<p class="text-align-center" style="font-size: 18px";><br />En este momento no hay menú publicado. Revisa mas tarde!<br /></p>';
      } else {
          html += '<div class="list links-list">';
          html += '<ul>';
          html += items_html;
          html += '</ul>';
          html += '</div>';
      }

      html += '</div>';
      html += '</div>';

      if(sugerencias_html !== '') {

          html += '<div class="card sugerencia-card-header" style="background: #2488d9;color: white;">';
          html += '<div style="background-image:url(img/food_bg/7.jpg)" class="card-header align-items-flex-start"><p class="text-align-center"><strong>Sugerencia del Chef</strong></p></div>';
          html += '<div class="card-content card-content-padding"><p class="text-align-center" style="font-size:15px;">HOY</p>';
          html += sugerencias_html;
          html += '</div></div>';

      }

      html += '<p class="text-align-center"><img src="img/logo_black.png" width="44" style="opacity:0.5"></p>';


      $$('#menuanticipado-services-content-tabs').html('<div class="tab tab-active" id="0">' + html + '</div>')
      $$('#services-bar-menuanticipado .segmented').html('<a href="#0" class="button  tab-link-active tab-link">HOY</a>');

      $$('#loading-menuanticipado').hide();
    }
    else {
      // Si estan definidos servicios ej: Desayuno Almuerzo Cena
      _.each(services, function (element, serviceIndex) {
          var serviceIndex = serviceIndex;
          var foodImages = ['desayuno', 'almuerzo', 'once']

          types = getTypes(menuServices[element]);
          menu  = _.groupBy(menuServices[element], 'tipo');

          html  = '<br><div class="card service-card-header">';
          html += '<div style="background-image:url(img/services/' + foodImages[serviceIndex] + '.jpg)" class="card-header align-items-flex-end"><strong>' + element + ', ' + dateSpanishLong + '</strong></div>';
          html += '<div class="card-content card-content-padding">';

          items_html = '';
          no_items_in_service = true;

          _.each(types, function (element, index) {
              items_html += '       <li class="item-divider"><strong>' + element + '</strong></li>';

              var typeIndex = index;
              _.each(menu[element], function (element, index) {

                  no_items_in_service = false;

                  if((element.calorias == null) || (element.calorias.trim() == '')) {
                      if (element.nombre == null)
                          element.nombre = "";
                      items_html += '<li><a href="#" class="item-link item-content type_'  + serviceIndex + '_' + typeIndex + '"" data-type="' + serviceIndex + '_' + typeIndex + '"><div class="item-title">' + element.nombre.toUpperCase().trim() + '</div><div class="item-after"></div></a></li>';
                  } else {
                      items_html += '<li><a href="#" onclick="toggleKCal(this)" class="item-link item-content type_'  + serviceIndex + '_' + typeIndex + '""  data-type="' + serviceIndex + '_' + typeIndex  + '" data-kcal="' + element.calorias.trim() + '"><div class="item-title">' + element.nombre.toUpperCase().trim() + '<br /><span style="font-size:13px">'  + element.calorias.trim() + ' Calorías</span></div></a></li>';
                  }
              })
          });

          sugerenciasForService = _.where(todaySugerencia, {servicio: element});

          sugerencias_html = '';

          _.each(sugerenciasForService, function (element, index) {
              sugerencias_html += '<p class="text-align-center" style="font-size: 21px;  margin-top: -13px !important;">' + element.plato + '</p>';
          });

          if(no_items_in_service) {
              html += '<p class="text-align-center" style="font-size: 18px;"><br />En este momento no hay menú publicado para este servicio. Revisa mas tarde!<br /></p>';
          } else {
              html += '<div class="list links-list">';
              html += '<ul>';
              html += items_html;
              html += '</ul>';
              html += '</div>';
          }

          html += '</div>';
          html += '</div>';

          if(sugerencias_html !== '') {
              html += '<div class="card sugerencia-card-header" style="background: #2488d9;color: white;text-shadow: -1px -1px 11px #709ec5;">';
              html += '<div style="background-image:url(img/food_bg/7.jpg)" class="card-header align-items-flex-start"><p class="text-align-center"><strong>Sugerencia del Chef</strong></p></div>';
              html += '<div class="card-content card-content-padding"><p class="text-align-center" style="font-size:15px;">HOY</p>';
              html += sugerencias_html;
              html += '</div></div>';
          }

          html += '<p class="text-align-center"><img src="img/logo_black.png" width="44" style="opacity:0.5"></p>';

          activeClass = serviceIndex == 0 ? 'tab-link-active' : '';

          $$('#services-bar-menuanticipado .segmented').append('<a href="#' + element + '_anticipado" class="button ' + activeClass + ' tab-link">' + element + '</a>');

          activeClass = serviceIndex == 0 ? 'tab-active' : '';
          $$('#menuanticipado-services-content-tabs').append('<div class="tab ' + activeClass + '" id="' + element + '_anticipado">' + html + '</div>')
      })

      $$('#menuanticipado-services-content-tabs').css('margin-top', '0px');
      $$('#services-bar-menuanticipado').show();
      $$('#loading-menuanticipado').hide();
      $$('.page.page-current').addClass('page-with-subnavbar');
    }
  });
}

function getTipoPlato(element){
  className = element.parentElement.className; //element.parentElement.previousElementSibling.className;
  while(className!='item-divider'){
    element = element.parentElement.previousElementSibling.children[0];
    className = element.parentElement.className;
  }
  return element.innerText;
}

function loadNews() {
  $$('#news-content').html('');
  $$('#loading-news').show();

  Framework7.request.json('http://sodexo.digitalboard.cl/files/sodexo/mobile/get_branch_news.php?branch_id=' + branchID, function (json_data) {
    html = '';

    //console.log(json_data);
    //console.log(_.sortBy(json_data, 'id'));
    //_.each(json_data.news.reverse(), function(element, index) {
    _.each(json_data.news, function(element, index) {
        branchsArray = element.branchs.split('|#|');

        if(element.branchs != '' && branchsArray.indexOf(branchID) !== -1) {
            html = html + '<div class="card demo-facebook-card">';
            html = html + '  <div class="card-header">';
            html = html + '    <div class="demo-facebook-date">' + element.etiqueta + '</div>';
            html = html + '    <div class="demo-facebook-name">' + element.titulo + '</div>';
            html = html + '  </div>';

            if(element.type == 'video') {
                html = html + '  <div class="card-content"><a href="#" onclick="toggleVideoBrowser(this)" data-url="' + element.video + '"><div src="img/icon_play.png" class="play" style=" background-image: url(img/icon_play.png);position: absolute;right: 0px;top: 0px;left:0px; bottom:0px; background-position: center;"  ></div><img src="http://sodexo.digitalboard.cl/files/sodexo/' + element.imagen + '" width="100%"/></a></div>';
            }
            if(element.type == 'imagen' || element.type == 'imagen-link') {
                html = html + '  <div class="card-content"><img src="http://sodexo.digitalboard.cl/files/sodexo/' + element.imagen + '" width="100%"/></div>';
            }
            if(element.type == 'imagen-link') {
                html = html + '<div class="card-footer"><a href="' + element.link + '" class="link external">' + element.accion + '</a></div>';
            }
            html = html + '</div>';
        }
    })

    $$('#loading-news').hide();
    $$('#news-content').html(html);
  })
}

function hidePedidoAnticipadoButtonByHour(){
  var date = new Date;
  var currHour = date.getHours();
  var currMin = date.getMinutes();
  if (currHour >= 15 && currMin > 0){
      //if(document.getElementById("btnHacerPedidoAnticipado") != null)
      //    document.getElementById("btnHacerPedidoAnticipado").style = "display:none";
      //if(document.getElementById("menuanticipado-tab") != null)
      //    document.getElementById("menuanticipado-tab").style = "display:none";
      //if(document.getElementById("mnuLeftMenuAnticipado") != null)
      //    document.getElementById("mnuLeftMenuAnticipado").style = "display:none";
  } else {
      //hayPedidoManana = false;
      //if(document.getElementById("btnHacerPedidoAnticipado") != null)
      //    document.getElementById("btnHacerPedidoAnticipado").style = "display:unset";
      //if(document.getElementById("menuanticipado-tab") != null)
      //    document.getElementById("menuanticipado-tab").style = "display:unset";
      //if(document.getElementById("mnuLeftMenuAnticipado") != null)
      //    document.getElementById("mnuLeftMenuAnticipado").style = "display:unset";
  }
  if(window.localStorage["pedidos"] === "false") {
    //document.getElementById("btnHacerPedidoAnticipado").style = "display:none";
    document.getElementById("menuanticipado-tab").style = "display:none";
    document.getElementById("mnuLeftMenuAnticipado").style = "display:none";
    document.getElementById("mnuLeftMenuMisPedidos").style = "display:none";
  }
  else {
    //document.getElementById("btnHacerPedidoAnticipado").style = "display:unset";
    document.getElementById("menuanticipado-tab").style = "display:unset";
    document.getElementById("mnuLeftMenuAnticipado").style = "display:unset";
    document.getElementById("mnuLeftMenuMisPedidos").style = "display:unset";
  }
}

function checkDatePedidos() {
  $$('#mispedidos-content').html('');
  $$('#loading-mispedidos').show();
  var strFechaPedido = '';

  html = '';

  var tableCasino = window.localStorage["codename"];
  var rut = window.localStorage["rut"];

  //Framework7.request.json('http://digitalboardoff.cl/app/webroot/files/api/web/pedido/actual?rut='+rut+'&casino='+tableCasino, function (json_data) {
  Framework7.request.json('http://sodexo.digitalboard.cl/files/api/web/pedido/actual?rut='+rut+'&casino='+tableCasino, function (json_data) {
    html = '';

    if (json_data.length > 0){
        hayPedidoHoy = true;
        //if(document.getElementById("btnHacerPedido") != null)
        //    document.getElementById("btnHacerPedido").style = "display:none";
    } else {
        hayPedidoHoy = false;
        //if(document.getElementById("btnHacerPedido") != null)
        //    document.getElementById("btnHacerPedido").style = "display:unset";
    }

    date = new Date();
    date.setDate(date.getDate() + 1);
    strDate = date.yyyymmdd();
    //Framework7.request.json('http://digitalboardoff.cl/app/webroot/files/api/web/pedidoanticipado/actual?rut='+rut+'&casino='+tableCasino+'&fecha='+strDate, function (json_data) {
    Framework7.request.json('http://sodexo.digitalboard.cl/files/api/web/pedidoanticipado/actual?rut='+rut+'&casino='+tableCasino+'&fecha='+strDate, function (json_data) {
      if (json_data.length > 0){
          hayPedidoManana = true;
          /*
          if(document.getElementById("btnHacerPedidoAnticipado") != null)
              document.getElementById("btnHacerPedidoAnticipado").style = "display:none";
          if(document.getElementById("menuanticipado-tab") != null)
              document.getElementById("menuanticipado-tab").style = "display:none";
          if(document.getElementById("mnuLeftMenuAnticipado") != null)
              document.getElementById("mnuLeftMenuAnticipado").style = "display:none";
          */
      } else {
          hayPedidoManana = false;
          /*
          if(document.getElementById("btnHacerPedidoAnticipado") != null)
              document.getElementById("btnHacerPedidoAnticipado").style = "display:unset";
          if(document.getElementById("menuanticipado-tab") != null)
              document.getElementById("menuanticipado-tab").style = "display:unset";
          if(document.getElementById("mnuLeftMenuAnticipado") != null)
              document.getElementById("mnuLeftMenuAnticipado").style = "display:unset";
          */
          //hidePedidoAnticipadoButtonByHour(); // oculta el boton de pedido anticipado si se pasa la hora
      }
    });
  });
}

function loadMisPedidos() {
  $$('#mispedidos-content').html('');
  $$('#loading-mispedidos').show();
  var strFechaPedido = '';

  html = '';

  var tableCasino = window.localStorage["codename"];
  var rut = window.localStorage["rut"];

  //Framework7.request.json('http://digitalboardoff.cl/app/webroot/files/api/web/pedido/actual?rut='+rut+'&casino='+tableCasino, function (json_data) {
  Framework7.request.json('http://sodexo.digitalboard.cl/files/api/web/pedido/actual?rut='+rut+'&casino='+tableCasino, function (json_data) {
      html = '';

      if (json_data.length > 0){

          html = html + '<div class="card demo-facebook-card">';
          html = html + '  <div class="card-header">';

          let arrayFecha = json_data[0].fecha.split(' ')[0].split('-');
          html = html + '<h3 style="color: #6464cc">Pedido para el: ' + arrayFecha[2] + '/' + arrayFecha[1] + '/' + arrayFecha[0] + '</h3>';
          strFechaPedido = json_data[0].fecha.split(' ')[0];

          if (services != false)
              services.forEach(function (item){
                  service = item;
                  filtered = _.filter(json_data,item=>{
                      return item.servicio == service;
                  });
                  if (filtered.length > 0){
                      html = html + '    <div style="color: #1b639c"><strong>' + service.toUpperCase() + '</strong></div>';
                      filtered.forEach(function (filteredItem){
                          html = html + '    <div>' + filteredItem.plato + '</div>';
                      })
                  }
              });
          else {
              //html = html + '    <div style="color: #1b639c"><strong>' + service.toUpperCase() + '</strong></div>';
              json_data.forEach(function (filteredItem){
                  html = html + '    <div>' + filteredItem.plato + '</div>';
              })
          }

          html = html + '<div style="text-align: right;">';
          html = html + '<button onclick="cancelaPedido(\''+strFechaPedido+'\')" class="swal-button" style="color: white; background-color: #fd2b2b;width: 190px;">Cancelar Pedido</button>';
          html = html + '</div>';

          html = html + '  </div>';
          html = html + '</div>';
      }

      date = new Date();
      date.setDate(date.getDate() + 1);
      strDate = date.yyyymmdd();
      //Framework7.request.json('http://digitalboardoff.cl/app/webroot/files/api/web/pedidoanticipado/actual?rut='+rut+'&casino='+tableCasino+'&fecha='+strDate, function (json_data) {
      Framework7.request.json('http://sodexo.digitalboard.cl/files/api/web/pedidoanticipado/actual?rut='+rut+'&casino='+tableCasino+'&fecha='+strDate, function (json_data) {

          if (json_data.length > 0){
              html = html + '<div class="card demo-facebook-card">';
              html = html + '  <div class="card-header">';

              let arrayFecha = json_data[0].fecha.split(' ')[0].split('-');
              html = html + '<h3 style="color: #6464cc">Pedido para el: ' + arrayFecha[2] + '/' + arrayFecha[1] + '/' + arrayFecha[0] + '</h3>';
              strFechaPedido = json_data[0].fecha.split(' ')[0];

              if (services != false)
                  services.forEach(function (item){
                      service = item;
                      filtered = _.filter(json_data,item=>{
                          return item.servicio == service;
                      });
                      if (filtered.length > 0){
                          html = html + '    <div style="color: #1b639c"><strong>' + service.toUpperCase() + '</strong></div>'
                          filtered.forEach(function (filteredItem){
                              html = html + '    <div>' + filteredItem.plato + '</div>';
                          })
                      }
                  });
              else {
                  json_data.forEach(function (filteredItem){
                      html = html + '    <div>' + filteredItem.plato + '</div>';
                  })
              }

              html = html + '<div style="text-align: right;">';
              html = html + '<button onclick="cancelaPedido(\''+strFechaPedido+'\')" class="swal-button" style="color: white; background-color: #fd2b2b;width: 190px;">Cancelar Pedido</button>';
              html = html + '</div>';

              html = html + '  </div>';
              html = html + '</div>';
          }

          $$('#mispedidos-content').html(html);
          $$('#loading-mispedidos').hide();
      });
  });
}

function cancelaPedido(strFecha) {
  var payload = {};

  payload["rut"] = window.localStorage["rut"];
  payload["casino"] = window.localStorage["codename"];
  payload["fecha"] = strFecha;

  var xhttp = new XMLHttpRequest();
  //xhttp.open("DELETE", "http://digitalboardoff.cl/app/webroot/files/api/web/pedido/", true);
  xhttp.open("DELETE", "http://sodexo.digitalboard.cl/files/api/web/pedido/", true);
  xhttp.setRequestHeader("Content-type", "application/json;charset=UTF-8");
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      swal({
        title: "Pedido eliminado",
        icon: "success",
      });
      // recargar pantalla
      loadMisPedidos()
    }
    if (xhttp.readyState == 2 && xhttp.status == 500) {
      swal({
        text: "Hubo un error al eliminar el pedido",
        icon: "error",
      });
      console.log(xhttp.responseText);
    }
  }

  var data = JSON.stringify(payload);
  swal({
    title: 'Esta seguro de Eliminar el Pedido?',
    confirmButtonColor: '#2196f3',
    buttons: [
      'No',
      'Si'
    ]
  }).then(function(isConfirm) {
    if (isConfirm) {
      xhttp.send(data);
    }
  });
}

function toggleVideoBrowser(element) {
  var photoBrowser = appF7.photoBrowser.create({
    photos: [
        '<video src="' + $$(element).data('url') + '"  type="video/mp4" controls width="100%"></video>'
    ],
    theme: 'dark',
    type: 'standalone',
    on: {
        opened: function () {
            //StatusBar.backgroundColorByHexString('#000')
        },
        closed: function () {
            //StatusBar.backgroundColorByHexString('#6361d3');
        }
    }
  });
  photoBrowser.open();
}

function windowOpen($url) {
  var ref = window.open(encodeURI(url), '_blank', 'location=yes');
}

function clearCasino() {
  window.plugins.PushbotsPlugin.untag("bID-" + branchID);
  window.localStorage.clear();
  branchID = 0;
  initFlow();
}

// CONFIG MODAL ************************************************************* //
var dynamicSheet = appF7.sheet.create({
  content:
    '<div class="sheet-modal" style="height: auto">' +
    '<div class="sheet-modal-inner"  style="border-top: 4px solid #1d1db9">' +
    '<div class="block">' +
    '<h2 style="margin-top: -8px;font-size: 24px;">Te Damos la Bienvenida!</h2> <span style="margin-top: -25px;font-size: 18px;display: inline-block;color: #333;">Ingresa tu RUT y el código de casino para continuar...</span>' +
    '</div>' +
    '<div class="list no-hairlines-md" style="margin-top: -24px;">' +
    '   <ul>' +
    '      <li class="item-content item-input">' +
    '          <div class="item-inner">' +
    '               <div class="item-input-wrap">' +
    '                   <input id="input-rut" type="text" class="text-align-center" placeholder="Ingrese su RUT" style="font-size: 25px; height: 40px;">' +
    '                   <span class="input-clear-button"></span>' +
    '               </div>' +
    '          </div>' +
    '      </li>  ' +
    '      <li class="item-content item-input">' +
    '          <div class="item-inner">' +
    '              <div class="item-input-wrap">' +
    '                  <input id="input-branch-id" type="text" class="text-align-center" placeholder="" style="font-size: 25px; height: 40px;">' +
    '                  <span class="input-clear-button"></span>' +
    '              </div>' +
    '         </div>' +
    '      </li>' +
    '   </ul>' +
    '</div>' +
    '<div class="block">' +
    '   <div class="row">' +
    '       <button id="button-save-branch" class="col button button-fill button-big" style="font-size: 18px;">Aceptar</button>' +
    '   </div>' +
    '</div>' +
    '</div>' +
    '</div>'
});

// **** GLOBAL FUNCTIONS **************************************************** //
function toggleKCal(e) {
  type = $$(e).data('type');
  service = $$(e).data('service');

  if($$(e).hasClass('with-sum-kcal')) {
    $$(e).removeClass('with-sum-kcal');
  } else {
    if (localStorage["multiselect"] === "true")
      $$(e).addClass('with-sum-kcal');
    else
      if($$('.with-sum-kcal.type_' + type).length == 0) {
        $$(e).addClass('with-sum-kcal');
    }
  }
  setKCalTotal();
}

function setKCalTotal() {
  var total = 0;
  $$("#total-items").html('0 Items ');
  $$('.with-sum-kcal').each(function(i,e) {
    if(i == 0) {
      switch(appF7.views.current.router.currentRoute.name){
        case "menu":
          $$("#calc-string").html('<strong id="total-items">0 Items</strong>, Total: <strong>&nbsp;<span id="total-kcal">0</span> Calorías</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
          $$("#calc-string-anticipado").html('');
          break;
        case "menuanticipado":
          $$("#calc-string-anticipado").html('<strong id="total-items">0 Items</strong>, Total: <strong>&nbsp;<span id="total-kcal">0</span> Calorías</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
          $$("#calc-string").html('');
          break;
      }
    }
    total = total + parseInt($$(e).data('kcal'));
    $$("#total-items").html((i + 1) + (i == 0 ? ' Item ' : ' Items'));
  })
  if(total >= 0) {
     // $$('#calories-bar').css('background-color', '#00c853');
  }
  if(total >= 800) {
     // $$('#calories-bar').css('background-color', '#fdc000');
  }
  if(total >= 1200) {
    //  $$('#calories-bar').css('background-color', '#d82215');
  }
  $$("#total-kcal").html(total);
}

function getBranchID() {
  var storage = window.localStorage;
  var value = storage.getItem('branch');

  if (value == null) {
    branchID = 0;
    return false;
  } else {
    branchID = value;
    return value;
  }
}

function getBranchName() {
  var storage = window.localStorage;
  var value = storage.getItem('name');

  $$('#branch-name').html(value);
  if (value == null) {
    branchName = '';
    return false;
  } else {
    branchName = value;
    return value;
  }
}

function setBranchInfo(id, name) {
  var storage = window.localStorage;

  storage.setItem('branch', id);
  storage.setItem('name', name);

  branchName = name;
  branchID = id;

  return true;
}

function getServices(data) {
  returnServices = new Array();
  services = _.groupBy(data, 'servicio');

  _.each(services, function (element, index, list) {
    if (index !== 'undefined') {
      returnServices.push(index);
    }
  })

  if (returnServices.length != 0) {
    return returnServices;
  } else {
    return false;
  }
}

function getTypes(data) {
  returnTypes = new Array();
  orderedTypes = new Array();

  types = _.groupBy(data, 'tipo');

  _.each(types, function (element,index, list) {
      if (element !== 'undefined') {
          returnTypes.push(index);
      }
  })

  //orderArray = ["Sopa / Crema", "Salad Bar", "Plato de Fondo", "Acompañamiento", "Postre", "Chocolate", "Agregados", "Cereal", "Porridge", "Agregados Dulce", "Postre Light", "Agregado Light", "Proteicos Calientes", "Vegetariano", "Proteicos Frios", "Sopa/Crema Normal", "Sopa/Crema Baja en Sodio"];
  orderArray = returnTypes;

  _.each(orderArray, function (element, index, list) {

      if (element !== 'undefined' && returnTypes.indexOf(element) !== -1) {
          orderedTypes.push(element);
      }
  })

  if (orderedTypes.length != 0) {
      return orderedTypes;
  } else {
      return false;
  }
}

function getSugerenciaForDate(data, date) {
    return _.where(data, {fecha: date});
}

function getMenuForDate(data, date) {
    return _.where(data, {fecha: date});
}
