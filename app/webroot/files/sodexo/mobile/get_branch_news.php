<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
header("Content-Type: application/json");

if(!isset($_GET['branch_id'])) {
    die('{"error":"BAD_REQUEST"}');
}

$info_branch = false;
$json_branchs  = file_get_contents('data/branchs.json');
$json_branchs  = json_decode($json_branchs, true);

foreach($json_branchs as $branchID => $branch) {
    
    if($branchID == $_GET['branch_id']) {
        $info_branch = $branch;
    }
}   


if($info_branch === false) {
    die('{error:NOT_FOUND}');
} else {
    
    $json_news        = isset($info_branch['news']) ? file_get_contents($info_branch['news']) : false;    
    
    if($json_news) {
        echo '{"news": '.$json_news.'}';    
    } else {
        die('{"error":"NOT_FOUND"}');
    }
    
}

?>