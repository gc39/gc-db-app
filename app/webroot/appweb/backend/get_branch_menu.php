<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
header("Content-Type: application/json");

if(!isset($_GET['branch_id'])) {
    die('{"error":"BAD_REQUEST"}');
}

$contingencia = '';
if(isset($_GET['contingencia'])){
    $contingencia = $_GET['contingencia'];
}

$info_branch = false;
$json_branchs  = file_get_contents('https://sodexo.digitalboard.app/files/sodexo/templates/json/app/AppWeb.json');
$json_branchs  = json_decode($json_branchs, true);





if ($contingencia != ''){
    $json_contingencia  = file_get_contents($contingencia);
    $json_contingencia  = json_decode($json_contingencia, true);

    $today = date('d-m-Y', time());
    //$today = "15-10-2020";

    //{
    //    "id": "250",
    //    "tipo": "",
    //    "nombre": "",
    //    "calorias": "",
    //    "fecha": "",
    //    "created": "2020-11-02 06:09:59",
    //    "modified": "2020-11-02 06:09:59"
    //}

    foreach($json_contingencia as $contingenciaID => $contingenciaItem) {
        if ($today == $contingenciaItem["fecha"]){
            $arr_result = array();
            $arr_result["id"] = 0;
            $arr_result["tipo"] = "Menu Contingencia";
            $arr_result["nombre"] = $contingenciaItem["platos"];
            $arr_result["calorias"] = "";
            $arr_result["fecha"] = $today;
            $arr_result["created"] = $today;
            $arr_result["modified"] = $today;

            $content = $contingenciaItem["platos"];
            echo '{"menu": ['.json_encode($arr_result).']}';
            die();
        }
    } 
}

// foreach($json_branchs as $branchID => $branch) {
//     if($branchID == $_GET['branch_id']) {
//         $info_branch = $branch;
//     }
// } 

foreach($json_branchs as $branchID => $branch) {
    //echo strtolower($branchID).' == '.strtolower($_GET['branch_id']).'<br>';
    if(strtolower($branchID) == strtolower($_GET['branch_id'])) {
        //$masterObj = getMasterCode2($slide->codigo);
        //$slides2[$count]['nombre'] = ($masterObj->nombre != null)? $masterObj->nombre : '';
        $info_branch = $branch;
    }
} 

// Formatos de Fecha y hora a utilizar
$fecha          = new DateTime("now", new DateTimeZone('America/Santiago'));
$today_ts       = strtotime($fecha->format('d-m-Y'));
 

if($info_branch === false) {
    die('{error:NOT_FOUND}');
} else {

    $json_menu        = isset($info_branch['menu']) ? file_get_contents($info_branch['menu']) : false;
    $json_menu   = json_decode($json_menu , true);

    $count = 0;
    foreach ($json_menu as $index => &$slide) {
        $date_parts = split('-', $slide['fecha']);
		$slide_ts = strtotime($date_parts[1] . '/' . $date_parts[0] . '/' . $date_parts[2]);
		if ($slide_ts == $today_ts) {

            $masterObj = getMasterCode2($slide['codigo']);

            $slides2[$count]['id'] = $slide['id'];
            $slides2[$count]['servicio'] = str_replace(' ','_', $slide['servicio']);
            $slides2[$count]['tipo'] = $slide['tipo']; 
            $slides2[$count]['fecha'] = $slide['fecha'];

            $slides2[$count]['nombre'] = ($masterObj->nombre_espanol != null)? $masterObj->nombre_espanol : ''; 
            $slides2[$count]['calorias'] = ($slide['calorias_temp'] != null && $slide['calorias_temp'] != " ")? $slide['calorias_temp'] :$masterObj->calorias;
            $slides2[$count]['alergeno_1'] = ($masterObj->lactosa == "Si")? 'Lactosa' : '';  
            $slides2[$count]['alergeno_2'] = ($masterObj->soya == "Si")? 'Soya' : '';  
            $slides2[$count]['alergeno_3'] = ($masterObj->frutos_secos == "Si")? 'Frutos Secos' : '';  
            $slides2[$count]['gluten']  = $masterObj->gluten;
            $slides2[$count]['vegano']  = $masterObj->vegano;
            $slides2[$count]['vegetariano']  = $masterObj->vegetariano;
            $slides2[$count]['azucar']  = $masterObj->altoazucar;
            $slides2[$count]['sodio']  = $masterObj->altosodio;

            $slides2[$count]['created'] = $slide['created'];
            $slides2[$count]['modified'] = $slide['modified'];
            

            // Si el plato temporal existe entonces asignara los datos por su codigo temporal para que se muestre en la imagen
            if ($slide['codigo_temp'] != NULL) {
                //Incluye datos de Config y valida 
                $includeOK = include_once __DIR__ . '/../../../Config/config.php';
                if ($includeOK != true) die();
                

                $sql = "SELECT
                    maestro_temporal.nombre_espanol, 
                    maestro_temporal.nombre_ingles, 
                    maestro_temporal.calorias, 
                    maestro_temporal.lactosa, 
                    maestro_temporal.soya, 
                    maestro_temporal.frutos_secos, 
                    maestro_temporal.gluten, 
                    maestro_temporal.vegano, 
                    maestro_temporal.vegetariano, 
                    maestro_temporal.altoazucar, 
                    maestro_temporal.altosodio, 
                    maestro_temporal.codigo_temporal
                FROM
                    maestro_temporal
                WHERE
                    maestro_temporal.codigo_temporal = ".$slide['codigo_temp'];

                
                //Conecta a base de datos
                $obj_conexion = new mysqli(LOCALSERVER, LOCALUSERDB, LOCALPASSDB, LOCALDB) or die("db_error");
                if (!$resultado = $obj_conexion->query($sql)) die();

                if ($resultado->num_rows != 0) {
                    while ($rows = $resultado->fetch_assoc()) {
                        echo $slides2[$count]['nombre'] = $rows['nombre_espanol'];  
                        $slides2[$count]['calorias'] = $rows['calorias'];
                        $slides2[$count]['alergeno_1'] = ($rows['lactosa'] == "Si")? 'Lactosa' : '';  
                        $slides2[$count]['alergeno_2'] = ($rows['soya'] == "Si")? 'Soya' : '';  
                        $slides2[$count]['alergeno_3'] = ($rows['frutos_secos'] == "Si")? 'Frutos Secos' : '';  
                        $slides2[$count]['gluten']  = $rows['gluten'];
                        $slides2[$count]['vegano']  = $rows['vegano'];
                        $slides2[$count]['vegetariano']  = $rows['vegetariano'];
                        $slides2[$count]['azucar']  = $rows['altoazucar'];
                        $slides2[$count]['sodio']  = $rows['altosodio'];
                    }
                }
            }
            $count++;
        }
    }
    $json_menu2   = json_encode($slides2 , true);

    // echo '<pre>' . var_export($slides2, true) . '</pre>';
    // die();


    $json_sugerencia  = isset($info_branch['sugerencia']) ? file_get_contents($info_branch['sugerencia']) : '"NOT_FOUND"';

    
    if($json_menu) {
       // echo '{"menu": '.$json_menu.',"sugerencia":'.$json_sugerencia.'}';  
        echo '{"menu": '.$json_menu2.',"sugerencia":'.$json_sugerencia.'}';   
    } else {
        die('{"error":"NOT_FOUND"}');
    }
    
}

function getMasterCode2($code){
    $jsonFileMaster     = file_get_contents('https://sodexo.digitalboard.app/files/sodexo/templates/json/prod/maestro_productos-data.json');
    $jsonDataMaster     = json_decode($jsonFileMaster);
    foreach ($jsonDataMaster as $master){
        if(trim($master->codigo) == trim($code)){
           // print_r($master);
            return $master;
        }
    }
}
