<?php
header('Access-Control-Allow-Origin: *'); 
header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
header("Content-Type: application/json");

if(!isset($_GET['data'])) {
    die('{"error":"BAD_REQUEST"}');
}

$data = $_GET['data'];

$url = 'https://sodexo.digitalboard.cl/files/api/web/pedido/';
$header_response = '';
$rtn = curl_to_host('DELETE', $url, null, $data, $header_response);
echo $rtn;

function curl_to_host($method, $url, $headers, $data, &$resp_headers){
    $ch=curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
    curl_setopt($ch, CURLOPT_HEADER, 1);

    if ($method=='DELETE'){
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data))
            );
    }

    $rtn=curl_exec($ch);
    curl_close($ch);

    $rtn=explode("\r\n\r\nHTTP/", $rtn, 2);
    $rtn=(count($rtn)>1 ? 'HTTP/' : '').array_pop($rtn);
    list($str_resp_headers, $rtn)=explode("\r\n\r\n", $rtn, 2);

    $str_resp_headers=explode("\r\n", $str_resp_headers);
    array_shift($str_resp_headers);
    $resp_headers=array();
    foreach ($str_resp_headers as $k=>$v){
        $v=explode(': ', $v, 2);
        $resp_headers[$v[0]]=$v[1];
    }

    return $rtn;
}