<?php
header('Access-Control-Allow-Origin: *'); 
header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
header("Content-Type: application/json");

if(!isset($_GET['branch_id'])) {
    die('{"error":"BAD_REQUEST."}');
}

// $url = 'https://sodexo.digitalboard.cl/files/sodexo/mobile/get_branch_info.php?branch_id='.$_GET['branch_id'];
// $json_branchs  = file_get_contents($url);
// echo $json_branchs;


$return_branch = false;
$json_branchs  = file_get_contents('https://sodexo.digitalboard.app/files/sodexo/templates/json/app/AppWeb.json');
$json_branchs  = json_decode($json_branchs, true);

foreach($json_branchs as $branchID => $branch) {
    //echo strtolower($branchID).' == '.strtolower($_GET['branch_id']).'<br>';
    if(strtolower($branchID) == strtolower($_GET['branch_id'])) {
        $return_branch = $branch;
    }
}   

//echo '<pre>' . var_export($branch, true) . '</pre>';
//die();

if($return_branch === false) {
    die('{"error":"NOT_FOUND."}');
} else {
    echo json_encode($return_branch);
}

?>