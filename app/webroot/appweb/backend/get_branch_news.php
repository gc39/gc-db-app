<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
header("Content-Type: application/json");

if(!isset($_GET['branch_id'])) {
    die('{"error":"BAD_REQUEST"}');
}

$url = 'https://sodexo.digitalboard.cl/files/sodexo/mobile/get_branch_news.php?branch_id='.$_GET['branch_id'];
$json_branchs  = file_get_contents($url);
echo $json_branchs;
