<?php
header('Access-Control-Allow-Origin: *'); 
header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
header("Content-Type: application/json");

if(!isset($_GET['rut'])) {
    die('{"error":"BAD_REQUEST"}');
}
if(!isset($_GET['casino'])) {
    die('{"error":"BAD_REQUEST"}');
}
if(!isset($_GET['fecha'])) {
    die('{"error":"BAD_REQUEST"}');
}

$rut = $_GET['rut'];
$casino = $_GET['casino'];
$fecha = $_GET['fecha'];

$url = 'https://sodexo.digitalboard.cl/files/api/web/pedidoanticipado/actual?rut='.$rut.'&casino='.$casino.'&fecha='.$fecha;
$header_response = '';
$rtn = curl_to_host('GET', $url, null, null, $header_response);
echo $rtn;

//clearstatcache($url);
//$result  = file_get_contents($url);
//echo $result;

function curl_to_host($method, $url, $headers, $data, &$resp_headers){
    
    if ($method=='GET'){
        $url = $url . '&rnd='.rand(10000, 99999);
    }
    $ch=curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
    curl_setopt($ch, CURLOPT_HEADER, 1);

    if ($method=='DELETE'){
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data))
            );
    }
    if ($method=='GET'){
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Cache-Control: no-cache')
            );
    }

    $rtn=curl_exec($ch);
    curl_close($ch);

    $rtn=explode("\r\n\r\nHTTP/", $rtn, 2);
    $rtn=(count($rtn)>1 ? 'HTTP/' : '').array_pop($rtn);
    list($str_resp_headers, $rtn)=explode("\r\n\r\n", $rtn, 2);

    $str_resp_headers=explode("\r\n", $str_resp_headers);
    array_shift($str_resp_headers);
    $resp_headers=array();
    foreach ($str_resp_headers as $k=>$v){
        $v=explode(': ', $v, 2);
        $resp_headers[$v[0]]=$v[1];
    }

    return $rtn;
}