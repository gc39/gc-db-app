<?php
header('Access-Control-Allow-Origin: *'); 
header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
header("Content-Type: application/json");

if(!isset($_GET['rut'])) {
    die('{"error":"BAD_REQUEST"}');
}
if(!isset($_GET['casino'])) {
    die('{"error":"BAD_REQUEST"}');
}

$rut = $_GET['rut'];
$casino = $_GET['casino'];

$url = 'https://sodexo.digitalboard.cl/files/api/web/pedido/actual?rut='.$rut.'&casino='.$casino;
$result  = file_get_contents($url);
echo $result;