﻿var simularFecha;

function initiFilters() {

	simularFecha =  getParameterByName('fecha');
	
	$( 'body' ).append( '<div id="filterMenu"><div id="toggleFilter"><img src="/images/config2.png" /></div><div class="tipo"><span class="tittle-filtro" > FILTROS </span><div class="filtros">Sin Filtros</div></div><div class="date"><b>FECHA</b><input type="text" class="datepicker" value=""  readonly="readonly" /></div></div>' );

	if(!simularFecha){
		var selectedDate = new Date();
	}else{
		var selectedDate = new Date(simularFecha.split( '-' )[ 2 ], simularFecha.split( '-' )[ 1 ] - 1, simularFecha.split( '-' )[ 0 ]);
	}
	
	$( '.datepicker' ).val( ( '0' + selectedDate.getDate()).slice( -2 ) + '-' + (  '0' + ( selectedDate.getMonth() + 1 ) ).slice( -2 ) + '-' + selectedDate.getFullYear() );
				
	$('.datepicker').datepicker({
		dateFormat: 'dd-mm-yy',
		defaultDate: selectedDate,
		onSelect: function( date ){
			location.href = '?fecha=' + date + '&filter=' + filter ;
		},
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio','Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun','Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
	});
	
	 $('#toggleFilter').click(function() {

		if($('#toggleFilter').hasClass('open')) {
			$('#filterMenu').css('width', '0px');
		} else {
			$('#filterMenu').css('width', '210px');
		}

		$('#toggleFilter').toggleClass('open');
	});
}

function getComparationDate(){
	if(!simularFecha){
		var today = new Date();
	}else{
		var today = new Date(simularFecha.split( '-' )[ 2 ], simularFecha.split( '-' )[ 1 ] - 1, simularFecha.split( '-' )[ 0 ]);
	}
	return today; 
}

function getComparationDateBirthday(range) {

	var listaFecha = [];

	if(!simularFecha) {
		var inicio = new XDate();
	} else {
		var inicio = new XDate(simularFecha.split( '-' )[ 2 ], simularFecha.split( '-' )[ 1 ] - 1, simularFecha.split( '-' )[ 0 ] );
	}

	listaFecha[0] =inicio.toString( "dd-MM" );;
	for(i = 1; i < range; i++) {
		listaFecha[i] = inicio.addDays( 1 ).toString( "dd-MM" );
	}

	return listaFecha;
}

function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
