<?php

$apiroot = '/home/digitalboard/public_html/digitalboard-ms-fisico/app/Api/sodexo';

require_once $apiroot.'/vendor/autoload.php';

$app = new Silex\Application();

require $apiroot.'/app/config/config_prod.php';
require $apiroot.'/app/config/parameters.php';
require $apiroot.'/app/routes.php';
require $apiroot.'/app/providers.php';
require $apiroot.'/src/index.php';

$app['environment'] = "prod";
$app['debug'] = false;
$app['log.level'] = Monolog\Logger::ERROR;

$app['http_cache']->run();
