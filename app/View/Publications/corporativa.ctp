
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Centro de Publicación <small></small></h3>
    </div>
</div>
<!-- /page header -->

<div class="panel panel-default">
    <div class="panel-body">
        <div class="bg-success with-padding block-inner success-folder mensaje" style="display:none;">Se crearon las carpetas! (DB, FTP, MI)</div>
        <div class="bg-danger with-padding block-inner danger-folder-ftp mensaje" style="display:none;">No se pudieron crear las carpetas FTP</div>
        <div class="bg-danger with-padding block-inner danger-folder-content mensaje" style="display:none;">El contenido Remoto no se ha podido crear</div>
        <div class="bg-danger with-padding block-inner danger-folder mensaje" style="display:none;">No se crearon las carpetas</div>
        <form action="#" id="corporativo" name="corporativo">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="client">cliente:</label>
                    <input class="form-control" type="text" name="client" id="client" required="">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="branch">Sucursal:</label>
                    <input class="form-control" type="text" name="branch" id="branch" required="">
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    
  $("#services").select2();
  $("#orientacion").select2();
  
  $('.addServiceButton').click(function(){
      if($('#addService').val() != ''){
            service = $('#addService').val();
            
            service = service.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
            
            valService =  service.substr(0, 3);
            
            
            $('#services').append('<option value="' + valService + '" selected="">' + service + '</option>');
            $('#services').trigger('change');
      }
    
  });
  
  $('.corporativo').click(function(){
        $('.mensaje').fadeOut('slow');
        $('.corporativo').attr('disabled','disabled');
      
        $.ajax({
            url: '/publications/corporativo',
            type: 'post',
            data: $('#corporativo').serialize(),
             success: function(data){
                if(data == 'OK'){
                    $('.corporativo').removeAttr('disabled');
                    $('.success-folder').fadeIn('slow');
                }else if(data == 'ERROR FTP'){
                    $('.corporativo').removeAttr('disabled');
                    $('.danger-folder-ftp').fadeIn('slow');
                }else if(data == 'NO_CONTENT'){
                    $('.corporativo').removeAttr('disabled');
                    $('.danger-folder-content').fadeIn('slow');
                }else{
                     $('.corporativo').removeAttr('disabled');
                    $('.danger-folder').fadeIn('slow');
                }
           }
        });
  });
  
  
  
</script>