
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Centro de Publicación <small></small></h3>
    </div>
</div>
<!-- /page header -->

<div class="panel panel-default">
       
    <div class="panel-body">
        <div class="bg-success with-padding block-inner success-folder mensaje" style="display:none;">Se crearon las carpetas! (DB, FTP, MI)</div>
        <div class="bg-danger with-padding block-inner danger-folder-ftp mensaje" style="display:none;">No se pudieron crear las carpetas FTP</div>
        <div class="bg-danger with-padding block-inner danger-folder-content mensaje" style="display:none;">El contenido Remoto no se ha podido crear</div>
        <div class="bg-danger with-padding block-inner danger-folder mensaje" style="display:none;">No se crearon las carpetas</div>
        <form action="#" id="generateFolder" name="generateFolder">
        <div class="col-md-12">
            <div class="form-group">
                <label for="client">cliente:</label>
                <input class="form-control" type="text" name="client" id="client" required="">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="branch">Sucursal:</label>
                <input class="form-control" type="text" name="branch" id="branch" required="">
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="services">Servicio:</label>
                <select name="services[]" id="services" multiple="" style="width:100%;" required="" >
                    <option value="selectBranch">Seleccione Sucursal</option>
                    <option value="Alm">Almuerzo</option>
                    <option value="Cen">Cena</option>
                    <option value="Des">Desasyuno</option>
                    <option value="Onc">Once</option>
                    <option value="Cho">Choca</option>
                    <option value="Chob">Choca B (Chob)</option>
                    <option value="Choc">Choca C (Choc)</option>
                    <option value="Cen_Tar">Cena Tarde(Cen_Tar)</option>
                    <option value="CenTa">Cena Tarde(CenTa)</option>
                    <option value="CenMa">Cena Madrugada(CenMa)</option>
                    <option value="Cen_Noc">Cena Noche</option>
                    <option value="Noc">Nochera (Noc)</option>
                    <option value="Com">Comida (Com)</option>
                     <option value="CenNoc">Cena Nochera (CenNoc)</option>
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="orientacion">orientación :</label>
                <select name="orientacion[]" id="orientacion"  multiple="" style="width:100%;" required="">
                    <option value="Arr">Arriba</option>
                    <option value="Aba">Abajo</option>
                    <option value="Izq">Izquierda</option>
                    <option value="Der">Derecha</option>
                    <option value="ArrIzq">Arriba Izquierda</option>
                    <option value="ArrDer">Arriba Derecha</option>
                    <option value="IzqAba">Abajo Izquierda</option>
                    <option value="DerAba">Abajo Derecha</option>
                    <option value="Slide">Slide</option>
                    
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="branch">Año:</label>
                <input class="form-control" type="text" name="ano" id="ano" required="">
            </div>
        </div>
            
        <div class="col-md-12">
            <div class="form-group">
                <label for="branch">Mes min:</label>
                <input class="form-control" type="text" name="mes_min" id="mes_min" required="">
            </div>
        </div>
        
        <div class="col-md-12">
            <div class="form-group">
                <label for="branch">Mes max:</label>
                <input class="form-control" type="text" name="mes_max" id="mes_max" required="">
            </div>
        </div>
            
        <div class="clearfix"></div>
        <div class="col-md-12">
            <div class="form-group">
                <button class="btn btn-primary generateFolder" type="button">Generar Carpetas!</button>
            </div>  
        </div>
            
        </form>
    </div>
        
</div>

 


<script>
    
  $("#services").select2();
  $("#orientacion").select2();
  
  $('.addServiceButton').click(function(){
      if($('#addService').val() != ''){
            service = $('#addService').val();
            
            service = service.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
            
            valService =  service.substr(0, 3);
            
            
            $('#services').append('<option value="' + valService + '" selected="">' + service + '</option>');
            $('#services').trigger('change');
      }
    
  });
  
  $('.generateFolder').click(function(){
        $('.mensaje').fadeOut('slow');
        $('.generateFolder').attr('disabled','disabled');
      
        $.ajax({
            url: '/publications/generateYearFolder',
            type: 'post',
            data: $('#generateFolder').serialize(),
             success: function(data){
                if(data == 'OK'){
                    $('.generateFolder').removeAttr('disabled');
                    $('.success-folder').fadeIn('slow');
                }else if(data == 'ERROR FTP'){
                    $('.generateFolder').removeAttr('disabled');
                    $('.danger-folder-ftp').fadeIn('slow');
                }else if(data == 'NO_CONTENT'){
                    $('.generateFolder').removeAttr('disabled');
                    $('.danger-folder-content').fadeIn('slow');
                }else{
                     $('.generateFolder').removeAttr('disabled');
                    $('.danger-folder').fadeIn('slow');
                }
           }
        });
  });
  
  
  
</script>