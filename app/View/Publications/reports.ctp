
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Reportes<small></small></h3>
    </div>
</div>
<!-- /page header -->


<!-- /breadcrumbs line -->
<div style="margin-bottom: 35px;margin-right:16px;">
    <a class="pull-right" href="/publications/reportsDownload" style="margin-bottom: 10px;">Descargar en CSV  <i class="icon-download4"></i></a>
</div>
<?php foreach ($publications as $datePublication => $publication) { ?>         
    <?php foreach ($publication as $modulePublication){  ?>
    <div class="panel publications panel-default panel-success" style="margin-bottom: 20px">
        <div class="panel-heading">
            <h5 class="panel-title" style="font-size: 13px;">
                <?= $datePublication ?> - <?=count($modulePublication['publish']) ?> <?php if(count($modulePublication['publish']) > 1){ echo "Publicaciones"; } else {echo "Publicación";}?> en <?= $modulePublication['displayName'] ?>
            </h5>
        </div>
        
        <div class="callout callout-info callout-edit fade in">
            <ul class="nav nav-pills nav-justified">
                <li class=""><a href="#"><i class="icon-calendar4"></i> Cantidad de ediciones para publicación de hoy <span class="label label-default"><?=count($modulePublication['today'])?></span></a></li>
                <li class=""><a href="#"><i class="icon-calendar"></i> Cantidad de ediciones para Publicaciones Futuras <span class="label label-default"><?=count($modulePublication['future'])?></span></a></li>                                
            </ul>
        </div>
    </div>
    <?php } ?>
<?php } ?>

<?php if($pages > 1) { ?>
<div class="block text-center">
    <ul class="pagination">
        <?php if($page > 1) { ?>
        <li><a href="/publications/reports/<?=($page - 1)?>">←</a></li>
        <?php } ?>
        <?php for($i = 1; $i <= $pages; $i++) { ?>
        <li <?php if($page == $i) { ?>class="active"<?php } ?>><a href="/publications/reports/<?=$i?>"><?=$i?></a></li>  
        <?php } ?>
        <?php if($page < $pages) { ?>
        <li><a href="/publications/reports/<?=($page + 1)?>">→</a></li>
        <?php } ?>        
    </ul>
</div>
<?php } ?>
<script>
    
   
</script>
<style>
    .callout-edit { margin-bottom: 0px !important; border-left: 0px !important;}
    
</style>