<div class="modal-body with-padding">
  <?php
  if($status['success']) { 
    $multiple_pub = constant("MULTIPLE_PUB");
    $caption = 'Publicacion en Proceso';

    $text = 'el proceso se esta ejecutando.';

  ?>
      <div class="alert alert-block alert-info fade in block-inner">
        <h6><i class="icon-screen"></i> Procesando</h6>
        <hr>
        <p>El reporte de casinos de Salud ha sido procesado.</p>
        <div class="text-left">
          <a class="btn btn-default" href="#" data-dismiss="modal">Cerrar</a>
        </div>
      </div>
  <?php
    }
  ?>
</div>

<script>

    $(document).ready(function() {

        $('#visual-loader').hide();

        $('.alert-<?=$clientFolder.'-'.$moduleKey?>').remove();

    });

</script>
