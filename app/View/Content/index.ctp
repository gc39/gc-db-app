<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.13.1/underscore-min.js"></script>
<!-- Page header --> 
<div class="page-header">
    <div class="page-title">
        <h3><i class="<?=$modulesMenu[$moduleKey]['icon']?>" style="font-size: 24px;margin-top: -4px;"></i> <?=$viewTitle?> <small></small></h3>
    </div>
</div>

<!-- /page header -->

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">

    <ul class="breadcrumb">
        <li><a href="/">Home..,</a></li>
        <li class='active'><?=$viewTitle?></li>
    </ul>

</div>
<!-- /breadcrumbs line -->

<?php
echo $this->Session->flash();

$boton_procesar = false;
if (substr($moduleKey, -21) == 'reportes_modificacion'){
    $boton_procesar = true;
}

$boton_procesar_reportecorporativo = false;
if (substr($moduleKey, -13) == 'reportes_corp'){
    $boton_procesar_reportecorporativo = true;
}

$boton_procesar_reportemineria = false;
if (substr($moduleKey, -16) == 'reportes_mineria'){
    $boton_procesar_reportemineria = true;
}

$boton_procesar_reportesalud = false;
if (substr($moduleKey, -14) == 'reportes_salud'){
    $boton_procesar_reportesalud = true;
}

if($modulesMenu[$moduleKey]['button_add'] === 'false')
    $modulesMenu[$moduleKey]['button_add'] = false;
if($modulesMenu[$moduleKey]['allow_import'] === 'false')
    $modulesMenu[$moduleKey]['allow_import'] = false;
if($modulesMenu[$moduleKey]['publish_url'] === '')
    $modulesMenu[$moduleKey]['publish_url'] = true;
if($modulesMenu[$moduleKey]['publish_url'] === 'false')
    $modulesMenu[$moduleKey]['publish_url'] = false;
if($modulesMenu[$moduleKey]['justificar_borrado'] === 'true')
    $modulesMenu[$moduleKey]['justificar_borrado'] = 'true';
else
    $modulesMenu[$moduleKey]['justificar_borrado'] = 'false';


// Elimina de la lista de motivos 'No existe en el maestro producto'
$motivosListTmp = array();
foreach($motivosList as $motivo ){
    if(strpos($motivo[1], "Maestro") == false){
        $motivosListTmp[] = $motivo;
    }
}
$motivosList = $motivosListTmp;

?>

<?php
    // Cambio aplicado especificamente para el Cliente Cruz Verde
    if ($moduleKey != 'publish') {
?>

<?php
  //  if($clientFolder == 'demo'){file_exists ( string $filename )
        foreach ($sessionModuleEdit as $edit ){
            if($edit == $module){
                if($module != "maestro_productos"){
                    ?>
                    <div class="alert alert-info fade in alert-<?=$clientFolder.'-'.$module?>">
                        <i class="icon-info"></i>Existen cambios sin publicar. Cuando finalice todos los cambios publique
                    </div><br />
                    <?php 
                }
            }
        }
  //  }
?>
<?php if(!empty($modulesMenu[$moduleKey]['advice'])) { ?>

    <div class="alert alert-danger fade in">
        <i class="icon-info"></i> <?=$modulesMenu[$moduleKey]['advice']?>
    </div>
<?php } ?>

<div id="integrity-errors" class="alert alert-info fade in hide">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <i class="icon-info"></i> Existen <span></span> errores de integridad, <a id="show-all-rows" href="#" style="display: none">volver a ver todos los elementos</a> <a id="show-integrity-errors" href="#">ver solo elementos con errores</a>.
</div>

<div id="action-bar">

    <?php
    if(($userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array($moduleKey, $userLogged['User']['modules']['edit']))  && !$modulesMenu[$moduleKey]['no_editable']) { ?>
    <?php if($modulesMenu[$moduleKey]['button_add']!=false) { ?>
    <a type="button" class="btn btn-success pull-left" href="/<?=$moduleKey.'/edit/new'?>"><i class="icon-plus"></i> Agregar</a>
    <?php } ?>

    <?php if($moduleKey == 'config_casinos') { ?>
    <a type="button" class="btn btn-success pull-left" href="/<?=$moduleKey.'/createCasino'?>"><i class="icon-plus"></i> Agregar</a>
    <?php } ?>

    <?php } ?>

    <button type="button" class="hide btn btn-default <?php if($isSearch) { ?>activated<?php }?>" id="toggle-filters"><i class="icon-search3"></i> <?php if($isSearch) { ?>Desactivar<?php } else { ?>Activar<?php } ?> Filtros</button>

    <?php if(($userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array($moduleKey, $userLogged['User']['modules']['edit']))) { ?>
        <a id="duplicate-content" class="hide btn btn-icon btn-default  pull-left" style="margin-left: 5px;" data-toggle="modal" href="#duplicate-modal"><i class="icon-copy"></i></a>
        <?php if($modulesMenu[$moduleKey]['justificar_borrado']==='true') { ?>
            <a id="delete-content" type="button" class="hide btn btn-icon btn-default  pull-left" style="margin-left: 5px;" data-toggle="modal" href="#delete-modal-motivo"><i class="icon-remove2"></i></a>
        <?php }else{?>
            <a id="delete-content" type="button" class="hide btn btn-icon btn-default  pull-left" style="margin-left: 5px;" data-toggle="modal" href="#delete-modal"><i class="icon-remove2"></i></a>
        <?php }?>
    <?php }?>

    <?php if ($userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array($moduleKey, $userLogged['User']['modules']['edit'])) {?>
        <?php if($modulesMenu[$moduleKey]['allow_import']) { ?>
        <a data-toggle="modal" class="btn btn-default pull-left" href="#import-csv" style="margin-left: 5px;" ><i class="icon-upload"></i> Importar</a>
        <?php } ?>
        <?php if($modulesMenu[$moduleKey]['allow_import_xls']) { ?>
        <a data-toggle="modal" class="btn btn-default pull-left" href="#import-xls" style="margin-left: 5px;" ><i class="icon-upload"></i> Importar</a>
        <?php } ?>
    <?php }?>

    <?php if($modulesMenu[$moduleKey]['download_csv']|| in_array($moduleKey, $userLogged['User']['modules']['edit']) ) { ?>
        <a data-toggle="modal" class="btn btn-default pull-left" href="/<?=$moduleKey?>/downloadCSV" style="margin-left: 5px;" ><i class="icon-file-excel"></i> Descargar CSV</a>
    <?php } ?>
    <?php 
    if($modulesMenu[$moduleKey]['export_excel'] and $userLogged['User']['rol'] == 'PLATFORM_ADMIN') { ?>
        <a data-toggle="modal" class="btn btn-default pull-left" href="/<?=$moduleKey?>/downloadReportCSV" style="margin-left: 5px;" ><i class="icon-file-excel"></i> Descargar Excel</a>
    <?php } ?>

    <?php if($moduleKey == 'config_modulos') { ?>
    <a type="button" class="btn" href="/tools/reset"></i>Resetear</a>
    <?php } ?>

    <div class="col-xs-3">
        <div class="input-group">
            <input id="search" name="search" type="text" class="form-control" value="<?=$searchTerm?>">

            <span class="input-group-btn">
                <button class="btn btn-primary btn-icon" type="button" id="search-action"><i class="icon-search3"></i></button>
            </span>

        </div>
    </div>


    <?php

    if(!isset($modulesMenu[$moduleKey]['download_mode']) || $modulesMenu[$moduleKey]['download_mode'] == false) {
        if( $userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array( $moduleKey, $userLogged['User']['modules']['publish'])) {
        ?>

        <?php if($modulesMenu[$moduleKey]['publish_url']) { ?>
        <button id="publish-button" type="button" class="btn btn-primary pull-right" style="margin-left:4px"  data-toggle="modal"  href="/<?=$moduleKey?>/makePublish?v=<?=time()?>&u=manual" data-target="#publish_modal" onclick="javascript: $('#visual-loader').show();<?php if($modulesMenu[$moduleKey]['refresh_on_publish']) echo 'location.reload()'; ?>"><i class="icon-screen"></i> Publicar</button>
        <?php } ?>

        <?php if($boton_procesar) { ?>
            <button id="publish-button" type="button" class="btn btn-primary pull-right" style="margin-left:4px"  data-toggle="modal"  href="/<?=$moduleKey?>/makeProcess?v=<?=time()?>&u=manual" data-target="#process_modal" onclick="javascript: $('#visual-loader').show();<?php if($modulesMenu[$moduleKey]['refresh_on_publish']) echo 'location.reload()'; ?>"><i class="icon-file"></i> Procesar</button>
        <?php } ?>   

        <?php if($boton_procesar_reportecorporativo) { ?>
            <button id="publish-button" type="button" class="btn btn-primary pull-right" style="margin-left:4px"  data-toggle="modal"  href="/<?=$moduleKey?>/makeProcessCorporativo?v=<?=time()?>&u=manual" data-target="#process_modal" onclick="javascript: $('#visual-loader').show();<?php if($modulesMenu[$moduleKey]['refresh_on_publish']) echo 'location.reload()'; ?>"><i class="icon-file"></i> Procesar</button>
        <?php } ?>

        <?php if($boton_procesar_reportemineria) { ?>
            <button id="publish-button" type="button" class="btn btn-primary pull-right" style="margin-left:4px"  data-toggle="modal"  href="/<?=$moduleKey?>/makeProcessMineria?v=<?=time()?>&u=manual" data-target="#process_modal" onclick="javascript: $('#visual-loader').show();<?php if($modulesMenu[$moduleKey]['refresh_on_publish']) echo 'location.reload()'; ?>"><i class="icon-file"></i> Procesar</button>
        <?php } ?>

        <?php if($boton_procesar_reportesalud) { ?>
            <button id="publish-button" type="button" class="btn btn-primary pull-right" style="margin-left:4px"  data-toggle="modal"  href="/<?=$moduleKey?>/makeProcessSalud?v=<?=time()?>&u=manual" data-target="#process_modal" onclick="javascript: $('#visual-loader').show();<?php if($modulesMenu[$moduleKey]['refresh_on_publish']) echo 'location.reload()'; ?>"><i class="icon-file"></i> Procesar</button>
        <?php } ?>
        
        <?php } ?>

        <?php

        if(isset($modulesMenu[$moduleKey]['preview_url'])) {
            

            if(!isset($modulesMenu[$moduleKey]['preview_url']['url'])) {
                if($modulesMenu[$moduleKey]['preview_url'] != 'false'){?>

            <button type="button" class="btn btn-primary pull-right" data-toggle="modal" role="button" href="/<?=$moduleKey?>/makePreview" data-target="#publish_modal" onclick="javascript: $('#visual-loader').show();"><i class="icon-eye7"></i> Previsualizar</button>

            <?php }} else {?>

                <div class="btn-group pull-right">
                    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" style="width: 180px;">Previsualizar <span class="caret"></span></button>
                    <ul class="dropdown-menu icons-right">

                        <?php foreach($modulesMenu[$moduleKey]['preview_url']['url'] as $key => $url) {?>

                            <li><a  data-toggle="modal"  href="/<?=$moduleKey?>/makePreview/?urlIndex=<?=$key?>" data-target="#publish_modal" onclick="javascript: $('#visual-loader').show();"><i class="icon-eye7"></i> <?=$url['@label']?></a></li>

                        <?php }?>
                    </ul>
                </div>

            <?php
            }

        }

    } else {?>
        <button type="button" class="btn btn-primary pull-right" style="margin-left:4px"  data-toggle="modal"  href="/<?=$moduleKey?>/makePreview" data-target="#publish_modal" onclick="javascript: $('#visual-loader').show();"><i class="icon-file-download"></i> Previsualizar Y Descargar</button>
    <?php }

    ?>

    <div class="clearfix"></div>
</div>

<?php
    $current_url = $_SERVER["REQUEST_URI"];
    $current_url_array = explode('/', $current_url);
    $current_url = $current_url_array[1];
    $modulesMenu_corporativo_total = 0;

    foreach($modulesMenu_corporativo as $module_array) {
        if (strpos($current_url, $module_array[1]) !== false){
            $modulesMenu_corporativo_total++;
        }
    }
?>

<?php
if($isSearch) {
?>
<br>
<p><b>Buscando "<?=$searchTerm?>"</b> [<a href="/<?=$moduleKey?>">Limpiar Busqueda</a>]</p>
<?php } ?>
<div class="panel panel-default">
    <div class="table-responsive with-horizontal-scroll">

        <table id="content-list" class="table table-bordered">
            <thead>
                <tr>
                    <th width="10" style="vertical-align:bottom">
                        <?php
                            if(!$modulesMenu[$moduleKey]['no_checkbox']) {
                        ?>
                            <input id="toggle-select-all" type="checkbox">
                        <?php
                            }
                        ?>
                    </th>
                    <?php
                    if($customFields) {

                        $orderDirectionLink = $orderDirection == 'ASC' ? 'DESC' : 'ASC';
                        $orderDirectionIcon = $orderDirection == 'ASC' ? 'icon-arrow-up2' : 'icon-arrow-down2';

                        foreach($customFields as $header) {

                            if($header['@visible-in-list'] == 'true') {

                                $colums[$header['@key']] = $header['@label'];

                                $orderIconVisibleClass = $orderBy == $header['@key'] ? '' : 'hide';

                                if($header['@type'] != 'image') {
                                ?>
                                <th width="<?=$header['@column-width']?>"><a href="/<?=$moduleKey?>/<?=$page?>?orderBy=<?=$header['@key']?>&order=<?=$orderDirectionLink?>&s=<?=$searchTerm?>"><?=$header['@label']?> <i class="<?=$orderDirectionIcon?> pull-right <?=$orderIconVisibleClass?>"></i></a></th>
                                <?php
                                } else {
                                ?>
                                <th width="<?=$header['@column-width']?>"><?=$header['@label']?></th>
                                <?php
                                }
                            }
                        }
                    }


                    ?>
                    <th></th>

                </tr>
            </thead>
            <tbody>

                <?php

                $line_restriction = ['reportes_corp','reportes_mineria','reportes_salud'];
                $restringir_reporte = false;

                if (in_array($module, $line_restriction) )
                {
                    $restringir_reporte = true;
                }

                if($contentList) {

                foreach($contentList as $content) { 

                    if(!$restringir_reporte || ($restringir_reporte && $content['Content']['accion'] != 'upload' && $content['Content']['accion'] != 'Publicar' && $content['Content']['accion'] != 'codigo no existe')){

                        $contentID = $content['Content']['id'];
                ?>
                <tr data-id="<?=$contentID?>">
                    <td>
                        <?php
                            if(!$modulesMenu[$moduleKey]['no_checkbox']) {
                        ?>
                            <input id="content-<?=$contentID?>" class="input-selected" type="checkbox">
                        <?php
                            }
                        ?>
                    </td>
                    <?php
                    foreach($customFields as $fieldKEY => $field) {

                        $validationRelatedErrors = false;

                        if($field['@visible-in-list'] == 'true') {


                            if($field['@type'] == 'static-image') {
                                $bgColor = isset($field['@column-bg-color']) ? 'background-color: '.$field['@column-bg-color'] : '';
                                echo '<td style="'.$bgColor.'"><img src="/files/'.$this->Image->getStaticFilename($content['Content'][$field['@key']]).'" class="view product-thumb" data-image-id="'.$content['Content'][$field['@key']].'" /></td>';

                            } elseif($field['@type'] == 'image') {
                                $bgColor = isset($field['@column-bg-color']) ? 'background-color: '.$field['@column-bg-color'] : '';
                                echo '<td style="'.$bgColor.'"><img src="/files/'.$this->Image->getFilename($content['Content'][$field['@key']]).'" class="view product-thumb" data-image-id="'.$content['Content'][$field['@key']].'" /></td>';
                            } elseif($field['@type'] == 'date') {

                                $contentShow = '';

                                if(isset($field['@no-transform-to-mysql']) && $field['@no-transform-to-mysql'] == true) {
                                    $contentShow = $content['Content'][$field['@key']];
                                } else {
                                    if(!empty($content['Content'][$field['@key']])) {
                                        $dateParts   = explode('-', $content['Content'][$field['@key']]);
                                        $contentShow = count($dateParts) == 3 ? $dateParts[2].'-'.$dateParts[1].'-'.$dateParts[0] : '';
                                    }
                                }

                                echo '<td>'.$contentShow.'</td>';


                            } elseif($field['@type'] == 'tmp_code') {
                                echo '<td>'.$content['Content'][$field['@key']].'</td>';
                                ?>
                                <?php
                            } elseif($field['@type'] == 'select' && !isset($field['@multiple'])) {

                                if($field['@multiple']){

                                        $optionsExplode = explode("|#|", $content['Content'][$field['@key']]);
                                        $contShow = [];

                                        foreach($field['option'] as $option ){
                                            foreach ($optionsExplode as $opt){
                                                if(strtolower($opt) == strtolower($option['@value'])) {
                                                    $contShow[] = $option['@'];
                                                }
                                            }
                                        }

                                        echo '<td>' . implode("<br>", $contShow) . '</td>';

                                }else{
                                    $contentShow = '';

                                    foreach($field['option'] as $option ){

                                        if(strtolower($content['Content'][$field['@key']]) == strtolower($option['@value'])) {

                                            $contentShow = $option['@'];
                                        }
                                    }

                                    echo '<td>'.$contentShow.'</td>';
                                }

                            } elseif(isset($field['@box']) && $field['@box'] == 'false') {
                                
                                if($field['@multiple']){

                                    $optionsExplode = explode("|#|", $content['Content'][$field['@key']]);
                                    $contShow = [];

                                    foreach($field['option'] as $option ){
                                        foreach ($optionsExplode as $opt){
                                            if(strtolower($opt) == strtolower($option['@value'])) {
                                                $contShow[] = $option['@'];
                                            }
                                        }
                                    }

                                    echo '<td>' . implode("<br>", $contShow) . '</td>';

                                }else{
                                    $contentShow = '';

                                    foreach($field['option'] as $option ){

                                        if(strtolower($content['Content'][$field['@key']]) == strtolower($option['@value'])) {

                                            $contentShow = $option['@'];
                                        }
                                    }

                                    echo '<td>'.$contentShow.'</td>';
                                }

                            } elseif(isset($field['@rendermodules']) && $field['@rendermodules'] == 'true') {
                                $contentShow = $content['Content'][$field['@key']];

                                $optionsExplode = explode("|#|", $content['Content'][$field['@key']]);
                                $contShow = [];

                                echo '<td>';
                                echo '<div class="select2-container select2-container-multi" id="s2id_orientacion" style="width:100%;"><ul class="select2-choices" style="border: 0px">';

                                $cuenta_local = 0;

                                foreach($optionsExplode as $key_casino ){
                                    if ($key_casino != ''){
                                        echo '<li class="select2-search-choice" style="cursor: pointer" onclick="call_casino(\''.$key_casino.'\')"><div>';
                                        echo $key_casino;
                                        echo '</div></li>';
                                        $cuenta_local++;
                                    }
                                }

                                echo '<li><div style="color: #428bca;padding: 7px 12px 8px 26px;font-weight: bold;font-size: medium;">';
                                echo $cuenta_local;
                                echo ' de ';
                                echo $modulesMenu_corporativo_total;
                                echo '</div></li>';

                                echo '</ul></div>';
                                echo '</td>';

                            } elseif($field['@type'] == 'video-upload') {

                                if($content['Content'][$field['@key']] != '') {
                                    echo '<td><a id="view-video" href="/gallery/viewVideo?video='.$content['Content'][$field['@key']].'" class="open-media" data-toggle="modal" role="button"  data-target="#remote_modal_video" style="margin-left:5px;margin-top:5px"><i class="icon-eye3"></i> </a></td>';
                                } else {
                                    echo '<td></td>';
                                }

                            } else {

                                $contentShow = $content['Content'][$field['@key']];

                                if(isset($field['@related-to-id']) && isset($field['@related-to-show']) ) {

                                    $relatedParts = explode('.', $field['@related-to-id']);
                                    $relatedTable = isset($relatedParts[0]) ? $relatedParts[0] : false;
                                    $relatedField = isset($relatedParts[1]) ? $relatedParts[1] : false;

                                    $relatedParts     = explode('.', $field['@related-to-show']);
                                    $relatedShowTable = isset($relatedParts[0]) ? $relatedParts[0] : false;
                                    $relatedShowField = isset($relatedParts[1]) ? $relatedParts[1] : false;

                                    if($relatedTable && $relatedField) {

                                        $validate_related_fields = false;

                                        $contentRelated =  $this->Related->getRelated($relatedTable, $content['Content'][$field['@key']], $relatedField);

                                        if(isset($field['@validate-related-fields']))  {

                                            $validate_related_fields = explode('|', $field['@validate-related-fields']);

                                            foreach ($validate_related_fields as $validateField) {
                                                if(!isset($contentRelated[0][$relatedTable][$validateField]) || empty($contentRelated[0][$relatedTable][$validateField])) {
                                                    $validationRelatedErrors[] = $validateField;
                                                }
                                            }

                                        }

                                        $linkRelatedID = isset($contentRelated[0][$relatedShowTable]['id']) ? $contentRelated[0][$relatedShowTable]['id'] : '#';
                                        $contentRelated = isset($contentRelated[0][$relatedShowTable][$relatedShowField]) ? $contentRelated[0][$relatedShowTable][$relatedShowField] : '------';

                                        if($contentRelated != '------') {
                                            $contentShow    = '[<a>'.$contentShow.'</a>] '.$contentRelated;
                                        } else {
                                            $contentShow    = '['.$contentShow.'] <b>No Encontrado</b>';
                                        }

                                    }

                                }

                                $errorClass = $validationRelatedErrors != false ? 'HAVE_ERROR' : '';
                                echo '<td>'.$contentShow.'</td>';
                            }
                        }
                    }

                    ?>
                    <td align="right" class="<?=$errorClass?>">
                        <?php
                        if(($userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array($moduleKey, $userLogged['User']['modules']['edit']))  && !$modulesMenu[$moduleKey]['no_editable']) {
                        ?>
                            <?php 
                                if ($userLogged['User']['rol'] == 'PLATFORM_ADMIN' && $moduleKey == 'cronjob_modulos'){
                                    echo '<a type="button" class="btn" target="_blank" data-target="#publish_modal" rel="noopener noreferrer" style="margin-right: 30px" href="./executejob.php?cliente_sucursal='.$content['Content']['cliente_sucursal'] . '">ejecutar</a>';
                                }
                            ?>

                            <?php 
                            if ($moduleKey == 'config_casinos') {?>

                                <a href="/<?=$moduleKey?>/editcasino/<?=$contentID?>"><i class="icon-pencil3"></i></a>

                            <?php }
                                else {
                            ?>

                            <?php if($searchTerm){ ?>
                                <a href="/<?=$moduleKey?>/edit/<?=$contentID?>/?s=<?=$searchTerm?>"><i class="icon-pencil3"></i></a>
                            <?php }else{ ?>
                                <a href="/<?=$moduleKey?>/edit/<?=$contentID?>"><i class="icon-pencil3"></i></a>
                            <?php }?>

                            <?php }?>


                        <?php }?>
                    </td>

                </tr>
                <?php
                        } 
                    }
                }?>
            </tbody>
        </table>

    </div>

</div>

<?php if($pages > 1) { ?>
<div class="block text-center">
    <ul class="pagination">
        <? if($page > 1) { ?>
        <li><a href="/<?=$moduleKey?>/<?=($page - 1).$queryString?>">←</a></li>
        <? } ?>
        <? for($i = 1; $i <= $pages; $i++) { ?>
        <li <?php if($page == $i) { ?>class="active"<?php } ?>><a href="/<?=$moduleKey?>/<?=$i.$queryString?>"><?=$i?></a></li>
        <? } ?>
        <? if($page < $pages) { ?>
        <li><a href="/<?=$moduleKey?>/<?=($page + 1).$queryString?>">→</a></li>
        <? } ?>
    </ul>
</div>
<?php } ?>

<?php } ?>
<?php
if ($moduleKey == 'publish') {
?>
    <button type="button" class="btn btn-primary pull-right" style="margin-left:4px"  data-toggle="modal"  href="/<?=$moduleKey?>/makeSync" data-target="#publish_modal" onclick="javascript: $('#visual-loader').show();"><i class="icon-screen"></i> Sincronizar con Servidor Remoto</button>
    <br><br><br><br>
<?php
}
?>

<!-- Modal with remote path -->
<div id="publish_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>

<div id="process_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>

<!-- Modal with remote path -->
<div id="remote_modal_video" class="modal fade" tabindex="-1" role="dialog" >
    <div class="modal-dialog" style="width: 500px">
        <div class="modal-content">

        </div>
    </div>
</div>

<!-- Modal with remote path -->
<div id="send_notifications_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- modal with remote path -->

<div id="import-csv" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form enctype="multipart/form-data" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Importar CSV</h4>
                </div>

                <div class="modal-body with-padding">

                    <div class="alert alert-block alert-info fade in block-inner">
                        <?php

                            foreach($modulesMenu[$moduleKey]['import_map']['map'] as $column) {

                                if(strtolower($column['@column']) != '_db_order') {
                                    $optional = $column['@optional'] == 'yes' ? ' (Opcional)' : '';
                                    $additional_info = isset($column['@additional-info']) ? ' '.$column['@additional-info'] : '';
                                    $columns[] = $column['@column'].$additional_info.$optional;
                                }
                            }

                            $expectedColumns = implode(', ', $columns);

                        ?>
                        <p>Seleccione el archivo CSV para importar. Se esperan las siguientes columnas en cualquier orden: <?=$expectedColumns?>.</p>
                        <br>

                        <input type="hidden" name="import_from_csv" value="1">
                        <input type="file" name="file" id="files" class="styled"  onchange="onChange(event)" onclick="javascript: limpia(this);" onBlur="javascript: verifica(this);" required>
                        <br> <br>
                        <label for="field_separator">Separador de Columnas</label>
                        <select id="field_separator" name="field_separator" class="form-control">
                            <option value=";" selected="">";" (Punto y Coma)</option>
                            <option value=",">"," (Coma)</option>
                        </select>
                    </div>

					<div class="panel-group">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h6 class="panel-title panel-trigger">
									<a href="#question1" data-toggle="collapse">¿Cómo crear un <b>CSV</b>?</a>
								</h6>
							</div>
							<div class="panel-collapse collapse " id="question1" style="height: auto;">
								<div class="panel-body">
									<ul>
                                        <li><b>Paso 1</b>: Abrir el archivo en Excel</li>
										<li><b>Paso 2</b>: Click en menú "Archivo"</li>
										<li><b>Paso 3</b>: Click en menú "Guardar Como..."</li>
										<li><b>Paso 4</b>: Seleccionar tipo: CSV (delimitado por comas)</li>
                                        <li><b>Paso 5</b>: Seleccionar la codificación: "UTF-8"</li>
										<li><b>Paso 6</b>: Guardar</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
                </div>

                <div class="modal-footer">
					<label class="checkbox checkbox-primary pull-left">
						<input class="styled" type="checkbox" name="delete_all" value="1">
						Eliminar todos los datos ahora existentes
					</label>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <input type="submit" class="btn btn-primary" name="load_data" id="load_data" value="Importar" disabled>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="import-xls" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form enctype="multipart/form-data" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Importar XLS</h4>
                </div>

                <div class="modal-body with-padding">

                    <div class="alert alert-block alert-info fade in block-inner">
                        <?php
                            foreach($modulesMenu[$moduleKey]['import_map_xls']['map'] as $column) {
                                if(strtolower($column['@column']) != '_db_order') {
                                    $optional = $column['@optional'] == 'yes' ? ' (Opcional)' : '';
                                    $additional_info = isset($column['@additional-info']) ? ' '.$column['@additional-info'] : '';
                                    $columns[] = $column['@column'].$additional_info.$optional;
                                }
                            }
                            $expectedColumns = implode(', ', $columns);
                        ?>
                        <p>Seleccione el archivo XLS para importar. Se esperan las siguientes columnas en cualquier orden: <?=$expectedColumns?>.</p>
                        <br>

                        <input type="hidden" name="import_from_xls" value="1">
                        <input type="file" name="file" class="styled">
                    </div>
					<!---->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <input type="submit" class="btn btn-primary" value="ImportarXLS">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="duplicate-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form enctype="multipart/form-data" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Duplicar Contenido</h4>
                </div>

                <div class="modal-body with-padding">

                    <br>
                    <label>Seleccione donde duplicar...</label>

                    <select name="duplicate-destination" class="form-control">
                        <option value="">Seleccionar...</option>
                    <?php
                        foreach($availableDuplicateDestinations as $key => $duplicateDestination) {
                    ?>
                        <option value="<?=$key?>"><?=$duplicateDestination?></option>
                    <?php
                        }
                    ?>
                    </select>

                    <input type="hidden" name="id-row-duplicate" value="">
                    <input type="hidden" name="duplicate-action" value="1">

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <input type="submit" class="btn btn-primary" value="Duplicar">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="delete-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form enctype="multipart/form-data" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Eliminar Contenido</h4>
                </div>

                <div class="modal-body with-padding">

                    <p>¿Esta seguro que desea eliminar este contenido? Esta accion no  se puede deshacer</P>

                    <input type="hidden" name="id-row-delete" value="">
                    <input type="hidden" name="id-row-servicios" value="">
                    <input type="hidden" name="id-row-platos" value="">
                    <input type="hidden" name="tipo-platos" value="">
                    <input type="hidden" name="id-row-platos-temporales" value="">
                    <input type="hidden" name="delete-action" value="1">
                    <input type="hidden" name="s" value="<?=$_GET['s']?>">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <input type="submit" class="btn btn-primary" value="Eliminar">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="delete-modal-motivo" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form enctype="multipart/form-data" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Eliminar Contenido</h4>
                </div>

                <div class="modal-body with-padding">

                    <p>Indique el motivo para eliminar el plato</P>

                    <select name="select" style="font-size: 17px; width: 100%">
                        <?php
                        foreach($motivosList as $motivo ){?>
                        <option value="<?=$motivo['0'].':'.$motivo['1']?>"><?=$motivo['1']?></option>
                        <?php } ?>
                    </select>

                    <input type="hidden" name="id-row-delete" value="">
                    <input type="hidden" name="id-row-servicios" value="">
                    <input type="hidden" name="id-row-platos" value="">
                    <input type="hidden" name="id-row-platos-temporales" value="">
                    <input type="hidden" name="tipo-platos" value="">
                    <input type="hidden" name="fecha_exhib" value="">
                    <input type="hidden" name="delete-action-motivo" value="1">
                    <input type="hidden" name="s" value="<?=$_GET['s']?>">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <input type="submit" class="btn btn-primary" value="Eliminar">
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    var valid_error ='<table class="table"><thead><tr><th scope="col">#</th><th style="text-align:center"scope="col">CELDA</th><th style="text-align:center" scope="col">ERROR</th></tr></thead><tbody>';
    var error='';
    var codes_to_verify = [];           // codigos de plato para verificar
    var codes_to_verify_position = [];  // posicion de los codigos de plato para verificar

    function updateUrlCache() {
        $('#publish-button').attr('href', '/<?=$moduleKey?>/makePublish?v=' + Math.floor((Math.random() * 99999) + 1));
    }

    function limpia(elemento)
    {
        elemento.value = "";
    }
    function verifica(elemento)
    {
        if(elemento.value == "")
        elemento.value = "";
    }
    function isNumeric(str) {
        if (typeof str != "string") return false   
        return !isNaN(str) && 
                !isNaN(parseFloat(str)) 
    }

    function onChange(event) {
        var file = event.target.files[0];
        var reader = new FileReader();
        reader.readAsText(file, 'ISO-8859-1');
        reader.onload = function(event) {
            valid_error ='<table class="table"><thead><tr><th scope="col">#</th><th style="text-align:center"scope="col">CELDA</th><th style="text-align:center" scope="col">ERROR</th></tr></thead><tbody>';
            var error='';

            // El texto del archivo se mostrará por consola aquí
                //console.log(event.target.result);
            var data = event.target.result ;
            var csv_data = data.split(/\r?\n|\r/);
            if (csv_data[csv_data.length-1]=="") csv_data.pop();
            var contFila=1;
            var letra=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","U","V","W","X","Y","Z"];

            // corroborar la cantidad de platos
            // debe hacerse por servicio
            var tmp = [];
            var tmp_cargaminuta = [];
            for(var count = 1; count<csv_data.length; count++) {
                var tmp_record = csv_data[count].split(";");
                var tmp_servicio = tmp_record[0];
                var tmp_tipo = tmp_record[1];
                var tmp_codigo = tmp_record[2];
                var tmp_calorias = tmp_record[3];
                var tmp_fecha = tmp_record[4];

                var tmp_item = {};
                tmp_item.Servicio = tmp_servicio;
                tmp_item.Tipo = tmp_tipo;
                tmp_item.Codigo = tmp_codigo;
                tmp_item.Calorias = tmp_calorias;
                tmp_item.Fecha = tmp_fecha;

                tmp_cargaminuta.push(tmp_item);
            }
            var tmp_agrupacionxFecha = _.groupBy(tmp_cargaminuta, "Fecha");
            var tmp_fechas = Object.keys(tmp_agrupacionxFecha);
            var numFechas = Object.keys(tmp_agrupacionxFecha).length;

            codes_to_verify = []; // codigos de plato para verificar
            codes_to_verify_position = []; // posicion de los codigos de plato para verificar
            var agrupacionxFecha = {};

            // Luego de agrupar por fechas hay que agrupar por servicios
            for(var keyindex = 0; keyindex < tmp_fechas.length; keyindex++){
                var menu_fecha = tmp_agrupacionxFecha[tmp_fechas[keyindex]];
                agrupacionxFecha[tmp_fechas[keyindex]] = _.groupBy(menu_fecha, "Servicio");
            }

            for(var keyindex = 0; keyindex < tmp_fechas.length; keyindex++){
                // verificar las rectricciones para esta fecha
                var menu_fecha = agrupacionxFecha[tmp_fechas[keyindex]];

                // evaluar por servicio
                for(var index_servicio = 0; index_servicio < import_services.length; index_servicio++){
                    var tserv = import_services[index_servicio];

                    var grupoServicio = _.groupBy(menu_fecha[tserv], "Servicio");
                    var keyServicios = Object.keys(grupoServicio);
                    var numServicios = Object.keys(grupoServicio).length;

                    for(var keyservicio = 0; keyservicio < numServicios; keyservicio++){
                        var tmp_menu_servicio = grupoServicio[keyServicios[keyservicio]];

                        var group_tipo = _.groupBy(tmp_menu_servicio, "Tipo");
                        var keys_group_tipo = Object.keys(group_tipo);
                        var num_keys_group_tipo = Object.keys(group_tipo).length;

                        for(var index_key_group_tipo = 0; index_key_group_tipo < num_keys_group_tipo; index_key_group_tipo++){
                            var restrinccion_tipo = import_constrain[tserv][keys_group_tipo[index_key_group_tipo]];
                            var num_platos_menu = group_tipo[keys_group_tipo[index_key_group_tipo]].length;

                            if(restrinccion_tipo !== num_platos_menu){
                                // no cumple con las restricciones
                                var tmpmsg = 'deben haber '+restrinccion_tipo+' platos de ' + keys_group_tipo[index_key_group_tipo] + ' en el servicio de '+tserv+' para la fecha '+tmp_fechas[keyindex];
                                valid_error += '<tr> <th scope="row"></th><td></td> <td>'+tmpmsg+'</td></tr>';
                                error = 'Error';
                            }
                        }
                    }

                    // se aplica otro metodo para verificar que no falten platos
                    for(var keyservicio = 0; keyservicio < numServicios; keyservicio++){
                        var tmp_menu_servicio = grupoServicio[keyServicios[keyservicio]];

                        var group_tipo = _.groupBy(tmp_menu_servicio, "Tipo");
                        var keys = Object.keys(group_tipo);
                        var constrain_service = {};

                        Object.entries(group_tipo).map(itm => {
                            constrain_service[itm[0]] = itm[1].length;
                        });

                        if(!shallowEqual(import_constrain[keyServicios[keyservicio]], constrain_service)){
                            var tmpmsg = 'el menu cargado en el servicio de '+tserv+' para la fecha '+tmp_fechas[keyindex]+' esta incompleto';
                            valid_error += '<tr> <th scope="row"></th><td></td> <td>'+tmpmsg+'</td></tr>';
                            error = 'Error';
                        }
                    }
                }
            }

            for(var count = 0; count<csv_data.length; count++){
                var line_data = csv_data[count].trim();
                var cell_data = csv_data[count].split(";");
                for(var cell_count=0; cell_count<cell_data.length; cell_count++)
                {
                    var fil=count+1;
                    var col= letra[cell_count];
                    
                    if(count === 0)
                    {
                        var servicio = cell_data.indexOf("Servicio");
                        var tipo = cell_data.indexOf("Tipo");
                        var codigo = cell_data.indexOf("Codigo");
                        var calorias = cell_data.indexOf("Calorias");
                        var fecha = cell_data.indexOf("Fecha");
                    }
                    else
                    {
                        if(line_data == ""){
                            valid_error += '<tr> <th scope="row">'+contFila+'</th><td></td> <td>Línea vacía.</td></tr>';
                            error = 'Error';
                            contFila+=1;
                        }

                        if( cell_count == servicio || cell_count == tipo){

                        }
                        else if( cell_count == codigo){
                            var codigo_plato = cell_data[codigo];

                            if (codigo_plato.trim() == ''){
                                valid_error += '<tr> <th scope="row">'+contFila+'</th><td>'+col+fil+'</td> <td>Código faltante</td></tr>';
                                error = 'Error';
                                contFila+=1;
                            }else{
                                codes_to_verify.push(codigo_plato);
                                codes_to_verify_position.push(count);
                                // https://sodexo.digitalboard.app/Cliente_Sucursal_menu_diario/checkcode?code=1111
                            }
                            
                        }/*valida campo vacio*/
                        else if (cell_data[cell_count] == '') 
                        {
                            if (cell_count != calorias){
                                valid_error += '<tr> <th scope="row">'+contFila+'</th><td>'+col+fil+'</td> <td>Celda Vacia</td></tr>';
                                error = 'Error';
                            }
                            contFila+=1;
                        }
                        else if (cell_count == calorias) 
                        {
                            var cal = cell_data[calorias];
                            if (!isNumeric(cal)){
                                valid_error += '<tr> <th scope="row">'+contFila+'</th><td>'+col+fil+'</td> <td>Debe ser numérico</td></tr>';
                                error = 'Error';
                                contFila+=1;
                            }
                        }
                        else if (cell_count == fecha) 
                        {
                            diaActual = new Date();
                            var day = diaActual.getDate();
                            var month = diaActual.getMonth()+1;
                            var year = diaActual.getFullYear();
                            
                            //transformo las fechas y igualo los formatos dd-mm-yy
                            if(month < 10){ month = '0'+ month;}
                            fecha1  = day + '-' + month + '-' + year;
                            fecha2 = cell_data[cell_count];
                            
                            //Split de las fechas recibidas para separarlas
                            var x = fecha1.split('-');
                            var z = fecha2.split('-');
                            var z2 = fecha2.split('/');
                            
                            //Cambiamos el orden al formato americano, de esto dd-mm-yy a mm-dd-yy
                            fecha1 = x[1] + '-' + x[0] + '-' + x[2];
                            if(z2.length == 3 && z.length == 1){
                                fecha2 = z2[1] + '-' + z2[0] + '-' + z2[2];
                                z = z2;
                            }else{
                                fecha2 = z[1] + '-' + z[0] + '-' + z[2];
                            }
                            
                            //Comparamos las fechas
                            if (Date.parse(fecha1) <= Date.parse(fecha2) && z[2].length == 4 && z[1].length == 2 && z[0].length == 2){
                                //console.log(fecha1 + ' |<=| ' + fecha2);
                            }else{
                                valid_error += '<tr> <th scope="row">'+contFila+'</th><td>'+col+fil+'</td> <td>Fecha no cumple con formato(dd-mm-yyyy) o es anterior a la fecha de hoy | <h class="text-danger">'+cell_data[cell_count]+'</h></td></tr>';
                                error = 'Error';
                                contFila+=1;
                            }
                        }
                    }
                }
            }
                
            // var uriCheckMissingCodes = window.location.href+'/checkcode';
            // $.post( uriCheckMissingCodes, { values: codes_to_verify }, function( data ) {
            //     //success: function (data,status,xhr) {
            //     var error_array = JSON.parse(data);

            //     for(var idx_error = 0; idx_error < error_array.length; idx_error++){
            //         valid_error += '<tr><th scope="row"></th><td>C'+(codes_to_verify.indexOf(error_array[idx_error])+2).toString()+'</td> <td>El código de plato '+error_array[idx_error]+' no existe</td></tr>';
            //     }

                valid_error += '</tbody></table>';

                // if (error_array.length != 0)
                //     error = 'Error';
                
                //alerta y validacion final
                if (error != "")
                {
                    document.getElementById("load_data").disabled = true;
                    valid_error = '<strong>(Si tiene alguna duda comuníquese con soporte@grupoclan.cl)</strong>\n' + valid_error; 
                    Swal.queue([{
                        title: 'Te informamos que el archivo de carga contiene los siguientes errores: ' , 
                        width: 800,     
                        html: `
                        ${valid_error}`,						  
                        confirmButtonText: 'Aceptar',
                        showLoaderOnConfirm: true,

                    }])
                    //alert('Para continuar debe corregir los siguientes problemas: \n' +valid_error);
                }else{
                    document.getElementById("load_data").disabled = false;
                    console.log('Puede subir el csv');
                }

            //});
        };

        reader.readAsText(file);
    }

    // permite evaluar la igualdad entre objetos
    function shallowEqual(object1, object2) {
        const keys1 = Object.keys(object1);
        const keys2 = Object.keys(object2);
        if (keys1.length !== keys2.length) {
            return false;
        }
        for (let key of keys1) {
            if (object1[key] !== object2[key]) {
            return false;
            }
        }
        return true;
    }

    $(document).ready(function() {
        var integrity_errors = $('td.HAVE_ERROR').size();

        if(integrity_errors > 0) {
            $('#integrity-errors span').text(integrity_errors);
            $('#integrity-errors').removeClass('hide');
        }

        $('#show-integrity-errors').click(function() {

            $('#show-all-rows').show();
            $('#show-integrity-errors').hide();

            $('table tbody tr').each(function() {
                if($(this).find('td.HAVE_ERROR').size() == 0) {
                    $(this).hide();
                }
            })
        })

        $('#show-all-rows').click(function() {
            $('#show-all-rows').hide();
            $('#show-integrity-errors').show();
            $('table tr').show();
        })

        $('#duplicate-content').removeClass('hide');
        $('#delete-content').removeClass('hide');
        $('#duplicate-content').hide();
        $('#delete-content').hide();

        $("#toggle-select-all").change(function () {

            $("input.input-selected").prop('checked', $(this).prop("checked"));

            if($('.input-selected:checked').size() > 0 ) {
                $('#duplicate-content').show();
                $('#delete-content').show();
            } else {
                $('#duplicate-content').hide();
                $('#delete-content').hide();
            }

            var selectedIDs = new Array();
            //if menu diario

            //console.log('<?=substr($module, -11)?>');

            //IF para separar el seleccionar del menu diario
            if('<?=substr($module, -11)?>' === 'menu_diario')
            {
                
                var delServicios = new Array();
                var delPlatos = new Array();
                var tipoPlatos = new Array();
                var delFechaExhib = new Array();

                $('.input-selected:checked').each(function() {
                    id = $(this).attr('id');
                    id = id.split('-');
                    id = id[1];
                    selectedIDs.push(id);

                    nameServicio = $(this).parent().parent().children()[1].innerText;
                    delServicios.push(nameServicio);

                    namePlato = $(this).parent().parent().children()[3].innerText;
                    delPlatos.push(namePlato);

                    fechaExhib = $(this).parent().parent().children()[5].innerText;
                    delFechaExhib.push(fechaExhib);

                    tipoPlato = $(this).parent().parent().children()[2].innerText;
                    tipoPlatos.push(tipoPlato);
                });

                $('input[name="id-row-delete"]').val(selectedIDs.join(','));
                $('input[name="id-row-servicios"]').val(delServicios.join(','));
                $('input[name="id-row-platos"]').val(delPlatos.join(',')); 
                $('input[name="id-row-platos-temporales"]').val(delPlatosTemporales.join(',')); 
                $('input[name="tipo-platos"]').val(tipoPlatos.join(',')); 
                $('input[name="fecha_exhib"]').val(delFechaExhib.join(','));
                $('input[name="id-row-duplicate"]').val(selectedIDs.join(','));

            }else{

                $('.input-selected:checked').each(function() {
                    id = $(this).attr('id');
                    id = id.split('-');
                    id = id[1];
                    selectedIDs.push(id);
                });

                $('input[name="id-row-delete"]').val(selectedIDs.join(','));
                $('input[name="id-row-duplicate"]').val(selectedIDs.join(','));

            }
            



        })



        $('.input-selected').change(function() {

            if($('.input-selected:checked').size() > 0 ) {
                $('#duplicate-content').show();
                $('#delete-content').show();
            } else {
                $('#duplicate-content').hide();
                $('#delete-content').hide();
            }

            var selectedIDs = new Array();
            var delServicios = new Array();
            var delPlatos = new Array();
            var delPlatosTemporales = new Array();
            var tipoPlatos = new Array();
            var delFechaExhib = new Array();
            $('.input-selected:checked').each(function() {
                id = $(this).attr('id');
                id = id.split('-');
                id = id[1];
                selectedIDs.push(id);

                try{
                    nameServicio = $(this).parent().parent().children()[1].innerText;
                    delServicios.push(nameServicio);
                }
                catch(ex){
                }

                try{
                    tipoPlato = $(this).parent().parent().children()[2].innerText;
                    tipoPlatos.push(tipoPlato);
                }
                catch(ex){
                }

                try{
                    namePlato = $(this).parent().parent().children()[3].innerText;
                    delPlatos.push(namePlato);
                }
                catch(ex){
                }

                try{
                    fechaExhib = $(this).parent().parent().children()[5].innerText;
                    delFechaExhib.push(fechaExhib);
                }
                catch(ex){
                }

                try{
                    namePlatoTemporal = $(this).parent().parent().children()[6].innerText;
                    delPlatosTemporales.push(namePlatoTemporal);
                }
                catch(ex){
                }
            });

            $('input[name="id-row-delete"]').val(selectedIDs.join(','));
            $('input[name="id-row-servicios"]').val(delServicios.join(','));
            $('input[name="id-row-platos"]').val(delPlatos.join(','));
            $('input[name="id-row-platos-temporales"]').val(delPlatosTemporales.join(','));
            $('input[name="tipo-platos"]').val(tipoPlatos.join(',')); 
            $('input[name="fecha_exhib"]').val(delFechaExhib.join(','));
            $('input[name="id-row-duplicate"]').val(selectedIDs.join(','));
        });

        $('#search-action').click(function() {

            var search = $.trim($('#search').val());

            if(search != '') {
                location.href = '/<?=$moduleKey?>/?s=' + search;
            }
        })
    }) 
</script>

<script>
    <?php
    $import_services = array();
    echo 'var import_services = [];';
    echo "\r";

    foreach($modulesMenu[$moduleKey]['tipos_platos']['tipo_plato'] as $column) {
        $service = $column['@servicio'];

        if (!in_array($service, $import_services)) {
            $import_services[] = $service;
            echo "import_services.push('$service');";
            echo "\r";
        }
    }

    echo 'var import_constrain = {};';

    foreach($modulesMenu[$moduleKey]['tipos_platos']['tipo_plato'] as $column) {
        $name = $column['@tipoplato'];
        $cant = $column['@cantidad'];
        $service = $column['@servicio'];
        echo "if (import_constrain['$service'] == undefined) import_constrain['$service'] = {};";
        echo "import_constrain['$service']['$name'] = $cant;";
        echo "\r";
    }

    ?>
</script>