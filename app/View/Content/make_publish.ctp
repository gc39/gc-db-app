<div class="modal-body with-padding">
<?php
  if($status['success']) { 
    $multiple_pub = constant("MULTIPLE_PUB");
    $caption = 'Publicacion en Proceso';

    //echo $moduleKey;

    if($clientFolder != 'sodexo' || $multiple_pub == true or $moduleKey == 'config_app_mobile' or $moduleKey == 'maestro_productos')
    {
      //$text = 'Su publicación sera procesada en unos minutos, para revisar el estado de su publicación ingrese al <a href="/publications">Centro de Publicacion</a>.';
      $text = 'Su publicación sera procesada en unos minutos.';
    }else
    {
      $text = 'Su publicación será procesada y descargada en pantallas entre 20 minutos y 60 minutos aproximadamente.';
      
      if($clientFolder == 'sodexo')
      {
        
        $casino_name = split(':',$modulesMenu[$moduleKey]['name'])[0];
        $prefijo = substr($casino_name, 0, 3); 
        
        if($prefijo != 'APP')
        {
          $obj_conexion = new mysqli(LOCALSERVER,LOCALUSERDB,LOCALPASSDB,LOCALDB) or die ("no se pudo realizar la conexion");
          
          if(!$obj_conexion)
          {
            echo "<h3>No se ha podido conectar PHP - MySQL, verifique sus datos.</h3><hr><br>";
            die();

          }else
          {
          
          }

          // si tiene el tag cronjob significa que debe copiar el json a las tablas de cronjob y la otra
          //echo $module['gcdb']['cronjob'];
          if($modulesMenu[$moduleKey]["cronjob"] == 'true'){
            $qry = 'DELETE FROM CronJob_Modulos;';
            $obj_conexion->query($qry);

            $qry = 'INSERT INTO CronJob_Modulos SELECT id_job as id, origen as Origen, destino as Destino, modulo as Modulo, id_cronjob_modulo as id_CronJobFtp, activado as Activado, NULL as DefaultLogo from cronjob_casinos_modulos;';
            $obj_conexion->query($qry);

            $qry = 'DELETE FROM CronJob_Carpetas;';
            $obj_conexion->query($qry);

            $qry = 'INSERT INTO CronJob_Carpetas SELECT id, id_cronjob_modulo as Id_CronJob_Modulo, servicio as Servicio, logo_sodexo as LogoSodexo FROM cronjob_casinos;';
            $obj_conexion->query($qry);

          }else
          {
            $cliente_sucursal = $modulesMenu[$moduleKey]['cliente'].'_'.$modulesMenu[$moduleKey]['sucursal'];
            $qry = "SELECT config_pantallas.macadress FROM config_pantallas INNER JOIN config_modulos ON config_pantallas.id_config_modulos = config_modulos.id WHERE config_modulos.cliente_sucursal = '$cliente_sucursal'";
            $resultado = $obj_conexion->query($qry);

            if (!$resultado = $obj_conexion->query($qry))
            {
                echo $select_qry;
                echo ".Lo sentimos, este sitio web está experimentando problemas.";
                die();
            }
            //$macaddress = $resultado->fetch_assoc()["macadress"];
            $macaddresses = Array();
            while ($registro = $resultado->fetch_assoc()) {
                $macaddresses[] = $registro['macadress'];
            }

            //var_dump($macaddresses);
          }

          $obj_conexion->close();

          // examina los datos de json para insertar en la tabla monitoreo_publicacion
          //if($moduleKey == 'app_demo_menu_diario'){
          //  $uri = "http://api.digitalboard.cl/sodexo/publication/{$moduleKey}/monitoreo";
          //  ini_set('default_socket_timeout', 3);
          //  file_get_contents($uri);
          //}

          //if($moduleKey == 'cafeteria_unidad1_menu_diario'){
          //$uri = "http://api.digitalboard.cl/sodexo/display/{$moduleKey}/macaddress";
          //ini_set('default_socket_timeout', 3);
          //$macaddresses = file_get_contents($uri);
          
          $connectivity_ok = 1;
          $connectivity_fail_array = array();

          try
          {
            //$macaddresses = json_decode($macaddresses);

            foreach ($macaddresses as $macaddress) 
            {

              $uri = "http://190.151.100.11:9502/devices/{$macaddress}?now=true";

              $display_connectivity = file_get_contents($uri);
              $display_connectivity = json_decode($display_connectivity);

              $status_conectivity = $display_connectivity->status;

              switch ($status_conectivity) 
              {
                case 'powerOn':
                  break;
                case 'powerOff':
                  $connectivity_ok = 0;
                  $connectivity_fail_array[] = $macaddress;
                  break;
                case 'unknown':
                  $connectivity_ok = 0;
                  break;
              }
            }

          }catch(Exception $e){
            echo '';
          }

          $num_pantallas_sin_conexion = count($connectivity_fail_array);

          if ($num_pantallas_sin_conexion > 1){
            switch($connectivity_ok)
            {
              case 0:
                $caption = "Las pantallas del casino {$casino_name} se encuentran fuera de línea o apagadas. Por ahora no es posible actualizar su contenido, lo harán una vez recupere la conectividad.";
                $text = 'Para recuperar la conectividad ejecute el siguiente protocolo:<br><br>Si las pantallas se encuetran apagada, enciéndalas. <br><br>Si las pantallas se encuentran encendida desenchufelas, espere 3 minutos y vuelva a enchufarlas. Estas encenderán automáticamente.<br>Si luego de 25 minutos las pantallas siguen sin actualizar el contenido, escríbanos a soporte@grupoclan.cl para poder asistirlo.';
                // mandar email
                break;
              case 1:
                $caption = "La pantalla del casino {$casino_name} se encuentra en línea y actualizará el contenido.";
                $text = 'Si luego de 25 minutos la pantalla sigue sin actualizar el contenido, escríbanos a soporte@grupoclan.cl para poder asistirlo.';
                break;
            }

          }else 
          {
            switch($connectivity_ok)
            {
              case 0:
                if($num_pantallas_sin_conexion != 0) 
                {
                  $caption = "Una de sus pantallas del casino {$casino_name} se encuentra fuera de línea o apagada. Por ahora no es posible actualizar su contenido, lo hará una vez recupere la conectividad.";
                  $text = 'Para recuperar su conectividad ejecute el siguiente protocolo:<br><br>Si las pantallas se encuetran apagada, enciéndalas. <br><br>Si la pantalla se encuentra encendida desenchufela, espere 3 minutos y vuelva a enchufarla. Esta encenderá automáticamente.<br>Si luego de 25 minutos las pantalla siguen sin actualizar el contenido, escríbanos a soporte@grupoclan.cl para poder asistirlo.';
                  // mandar email
                }else 
                {
                  $caption = "La pantalla del casino {$casino_name} se encuentra fuera de línea o apagada. Por ahora no es posible actualizar su contenido, lo hará una vez recupere la conectividad.";
                  $text = 'Para recuperar su conectividad ejecute el siguiente protocolo:<br><br>Si las pantallas se encuetran apagada, enciéndalas. <br><br>Si la pantalla se encuentra encendida desenchufela, espere 3 minutos y vuelva a enchufarla. Esta encenderá automáticamente.<br>Si luego de 25 minutos las pantalla siguen sin actualizar el contenido, escríbanos a soporte@grupoclan.cl para poder asistirlo.';
                  // mandar email
                }
                break;

              case 1:
                $caption = "La pantalla del casino {$casino_name} se encuentra en línea y actualizará el contenido.";
                $text = 'Si luego de 15 minutos la pantalla sigue sin actualizar el contenido, escríbanos a soporte@grupoclan.cl para poder asistirlo.';
                break;
            }
          }

        }else 
        {
          $caption = "Publicacion correcta.";
          $text = 'Su publicación sera procesada en unos minutos.';
        }
      }

    }
    
    if($client != 'johnson' && $client != 'paris' && $client != 'cruzverde')
    { 
      ?>
      <div class="alert alert-block alert-info fade in block-inner">
        <h6><i class="icon-screen"></i> <?=$caption?></h6>
        <hr>
        <p><?=$text?></p>
        <div class="text-left">
            <a class="btn btn-info" href="/publications">Ir al Centro de Publicación</a>
            <a class="btn btn-default  pull-right" href="#" data-dismiss="modal">Cerrar</a>
        </div>
      </div>
      <?php 
    } elseif($client == 'cruzverde') 
    { 
      ?>
      <div class="alert alert-block alert-info fade in block-inner">
        <h6><i class="icon-screen"></i> Publicacion en Proceso</h6>
        <hr>
        <p>Su publicación sera procesada en unos minutos.</p>
        <div class="text-left">
          <a class="btn btn-default" href="#" data-dismiss="modal">Cerrar</a>
        </div>
      </div>
      <?php 
    } else 
    { 
      ?>
        <div class="alert alert-block alert-info fade in block-inner">
          <h6><i class="icon-screen"></i> Publicacion en Proceso</h6>
          <hr>
          <p>Su publicación sera procesada en unos minutos.</p>
          <div class="text-left">
              <a class="btn btn-default" href="#" data-dismiss="modal">Cerrar</a>
          </div>
        </div>
        <?php 
    } 
  } else 
  { ?>
    <div class="alert alert-block alert-danger fade in block-inner">
        <h6><i class="icon-eye7"></i> Fallo al Publicar!</h6>
        <hr>
        <p><?=$status['message']?></p>
        <div class="text-left">
            <a class="btn btn-info" href="#" data-dismiss="modal">Cerrar</a>
        </div>
    </div>
    <?php 
  } ?>

</div>

<script>

    $(document).ready(function() {

        $('#visual-loader').hide();

        $('.alert-<?=$clientFolder.'-'.$moduleKey?>').remove();

    });

</script>
