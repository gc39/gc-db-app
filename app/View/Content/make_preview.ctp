<?php
// if($modulesMenu[$moduleKey]['multiplepublication'] == 'true'){
// }

//if($moduleKey == 'collahuasi_coposa_bloque_inferior') {
//    echo '<pre>';
//    print_r($modulesMenu[$moduleKey]['preview_url']);
//    echo '</pre>';
//        
//    die();
//}
?>
<div class="modal-body with-padding"> 
    
    <?php 
        
    if($status['success']) { ?>
        <div class="alert alert-block alert-info fade in block-inner">            
            <h6><i class="icon-eye7"></i> Previsualización Lista!</h6>
            <hr>
            <p>Se genero la previsualización con éxito. Para revisarla haga click en "Previsualizar" de lo contrario debe hacer click en "Cerrar".</p>
            <div class="text-left">                
                <a class="btn btn-info" href="#" onclick="popupPreview()"><i class="icon-eye7"></i> Previsualizar</a>
                <a class="btn btn-default" href="#" data-dismiss="modal">Cerrar</a> 
            </div>
        </div>
    <?php } else { ?>
        <div class="alert alert-block alert-danger fade in block-inner">            
            <h6><i class="icon-eye7"></i> Fallo al Previsualizar!</h6>
            <hr>
            <p><?=$status['message']?></p>
            <div class="text-left">                                
                <a class="btn btn-info" href="#" data-dismiss="modal">Cerrar</a> 
            </div>
        </div>
    <?php } ?>
    
</div>

<script>
    
    $(document).ready(function() {
        $('#visual-loader').hide();
    });        
    
    
    function popupPreview(){
        
        var environment = "test";
        var scrollBar = ",scrollbars=1";
		
        <?php
		
        if( $modulesMenu[$moduleKey]['preview_width'] && $modulesMenu[$moduleKey]['preview_height']){
            $windowSize = "width=".$modulesMenu[$moduleKey]['preview_width'] .",height=".$modulesMenu[$moduleKey]['preview_height'];
        } else {
            $windowSize = "width=1280,height=720";
        }
        ?>

        <?php if(isset($modulesMenu[$moduleKey]['preview_url']['@using-static'])) { ?>
            window.open("<?=$modulesMenu[$moduleKey]['preview_url']['@']?>?client=<?=$clientFolder?>&enviroment=" + environment + "&entity=<?=$moduleKey?>", "preview_" + Math.floor((Math.random() * 999) + 1), "<?php echo $windowSize ?>" + scrollBar);
        <?php } elseif(isset($modulesMenu[$moduleKey]['preview_url']['@with-module-param'])) {  ?>
            window.open("<?=$modulesMenu[$moduleKey]['preview_url']['@']?>?module=<?=$moduleKey?>", "preview_" + Math.floor((Math.random() * 999) + 1), "<?php echo $windowSize ?>" + scrollBar);        
        <?php } elseif($urlIndex === false) { ?>
            window.open("<?=$modulesMenu[$moduleKey]['preview_url']?>" + environment + "/<?=$moduleKey?>.html", "preview_" + Math.floor((Math.random() * 999) + 1), "<?php echo $windowSize ?>" + scrollBar);
        <?php } else { ?>            
            window.open("<?=$modulesMenu[$moduleKey]['preview_url']['url'][$urlIndex]['@']?>", "preview_" + Math.floor((Math.random() * 999) + 1), "<?php echo $windowSize ?>" + scrollBar);
        <?php } ?>
	}

</script>