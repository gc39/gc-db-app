<style>
    .modal-dialog {
        width: 800px;
        margin: 30px auto;
}
</style>

<div class="page-header">
    <div class="page-title">
        <h3><?=$viewTitle?></h3>
    </div>
</div>
<!-- /page header -->

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li> 
        <li class='active'><?=$viewTitle?></li> 
    </ul>
</div> 

<!-- /breadcrumbs line -->
<?php
echo $this->Session->flash();

$es_menudiario_casino = FALSE;
$existe_plato = TRUE;
$tiene_temporal = FALSE;
echo '<script>';
if (substr($moduleKey, -11) == 'menu_diario'){
    $es_menudiario_casino = TRUE;
    echo 'var es_menudiario_casino = true;';
    echo 'var existePlato = true;';
    echo 'var tienePlatoTemp = false;';

    if ($platoMaestro == NULL){
        $existe_plato = FALSE;
        echo 'var existePlato = false;';
    }

    if ($platoTemporal != NULL){
        $tiene_temporal = TRUE;
        echo 'var tienePlatoTemp = true;';
    }
}else{
    echo 'var es_menudiario_casino = false;';
}

//echo '<script>';

echo 'var fields_required = {};';
foreach($fields as $field){
    foreach($field as $item){
        if($item['@validations'] == 'required')
            echo 'fields_required.'.$item['@key'].' = true;';
        //print_r($item['@key']);
        //print_r($item['@validations']);
    }
}

echo '</script>';

// Elimina de la lista de motivos si el plato existe o no en el maestro producto
$motivosListTmp = array();
if ($existe_plato || (!$existe_plato && $tiene_temporal)){
    foreach($motivosList as $motivo ){
        if(strpos($motivo[1], "Maestro") == false){
            $motivosListTmp[] = $motivo;
        }
    }
}else{
    foreach($motivosList as $motivo ){
        if(strpos($motivo[1], "Maestro") != false){
            $motivosListTmp[] = $motivo;
        }
    }
}
$motivosList = $motivosListTmp;

$nuevo_reportes_creacion_de_codigos = false;

if (isset($_GET["Service"]) && isset($_GET["Service"]) && isset($_GET["Cliente"])){
    $nuevo_reportes_creacion_de_codigos = true;
    $service = $_GET["Service"];
    $segmento = $_GET["Segmento"];
    $cliente = $_GET["Cliente"];
    $sucursal = $_GET["Sucursal"];
    $fechahoy = date("d/m/Y");

    echo "<script>var segmento = '$segmento'; var cliente = '$cliente'; var hoy = '$fechahoy';</script>";
}
$Cliente = $modulesMenu[$moduleKey]['cliente'];
$Sucursal =  $modulesMenu[$moduleKey]['sucursal'];
$Fields =  $modulesMenu[$moduleKey]['fields'];
?>

<form id="edit-form" class="form-horizontal" role="form" method="POST" onsubmit="beforeSubmit()">
    <div class="panel panel-default">
    
        <div class="panel-body">
            <input type="hidden" name="s" value="<?=$_GET['s']?>" >
            <?php
            
            $system_fields = array('id', 'created', 'modified');
            $validationsRules['rules'] = array();

            foreach($customFields as $field) {
                
                if(!in_array($field['@key'], $system_fields) ) {

                    $field['@type'] = $field['@type'] == 'richtext' ? 'richtext_summer' : $field['@type'];
                
                    if($field['@type'] == 'text-related') {
                        
                        $relatedParts = explode('.', $field['@related-to-id']);
                        $relatedTable = $relatedParts[0];
                        $relatedID    = $relatedParts[1];
                        
                        $relatedParts = explode('.', $field['@related-to-show']);
                        $relatedShow  = $relatedParts[1];

                        // hacer no editable en caso de que sea un casino menudiario sin codigo de plato maestro existente
                        //[@related-to-id] => maestro_productos.codigo
                        if($es_menudiario_casino){
                            if(!$existe_plato && !$tiene_temporal){
                                $field['@readonly'] = true;
                                $field['@type'] = 'text';
                            }
                        }
                        
                    } else {
                        
                        $relatedTable = '';
                        $relatedID    = '';
                        $relatedShow  = '';
                        
                    }
                    
                    if($field['@type'] != 'select'){
                        // Se encarga de colocar el id del cronjob por defecto al hacer un nuevo registro
                        //echo $field['@key'];
                        if ($field['@key'] == 'id_cronjob_modulo' && strpos($_SERVER['REQUEST_URI'], '/new') !== false){
                            //echo $nuevo_reportes_creacion_de_codigos;
                            echo $this->element('custom_types/'.$field['@type'], array('relatedTable' => $relatedTable, 'relatedID' => $relatedID, 'relatedShow' => $relatedShow, 'field' => $field, 'moduleKey' => $moduleKey, 'content' => $_GET['cronjobid']));
                        }
                        else{
                            //escribe los datos cuando se llama la ventana modal echo $field['@key'];
                            if ($nuevo_reportes_creacion_de_codigos && ($field['@key']=='servicio' || $field['@key']=='Segmento' || $field['@key']=='cliente' || $field['@key']=='sucursal')){
                                if ($field['@key']=='servicio')
                                    $value = $service;
                                if ($field['@key']=='Segmento')
                                    $value = $segmento;
                                if ($field['@key']=='cliente')
                                    $value = $cliente;
                                if ($field['@key']=='sucursal')
                                    $value = $sucursal;
                                echo $this->element('custom_types/'.$field['@type'], array('relatedTable' => $relatedTable, 'relatedID' => $relatedID, 'relatedShow' => $relatedShow, 'field' => $field, 'moduleKey' => $moduleKey, 'content' => $value)); 
                            }
                            else
                                if ($field['@key'] == 'temporal'){
                                    $servicio = $content['Content']['servicio'];
                                    //$tipo = $content['Content']['tipo'];
                                    echo $this->element('custom_types/'.$field['@type'], array('relatedTable' => $relatedTable, 'relatedID' => $relatedID, 'relatedShow' => $relatedShow, 'field' => $field, 'moduleKey' => $moduleKey, 'content' => $content['Content'][$field['@key']], 'cliente' => $Cliente, 'sucursal' => $Sucursal, 'servicio' => $servicio));
                                }
                                else{
                                    if ($nuevo_reportes_creacion_de_codigos && ($field['@key']=='fecha'))
                                        echo $this->element('custom_types/'.$field['@type'], array('relatedTable' => $relatedTable, 'relatedID' => $relatedID, 'relatedShow' => $relatedShow, 'field' => $field, 'moduleKey' => $moduleKey, 'fechahoy' => $fechahoy, 'content' => $content['Content'][$field['@key']]));
                                    else
                                        echo $this->element('custom_types/'.$field['@type'], array('relatedTable' => $relatedTable, 'relatedID' => $relatedID, 'relatedShow' => $relatedShow, 'field' => $field, 'moduleKey' => $moduleKey, 'content' => $content['Content'][$field['@key']])); 
                                }
                        }
                    } else {
                        if($field['@rendermodules'] != 'true'){
                            echo $this->element('custom_types/'.$field['@type'], array('relatedTable' => $relatedTable, 'relatedID' => $relatedID, 'relatedShow' => $relatedShow, 'field' => $field, 'moduleKey' => $moduleKey, 'content' => $content['Content'][$field['@key']])); 
                        }
                        else
                            {
                                $modulemask = $field['@modulemask'];

                                $tmp_casinos = Array();
                                $casino_names = Array();
                                $tmp_casinos = explode("|#|", $content['Content'][$field['@key']]);
                                foreach($tmp_casinos as $item_key_casino){
                                    $casino_names[] = $modulesMenu[$item_key_casino]['name'];
                            
                                }
                            ?>

                            
                            <div class="form-group form-group-modules">
                                <label class="col-sm-2 control-label text-right">Casinos</label>
                                <div class="col-sm-10">
                                
                                    <div class="form-group module-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label></label> 
                                                <label class="pull-right"><input type="checkbox" id="selectedAll" /> Seleccionar Todo</label>
                                                <div class="clearfix"></div>
                                                <div class="list-group">
                                                <?php
                                                    $current_url = $_SERVER["REQUEST_URI"];
                                                    $current_url_array = explode('/', $current_url);
                                                    $current_url = $current_url_array[1];

                                                    // foreach($modulesMenu_corporativo as $module_array) {

                                                    //     print_r($module_array);
                                                    //     echo '<br>';

                                                    //     $url = $modulesMenu[$module_array[0]]['url'];
                                                    //     $casino_name = $modulesMenu[$module_array[0]]['name'];
                                                    //     echo $url;
                                                    //     echo '<br>';
                                                    //     echo $casino_name;
                                                    //     echo '<br>';
                                                    //     echo '<br>';
                                                    // }
                                                    // die();

                                                    //var_dump($modulesMenu_corporativo);
                                                    //var_dump('Cliente_Sucursal_menu_diario');
                                                    //var_dump($casino_names);

                                                    foreach($modulesMenu_corporativo as $module_array) {
                                                        // Si se va a filtro por el module mask
                                                        // if (strpos($module['url'], $modulemask) !== false){
                                                        
                                                        //var_dump($module['url']);
                                                        //var_dump($modulesMenu_corporativo);

                                                        // var_dump($modulesMenu[$module_array[0]]["url"]);
                                                        // echo '<br>';
                                                        //var_dump($casino_names);
                                                        //echo '<br>';
                                                        // echo $module_array[0];
                                                        // echo '<br>';
                                                        // echo $module_array[1];
                                                        // echo '<br>';
                                                        // die();

                                                        $url = $modulesMenu[$module_array[0]]['url'];
                                                        //echo $url;
                                                        $casino_name = $modulesMenu[$module_array[0]]['name'];
                                                        //echo $casino_name;
                                                        //echo $url;
                                                        //echo '<br>';
                                                        //echo $module_array[1];
                                                        //echo '<br>';
                                                        //die();
                                                        
                                                        // Ahora se va a filtrar por una query de la base de datos
                                                        //if (in_array($current_url, $module_array[1])) {

                                                        if (strpos($current_url, $module_array[1]) !== false){
                                                            if (isset($url)){
                                                                ?>
                                                                    <div class="list-group-item" style="padding: 8px 12px">
                                                                        <span class=""><?php echo $url; ?></span>
                                                                        <label class="pull-right">
                                                                            <input type="checkbox" class="publish" name="publish[]" value="<?echo $url;?>" <?php if(in_array($casino_name, $casino_names)){?> checked="checked" <?php } ?> /> Publicar
                                                                        </label>
                                                                    </div>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <?php
                        }
                    }
                }
                
                if(isset($field['@validations'])) {
                    
                    $validations = explode('|', $field['@validations']);
                    
                    foreach ($validations as $validation) {                        
                        $validationValue = $validation == 'remote' ? '/' . $moduleKey . '/validateUnique?id='.$contentID.'&field=' . $field['@key'] : true;
                        $validationsRules['rules'][$field['@key']][$validation] = $validationValue;
                    }
                }
            }

            ?>                      

            <?php
                if($nuevo_reportes_creacion_de_codigos == false) {
            ?>
            <div class="form-actions text-right">
                <input type="hidden" value="<?=$contentID?>" name="edit-content-id" id="edit-content-id">
                <?php if($_GET['s']){ ?>
                    <a type="button" href="/<?=$moduleKey?>/?s=<?=$_GET['s']?>" class="btn btn-default">Cancelar</a>
                <?php }else{?>
                    <a type="button" href="/<?=$moduleKey?>" class="btn btn-default">Cancelar</a>
                <?php } ?>
                
                <?php if($modulesMenu[$moduleKey]['justificar_borrado']==='true') { ?>
                    <a id="update-content" type="button" class="btn btn-primary" style="margin-left: 5px;" data-toggle="modal" href="#update-modal-motivo">Guardar</a>
                <?php }else{?>
                    <input id="submitEditForm" type="submit" value="Guardar" class="btn btn-primary">
                <?php }?>
            </div>
            <?php
                }
            ?>

            <?php
                if($nuevo_reportes_creacion_de_codigos == true) {
            ?>
            <div class="text-right">
                <a type="button" class="btn btn-default" onclick="closemodal()">Cancelar</a>
                <a id="submitEditForm" type="button" class="btn btn-primary" onclick="sendTemporalCodeCreation()">Guardar</a>              
            </div>
            <?php
                }
            ?>
            
        </div>
        
    </div>
    <input type="hidden" id="orig_servicio" name="orig_servicio" value="">
    <input type="hidden" id="orig_tipo" name="orig_tipo" value="">
    <input type="hidden" id="orig_s2id_codigo" name="orig_s2id_codigo" value="">
    <input type="hidden" id="orig_calorias_temp" name="orig_calorias_temp" value="">
    <input type="hidden" id="orig_fecha" name="orig_fecha" value="">
    <input type="hidden" id="orig_tmpCode" name="orig_tmpCode" value="">
    <input type="hidden" id="motivo_cambio" name="motivo_cambio" value="0:_">
</form>

<!-- Modal with remote path -->
<div id="remote_modal" class="modal fade" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<!-- Modal with remote path -->
<div id="remote_modal_video" class="modal fade" tabindex="-1" role="dialog" >
    <div class="modal-dialog" style="width: 500px">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<!-- Modal with remote path -->
<div id="update-modal-motivo" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Editando Contenido</h4>
            </div>

            <div class="modal-body with-padding">
                <p>Indique el motivo</P>

                <select id="cmb_motivo" name="select" style="font-size: 17px; width: 100%">
                    <?php
                    foreach($motivosList as $motivo ){?>
                    <option value="<?=$motivo['0'].':'.$motivo['1']?>"><?=$motivo['1']?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button id="cmdModifConMotivo" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script>
    
    $(document).ready(function() {

        if (es_menudiario_casino){
            if(!existePlato){
                $("#motivo").html('<option value="No existe no Maestro Producto (Desactualizado)" data-show-fields="" data-hide-fields="">No existe Maestro Producto (Desactualizado)</option>');
            }else{
                $("#motivo").html('<option value="Preparación local Temporal" data-show-fields="" data-hide-fields="">Preparación local Temporal</option>');
            }
        } 

        generateDatepickers();
        generateVideoUpload();

        $("#orig_servicio").val($("#servicio").val());
        $("#orig_tipo").val($("#tipo").val());
        $("#orig_s2id_codigo").val($("#s2id_codigo .select2-chosen").text());
        $("#orig_calorias_temp").val($("#calorias_temp").val());
        $("#orig_fecha").val($("#fecha").val());
        $("#orig_tmpCode").val($("#tmpCode").val());

        $( '#selectedAll' ).change(function(){
            if($("#selectedAll").is(':checked')) {  
                $( '.publish' ).prop( 'checked', true );
            }else{
                $( '.publish' ).prop( 'checked', false );
            }
        });

        $('#motivo_cambio').val($('#cmb_motivo').val());
        $( '#cmb_motivo' ).change(function(){
            $('#motivo_cambio').val($('#cmb_motivo').val());
        });

        $('#cmdModifConMotivo').click(function(){
            document.getElementById('edit-form').submit();
        });

        if($('#edit-form .logical-select-behavior').size() > 0) {
            
            $('#edit-form .logical-select-behavior').each(function() {
                
                show_fields = $(this).find('option:selected').data('show-fields');
                hide_fields = $(this).find('option:selected').data('hide-fields');
                show_fields = show_fields.split('|');
                hide_fields = hide_fields.split('|');                                

                for(i in show_fields) {
                    $('.form-group-' + show_fields[i]).show();
                }

                for(i in hide_fields) {
                    $('.form-group-' + hide_fields[i]).hide();
                }
                
            });
        }
        
        if($('.editor').size() > 0) {
            $( '.editor' ).summernote({
                height: 180,
                tabsize: 4, 
                lang: 'es-ES',
                callbacks: {
                    onPaste: function (e) {
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        document.execCommand('insertText', false, bufferText);
                    }
                },
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'clear']],
                    ['para', ['ul', 'ol','paragraph']]
                ]
            });
        }
        
        $('#edit-form .clear-image').click(function() {
        
            fieldKey = $(this).data('field-key');
            
            $('#edit-form #thumb-' + fieldKey).attr('src', '/files/no_image.gif');   
            $('#edit-form #thumb-link-' + fieldKey).attr('href', '/gallery/mediaModal/0/' + fieldKey + '/<?=$moduleKey?>'); 
            $('#edit-form #button-link-' + fieldKey).attr('href', '/gallery/mediaModal/0/' + fieldKey + '/<?=$moduleKey?>');                           
            $('#edit-form input#' + fieldKey).val('');
            $('#edit-form #crop-link-' + fieldKey).hide();
            $('#edit-form #clear-button-' + fieldKey).hide();
        
        })
        
        $('#edit-form .clear-video').click(function() {
        
            fieldKey = $(this).data('field-key');
            
            $('#edit-form input#' + fieldKey).val('');
            $('#edit-form #view-video').hide();
        
        })        
        
        $('#edit-form .linked-behavior').change(function() {
            
            group_main      = $(this).data('linked-to');
            group_visible   = $(this).find('option:selected').data('visible-group');                        
            
            if(typeof group_visible === "undefined") {                
                return false;
            }
            
            expected_width  = $('#'+ group_visible).data('expected-width');
            expected_height = $('#'+ group_visible).data('expected-height');

            $('#crop-link-' + group_main).data('expected-width', expected_width);
            $('#crop-link-' + group_main).data('expected-height', expected_height);
            
            crop_link = $('#button-link-' + group_main).attr('href') + '/' + expected_width + ':' + expected_height;
            crop_link = crop_link.replace('mediaModal', 'cropModal');
            
            $('#crop-link-' + group_main).attr('href', crop_link);
            
            $('#edit-form .group-' + group_main).hide();
            $('#edit-form .group-' + group_visible).show();
        
        })
        
        $('#edit-form .logical-select-behavior').change(function() {
            
            show_fields = $(this).find('option:selected').data('show-fields');
            hide_fields = $(this).find('option:selected').data('hide-fields');
            show_fields = show_fields.split('|');
            hide_fields = hide_fields.split('|');                                
            
            for(i in show_fields) {
                $('.form-group-' + show_fields[i]).show();
            }
            
            for(i in hide_fields) {
                $('.form-group-' + hide_fields[i]).hide();
            }

        })

        $('#edit-form .linked-behavior').trigger('change');        
        $("#edit-form").validate(<?=json_encode($validationsRules)?>);

    });

    <?php 
    if (strpos($_SERVER['REQUEST_URI'], '/new') == false){
    ?>

    function sendTemporalCodeCreation(){
        var subdomain = '<?=$subdomain?>';
        var cliente_sucursal = "<?php echo $Cliente.'_'.$Sucursal.'_menu_diario'?>";

        var postdata = {};
        postdata["table"] = "<?php echo $Cliente.'_'.$Sucursal.'_menu_diario'?>";
        //postdata["sucursal"] = <?php echo $Sucursal?>;
        //postdata["user"] = <?php echo $userLogged['User']['email']?>;

        var tmpfecha = hoy.split('/');

        postdata["id"] = $id = $("#edit-content-id").val();
        postdata["fecha"] = tmpfecha[2]+'-'+tmpfecha[1]+'-'+tmpfecha[0];
        postdata["servicio"] = $("#servicio").val();
        postdata["nombre"] = $("#nombre_espanol").val().trim();
        postdata["segmento"] = "<?php echo $segmento;?>";
        postdata["cliente"] = "<?php echo $Cliente;?>";
        postdata["sucursal"] = $("#sucursal").val();
        postdata["motivo"] = $("#motivo").val();
        postdata["nombreingles"] = $("#nombre_ingles").val();
        postdata["calorias"] = $("#calorias").val().trim();
        postdata["lactosa"] = $("#lactosa").val();
        postdata["soya"] = $("#soya").val();
        postdata["frutosecos"] = $("#frutos_secos").val();
        postdata["gluten"] = $("#gluten").val();
        postdata["vegano"] = $("#vegano").val();
        postdata["vegetariano"] = $("#vegetariano").val();
        postdata["altoenazucar"] = $("#altoazucar").val();
        postdata["altoensodio"] = $("#altosodio").val();
        postdata["user"] = '<?php echo $userLogged['User']['email']?>';

        if (postdata["nombre"] == ""){
            alert("nombre de plato requerido");
            return;
        }

        if (postdata["calorias"] == ""){
            alert("calorias requerida");
            return;
        }

        if (!isNumeric(postdata["calorias"])){
            alert("calorias debe ser numérico");
            return;
        }

        document.getElementsByName('calorias_temp')[0].value = postdata["calorias"];
        
        $.ajax({
                    type: 'GET',
                    url: '/'+cliente_sucursal+'/createTmpDish/',
                    data: postdata,  
                    success: function(data){
                        if (data == '-1'){
                            alert('error guardando el registro');
                        }
                        else{
                            //alert('codigo agregado ' + data);
                            $("#tmpCode").val(postdata["nombre"]);
                            $("#codigo_temp").val(data);
                            $('#temporal_modal').modal('hide');
                        }
                    }
                });
    }

    <?php }
    ?>
    
    function generateVideoUpload() {
    
        var uploader = new plupload.Uploader({
            runtimes : 'html5',
            browse_button : 'button-browser-video', // you can pass an id...
            container: document.getElementById('video-upload-controls'), // ... or DOM Element itself
            url : '/<?=$moduleKey?>/uploadQueComoServer',
            urllocal : '/<?=$moduleKey?>/uploadAlternative',
            urlold : '/<?=$moduleKey?>/uploadS3',

            filters : {
                max_file_size : '150mb',
                mime_types: [
                    {title : "MP4 Video", extensions : "mp4"},
                ]
            },
            init: {
                PostInit: function() {
                    document.getElementById('video-filelist').innerHTML = '';
                    document.getElementById('start-video-upload').onclick = function() {
                        uploader.start();
                        return false;
                    };
                },
                FilesAdded: function(up, files) {
                    
                    $('#start-video-upload').show();
                    
                    plupload.each(files, function(file) {
                        $('#start-video-upload').html('Iniciar carga de ' + file.name + ' (' + plupload.formatSize(file.size) + ')');
                    });
                },
                BeforeUpload: function(up, files) {
                    $('#submitEditForm').attr('disabled', 'disabled');
                    plupload.each(files, function(file) {
                    $('#start-video-upload').html('Subiendo Archivo <b></b>');
                    });
                },
                
                UploadProgress: function(up, file) {                   
                    $('#start-video-upload b').html(file.percent + "%");
                    
                    if(file.percent >= 100) {
                        $('#start-video-upload').html("Sincronizando, por favor espere...");
                    }
                },
                Error: function(up, err) {
                    document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
                },
                FileUploaded: function(uploader,file,result) {
                    
                    $('#submitEditForm').removeAttr('disabled');
                    $('#start-video-upload').hide();
                    
                    $('#view-video').attr('href', '/gallery/viewVideo?video=' + result.response);
                    $('#view-video').show();
                    
                    $('.video-upload-url').val(result.response);
                    
                },
            }
        });
        
        uploader.init();

    }
    function beforeSubmit() {
        
        $('#edit-form .form-group:hidden').remove();       
        
        $('.editor').each(function() {            
            id = $(this).data('id');
            $('#' + id).val(cleanHTML($(this).summernote('code')));           
        })
        
    }
    
    function cleanHTML(txt){
        var sS=/(\r| class=(")?Mso[a-zA-Z]+(")?)/g;
        var out=txt.replace(sS,' ');
        var nL=/(\n)+/g;
        out=out.replace(nL,'<br>');
        var cS=new RegExp('<!--(.*?)-->','gi');
        out=out.replace(cS,'');
        var tS=new RegExp('<(/)*(meta|link|span|\\?xml:|st1:|o:|font)(.*?)>','gi');
        out=out.replace(tS,'');
        var bT=['style','script','applet','embed','noframes','noscript'];
        for(var i=0;i<bT.length;i++){
            tS=new RegExp('<'+bT[i]+'.*?'+bT[i]+'(.*?)>','gi');
            out=out.replace(tS,'');
        }
        var bA=['style','start'];
        for(var ii=0;ii<bA.length;ii++){
            var aS=new RegExp(' '+bA[i]+'="(.*?)"','gi');
            out=out.replace(aS,'');
        }
        return out;
    };

    function closemodal(){
        $('#temporal_modal').modal('hide');
    }

    function isNumeric(str) {
        if (typeof str != "string") return false // we only process strings!  
        return !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
                !isNaN(parseFloat(str)) // ...and ensure strings of whitespace fail
    }

</script>