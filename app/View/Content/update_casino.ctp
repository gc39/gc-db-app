<!-- Se agrega la libreria de la grilla excel -->
<script src="https://bossanova.uk/jspreadsheet/v4/jexcel.js"></script>
<script src="https://jsuites.net/v4/jsuites.js"></script>
<link rel="stylesheet" href="https://bossanova.uk/jspreadsheet/v4/jexcel.css" type="text/css" />
<link rel="stylesheet" href="https://jsuites.net/v4/jsuites.css" type="text/css" />

<div class="page-header">
    <div class="page-title">
        <h3><?=$viewTitle?></h3>
    </div>
</div>
<!-- /page header -->

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li class='active'><?=$viewTitle?></li>
    </ul>
</div>
<!-- /breadcrumbs line -->

<?php
	echo $this->Session->flash();

	// Cargar las tablas del sistema
	$server_db  = LOCALSERVER;
	$user_db    = LOCALUSERDB;
	$password_db = LOCALPASSDB;
	$db_db = LOCALDB;


	$obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die ("db_error");
	if(!$obj_conexion)
	{
		$comentario = ": Error de Base de Datos";
		//Envia email de alerta Error
		//$mensajeSendMail = $comentario;
		//file_get_contents($serverApiSendMail."?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
		die();
	}

	$select_qry = "SELECT config_ftp.id, config_ftp.Ip, config_ftp.`Port`, config_ftp.Usuario, config_ftp.Clave FROM config_ftp";

	if (!$resultado = $obj_conexion->query($select_qry)) {
		//$comentario = $Tag.": Error en la query";
		//Envia email de alerta Error
		//$mensajeSendMail = $comentario;
		//file_get_contents($serverApiSendMail."?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
		die();
	}

	//------------------------ Obtiene las cuentas FTP ---------------------------------
	$cuentas_ftp = array();

	if ($resultado->num_rows != 0) {
		while ($rows = $resultado->fetch_assoc()) {
			$row = $rows;

			$id = $rows['id'];
			$ip = $rows['Ip'];
			$port = $rows['Port'];
			$usuario = $rows['Usuario'];
			$clave = $rows['Clave'];

			$cuenta_ftp = array();
			$cuenta_ftp['id'] = $id;
			$cuenta_ftp['name'] = '['.$id.'] '.$usuario;
			$cuentas_ftp[] = $cuenta_ftp;
		}
	}

	//------------- Obtiene los servicios con sus respectivas carpetas     ------------

	$select_qry = "SELECT id, nombre, carpeta FROM config_casinos_servicios";

	if (!$resultado = $obj_conexion->query($select_qry)) {
		//$comentario = $Tag.": Error en la query";
		//Envia email de alerta Error
		//$mensajeSendMail = $comentario;
		//file_get_contents($serverApiSendMail."?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
		die();
	}

	$casinos_servicios = array();
	$servicios_carpetas = array();

	if ($resultado->num_rows != 0) {
		while ($rows = $resultado->fetch_assoc()) {
			$row = $rows;

			$id = $rows['id'];
			$nombre = $rows['nombre'];
			$carpeta = $rows['carpeta'];

			$casino_servicio = array();
			$casino_servicio['id'] = $id;
			$casino_servicio['nombre'] = $nombre;
			$casino_servicio['carpeta'] = $carpeta;
			$casinos_servicios[] = $casino_servicio;

			$servicios_carpetas[$nombre] = $carpeta;
		}
	}
	//Cierra la conexion a la BD
	$obj_conexion->close();

	//-------------------------------------------------------------------------------------------

	function parse_casino_servicio($index, $val){
		return $val['nombre'];
	}

?>

<form id="create-casino-form" class="form-horizontal" role="form" method="POST" onsubmit="beforeSubmit()">

	<div class="panel panel-default">

		<div class="panel-body">

			<div class="form-group form-group-client">
				<label class="col-sm-2 control-label text-right">
					Cliente
				</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="Cliente" maxlength="64">
				</div>
			</div>

			<div class="form-group form-group-client">
				<label class="col-sm-2 control-label text-right">
					Sucursal
				</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="Sucursal" maxlength="64">
				</div>
			</div>

			<div class="form-group form-group-client">
				<label class="col-sm-2 control-label text-right">
					Nombre Ficticio
				</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="NombreFicticio" maxlength="64">
				</div>
			</div>

			<div class="form-group form-group-client">
				<label class="col-sm-2 control-label text-right">
					Segmento
				</label>
				<div class="col-sm-10">
					<select class="form-control logical-select-behavior" name="Segmento" id="segmento">
						<option value="Corporativo">Corporativo</option>
						<option value="Mineria">Mineria</option>
						<option value="Salud">Salud</option>
					</select>
				</div>
			</div>

			<div class="form-group form-group-client">
				<label class="col-sm-2 control-label text-right">
					Ftp
				</label>
				<div class="col-sm-10">
					<select class="form-control logical-select-behavior" name="Ftp" id="segmento">
						<?php
							foreach($cuentas_ftp as $cuenta_ftp){
								echo '<option value="'.$cuenta_ftp['id'].'">'.$cuenta_ftp['name'].'</option>';
							}
						?>
					</select>
				</div>
			</div>

			<div class="form-group form-group-client">
				<label class="col-sm-2 control-label text-right">
					Crear carpetas FTP
				</label>
				<div class="col-sm-10">
					<input type="checkbox" class="publish" name="Chk_create_ftp_folders" checked="checked">
				</div>
			</div>

			<div class="form-group form-group-client">
				<label class="col-sm-2 control-label text-right">
					Crear carpetas MagicInfo
				</label>
				<div class="col-sm-10">
					<input type="checkbox" class="publish" name="Chk_create_MI_folders" checked="checked">
				</div>
			</div>

			<div class="panel-body" style="border-width: 1px;border-style: solid;border-color: #dddddd;">
				<div class="col-sm-12">
					Menudiario
				</div>

				<div class="row">
					<div class="col-sm-12" style="margin-top: 25px;">
						<label class="col-sm-1 control-label text-left">
							Servicios
						</label>
						<div id="hojaServiciosMenudiario"></div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12" style="margin-top: 25px;">
						<label class="col-sm-1 control-label text-left">
							Tipos de platos
						</label>
						<div id="hojaTiposPlatosMenudiario"></div>
					</div>
				</div>
			</div>

			<br>

			<div class="panel-body" style="border-width: 1px;border-style: solid;border-color: #dddddd;">
				<div class="col-sm-12">
					<input type="checkbox" class="publish" name="Chk_fulzona1" checked="checked"> FullZona 1
				</div>
				<div class="row">

					<label class="col-sm-1 control-label text-right">
						Nombre
					</label>
					<div class="col-sm-11">
						<input type="text" name="Fullzona1_name" class="form-control" maxlength="64">
					</div>
				</div>
			</div>

			<br>

			<div class="panel-body" style="border-width: 1px;border-style: solid;border-color: #dddddd;">
				<div class="col-sm-12">
					<input type="checkbox" class="publish" name="Chk_fulzona2" checked="checked"> FullZona 2
				</div>
				<div class="row">

					<label class="col-sm-1 control-label text-right">
						Nombre
					</label>
					<div class="col-sm-11">
						<input type="text" name="Fullzona2_name" class="form-control" maxlength="64">
					</div>
				</div>
			</div>

			<br>

			<div class="panel-body" style="border-width: 1px;border-style: solid;border-color: #dddddd;">
				<div class="col-sm-12">
					<input type="checkbox" class="publish" name="Chk_fulzona3" checked="checked"> FullZona 3
				</div>
				<div class="row">

					<label class="col-sm-1 control-label text-right">
						Nombre
					</label>
					<div class="col-sm-11">
						<input type="text" name="Fullzona3_name" class="form-control" maxlength="64">
					</div>
				</div>
			</div>

			<br>

			<div class="panel-body" style="border-width: 1px;border-style: solid;border-color: #dddddd;">
				<div class="col-sm-12">
					<input type="checkbox" class="publish" name="Chk_cumpleanos" checked="checked"> Cumpleaños
				</div>
			</div>

			<br>

			<div class="panel-body" style="border-width: 1px;border-style: solid;border-color: #dddddd;">
				<div class="col-sm-12">
					<input type="checkbox" class="publish" name="Chk_menu_contingencia" checked="checked"> Menu Contigencia
				</div>
			</div>

			<br>

			<div class="panel-body" style="border-width: 1px;border-style: solid;border-color: #dddddd;">
				<div class="col-sm-12">
					<input type="checkbox" class="publish" name="Chk_sugerencia_chef" checked="checked"> Sugerencia del Chef
				</div>
			</div>

			<br>

			<div class="form-actions text-right">
				<input type="hidden" name="tbl-servicios" id="tbl-servicios">
				<input type="hidden" name="tbl-tipos-platos" id="tbl-tipos-platos">

				<input type="hidden" value="<?=$contentID?>" name="edit-content-id" id="edit-content-id">
				<?php if($_GET['s']){ ?>
					<a type="button" href="../../<?=$moduleKey?>/?s=<?=$_GET['s']?>" class="btn btn-default">Cancelar</a>
				<?php }else{?>
					<a type="button" href="../../<?=$moduleKey?>" class="btn btn-default">Cancelar</a>
				<?php } ?>
				<input type="submit" value="Guardar" class="btn btn-primary">
			</div>

		</div>

	</div>

</form>

<!-- Modal with remote path -->
<div id="remote_modal" class="modal fade" tabindex="-1" role="dialog" >
	<div class="modal-dialog modal-lg">
		<div class="modal-content">



			<?php

				//echo $this->element('custom_types/'.$field['@type'], array('relatedTable' => $relatedTable, 'relatedID' => $relatedID, 'relatedShow' => $relatedShow, 'field' => $field, 'moduleKey' => $moduleKey, 'content' => $content['Content'][$field['@key']]));

			?>

		</div>
	</div>
</div>

<script>
	var tblHojaServiciosMenudiario;
	var tblHojaTiposPlatosMenudiario;

	$(document).ready(function() {
		var data_hojaServiciosMenudiario = [
			['', '']
		];
		var data_hojaTiposPlatosMenudiario = [
			['', '', '', '', '', '']
		];

		// ---------- Rucaray Requinoa -------------
		// $('input[name="Cliente"]').val("Rucaray");
		// $('input[name="Sucursal"]').val("Requinoa");
		// $('input[name="NombreFicticio"]').val("Casino Rucaray Requinoa");
		// $('select[name="Segmento"]').val("Corporativo");
		// $('input[name="Fullzona1_name"]').val("Equilíbrate");
		// $('input[name="Fullzona2_name"]').val("Informativo");
		// $('input[name="Fullzona3_name"]').val("Campaña");
		// data_hojaServiciosMenudiario = [
		// 	['Almuerzo', '2'],
		// 	['Cena Tarde', '2']
		// ];
		// var data = 'Almuerzo,Sopa / Crema,1,1,1,,Almuerzo,Salad Bar,3,1,2,,Almuerzo,Hipocalórico,1,1,3,,Almuerzo,Plato de Fondo,2,2,4,,Almuerzo,Acompañamiento,2,2,5,,Almuerzo,Postre,3,2,6,,Cena Tarde,Sopa / Crema,1,1,1,,Cena Tarde,Salad Bar,3,1,2,,Cena Tarde,Hipocalórico,1,1,3,,Cena Tarde,Plato de Fondo,2,2,4,,Cena Tarde,Acompañamiento,2,2,5,,Cena Tarde,Postre,3,2,6,';
		// ----------------------------------------

		// ---------- Sika Lampa -------------
		$('input[name="Cliente"]').val("Sika.");
		$('input[name="Sucursal"]').val("Lampa");
		$('input[name="NombreFicticio"]').val("Casino Sika Lampa");
		$('select[name="Segmento"]').val("Corporativo");
		$('input[name="Fullzona1_name"]').val("Equilíbrate");
		$('input[name="Fullzona2_name"]').val("Informativo");
		$('input[name="Fullzona3_name"]').val("Campaña");
		data_hojaServiciosMenudiario = [
			['Desayuno', '2'],
			['Almuerzo', '2'],
			['Cena Tarde', '2'],
			['Once', '2']
		];
		var data = 'Desayuno,Bebidas Calientes,3,1,1,,Desayuno,Sandwich,2,1,2,,Almuerzo,Sopa / Crema,1,1,1,,Almuerzo,Salad Bar,3,1,2,,Almuerzo,Hipocalórico,1,1,3,,Almuerzo,Plato de Fondo,2,2,4,,Almuerzo,Acompañamiento,2,2,5,,Almuerzo,Postre,4,2,6,,Almuerzo,Vegetariano,1,1,4,,Once,Bebidas calientes,3,1,1,,Once,Bebidas frías,1,1,2,,Once,Sandwich,1,1,3,,Cena Tarde,Sopa / Crema,1,1,1,,Cena Tarde,Salad Bar,1,1,2,,Cena Tarde,Plato de Fondo,1,1,3,,Cena Tarde,Postre,2,1,4,';
		// ----------------------------------------


		var perChunk = 6;
		var inputArray;

		inputArray = data.split(',');
		var result = inputArray.reduce((resultArray, item, index) => { 
			const chunkIndex = Math.floor(index/perChunk)

			if(!resultArray[chunkIndex]) {
				resultArray[chunkIndex] = [];
			}

			resultArray[chunkIndex].push(item);

			return resultArray;
		}, []);
		var data_hojaTiposPlatosMenudiario = result;
		//------------------------------------------

		<?php
			$indexes = range(0, count($casinos_servicios)-1);

			$services_array = array_map( "parse_casino_servicio", $indexes, $casinos_servicios);
			$str = implode('","', $services_array);
			$services_array = '"'.$str.'"';

			//$tmp = $servicios_carpetas;

			if ($modulesCreated == 1){?>
				console.log("modulesCreated = true");

				var url = '<?=$url?>';
				var cliente = '<?=$cliente?>';
				var sucursal = '<?=$sucursal?>';
				var module = '<?=$module?>';
				var contentType = '<?=$contentType?>';
				var host = '<?=$host?>';
				var userAgent = '<?=$userAgent?>';
				var connection = '<?=$connection?>';
				var accept = '<?=$accept?>';
				var referer = '<?=$referer?>';
				var cookie = '<?=$cookie?>';

				$.ajax({
					url: url,
					headers: { "contentType": contentType, "userAgent": userAgent, "accept": accept },
					success: function(data) {
								console.log('se llamo a: '+url);
								//console.log('resultado:'+data);
								var newUrl = window.location.href+'Post?casino='+cliente+'_'+sucursal+'_menu_diario';
								window.location.href = newUrl;
							}
				});

				<?php
			} else {?>
				console.log("modulesCreated = false");
				<?php
			}

		?>

		tblHojaServiciosMenudiario = jspreadsheet(document.getElementById('hojaServiciosMenudiario'), {
			data:data_hojaServiciosMenudiario,
			columns: [
				{ type: 'dropdown', title:'Servicio', width:200, source:[ <?php echo $services_array?> ] },
				{ type: 'numeric', title:'Paginas', width:200 }
			]
		});

		tblHojaTiposPlatosMenudiario = jspreadsheet(document.getElementById('hojaTiposPlatosMenudiario'), {
			data:data_hojaTiposPlatosMenudiario,
			columns: [
				{ type: 'dropdown', title:'Servicio', width:200, source:[ <?php echo $services_array?> ] },
				{ type: 'text', title:'Tipo de plato', width:120 },
				{ type: 'numeric', title:'Cantidad', width:200 },
				{ type: 'numeric', title:'Pagina', width:200 },
				{ type: 'numeric', title:'Indice', width:200 },
				{ type: 'text', title:'Ingles', width:120 }
			]
		});

		/*
		jspreadsheet(document.getElementById('hojaFullzona1'), {
			data:data,
			columns: [
				{ type: 'text', title:'Servicio', width:120 },
				{ type: 'numeric', title:'Paginas', width:200 },
				{ type: 'dropdown', title:'Carpeta', width:200, source:[ "Alm", "Cen", "CenTar" ] },
				{ type: 'image', title:'Fondo', width:120 },
				{ type: 'checkbox', title:'Stock', width:80 }
			]
		});
		 */

	});

	function beforeSubmit() {

		$("#tbl-servicios").val(tblHojaServiciosMenudiario.getData());
		$("#tbl-tipos-platos").val(tblHojaTiposPlatosMenudiario.getData());

		$('#edit-form .form-group:hidden').remove();

		$('.editor').each(function() {
			id = $(this).data('id');
			$('#' + id).val(cleanHTML($(this).summernote('code')));
		})


	}

	function cleanHTML(txt){
		var sS=/(\r| class=(")?Mso[a-zA-Z]+(")?)/g;
		var out=txt.replace(sS,' ');
		var nL=/(\n)+/g;
		out=out.replace(nL,'<br>');
		var cS=new RegExp('<!--(.*?)-->','gi');
		out=out.replace(cS,'');
		var tS=new RegExp('<(/)*(meta|link|span|\\?xml:|st1:|o:|font)(.*?)>','gi');
		out=out.replace(tS,'');
		var bT=['style','script','applet','embed','noframes','noscript'];
		for(var i=0;i<bT.length;i++){
			tS=new RegExp('<'+bT[i]+'.*?'+bT[i]+'(.*?)>','gi');
			out=out.replace(tS,'');
		}
		var bA=['style','start'];
		for(var ii=0;ii<bA.length;ii++){
			var aS=new RegExp(' '+bA[i]+'="(.*?)"','gi');
			out=out.replace(aS,'');
		}
		return out;
	}

</script>
