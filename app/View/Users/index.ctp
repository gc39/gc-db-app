
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Usuarios y Permisos <small></small></h3>
    </div>
</div>
<!-- /page header -->

<?php

echo $this->Session->flash();
?>

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li> 
        <li class='active'>Usuarios y Permisos</li> 
    </ul>

</div>
<!-- /breadcrumbs line -->

<div id="action-bar">    
    <a href="/users/add" type="button" class="btn btn-default" id="add-new-content"><i class="icon-plus"></i> Nuevo Usuario</a> 
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title"><i class="icon-table2"></i> Usuarios y Permisos</h6></div>
    <div class="table-responsive with-horizontal-scroll">
        <table id="content-list" class="table table-bordered">
            <thead>
                <tr>                   
                    <th>Nombre Completo</th>                                                           
                    <th>Email</th>
                    <th>Rol</th>                    
                    <th>Modulos</th>                                        
                    <th></th>                    
                </tr>                               
            </thead>
            <tbody>                                
                <?php 
                
                foreach($usersList as $user) {
                ?>
                <tr>
                    <td><?=$user['User']['fullname']?></td>                                                           
                    <td><?=$user['User']['email']?></td>
                    <td><span class="label label-success"><?=$user['User']['rol']?></span></td> 
                    <td>
                            
                        <?php 
                        
                        if($user['User']['rol'] != 'PLATFORM_ADMIN') {
                            
                           if(isset($user['User']['modules']) && !empty($user['User']['modules'])) {
                           foreach ($user['User']['modules'] as $module)   {  
                        ?>
                            <span class="label label-success"><?=$module['Permission']['module']?></span>
                        <?php
                           }
                           } else { ?>
                           <span class="label label-success">ninguno</span> 
                        <?php
                           }
                        } else {?>
                            <span class="label label-success">todos</span>
                        <?php }?>
                            
                    </td>                              
                    <td align="right">
                        <a href="/users/edit/<?=$user['User']['id']?>" class="btn btn-xs btn-default btn-icon"><i class="icon-pencil3"></i></a>
                        <a href="/users/delete/<?=$user['User']['id']?>" class="btn btn-xs btn-danger btn-icon" onclick="return confirmDelete();"><i class="icon-remove3"></i></a>                       
                    </td>                   
                </tr>                
                <?php  } ?>
            </tbody>
        </table>
                 
    </div>
                
</div>

<script>
    
    function confirmDelete() {        
        return confirm('¿Desea borrar este item?');
    }
    
</script>