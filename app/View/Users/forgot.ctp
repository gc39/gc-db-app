<div class="login-wrapper">

<?php
echo $this->Session->flash();
?>
    <form action="" role="form" method="post">
        <div class="popup-header">            
            <span class="text-semibold">Recuperar Contraseña</span>            
        </div>
        <div class="well">
            <div class="form-group has-feedback">
                <label>Su Email</label>
                <input name="email" required type="email" class="form-control" placeholder="Ingrese su email">
                <i class="icon-mail form-control-feedback"></i>
            </div>
           
            <div class="row form-actions"> 
				
				<div class="col-xs-6">
                    <div class="checkbox checkbox-success">
                        <a href="/users/login">Regresar a Login</a>
                    </label>
                    </div>
                </div>
				
                <div class="col-xs-6">
                    <button type="submit" class="btn btn-warning pull-right">Enviar</button>
                </div>
            </div>
        </div>
    </form>
</div>