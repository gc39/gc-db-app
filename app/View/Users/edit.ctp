
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Usuarios y Permisos <small></small></h3>
    </div>
</div>
<!-- /page header -->


<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li> 
        <li><a href="/users">Usuarios y Permisos</a></li> 
        <li class='active'>Editar Usuario</li> 
    </ul>

</div>
<!-- /breadcrumbs line -->

<form action="" role="form" method="post">
    <div class="panel panel-default">
        <div class="panel-heading"><h6 class="panel-title"><i class="icon-profile"></i> Editar Usuario</h6></div>
        <div class="panel-body">
  
            <input name="id" type="hidden" class="form-control"  value="<?=$user['User']['id']?>">
             
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Nombre Completo:</label>
                        <input name="fullname" minlength="2" maxlength="70" type="text" required class="form-control" value="<?=$user['User']['fullname']?>">
                    </div>

                    <div class="col-md-6">
                        <label>Email:</label>
                        <input name="email" maxlength="70" type="email" required class="form-control" value="<?=$user['User']['email']?>">
                    </div>
                </div>
            </div>
                                  
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        
                        <label>Rol</label>
                        
                        <select required name="rol" id="rol" data-placeholder="Roles.." class="select-full select2-offscreen" tabindex="-1" title="">
                            <option value=""></option> 
                            <option value="PLATFORM_ADMIN" <?php if($user['User']['rol'] == 'PLATFORM_ADMIN') {?>selected<?php }?>>PLATFORM_ADMIN</option>                             
                            <option value="CLIENT" <?php if($user['User']['rol'] == 'CLIENT') {?>selected<?php }?>>CLIENT</option> 
                        </select>
                    </div>                   
                </div>
            </div>           

			<div class="form-group module-group <?php if($user['User']['rol'] == 'PLATFORM_ADMIN') {?>hide<?php }?>">
				<div class="row">
					<div class="col-md-12">
						<label>Modulos</label> 
                        <label class="pull-right"><input type="checkbox" id="selectedAll" /> Seleccionar Todo</label>
						<div class="list-group">
						<?php
							foreach($modulesMenu as $module) {
						?>
							<div class="list-group-item">
								<span class=""><?php echo $module['name']; ?></span>
								<label class="pull-right">
									<input type="checkbox" class="publish" name="publish[]" value="<?php echo $module['url'] ?>" <?php if(in_array($module['url'], $user['User']['modules']['publish'])){?> checked="checked" <?php } ?> /> Permitir Publicar
								</label>
                                
                                <label class="pull-right">
									<input type="checkbox" class="edit" name="edit[]" value="<?= $module['url'] ?>" <?php if(in_array($module['url'], $user['User']['modules']['edit'])){?> checked="checked" <?php } ?>/> Permitir Editar
									&nbsp;&nbsp;&nbsp;
								</label>
                                
								<label class="pull-right">
									<input type="checkbox" class="access" name="access[]" value="<?= $module['url'] ?>" <?php if(in_array($module['url'], $user['User']['modules']['view'])){?> checked="checked" <?php } ?>/> Permitir Ver
									&nbsp;&nbsp;&nbsp;
								</label>
                                
                              
								<div class="clearfix"></div>
							</div>
						<?php
							}
						?>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
     
    <div class="panel panel-default">
        <div class="panel-heading"><h6 class="panel-title"><i class="icon-profile"></i> Modificar Contraseña</h6></div>
        <div class="panel-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Nueva Contraseña:</label>
                        <input name="password" id="password" type="password" class="form-control">
                    </div>

                    <div class="col-md-6">
                        <label>Repetir Nueva Contraseña:</label>
                        <input id="password_retype" name="password_retype" type="password" class="form-control">
                    </div>
                </div>
            </div>            
        </div>
    </div>                

    <div class="form-actions text-right">                
        <input type="submit" value="Guardar" class="btn btn-primary">
    </div>
    <br>
</form>

<script>
  
    $(document).ready(function() {
        
        $('form').validate();
        
        $('#rol').select2();
        $('#rol').change(function() {            
            if($(this).val() != 'PLATFORM_ADMIN') {                
                $('.module-group').removeClass('hide');
            } else {
                $('.module-group').addClass('hide');
            }
        });
        
        $( '#selectedAll' ).change(function(){
            if($("#selectedAll").is(':checked')) {  
                $( '.publish' ).prop( 'checked', true );
                $( '.edit' ).prop( 'checked', true );
                $( '.access' ).prop( 'checked', true );
            }else{
                $( '.publish' ).prop( 'checked', false );
                $( '.edit' ).prop( 'checked', false );
                $( '.access' ).prop( 'checked', false );
            }
        });
    });
    
</script>