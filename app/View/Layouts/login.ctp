<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Digital Board - Iniciar Sesión</title>

<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/css/londinium-theme.min.css" rel="stylesheet" type="text/css">
<link href="/css/styles.min.css" rel="stylesheet" type="text/css">
<link href="/css/icons.min.css" rel="stylesheet" type="text/css">
<link href="/css/customs.min.css" rel="stylesheet" type="text/css">

<!-- link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css" -->

<script type="text/javascript" src="/js/jquery/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script type="text/javascript" src="/js/jquery/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="/js/jquery/jquery-ui-1.11.0/jquery-ui.min.js"></script>

<!--<script type="text/javascript" src="/js/plugins/charts/sparkline.min.js"></script>-->

<script type="text/javascript" src="/js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript" src="/js/plugins/forms/select2.min.js"></script>
<script type="text/javascript" src="/js/plugins/forms/inputmask.js"></script>
<script type="text/javascript" src="/js/plugins/forms/autosize.js"></script>
<script type="text/javascript" src="/js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript" src="/js/plugins/forms/listbox.js"></script>
<script type="text/javascript" src="/js/plugins/forms/multiselect.js"></script>
<script type="text/javascript" src="/js/plugins/forms/validate.min.js"></script>
<script type="text/javascript" src="/js/plugins/forms/validate_messages_es.js"></script>
<script type="text/javascript" src="/js/plugins/forms/tags.min.js"></script>
<script type="text/javascript" src="/js/plugins/forms/switch.min.js"></script>


<script type="text/javascript" src="/js/bootstrap.min.js"></script>

<script>
    
    $(document).ready(function() {
            
        $('form').validate();
    
    })
    
</script>

</head>

<body class="full-width page-condensed">
  
    <?php echo $this->fetch('content'); ?>                   
    
</body>

</html>    