<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<!-- <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" /> -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Digital Board - <?=$viewTitle?></title>

<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/css/londinium-theme.min.css?v=1" rel="stylesheet" type="text/css">
<link href="/css/styles.min.css" rel="stylesheet" type="text/css">
<link href="/css/icons.min.css" rel="stylesheet" type="text/css">
<link href="/js/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css">
<link href="/js/plugins/interface/fullcalendar.min.css" rel="stylesheet" type="text/css"/>

<link href="/js/cropper/cropper.min.css" rel="stylesheet" type="text/css"/>
<link href="/css/customs.min.css?v=2" rel="stylesheet" type="text/css">

<script type="text/javascript" src="/js/jquery/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script type="text/javascript" src="/js/jquery/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="/js/jquery/jquery-ui-1.11.0/jquery-ui.min.js"></script>


<script type="text/javascript" src="/js/plugins/jquery.jsonp.js"></script>

<script type="text/javascript" src="/js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript" src="/js/plugins/forms/select2.min.js"></script>

<script type="text/javascript" src="/js/plugins/forms/validate.min.js"></script>
<script type="text/javascript" src="/js/plugins/forms/validate_messages_es.js"></script>

        
<script type="text/javascript" src="/js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript" src="/js/plugins/forms/uploader/plupload.queue.min.js"></script>

<script src="/js/jquery.easyeditor.js"></script>
<link rel="stylesheet" href="/js/easyeditor.css">    

<script type="text/javascript" src="/js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript" src="/js/plugins/interface/moment.min.js"></script>
<script type="text/javascript" src="/js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript" src="/js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript" src="/js/plugins/interface/fullcalendar-lang-es.js"></script>
<script type="text/javascript" src="/js/plugins/interface/timepicker.min.js"></script>
<script type="text/javascript" src="/js/plugins/interface/collapsible.min.js"></script>

<script type="text/javascript" src="/js/plugins/interface/jquery.unveil.js"></script>
<script src="/js/jcrop/jquery.Jcrop.min.js" type="text/javascript"></script>

<script src="/js/cropper/cropper.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/application.js?v=4"></script>

<script type="text/javascript" src="/js/summernote/summernote.min.js"></script>
<link rel="stylesheet" href="/css/summernote/summernote.css"> 
<script src="/css/summernote/lang/summernote-es-ES.js"></script>

<script>


         $(document).ajaxSuccess(function(event, xhr, settings, data) {
            if(data == 'NO_LOGGED') {
                location.href = '/user/login';
            }
        });
        
      
</script>

<style>
    
    #visual-loader {    
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        z-index: 2000;
        background: #000000;
        background: rgba(27, 53, 16, 0.2);
        text-align: center;
    }

    .no-show {
        visibility: hidden!important;
    }
       
        #visual-loader .spinner {
            width: 40px;
            height: 40px;
            top: 40%;
            left: 48%;            
            position: fixed;
            margin: 0px auto;
        }
            #visual-loader .spinner span { 
                display: block;
                margin-top: 53px;
                color: #FFF;
                font-weight: bold;
                margin-left: -5px;
            }
            
            #visual-loader .double-bounce1, #visual-loader .double-bounce2 {
              width: 100%;
              height: 100%;
              border-radius: 50%;
              background-color: #FFF;
              opacity: 0.6;
              position: absolute;
              top: 0;
              left: 0;
              -webkit-animation: bounce 2.0s infinite ease-in-out;
              animation: bounce 2.0s infinite ease-in-out;
            }

            #visual-loader .double-bounce2 {
              -webkit-animation-delay: -1.0s;
              animation-delay: -1.0s;
            }

            @-webkit-keyframes bounce {
              0%, 100% { -webkit-transform: scale(0.0) }
              50% { -webkit-transform: scale(1.0) }
            }

            @keyframes bounce {
              0%, 100% { 
                transform: scale(0.0);
                -webkit-transform: scale(0.0);
              } 50% { 
                transform: scale(1.0);
                -webkit-transform: scale(1.0);
              }
            }
</style>

</head>

<body class="sidebar-wide">

    <div id="visual-loader" style="display:none">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
            <span>cargando</span>
        </div>
    </div>
    <?php
    
    $userLogged = CakeSession::read('LoggedUserNew');
  
    echo $this->element('main-nav', array('userLogged' => $userLogged)); ?>
   
    <!-- Page container -->

	<!-- Page container -->
 	<div class="page-container">

		<!-- Sidebar -->
		<div class="sidebar collapse">
			<div class="sidebar-content">

				<!-- User dropdown -->
				<div class="user-menu dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="/images/profile.png">
						<div class="user-info">
							<?=$userLogged['User']['fullname']?> <span>Administrador</span>
						</div>
					</a>
					<div class="popup dropdown-menu dropdown-menu-right">
					    <div class="thumbnail">
					    	<div class="thumb">
								<img src="/images/profile.png">								
						    </div>
					    
					    	<div class="caption text-center">
					    		<h6><?=$userLogged['User']['fullname']?> <small><?=$userLogged['User']['rol']?></small></h6>
					    	</div>
				    	</div>
					</div>
				</div>
				<!-- /user dropdown -->

                <?php echo $this->element('main-menu', array('userLogged' => $userLogged)); ?>    
		
			</div>
		</div>
		<!-- /sidebar --> 	
        
        <div class="page-content">
        <?php echo $this->fetch('content'); ?>        
        </div>
        <!-- /page content -->
        
        
        <?php echo $this->element('footer'); ?>        
        
	</div>
	<!-- /page container -->
    
    <div id="change-password-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modificar Contraseña</h4>
                </div>
                <form action="#" role="form" method="post" id="change-password-form">

                 
                    
                    <div class="modal-body with-padding">
                        
                        <div class="block-inner text-danger">
                            <h6 class="heading-hr">Modificar Contraseña<small class="display-block">Ingrese su contraseña actual y luego ingrese 2 veces su nueva contraseña.</small></h6>
                        </div>

                        <div class="form-group">
                            
                            <div id="feedback-error" class="alert alert-danger fade in block-inner">		                
                                <i class="icon-cancel-circle"></i> <span class="feedback"></span>
                            </div>
                            
                            <label>Contraseña Actual</label>
                            <input type="password" placeholder="" name="current-password" class="required form-control">                            
                        </div>
                        <hr>
                        <div class="form-group">
                            
                            <label>Nueva Contraseña</label>
                            <input type="password" placeholder="" name="new-password" id="new-password" class="required form-control">                            
                            <br>
                            <label>Repertir Contraseña</label>
                            <input type="password" placeholder="" name="new_password_confirm" id="new_password_confirm" class="required form-control">
                            
                        </div>
                        
                    </div>
                    
                    
                    <div class="modal-footer">
                        <button id="cancel-change-password" type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
                        <button id="send-change-password" type="button" class="btn btn-primary">Modificar Contraseña</button>
                    </div>

                    <script>
                        
                        $(document).ready(function() {
                            
                             $('#change-password-form').validate({rules: {            
                                new_password_confirm: {
                                  equalTo: "#new-password"
                                }}
                        
                            });

                            $('#feedback-error').hide();
                            
                            $('#send-change-password').click(function() {
                                
                                var validator = $( "#change-password-form" ).validate();
                                
                                if(!validator.form()) {
                                    return false;
                                }
                                
                                $.post('/users/updatePassword', $('#change-password-form').serialize(), function(response) {                                    
                                    
                                    if(response.status == 'OK') {
                                        
                                        $('.modal-body').text(response.message);                                                                                
                                        $('#send-change-password').hide();
                                        $('#cancel-change-password').text('Cerrar');
                                        
                                    } else {
                                        $('#feedback-error .feedback').text(response.message);                                        
                                        $('#feedback-error').show();
                                    }
                                    
                                }, 'json');
                            });
                        
                        })
                    </script>
                    
                </form>
                
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
            
</body>
</html>    