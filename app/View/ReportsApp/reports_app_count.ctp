<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3><i class="<?=$modulesMenu[$moduleKey]['icon']?>" style="font-size: 24px;margin-top: -4px;"></i> <?=$viewTitle?> <small></small></h3>
    </div>
</div>
<!-- /page header -->

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    
    <ul class="breadcrumb">
        <li><a href="/">Home...</a></li> 
        <li class='active'><?=$viewTitle?></li> 
    </ul>

</div>
<!-- /breadcrumbs line -->


<?php if(!empty($modulesMenu[$moduleKey]['advice'])) { ?>    
    
    <div class="alert alert-danger fade in">
        <i class="icon-info"></i> <?=$modulesMenu[$moduleKey]['advice']?>  
    </div>
<?php } ?>


<div id="action-bar">    
     <button type="button" class="hide btn btn-default <?php if($isSearch) { ?>activated<?php }?>" id="toggle-filters"><i class="icon-search3"></i> <?php if($isSearch) { ?>Desactivar<?php } else { ?>Activar<?php } ?> Filtros</button>           

    <div class="col-xs-3">
        <div class="input-group">
            <input id="search" name="search" type="text" class="form-control" value="<?=$searchTerm?>">            
          
            <span class="input-group-btn">                
                <button class="btn btn-primary btn-icon" type="button" id="search-action"><i class="icon-search3"></i></button>
            </span>    
                          
        </div>        
    </div>
 
    
    <button type="button" class="btn btn-primary pull-right" style="margin-left:4px"  data-toggle="modal"  href="/<?=$moduleKey?>/downloadReportsApp/count" data-target="#publish_modal22" onclick="javascript: $('#visual-loader').show();"><i class="icon-file-download"></i>Descargar</button> 
<a type="button" class="btn btn-primary pull-right" style="margin-right:5px;" role="button" href="/<?=$moduleKey?>" ><i class="icon-food2"></i>Volver al Menú Diario</a>
    <div class="clearfix"></div>
</div>

<?php 
if($isSearch) {
?>
<br>
<p><b>Buscando "<?=$searchTerm?>"</b> [<a href="/<?=$moduleKey?>/reportsAppCount/<?=$page?>">Limpiar Busqueda</a>]</p>
<?php } ?>

<div class="panel panel-default">
   <div class="table-responsive with-horizontal-scroll">
        
        <table id="content-list" class="table table-bordered">
            <thead>
                <tr>
                    <th width="350">Tipo</th>                                        
                    <th width="400">Plato</th>
                    <th width="380">Cantidad</th>
                    <th></th>
                    
                </tr>                               
            </thead>
            <tbody>                
                
                <?php 

                if($contentList) {
                
                foreach($contentList as $index => $content) {   
                   ///$contentID = $content['ReportsApp']['id'];
                ?>
                <tr data-id="<?=$index?>">
                    <td><?=$content['ReportsApp']['tipo']?></td>
                    <td><?=$content['ReportsApp']['plato']?></td>
                    <td><?=$content[0]['cantidad']?></td>
                    <td align="right"></td>                   
                </tr>                
                <?php } 
                }?>                               
            </tbody>
        </table>
                 
    </div>
                
</div>

<?php if($pages > 1) { ?>
<div class="block text-center">
    <ul class="pagination">
        <? if($page > 1) { ?>
        <li><a href="/<?=$moduleKey?>/reportsAppCount/<?=($page - 1).$queryString?>">←</a></li>
        <? } ?>
        <? for($i = 1; $i <= $pages; $i++) { ?>
        <li <?php if($page == $i) { ?>class="active"<?php } ?>><a href="/<?=$moduleKey?>/reportsAppCount/<?=$i.$queryString?>"><?=$i?></a></li>  
        <? } ?>
        <? if($page < $pages) { ?>
        <li><a href="/<?=$moduleKey?>/reportsAppCount/<?=($page + 1).$queryString?>">→</a></li>
        <? } ?>        
    </ul>
</div>
<?php } ?>
            
<div id="publish_modal22" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<div id="download_reports" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
    </div>
</div>
<script>   
    
    //$('#data-select').change(function(){
      
        /*
        $.ajax({
            url: '/<?=$moduleKey?>/index-ajax', 
            success: function(data){
                $("#graph-ot").html(data);
            }
        });*/
    //});
    
    
    
    function updateUrlCache() {        
      //  $('#publish-button').attr('href', '/<?=$moduleKey?>/makePublish?v=' + Math.floor((Math.random() * 99999) + 1));
    }
    
    $(document).ready(function() {
generateDatepickers();
        var integrity_errors = $('td.HAVE_ERROR').size();
             
        if(integrity_errors > 0) {
            $('#integrity-errors span').text(integrity_errors);
            $('#integrity-errors').removeClass('hide');
        }

        $('#search-action').click(function() {
            
            var search = $.trim($('#search').val());
            
            if(search != '') {
                location.href = '/<?=$moduleKey?>/reportsAppCount/<?=$page?>?s=' + search;
            }
        })
    })
</script>