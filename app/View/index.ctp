<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3><i class="<?=$modulesMenu[$moduleKey]['icon']?>" style="font-size: 24px;margin-top: -4px;"></i> <?=$viewTitle?> <small></small></h3>
    </div>
</div>
<!-- /page header -->

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li> 
        <li class='active'><?=$viewTitle?></li> 
    </ul>

</div>
<!-- /breadcrumbs line -->


<?php
echo $this->Session->flash();
?>

<div id="integrity-errors" class="alert alert-info fade in hide">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <i class="icon-info"></i> Existen <span></span> errores de integridad, <a id="show-all-rows" href="#" style="display: none">volver a ver todos los elementos</a> <a id="show-integrity-errors" href="#">ver solo elementos con errores</a>.
</div>

<div id="action-bar">    
    rrr
    <?php
    if(($userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array($moduleKey, $userLogged['User']['modules']['edit']))  && !$modulesMenu[$moduleKey]['no_editable']) { ?>
    <a type="button" class="btn btn-success pull-left" href="/<?=$moduleKey.'/edit/new'?>"><i class="icon-plus"></i> Agregar</a> 
    <?php } ?>
    
    <button type="button" class="hide btn btn-default <?php if($isSearch) { ?>activated<?php }?>" id="toggle-filters"><i class="icon-search3"></i> <?php if($isSearch) { ?>Desactivar<?php } else { ?>Activar<?php } ?> Filtros</button>           
    
    <?php
    if(($userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array($moduleKey, $userLogged['User']['modules']['edit']))) { ?>
    <a id="duplicate-content" class="hide btn btn-icon btn-default  pull-left" style="margin-left: 5px;" data-toggle="modal" href="#duplicate-modal"><i class="icon-copy"></i></a> 
    <a id="delete-content" type="button" class="hide btn btn-icon btn-default  pull-left" style="margin-left: 5px;" data-toggle="modal" href="#delete-modal"><i class="icon-remove2"></i></a>     
    <?php }?>
    
    <?php if($modulesMenu[$moduleKey]['allow_import']) { ?>    
    <a data-toggle="modal" class="btn btn-default pull-left" href="#import-csv" style="margin-left: 5px;" ><i class="icon-file-excel"></i> Importar</a>  	
    <?php } ?>
        
    <div class="col-xs-3">
        <div class="input-group">
            <input id="search" name="search" type="text" class="form-control" value="<?=$searchTerm?>">            
        
            <span class="input-group-btn">                
                <button class="btn btn-primary btn-icon" type="button" id="search-action"><i class="icon-search3"></i></button>
            </span>    
            
        </div>        
    </div>  
    
    <?php   
    if( $userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array( $moduleKey, $userLogged['User']['modules']['publish'])) {
    ?>
    <button type="button" class="btn btn-primary pull-right" style="margin-left:4px"  data-toggle="modal"  href="/<?=$moduleKey?>/makePublish" data-target="#publish_modal" onclick="javascript: $('#visual-loader').show();"><i class="icon-screen"></i> Publicar</button>
    <?php } ?>
    
    <?php if(!isset($modulesMenu[$moduleKey]['preview_url']['url'])) {?>
    <button type="button" class="btn btn-primary pull-right" data-toggle="modal" role="button" href="/<?=$moduleKey?>/makePreview" data-target="#publish_modal" onclick="javascript: $('#visual-loader').show();"><i class="icon-eye7"></i> Previsualizar</button>    
    <?php } else {?>

        <div class="btn-group pull-right">
            <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" style="width: 180px;">Previsualizar <span class="caret"></span></button>
            <ul class="dropdown-menu icons-right">
                
                <?php foreach($modulesMenu[$moduleKey]['preview_url']['url'] as $key => $url) {?>
                
                    <li><a  data-toggle="modal"  href="/<?=$moduleKey?>/makePreview/?urlIndex=<?=$key?>" data-target="#publish_modal" onclick="javascript: $('#visual-loader').show();"><i class="icon-eye7"></i> <?=$url['@label']?></a></li>
                
                <?php }?>
            </ul>
        </div>
    
    <?php
    
    }?>
    <div class="clearfix"></div>
</div>

<?php 
if($isSearch) {
?>
<br>
<p><b>Buscando "<?=$searchTerm?>"</b> se encontraron <?=count($contentList)?> resultados. <a href="/<?=$moduleKey?>">Limpiar Busqueda</a></p>
<?php } ?>
<div class="panel panel-default">
    <div class="table-responsive with-horizontal-scroll">
        
        <table id="content-list" class="table table-bordered">
            <thead>
                <tr>
                    <th width="10" style="vertical-align:bottom"><input id="toggle-select-all" type="checkbox"></th>
                    <?php 
                    if($customFields) {
                        
                        $orderDirectionLink = $orderDirection == 'ASC' ? 'DESC' : 'ASC';
                        $orderDirectionIcon = $orderDirection == 'ASC' ? 'icon-arrow-up2' : 'icon-arrow-down2';
                        
                        foreach($customFields as $header) {                                                                                                                
                            
                            if($header['@visible-in-list'] == 'true') {
                                
                                $colums[$header['@key']] = $header['@label'];
                                
                                $orderIconVisibleClass = $orderBy == $header['@key'] ? '' : 'hide';
                                
                                if($header['@type'] != 'image') {
                                ?>
                                <th width="<?=$header['@column-width']?>"><a href="/<?=$moduleKey?>?orderBy=<?=$header['@key']?>&order=<?=$orderDirectionLink?>&s=<?=$searchTerm?>"><?=$header['@label']?> <i class="<?=$orderDirectionIcon?> pull-right <?=$orderIconVisibleClass?>"></i></a></th>                                        
                                <?php                             
                                } else {
                                ?>
                                <th width="<?=$header['@column-width']?>"><?=$header['@label']?></th>                                        
                                <?php
                                }
                            }
                        }
                    }     
                    
                    
                    ?>
                    <th></th>
                    
                </tr>                               
            </thead>
            <tbody>                
                
                <?php 

                if($contentList) {
                
                foreach($contentList as $content) {   
                    $contentID = $content['Content']['id'];
                ?>
                <tr data-id="<?=$contentID?>">
                    <td><input id="content-<?=$contentID?>" class="input-selected" type="checkbox"></td>
                    <?php                              
                    foreach($customFields as $fieldKEY => $field) {                                               
                        
                        $validationRelatedErrors = false;
                        
                        if($field['@visible-in-list'] == 'true') {                                
                            
                            if($field['@type'] == 'static-image') {   
                                $bgColor = isset($field['@column-bg-color']) ? 'background-color: '.$field['@column-bg-color'] : '';
                                echo '<td style="'.$bgColor.'"><img src="/files/'.$this->Image->getStaticFilename($content['Content'][$field['@key']]).'" class="view product-thumb" data-image-id="'.$content['Content'][$field['@key']].'" /></td>';  
                            
                            } elseif($field['@type'] == 'image') {                                                                    
                                $bgColor = isset($field['@column-bg-color']) ? 'background-color: '.$field['@column-bg-color'] : '';
                                echo '<td style="'.$bgColor.'"><img src="/files/'.$this->Image->getFilename($content['Content'][$field['@key']]).'" class="view product-thumb" data-image-id="'.$content['Content'][$field['@key']].'" /></td>';                              
                            } elseif($field['@type'] == 'date') {
                                
                                $contentShow = '';
                                
                                if(isset($field['@no-transform-to-mysql']) && $field['@no-transform-to-mysql'] == true) {
                                    $contentShow = $content['Content'][$field['@key']];
                                } else {
                                    if(!empty($content['Content'][$field['@key']])) {                                   
                                        $dateParts   = explode('-', $content['Content'][$field['@key']]);                                                
                                        $contentShow = count($dateParts) == 3 ? $dateParts[2].'-'.$dateParts[1].'-'.$dateParts[0] : '';
                                    }
                                }
                                
                                echo '<td>'.$contentShow.'</td>';
                    
                            } elseif($field['@type'] == 'select') {
                                
                                $contentShow = '';
                                
                                foreach($field['option'] as $option ){
                                    
                                    if(strtolower($content['Content'][$field['@key']]) == strtolower($option['@value'])) {                                        
                                        $contentShow = $option['@'];                                   
                                    }
                                }
                                        
                                echo '<td>'.$contentShow.'</td>';
                                
                            } else {
                                
                                $contentShow = $content['Content'][$field['@key']];
                                
                                if(isset($field['@related-to-id']) && isset($field['@related-to-show']) ) {
                                    
                                    $relatedParts = explode('.', $field['@related-to-id']);
                                    $relatedTable = isset($relatedParts[0]) ? $relatedParts[0] : false;
                                    $relatedField = isset($relatedParts[1]) ? $relatedParts[1] : false;
                                    
                                    $relatedParts     = explode('.', $field['@related-to-show']);
                                    $relatedShowTable = isset($relatedParts[0]) ? $relatedParts[0] : false;
                                    $relatedShowField = isset($relatedParts[1]) ? $relatedParts[1] : false;                                    
                                    
                                    if($relatedTable && $relatedField) { 
                                        
                                        $validate_related_fields = false;
                                    
                                        $contentRelated =  $this->Related->getRelated($relatedTable, $content['Content'][$field['@key']], $relatedField);                                                                      
                                    
                                        if(isset($field['@validate-related-fields']))  {
                                            
                                            $validate_related_fields = explode('|', $field['@validate-related-fields']);                                                                                        
                                            
                                            foreach ($validate_related_fields as $validateField) {
                                                if(!isset($contentRelated[0][$relatedTable][$validateField]) || empty($contentRelated[0][$relatedTable][$validateField])) {
                                                    $validationRelatedErrors[] = $validateField;
                                                }
                                            }
                                            
                                        }
                                        
//                                        echo '<pre>';
//                                        echo $relatedShowTable.'<br>';
//                               
//                                        print_r($contentRelated[0][$relatedTable]['id']);
//                                        echo '</pre>';
//                                        die();
                                        $linkRelatedID = isset($contentRelated[0][$relatedShowTable]['id']) ? $contentRelated[0][$relatedShowTable]['id'] : '#';
                                        $contentRelated = isset($contentRelated[0][$relatedShowTable][$relatedShowField]) ? $contentRelated[0][$relatedShowTable][$relatedShowField] : 'No Encontrado';
                                        $contentShow    = '[<a href="/'.$relatedShowTable.'/edit/'.$linkRelatedID.'">'.$contentShow.'</a>] '.$contentRelated;
                                        
                                    }
                                    
                                }
                                
                                $errorClass = $validationRelatedErrors != false ? 'HAVE_ERROR' : '';
                                echo '<td>'.$contentShow.'</td>';
                            }      
                        }
                    }

                    
                    ?>                    
                    <td align="right" class="<?=$errorClass?>">
                        <?php 
                        if(($userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array($moduleKey, $userLogged['User']['modules']['edit']))  && !$modulesMenu[$moduleKey]['no_editable']) {
                        ?>
                            <?php if($searchTerm){ ?>
                                <a href="/<?=$moduleKey?>/edit/<?=$contentID?>/?s=<?=$searchTerm?>"><i class="icon-pencil3"></i></a>  
                            <?php }else{ ?>
                                <a href="/<?=$moduleKey?>/edit/<?=$contentID?>"><i class="icon-pencil3"></i></a>  
                            <?php }?>
                        <?php }?>
                    </td>                   
                
                </tr>                
                <?php } 
                }?>                               
            </tbody>
        </table>
        
    </div>
                
</div>

<?php if($pages > 1) { ?>
<div class="block text-center">
    <ul class="pagination">
        <? if($page > 1) { ?>
        <li><a href="/<?=$moduleKey?>/<?=($page - 1).$searchString?>">←</a></li>
        <? } ?>
        <? for($i = 1; $i <= $pages; $i++) { ?>
        <li <?php if($page == $i) { ?>class="active"<?php } ?>><a href="/<?=$moduleKey?>/<?=$i.$searchString?>"><?=$i?></a></li>  
        <? } ?>
        <? if($page < $pages) { ?>
        <li><a href="/<?=$moduleKey?>/<?=($page + 1).$searchString?>">→</a></li>
        <? } ?>        
    </ul>
</div>
<?php } ?>
            

<!-- Modal with remote path -->
<div id="publish_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<!-- modal with remote path -->
<div id="import-csv" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form enctype="multipart/form-data" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Importar CSV</h4>
                </div>

                <div class="modal-body with-padding">
                    
                    <div class="alert alert-block alert-info fade in block-inner">                         
                        <?php 
                                                                        
                            foreach($modulesMenu[$moduleKey]['import_map']['map'] as $column) {                                
                                
                                if(strtolower($column['@column']) != '_db_order') {
                                    $optional = $column['@optional'] == 'yes' ? ' (Opcional)' : '';
                                    $additional_info = isset($column['@additional-info']) ? ' '.$column['@additional-info'] : '';
                                    $columns[] = $column['@column'].$additional_info.$optional;   
                                }
                            }                               
                            
                            $expectedColumns = implode(', ', $columns);
                            
                        ?>                        
                        <p>Seleccione el archivo CSV para importar. Se esperan las siguientes columnas en cualquier orden: <?=$expectedColumns?>.</p>
                        <br>

                        <input type="hidden" name="import_from_csv" value="1">
                        <input type="file" name="file" class="styled">
                        <br> <br>
                        <label for="field_separator">Separador de Columnas</label>
                        <select id="field_separator" name="field_separator" class="form-control">
                            <option value=";" selected="">";" (Punto y Coma)</option>
                            <option value=",">"," (Coma)</option>
                        </select>
                    </div>

				
					<div class="panel-group">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h6 class="panel-title panel-trigger">
									<a href="#question1" data-toggle="collapse">¿Cómo crear un <b>CSV</b>?</a>
								</h6>
							</div>
							<div class="panel-collapse collapse " id="question1" style="height: auto;">
								<div class="panel-body">
									<ul>
                                        <li><b>Paso 1</b>: Abrir el archivo en Excel</li>
										<li><b>Paso 2</b>: Click en menú "Archivo"</li>
										<li><b>Paso 3</b>: Click en menú "Guardar Como..."</li>
										<li><b>Paso 4</b>: Seleccionar tipo: CSV (delimitado por comas)</li>
                                        <li><b>Paso 5</b>: Seleccionar la codificación: "UTF-8"</li>
										<li><b>Paso 6</b>: Guardar</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!---->
                </div>

                <div class="modal-footer">
					<label class="checkbox checkbox-primary pull-left">
						<input class="styled" type="checkbox" name="delete_all" value="1">
						Eliminar todos los datos ahora existentes
					</label>	
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <input type="submit" class="btn btn-primary" value="Importar">
                </div>
            </form>
        </div>
    </div>
</div>


<div id="duplicate-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form enctype="multipart/form-data" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Duplicar Contenido</h4>
                </div>

                <div class="modal-body with-padding">

                    <br>
                    <label>Seleccione donde duplicar...</label>

                    <select name="duplicate-destination" class="form-control">
                        <option value="">Seleccionar...</option>
                    <?php                             
                        foreach($availableDuplicateDestinations as $key => $duplicateDestination) {
                    ?> 
                        <option value="<?=$key?>"><?=$duplicateDestination?></option>
                    <?php                               
                        }
                    ?>   
                    </select>
                    
                    <input type="hidden" name="id-row-duplicate" value="">
                    <input type="hidden" name="duplicate-action" value="1">

                </div>

                <div class="modal-footer">					
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <input type="submit" class="btn btn-primary" value="Duplicar">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="delete-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form enctype="multipart/form-data" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Eliminar Contenido</h4>
                </div>

                <div class="modal-body with-padding">

                    <p>¿Esta seguro que desea eliminar este contenido? Esta accion no  se puede deshacer</P>
                    
                    <input type="hidden" name="id-row-delete" value="">
                    <input type="hidden" name="delete-action" value="1">
                    <input type="hidden" name="s" value="<?=$_GET['s']?>">
                </div>

                <div class="modal-footer">					
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <input type="submit" class="btn btn-primary" value="Eliminar">
                </div>
            </form>
        </div>
    </div>
</div>

<script>      
    
    $(document).ready(function() {
        
        var integrity_errors = $('td.HAVE_ERROR').size();
            
        if(integrity_errors > 0) {
            $('#integrity-errors span').text(integrity_errors);
            $('#integrity-errors').removeClass('hide');
        }
        
        $('#show-integrity-errors').click(function() {
            
            $('#show-all-rows').show();
            $('#show-integrity-errors').hide();
            
            $('table tbody tr').each(function() {
                if($(this).find('td.HAVE_ERROR').size() == 0) {
                $(this).hide();                             
                }               
            })
        })
        
        $('#show-all-rows').click(function() {
            $('#show-all-rows').hide();
            $('#show-integrity-errors').show();
            $('table tr').show();
        })
        
        $('#duplicate-content').removeClass('hide');
        $('#delete-content').removeClass('hide'); 
        $('#duplicate-content').hide();
        $('#delete-content').hide();                

        $("#toggle-select-all").change(function () {

            $("input.input-selected").prop('checked', $(this).prop("checked"));

            if($('.input-selected:checked').size() > 0 ) {
                $('#duplicate-content').show();
                $('#delete-content').show();
            } else {
                $('#duplicate-content').hide();
                $('#delete-content').hide();
            }

            var selectedIDs = new Array();
            $('.input-selected:checked').each(function() {        
                id = $(this).attr('id');
                id = id.split('-');
                id = id[1];
                selectedIDs.push(id);
            });

            $('input[name="id-row-delete"]').val(selectedIDs.join(','));                
            $('input[name="id-row-duplicate"]').val(selectedIDs.join(','));

        })

        $('.input-selected').change(function() {        

            if($('.input-selected:checked').size() > 0 ) {
                $('#duplicate-content').show();
                $('#delete-content').show();
            } else {
                $('#duplicate-content').hide();
                $('#delete-content').hide();
            }

            var selectedIDs = new Array();

            $('.input-selected:checked').each(function() {        
                id = $(this).attr('id');
                id = id.split('-');
                id = id[1];
                selectedIDs.push(id);
            });

            $('input[name="id-row-delete"]').val(selectedIDs.join(','));                
            $('input[name="id-row-duplicate"]').val(selectedIDs.join(','));

        });  
        
        $('#search-action').click(function() {
            
            var search = $.trim($('#search').val());
            
            if(search != '') {
                location.href = '/<?=$moduleKey?>/?s=' + search;
            }
        })
    })
</script>