<div class="form-group form-group-<?=$field['@key']?>">
    <label class="col-sm-2 control-label text-right"><?=$field['@label']?>
    
        <?php 
        if(!isset($field['variation'])) {
        ?>
        - <?=$field['@expected-width']?>x<?=$field['@expected-height']?>
        <?php 
        } else {
           foreach($field['variation'] as $variation) {
               echo '<span class="group-'.$field['@key'].' group-'.$variation['@name'].'">'.$variation['@expected-width'].'x'.$variation['@expected-height'].'</span>';
           }
        ?>
        
        <?php } ?>
    </label>
    
    <div class="col-sm-10">
         
        <input type="hidden" value="<?=$content?>" name="<?=$field['@key']?>" id="<?=$field['@key']?>" />
        <a id="thumb-link-<?=$field['@key']?>" class="open-media" data-toggle="modal" role="button" href="/gallery/mediaModal/<?=empty($content) ? 0 : $content?>/<?=$field['@key']?>/<?=$moduleKey?>" data-target="#remote_modal">
            <img id="thumb-<?=$field['@key']?>" src="/files/<?=$this->Image->getFilename($content)?>" class="view product-thumb" />   
        </a>
        
        <br /><br />
        <div class="btn-group">                        
            <a id="button-link-<?=$field['@key']?>" class="btn btn-default btn-xs pull-left open-media" data-toggle="modal" role="button" href="/gallery/mediaModal/<?=empty($content) ? 0 : $content?>/<?=$field['@key']?>/<?=$moduleKey?>" data-target="#remote_modal">Seleccionar Imagen...</a>            
            
            <?php 
            if(!isset($field['variation'])) {
                
                $expectedHeight = $field['@expected-height'];
                $expectedWidth  = $field['@expected-width'];
                $cropLink      = '/gallery/cropModal/'.$content.'/'.$field['@key'].'/'.$field['@expected-width'].':'.$field['@expected-height'].'/'.$moduleKey;
                
            } else {      
                $expectedHeight = 0;
                $expectedWidth  = 0;                
                $cropLink       = '/gallery/cropModal/'.$content.'/'.$field['@key'].'/0:0/'.$moduleKey;
                foreach ($field['variation'] as $variation) {
                    echo '<span id="'.$variation['@name'].'" class="hide" data-expected-width="'.$variation['@expected-width'].'" data-expected-height="'.$variation['@expected-height'].'"></span>';
                }
            }
            ?>                
            
            <a id="crop-link-<?=$field['@key']?>" data-module-key="<?=$moduleKey?>" data-expected-width="<?=$expectedWidth?>" data-expected-height="<?=$expectedHeight?>" class="open-crop btn btn-default btn-xs pull-left  <?php if(empty($content)) {?>hide<?php }?>" data-toggle="modal" role="button" href="<?=$cropLink?>" data-target="#remote_modal">Ajustar Tamaño...</a> 
            <a id="clear-button-<?=$field['@key']?>" class="btn btn-default btn-xs pull-left clear-image <?php if(empty($content)) {?>hide<?php }?>" data-field-key="<?=$field['@key']?>">Quitar...</a> 
        </div>
    </div>
</div>
<?php 
//pr($field);
?>