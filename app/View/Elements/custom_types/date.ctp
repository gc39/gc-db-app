<?php 

$dateParts = explode('-', $content);

if(isset($field['@no-transform-to-mysql']) && $field['@no-transform-to-mysql'] == true) {
    $content = count($dateParts) == 3 ? $dateParts[0].'/'.$dateParts[1].'/'.$dateParts[2] : '';
} else {
    $content = count($dateParts) == 3 ? $dateParts[2].'/'.$dateParts[1].'/'.$dateParts[0] : '';
}

if(isset($fechahoy))
    $content = $fechahoy;
    
?>
<div class="form-group form-group-<?=$field['@key']?>">
    <label class="col-sm-2 control-label text-right"><?=$field['@label']?></label>
    <div class="col-sm-10">
        <?php if($field['@editable'] == 'false'){?>
            <input class="form-control " name="<?=$field['@key']?>" id="<?=$field['@key']?>" autocomplete="off" value="<?=$content?>" readonly/>
        <?php } else{?>
            <input class="form-control datepicker" name="<?=$field['@key']?>" id="<?=$field['@key']?>" autocomplete="off" value="<?=$content?>"  <?php if($field['@readonly']=="true") { ?>readonly<?php }?> />
        <?php }?>
    </div>
</div>