<div class="form-group form-group-<?=$field['@key']?> <?=$field['@edit-visibity']?>">
    <label class="col-sm-2 control-label text-right"><?=$field['@label']?></label>
    <div class="col-sm-10">
                        
        <select <?php if(isset($field['@multiple']) && $field['@multiple'] === "true") {?>multiple="multiple"<?php }?> <?php if(isset($field['@readonly']) && $field['@readonly'] === "true") {?>disabled<?php }?> class="form-control <?php if(isset($field['@logical-show-behavior'])) {?>logical-select-behavior<?php }?>" <?php if(isset($field['@linked-to'])) {?>data-linked-to="<?=$field['@linked-to']?>"<?php }?> name="<?=$field['@key']?><?php if(isset($field['@multiple']) && $field['@multiple'] === "true") {?>[]<?php }?>" id="<?=$field['@key']?>" >

            <?php
            foreach($field['option'] as $option ){
                
                if(isset($field['@multiple']) && $field['@multiple'] === "true") {
                    
                    $selectedValuesArray = explode('|#|', $content);
                    $selectedTag = in_array($option['@value'], $selectedValuesArray) ? 'selected' : '';
                    
                } else {
                    $selectedTag = (strtolower($content) == strtolower($option['@value'])) ? 'selected' : '';
                }
                
                $linkedTo    = (isset($field['@linked-to'])) ? $option['@value'] : '';
            ?>
            <option <?=$selectedTag?> value="<?=$option['@value']?>" data-show-fields='<?=$option['@show']?>' data-hide-fields='<?=$option['@hide']?>'><?=$option['@']?></option>
            <?php } ?>
            
        </select>
    </div>
</div>