<!--<div class="form-group">
    <label class="col-sm-2 control-label text-right"><?=$field['@label']?></label>
    <div class="col-sm-10">
       <textarea rows="5" cols="5" class="form-control" name="<?=$field['@key']?>" id="<?=$field['@key']?>"><?=$content?></textarea>
         
    </div>
</div>-->

<div class="form-group form-group-<?=$field['@key']?>">
    <label class="col-sm-2 control-label text-right"><?=$field['@label']?></label>
    <div class="col-sm-10">       
     <textarea rows="5" cols="5" class="form-control" <?php if(isset($field['@max-length'])) { ?>maxlength="<?=$field['@max-length']?>"<?php }?> name="<?=$field['@key']?>" id="<?=$field['@key']?>"><?=$content?></textarea>
    </div>
</div>

