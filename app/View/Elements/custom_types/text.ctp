<div class="form-group form-group-<?=$field['@key']?> <?=$field['@edit-visibity']?>">
    <label class="col-sm-2 control-label text-right"><?=$field['@label']?></label>
    <div class="col-sm-10">  
        <input type="text" class="form-control" value="<?=$content?>" autocomplete="off" name="<?=$field['@key']?>" placeholder="<?=$field['@placeholder']?>" id="<?=$field['@key']?>" 
        <?php if(isset($field['@max-length'])) { ?>maxlength="<?=$field['@max-length']?>"<?php }?>
        <?php if($field['@readonly']=="true") { ?>readonly<?php }?>
        >
    </div>
</div>