<!-- Main navigation -->
<ul class="navigation">

    <li class="<?php if($activeMenu == 'dashboard') { ?>active<?php }?>"><a href="/"><span>Home</span> <i class="icon-home"></i></a></li>

    <?php
    
    /*
    foreach($modulesMenu as $keyModule => $module) { 
        
        $nameParts = explode(':', $module['name']);
        
        if(isset($nameParts[0]) && isset($nameParts[1])) {
            $groupModulesMenu[$nameParts[0]][$keyModule] = $module;
        } else {
            $groupModulesMenu['DB_UNGROUP'][$keyModule] = $module;
        }
    }
    */

    //Ordena array de menu alfabeticamente
    function OrdenaArray($a,$b)
    {
        if ($a['name']==$b['name']) return 0;
        return ($a['name'] < $b['name'])?-1:1;
    }
    uasort($modulesMenu,"OrdenaArray");

    foreach($modulesMenu as $keyModule => $module) { 
        
        $nameParts = explode(':', $module['name']);
        
        if(isset($nameParts[0]) && isset($nameParts[1])) {
            if($userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array($keyModule, $userLogged['User']['modules']['view'])){
                $groupModulesMenu[$nameParts[0]][$keyModule] = $module;
            }
        } else {
            if($userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array($keyModule, $userLogged['User']['modules']['view'])){
                $groupModulesMenu['DB_UNGROUP'][$keyModule] = $module;
            }
        }
    }

    
    
    foreach($groupModulesMenu as $keyGroup => $modulesMenu) { 
        
        if($keyGroup != 'DB_UNGROUP') 
        {                  
            reset($modulesMenu);
            $first_key = key($modulesMenu);                     
            
            //Titulos de los que SI tienen subtitulos
            echo '<li class="root-has-ul"><a href="#" class="has-ul"><span>'.$keyGroup.'</span><i class="icon-plus-circle"></i></a>';  
            echo '<ul>';            
        }
        
        
        foreach($modulesMenu as $key => $module) { 
            
            if($keyGroup != 'DB_UNGROUP') {
                
                $nameParts = explode(':', $module['name']);                
                $moduleNameHTML = $nameParts[1];
                
            } else {
                //Titulos de los que NO tienen subtitulos
                $moduleNameHTML = '<span>'.$module['name'].'</span> <i class="'.$module['icon'].'"></i>';
            }
            
            //subtitulos aplicando permisos
            if($userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array($module['url'], $userLogged['User']['modules']['view'])) 
            {
                ?>
                    <li class="module <?php if($activeMenu == $key){?>active<?} ?>">
                        <a href="/<?=$module['url']?>">
                            <?=$moduleNameHTML?>
                        </a>
                    </li>
                <? 
            }
        }
        
        if($keyGroup != 'DB_UNGROUP') {
            echo '</li></ul>';            
        }
    }
    ?>
    
<!--    <li <?php if($activeMenu == 'gallery') { ?>class="active"<?php }?>><a href="/gallery"><span>Galeria</span> <i class="icon-images"></i></a></li>    
    <li <?php if($activeMenu == 'monitor') { ?>class="active"<?php }?>><a href="/monitor"><span>Monitor</span> <i class="icon-meter-fast"></i></a></li> 
-->
    
    <?php    
    if($userLogged['User']['rol'] == 'PLATFORM_ADMIN') {
    ?>
    <li class="<?php if($activeMenu == 'users') { ?>active<?php }?> root-has-ul"><a href="/users"><span>Usuarios y Permisos</span> <i class="icon-user"></i></a></li>
    
    <li class="root-has-ul"><a href="#" class="has-ul"><span>Monitoreo</span> <i class="icon-wrench2"></i></a>
        <ul style="<?php if($activeMenu == 'm' ) { ?> display: block <?php }?>">
            <li class="active"><a href="/monitoreo/monitoreo_pantallas">Monitoreo Pantallas</a></li>
            <li <?php if($activeMenu == 'offlineTools') { ?>class="active"<?php }?>><a href="/monitoreo/carga_contenido">Carga de Contenido</a></li>
            <li <?php if($activeMenu == 'offlineTools') { ?>class="active"<?php }?>><a href="/monitoreo/control_publicacion">Control de Publicación</a></li>
            <li <?php if($activeMenu == 'offlineTools') { ?>class="active"<?php }?>><a href="/monitoreo/control_exhibicion">Control de Exhibición</a></li>
            <li <?php if($activeMenu == 'offlineTools') { ?>class="active"<?php }?>><a href="/monitoreo/control_conexion">Control de Conexión</a></li>
            <li <?php if($activeMenu == 'offlineTools') { ?>class="active"<?php }?>><a href="/monitoreo/control_general">Control de General</a></li>
		</ul>
	</li>

    <!-- 
    <li class="<?php if($activeMenu == 'tools' || $activeMenu == 'createClient' ) { ?>active<?php }?> root-has-ul"><a href="#" class="has-ul"><span>Herramientas</span> <i class="icon-wrench2"></i></a>
		<ul style="<?php if($activeMenu == 'tools' || $activeMenu == 'createClient' ) { ?> display: block <?php }?>">
			
            <li <?php if($activeMenu == 'offlineTools') { ?>class="active"<?php }?> ><a href="/tools/offlineTools">Offline Tools</a></li>
            <li <?php if($activeMenu == 'templatesURLS') { ?>class="active"<?php }?> ><a href="/tools/templatesURLs">Templates URLs</a></li>
		</ul>
	</li>
    -->
    
    
    <?php
    }    
    ?>
</ul>
<!-- /main navigation -->