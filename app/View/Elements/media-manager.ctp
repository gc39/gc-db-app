
<ul class="nav nav-tabs">
    <?php if(!$fromMediaModal) {?>
    <li class="active"><a href="/gallery" class="button-image" ><i class="icon-images"></i> Imagenes</a></li>
    <?php } else { 
		$random = time();
	?>        
    <li class="active"><a href="/gallery/mediaModal/<?php echo $selectedImageID . '/' . $inputField . '/' . $moduleKey . "/?r=".$random ?>" class="special-gallery-tab button-image"><i class="icon-images"></i> Imagenes</a></li>
    <?php } 
    
    if($userLogged['User']['rol'] != 'BASIC_USER' && $fromMediaModal) { ?>    
    <li><a href="#file-upload" data-toggle="tab"><i class="icon-upload4"></i> Subir Imagen</a></li>
    <?php } ?>
</ul>

<div id="media-manager" class="tab-content <?php if($fromMediaModal) {?>in-modal choose-media-enabled<?php } ?>">
	
    <!-- First tab -->
    <div class="tab-pane active fade in" id="image-grid">
		<div class="bar block clearfix hide">
			<form class="bar-left <?php if($fromMediaModal){ echo 'mediaModal'; } ?>" action="/gallery" method="GET" id="searchForm" >
				<label>Buscar Imagen: </label>
				<input type="text" name="search" id="search" class="form-control" placeholder="Buscar en galería..." value="<?=$search?>">
				<button type="submit" class="btn btn-primary btn-icon btn-loading" data-loading-text="<i class='icon-spinner7 spin'></i>"><i class="icon-search3"></i></button>
			</form>
		</div>
        
        <!-- With titles (frame) -->
        <div class="row">
            
            <?php 
            
            $filesPath = $clientFolder == '' ? '/files/' : '/files/' . $clientFolder . '/';			
			
            foreach($imagesList as $image) {
                                
                $imageURL = $clientFolder == '' ? '/files/'.$image['Image']['filename'] : '/files/' . $clientFolder . '/'. $image['Image']['filename'];
                
            ?>
            <div class="image-box col-lg-3 col-md-6 col-sm-6" data-image-id="<?=$image['Image']['id']?>">
                <div class="thumbnail thumbnail-boxed">
                    <div class="thumb <?php if($selectedImageID == $image['Image']['id'])  {?>selected<?php }?>" data-src="<?=$imageURL?>" data-image-id="<?=$image['Image']['id']?>">
                        
                        <img src="/images/interface/check-box-icon.png" class="checked" />
                        
                        <?php if($userLogged['User']['rol'] != 'BASIC_USER') { ?>    
                        <div class="thumb-options">
                            <span class="normal-options">
                                <a href="#" class="btn btn-icon btn-success edit" data-id="<?=$image['Image']['id']?>"><i class="icon-pencil3"></i></a>
                                <a href="#" class="btn btn-icon btn-success delete" data-id="<?=$image['Image']['id']?>"><i class="icon-remove"></i></a>
                            </span>
                            <span class="hide edit-options">
                                <a href="#" class="btn btn-icon btn-success save-edit" data-id="<?=$image['Image']['id']?>"><i class="icon-checkmark"></i></a>
                                <a href="#" class="btn btn-icon btn-default cancel-edit" data-id="<?=$image['Image']['id']?>"><i class="icon-close"></i></a>
                            </span>                            
                        </div>
                        <?php } ?>    
                    </div>
                    <div class="caption">
                        <span class="caption-title is-editable" data-id="<?=$image['Image']['id']?>"><?=$image['Image']['description']?></span>                                
                    </div>
                </div>
            </div>
            <?php 
			
			} ?>
            
        </div>
        <!-- /with titles (frame) -->

        <?php if($pages > 1) { ?>
        <div class="block text-center">
            <?php if($fromMediaModal) { ?>
            <ul class="pagination">
                <? if($page > 1) { ?>
                <li><a href="/gallery/mediaModal/<?=$selectedImageID?>/<?=$inputField?>/<?=$moduleKey?>/<?=($page - 1)."?r=".$random.'&search='.$search?>">←</a></li>
                <? } ?>
                <? for($i = 1; $i <= $pages; $i++) { ?>
                <li <?php if($page == $i) { ?>class="active"<?php } ?>><a href="/gallery/mediaModal/<?=$selectedImageID?>/<?=$inputField?>/<?=$moduleKey?>/<?=$i."?r=".$random.'&search='.$search?>"><?=$i?></a></li>  
                <? } ?>
                <? if($page < $pages) { ?>
                <li><a href="/gallery/mediaModal/<?=$selectedImageID?>/<?=$inputField?>/<?=$moduleKey?>/<?=($page + 1)."?r=".$random.'&search='.$search?>">→</a></li>
                <? } ?>        
            </ul>
            <?php } else { ?>
            <ul class="pagination">
                <? if($page > 1) { ?>
                <li><a href="/gallery/all/<?=($page - 1).'?search='.$search?>">←</a></li>
                <? } ?>
                <? for($i = 1; $i <= $pages; $i++) { ?>
                <li <?php if($page == $i) { ?>class="active"<?php } ?>><a href="/gallery/all/<?=$i.'?search='.$search?>"><?=$i?></a></li>  
                <? } ?>
                <? if($page < $pages) { ?>
                <li><a href="/gallery/all/<?=($page + 1).'?search='.$search?>">→</a></li>
                <? } ?>        
            </ul>
            <?php } ?>
        </div>
        <? } ?>

    </div>
    <!-- /first tab -->


    <!-- Second tab -->
    <div class="tab-pane fade" id="file-upload">
    
        
        <!-- Multiple file uploader inside panel -->
        <div class="panel panel-primary">
            <div class="panel-heading"><h6 class="panel-title"><i class="icon-file-upload"></i> Subir Imagenes</h6></div>
            <div class="multiple-uploader">Su navegador no soporta upload de archivos.</div>
        </div>
        <!-- /multiple file uploader with header inside panel -->

    </div>
    <!-- /second tab -->

</div>

<script>
    $(document).ready(function() {   
          
        //console.log('hola');
        setTimeout(function(){ 
            $('div.thumb').unveil(200, function() { }); 
            console.log('unveil');
        }, 200);
        
        var isLogged = true;
        
        //$(".multiple-uploader").unbind();
        $(".multiple-uploader").pluploadQueue({
            runtimes : 'html5, html4',
            url : '/gallery/upload/<?=$moduleKey?>',            
            unique_names : true,
            filters : {
                    max_file_size : '6mb',
                    mime_types: [
                            {title : "Image files", extensions : "jpg,gif,png"},				
                    ]
            },
            
            init : {
                
                BeforeUpload: function(up, file) {
                                         
                    $.ajax({
                        url: '/gallery/isLogged',
                        async: false
                    }).done(function(response) {                        
                        isLogged = response == 1 ? true : false;
                    })
                    
                    if(!isLogged) {
                        //$('.modal-content').load('/user/loggin');
                        location.href = '/user/login';
                    }
                    
                    return isLogged;
                  
                },
                
                UploadComplete: function(up, files) {                       
                    $('.button-image').attr( 'href', $('.button-image').attr( 'href' ) + '&upload=1' );
                    $('.button-image').trigger( 'click' );
                }
            }
        });

		$( '#searchForm' ).submit(function(e){
			if( $( this ).hasClass( 'mediaModal' )){
				
				$('.modal-content').load( '/gallery/mediaModal/<?=$selectedImageID?>/?search=' + $( '#search' ).val() );
				return false;
			}
		});
		
        $('a.special-gallery-tab').click(function(e) {            

            e.preventDefault();
            $('.modal-content').load($(this).attr('href'));            

        });
        
        $('.in-modal .pagination a').click(function(e) {
            
            e.preventDefault();
            $('.modal-content').load($(this).attr('href'));            
            
        });
        
        $('#media-manager .btn.edit').click(function(e) { 
            e.stopPropagation();
            makeImageEditable($(this).data('id'));
        });

        $('#media-manager .btn.cancel-edit').click(function(e) { 
            e.stopPropagation();
            cancelImageEditable($(this).data('id'));
        });    

        $('#media-manager .btn.save-edit').click(function(e) {    
            e.stopPropagation();
            saveImageEdit($(this).data('id'));
        }); 

        $('#media-manager .delete-image').click(function(e) {     
            e.stopPropagation();
            $(this).addClass('hide');
            $(this).parent().find('.product-thumb').attr('src', '/files/no_image.gif');
            $(this).parent().find('.product-thumb').attr('data-image-id', '0');        
        }); 

        $('#media-manager .delete').click(function(e) {     
            e.stopPropagation();
            if(confirm('¿Esta seguro que desea borrar esta imagen?')) {
                deleteImage($(this).data('id'));
            }               
        }); 

        $('.choose-media-enabled #image-grid .thumb').click(function() {                               
            
            $('.choose-media-enabled .thumb.selected').removeClass('selected');                      
            $(this).addClass('selected');                        
            
        });
        
        <?php if($fromUpload) {?>        
        $('.choose-media-enabled #image-grid .thumb:first').trigger('click');                
        <?php } ?>   
    
    });
    
    function deleteImage(id) {
	
		var tempScrollTop = $(window).scrollTop();
		
        $.get('/gallery/delete/' + id, function(data) {                            
            $('div[data-image-id=' + id + ']').effect('fade', {}, 700);       
        });

		setTimeout(function(){$(window).scrollTop(tempScrollTop)},1);
		
    }
    
    function makeImageEditable(id) {        

		var tempScrollTop = $(window).scrollTop();

        element = $('span[data-id=' + id + '].is-editable');
        
        if(!element) {
            return false;
        }
        
        $(element).parent().parent().find('.thumb-options').css('opacity', '1');
        $(element).parent().parent().find('.normal-options').addClass('hide');
        $(element).parent().parent().find('.caption').addClass('no-padding');
        $(element).parent().parent().find('.edit-options').removeClass('hide');            

        value = $(element).text();               

        $(element).attr('data-original-value', value);
        $(element).html('<input type="text" id="form-control-' + id + '" class="form-control" value="' + value + '" maxlength="255" style="height:29px;">');
		
		$("#form-control-" + id )[0].select();
		
		setTimeout(function(){$(window).scrollTop(tempScrollTop)},1);
        
    }

    function cancelImageEditable(id) {  
		
		var tempScrollTop = $(window).scrollTop();
          
        element = $('span[data-id=' + id + '].is-editable');
        
        if(!element) {
            return false;
        }
        
        $(element).text($(element).attr('data-original-value'))
                    
        $(element).parent().parent().find('.thumb-options').removeAttr('style');
        $(element).parent().parent().find('.edit-options').addClass('hide');  
        $(element).parent().parent().find('.normal-options').removeClass('hide');        
        $(element).parent().parent().find('.caption').removeClass('no-padding');
		
		setTimeout(function(){$(window).scrollTop(tempScrollTop)},1);
    }
    
    
    function saveImageEdit(id) {  
        
		var tempScrollTop = $(window).scrollTop();
		
        var haveError = false;                
        
        data_fields = '';
        
        var element = $('span[data-id=' + id + '].is-editable');
        
        if(!element) {
            return false;
        }
        
        data_fields = 'image_id_' + $(element).data('id') + '=' + $(element).find('input').val();                  
        
        if(data_fields != '') {            
            $.getJSON('/gallery/save?' + data_fields, function(data) {                                                       
                $(element).removeAttr('data-original-value');
                $(element).html(data.value);                                                            
                $(element).parent().effect('highlight', {}, 1000);                
                $(element).parent().parent().find('.thumb-options').removeAttr('style');
                $(element).parent().parent().find('.edit-options').addClass('hide');  
                $(element).parent().parent().find('.normal-options').removeClass('hide');    
                $(element).parent().parent().find('.caption').removeClass('no-padding');           
             });
        } else {
            $(element).parent().parent().find('.thumb-options').removeAttr('style');
            $(element).parent().parent().find('.edit-options').addClass('hide');  
            $(element).parent().parent().find('.normal-options').removeClass('hide');    
            $(element).parent().parent().find('.caption').removeClass('no-padding');            
        }
             
        setTimeout(function(){$(window).scrollTop(tempScrollTop)},1);
		
        return true;
                        
    }    
</script>   