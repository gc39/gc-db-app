<!-- Navbar -->
<div class="navbar navbar-inverse" role="navigation">
    <div class="navbar-header">
        <a class="navbar-brand" href="/">DIGITAL BOARD</a>
        <a class="sidebar-toggle"><i class="icon-paragraph-justify2"></i></a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
            <span class="sr-only">Toggle navbar</span>
            <i class="icon-grid3"></i>
        </button>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar">
            <span class="sr-only">Toggle navigation</span>
            <i class="icon-paragraph-justify2"></i>
        </button>
    </div>

    <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">

    <!--
    <?php if($client == 'sodexo'){ ?>
        <li class="user dropdown">
        

            <a class="dropdown-toggle" data-toggle="dropdown" style="background: #397CA5">
                <span> Publicaciones </span>
                <i class="caret"></i>
            </a>
            <ul class="user-menu dropdown-menu dropdown-menu-right icons-right">             
                <li><a href="/publications"> Centro de Publicación </a></li>
                <li><a href="/publications/reports"> Reportes </a></li>
                <li><a href="/publications/publicationsReviewImages"> Revisión Publicaciones </a></li>
                <?php if($userLogged['User']['rol'] == 'PLATFORM_ADMIN'){?>
                <li><a href="/publications/generateFolder"> Creación Carpetas </a></li>
                <?php } ?>
            </ul>
        </li> 
       
    <?php }else if($userLogged['User']['email'] == 'mr@grupoclan.cl' || ($client != 'johnson' && $client != 'paris') ){ ?>
        <li>
            <a class="dropdown-toggle" data-toggle="dropdownx" href="/publications" style="background: #397CA5">
                <i class="icon-screen2"></i> Centro de Publicación &nbsp;
                <span id="unread-notifications" class="hide label label-default">!</span>
            </a>                     
        </li>
    <?php } ?>

    -->

        <li class="user dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown">
                <img src="/images/profile.png" alt="">
                <span><?=$userLogged['User']['fullname']?></span>
                <i class="caret"></i>
            </a>
            <ul class="user-menu dropdown-menu dropdown-menu-right icons-right">             
                <li><a href="#" data-toggle="modal" data-target="#change-password-modal"><i class="icon-key2"></i> Modificar Contraseña</a></li>
                <li><a href="/users/login"><i class="icon-exit"></i> Salir</a></li>
            </ul>
        </li>
    </ul>
</div>
<!-- /navbar -->