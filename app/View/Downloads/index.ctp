
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Descargas</h3>
    </div>
</div>
<!-- /page header -->


<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li> 
        <li class='active'>Descargas</li> 
    </ul>

</div>
<!-- /breadcrumbs line -->
<div style="margin-bottom: 20px;">
<?php

$root = WWW_ROOT . 'files/sodexo/templates_images/Sura/U1/'. date('Y').'/'.date('m').'/'.date('j');
$root_www = '/files/sodexo/templates_images/Sura/U1/'. date('Y').'/'.date('m').'/'.date('j');
    
if($userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array( 'sura_unidad1_menu_diario', $userLogged['User']['modules']['view'])) {

    if(file_exists($root.'/Arr/version1_menu_diario.jpg')) {
?>

    <div class="col-lg-3 text-center">
        <img  src="<?=$root_www?>/Arr/version1_menu_diario.jpg" width="100%" /><br><br>
        <a href="<?=$root_www?>/Arr/version1_menu_diario.jpg" download="" class="btn btn-success">Descargar</a>
    </div>
    <div class="col-lg-3 text-center">
        <img  src="<?=$root_www?>/Arr/version2_menu_diario.jpg" width="100%" /><br><br>
        <a href="<?=$root_www?>/Arr/version2_menu_diario.jpg" download="" class="btn btn-success">Descargar</a>
    </div>
    <br><br>
<?php
    } else {
?>
    <div class="row">        
        <div class="col-md-6">
            <div class="alert alert-block alert-danger fade in block-inner">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h6><i class="icon-command"></i> Descargas no disponibles</h6>
                <hr>
                <p>No hay descargas disponibles para este dia.</p>                
            </div>           
        </div>
    </div> 
<?php
    }
?>


<?php } else { ?>
<div class="row">        
    <div class="col-md-6">
        <div class="alert alert-block alert-danger fade in block-inner">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h6><i class="icon-command"></i> Descargas no disponibles</h6>
            <hr>
            <p>Usted no tiene descargas disponibles.</p>                
        </div>           
    </div>
</div>  
<?php } ?>

</div>
<div class="clearfix"></div>