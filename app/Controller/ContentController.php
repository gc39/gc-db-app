<?php

include_once 'generateModuleFiles.php';
include_once 'XML2Array.php';
//include_once 'config.php'


App::uses('AppController', 'Controller');

class ContentController extends AppController
{

    public  $uses       = array('Content', 'Image', 'ImageCrop', 'PublishHistory', 'PublishPlayerStatus', 'PublishAlerts', 'PublishAlertsLogs', 'GCDS');
    private $modules    = false;
    private $moduleKey  = false;
    private $moduleData = false;

    public function beforeFilter()
    {
        parent::beforeFilter();

        $userLogged = parent::getLoggedUser();

        if (isset($_GET['fromJOB']) || is_array($userLogged)) {

            $this->modules = parent::initModules();
            if (!$this->modules) {
                die('Error al cargar el XML de configuracion de modulos');
            }

            // Consulta los modulos que son corporativos por base de datos
            $server_db = LOCALSERVER;
            $user_db = LOCALUSERDB;
            $password_db = LOCALPASSDB;
            $db_db = LOCALDB;

            $current_url = $_SERVER["REQUEST_URI"];
            $current_url_array = explode('/', $current_url);
            $current_url = $current_url_array[1];

            if (strpos($current_url, 'publicacionmultiple_') === 0) {
                $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
                if (!$obj_conexion)
                {
                    echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
                    die();
                }

                $select_qry = 'SELECT config_modulos.id, config_modulos.cliente_sucursal, concat( config_modulos.cliente_sucursal, "_menu_diario" ) AS nombre_modulo, config_publicacionmultiple.publicacion_multiple FROM config_modulos INNER JOIN config_publicacionmultiple ON config_modulos.id = config_publicacionmultiple.id_config_modulo';
                
                if (!$resultado = $obj_conexion->query($select_qry))
                {
                    echo $select_qry;
                    echo ".Lo sentimos, este sitio web está experimentando problemas.";
                    die();
                }
                $jobs_modulos = Array();
                while ($registro = $resultado->fetch_assoc()) {
                    $tmp_array = array();
                    $tmp_array[] = $registro['nombre_modulo'];
                    $tmp_array[] = $registro['publicacion_multiple'];
                    //$tmp_array[] = strtolower($registro['filtro_carpeta']);
                    $jobs_modulos[] = $tmp_array;
                }
                $this->set('modulesMenu_corporativo', $jobs_modulos);
                $obj_conexion->close();
            }

            // Fin de la consulta
            $modulesMenu = parent::getModulesMenu($this->modules);
            $client = $this->GCDS->getClientSubdomain();
            
            $this->moduleKey = isset($this->request->params['pass'][0]) ? $this->request->params['pass'][0] : false;
            $this->moduleKey = (!$this->moduleKey && isset($this->request->params['module'])) ? $this->request->params['module'] : $this->moduleKey;

            if (!isset($this->modules[$this->moduleKey])) {
                die('Modulo no configurado');
            } else {
                $this->moduleData = $this->modules[$this->moduleKey];
            }

            if (!isset($_GET['fromJOB']) && ($userLogged['User']['rol'] != 'PLATFORM_ADMIN' && !in_array($this->moduleKey, $userLogged['User']['modules']['view']))) {
                return $this->redirect('/users/login');
            } else {

                $this->set('userLogged', $userLogged);
                $this->set('client', $client);
                $this->set('modulesMenu', $modulesMenu);
                $this->set('activeMenu', $this->moduleKey);
                $this->set('moduleKey', $this->moduleKey);
                $this->set('viewTitle', $this->moduleData['gcdb']['content_name']);
                $this->set('subdomain', $client);
            }
        } else {

            if ($this->request->is('ajax')) {
                die('NO_LOGGED');
            } else {
                return $this->redirect('/users/login');
            }
        }
    }

    public function uploadQueComoServer() {
        $this->layout = 'ajax';
        $this->autoRender = false;

        if (STANDALONE_MODE === false)
        {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false)
            {

                $status = array(
                    'success' => false,
                    'message' => 'No se pudo resolver el cliente'
                );

            }

        }
        else
        {
            $clientFolder = '';
        }

        if (isset($_FILES['file']['tmp_name']))
        {
            // filename
            $file_name = $_FILES['file']['name'];
            $file_name = 'M-'.date('Y').date('m').date('d').'-'.date('H').date('i').date('s').'-'.$file_name;

            // sourcefile
            $temp_file_location = $_FILES['file']['tmp_name'];

            $ftp_server = 'digitalboard.app';
            $ftp_user_name = 'appnovedades@digitalboard.app';
            $ftp_user_pass = 'qazplm_01';

            // establecer una conexión básica
            $conn_id = ftp_connect($ftp_server);

            // iniciar sesión con nombre de usuario y contraseña
            $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
            
            // cargar un archivo
            //if (ftp_put($conn_id, $file_name, $temp_file_location, FTP_ASCII)) {
            if (ftp_put($conn_id, $file_name, $temp_file_location, FTP_BINARY)) {
                echo 'https://quecomo.digitalboard.app/videos/'.$file_name;
                unlink($temp_file_location);
            } else {
                echo "Error";
            }

            // cerrar la conexión ftp
            ftp_close($conn_id);
        }
    }

    public function uploadS3()
    {
        $this->layout = 'ajax';
        $this->autoRender = false;

        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {

                $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
            }
        } else {
            $clientFolder = '';
        }

        App::import('Vendor', 'aws', array('file' => 'aws' . DS . 'aws-autoloader.php'));

        if (isset($_FILES['file']['tmp_name'])) {

            $file_name = $_FILES['file']['name'];
            $temp_file_location = $_FILES['file']['tmp_name'];

            $s3 = new Aws\S3\S3Client([
                'region'  => 'sa-east-1',
                'version' => 'latest',
                'credentials' => [
                    'key'    => "AKIAI6RRD4UURE36HQPQ",
                    'secret' => "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",
                ]
            ]);

            $result = $s3->putObject([
                'Bucket' => 'data.digitalboard.cl',
                'Key'    => strtolower($clientFolder) . '/' . time() . '_' . rand(1, 99999) . '_' . $file_name,
                'SourceFile' => $temp_file_location
            ]);


            echo $result->get('ObjectURL');
        }
    }

    public function index($module, $page = 1)
    {
        $isSearch     = false;
        $searchTerm   = '';

        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {
                die('No se pudo determinar el subdominio del cliente');
            }
        } else {
            $clientFolder = '';
        }

        if (isset($this->request->data['import_from_csv'])) {

            $delete_all    = isset($this->request->data['delete_all']) ? true : false;
            // die($this->request->data['field_separator']);

            $userLogged = parent::getLoggedUser();
            $segmento = $this->modules[$this->moduleKey]["gcdb"]["segmento"];
            $cliente = $this->modules[$this->moduleKey]["gcdb"]["cliente"];
            $sucursal = $this->modules[$this->moduleKey]["gcdb"]["sucursal"];
            $usuario = $userLogged['User']['fullname'];
            $usuario_email = $userLogged['User']['email'];

            $userLogged = parent::getLoggedUser();
            $segmento = $this->moduleData['gcdb']['segmento'];
            $cliente = $this->moduleData['gcdb']['cliente'];
            $sucursal = $this->moduleData['gcdb']['sucursal'];

            // no es posible registrar el evento por aqui
            //$this->Content->registrarEventoImportacion($usuario_email, $segmento, $cliente, $sucursal);

            if (isset($this->moduleData['gcdb']['import_map'])) {
                $status_import = $this->Content->importFromCSVGeneric($this->params['form']['file']['tmp_name'], $delete_all, $module, $this->moduleData['gcdb']['import_map'], $this->request->data['field_separator'], $module, $segmento, $cliente, $sucursal, $usuario_email);
            } else {
                $status_import = $this->Content->importFromCSV($this->params['form']['file']['tmp_name'], $delete_all, $module);
            }

            if ($status_import === true) {
                //$response_updated = file_get_contents("http://api.digitalboard.cl/sodexo/modules/{$this->moduleKey}/updated");

                // $userLogged = parent::getLoggedUser();
                // $segmento = $this->moduleData['gcdb']['segmento'];
                // $cliente = $this->moduleData['gcdb']['cliente'];
                // $sucursal = $this->moduleData['gcdb']['sucursal'];

                // $this->Content->registrarEventoImportacion($usuario_email, $segmento, $cliente, $sucursal);

                $message = 'Se importo el CSV con éxito.';
                CakeSession::write('moduleEdit.' . $this->moduleKey, $this->moduleKey);
            } else {
                if ($status_import == 'MISSING_COLUMNS_SEPARATOR') {
                    $message = 'Faltan columnas, realice otro intento seleccionado otro separador de columnas.';
                }
                if ($status_import == 'MISSING_COLUMNS') {
                    $message = 'Faltan columnas, lea con atencion las columnas requeridas en la ventana de importación';
                }
                if ($status_import == 'FILE_NOT_FOUND') {
                    $message = 'Error al subir el archivo';
                }
                if ($status_import == 'INVALID_DATA') {
                    $message = 'Hay campos con datos inválidos';
                }
                if ($status_import == 'BAD_COLUMN_NUMBERS') {
                    $message = 'Número de columnas encontradas no coinciden con lo esperado';
                }
            }

            $this->Session->setFlash($message, 'flash_custom');
        }

        if (isset($this->request->data['duplicate-action'])) {

            if ($this->request->data['id-row-duplicate'] && $this->request->data['duplicate-destination']) {

                foreach ($this->moduleData['gcdb']['fields']['field'] as $field) {
                    if ($field['@key'] != 'id') {
                        $fields[] = $field['@key'];
                    }
                }

                $fields[] = 'created';
                $fields[] = 'modified';

                $db = ConnectionManager::getDataSource('default');

                $originIDs = explode(',', $this->request->data['id-row-duplicate']);

                foreach ($originIDs as $sourceID) {
                    $db->query('INSERT INTO ' . $this->request->data['duplicate-destination'] . ' (' . implode(',', $fields) . ') SELECT ' . implode(',', $fields) . ' FROM ' . $module . ' WHERE ' . $module . '.id = ' . $sourceID);
                }

                CakeSession::write('moduleEdit.' . $this->moduleKey, $this->moduleKey);
                $this->Session->setFlash('Contenido duplicado con éxito.', 'flash_custom');

                $this->redirect('/' . $module);
            }
        }

        if (isset($this->request->data['delete-action'])) {
            
            if ($this->request->data['id-row-delete']) {
                
                $db = ConnectionManager::getDataSource('default');

                $originIDs = explode(',', $this->request->data['id-row-delete']); 
                $originServices = explode(',', $this->request->data['id-row-servicios']);
                $originDishes = explode(',', $this->request->data['id-row-platos']);
                $originDishTypes = explode(',', $this->request->data['tipo-platos']);

                for($index = 0; $index < count($originIDs); $index++){
                    // $segmento,$cod_modificado,$accion,$fecha_exhib,$servicio,$codigo_plato,$nombre_plato,$cliente,$sucursal,$usuario

                    $userLogged = parent::getLoggedUser();
                    $segmento = $this->modules[$this->moduleKey]["gcdb"]["segmento"];
                    $cod_modificado = $originIDs[$index];
                    $accion = 'plato eliminado';
                    $fecha_exhib = '';
                    $servicio = $originServices[$index];
                    $codigo_plato = $originIDs[$index];
                    $nombre_plato = $originDishes[$index];
                    $tipo_plato = $originDishTypes[$index];
                    $cliente = $this->modules[$this->moduleKey]["gcdb"]["cliente"];
                    $sucursal = $this->modules[$this->moduleKey]["gcdb"]["sucursal"];
                    $usuario = $userLogged['User']['fullname'];

                    $sourceID = $originIDs[$index];

                    if($module == CONFIG_CASINOS){
                        //
                        // logica cuando se elimina casino
                        //
                        $xml_casino_module = $servicio . '_' . $tipo_plato . '_menu_diario';
                        $xml_casino_fullzonacorp1_module = $servicio . '_' . $tipo_plato . '_FullZonaCorp_1';
                        $xml_casino_fullzonacorp2_module = $servicio . '_' . $tipo_plato . '_FullZonaCorp_2';
                        $xml_casino_fullzonacorp3_module = $servicio . '_' . $tipo_plato . '_FullZonaCorp_3';
                        $xml_casino_fullzonacorpCumple_module = $servicio . '_' . $tipo_plato . '_FullZonaCorp_Cumple';
                        $xml_casino_fullzona_MinutaContig_module = $servicio . '_' . $tipo_plato . '_minuta_contingencia';
                        $xml_casino_fullzona_Sugerencia_module = $servicio . '_' . $tipo_plato . '_sugerencia_chef';

                        // Conecta al FTP
                        $ftp_conn = @ftp_connect(MISODEXOFTP, MISODEXOPORT);
                        $login_res = ftp_login($ftp_conn, MISODEXOFTPUSERNAME, MISODEXOFTPPASS);

                        // Borra la carpeta del casino
                        $dst_dir = 'SODEXO/'.$servicio.'/'.$tipo_plato;

                        if ($login_res == 1){
                            ContentController::recursiveDelete($ftp_conn, $dst_dir);
                            ftp_rmdir($ftp_conn, 'SODEXO/'.$servicio);
                            ftp_close($ftp_conn);
                        }

                        $select_qry = 'SELECT id FROM config_modulos WHERE nombre = "' . $servicio . '_' . $tipo_plato . '"';

                        $resultado = $db->query($select_qry);
                        $casinoId = $resultado[0]["config_modulos"]["id"];

                        if (file_exists(RUTAMODULOS . '/' . $xml_casino_module . '.xml')) {
                            unlink(RUTAMODULOS . '/' . $xml_casino_module . '.xml');
                        }
                        if (file_exists(RUTAMODULOS . '/' . $xml_casino_fullzonacorp1_module . '.xml')) {
                            unlink(RUTAMODULOS . '/' . $xml_casino_fullzonacorp1_module . '.xml');
                        }
                        if (file_exists(RUTAMODULOS . '/' . $xml_casino_fullzonacorp2_module . '.xml')) {
                            unlink(RUTAMODULOS . '/' . $xml_casino_fullzonacorp2_module . '.xml');
                        }
                        if (file_exists(RUTAMODULOS . '/' . $xml_casino_fullzonacorp3_module . '.xml')) {
                            unlink(RUTAMODULOS . '/' . $xml_casino_fullzonacorp3_module . '.xml');
                        }
                        if (file_exists(RUTAMODULOS . '/' . $xml_casino_fullzonacorpCumple_module . '.xml')) {
                            unlink(RUTAMODULOS . '/' . $xml_casino_fullzonacorpCumple_module . '.xml');
                        }
                        if (file_exists(RUTAMODULOS . '/' . $xml_casino_fullzona_MinutaContig_module . '.xml')) {
                            unlink(RUTAMODULOS . '/' . $xml_casino_fullzona_MinutaContig_module . '.xml');
                        }
                        if (file_exists(RUTAMODULOS . '/' . $xml_casino_fullzona_Sugerencia_module . '.xml')) {
                            unlink(RUTAMODULOS . '/' . $xml_casino_fullzona_Sugerencia_module . '.xml');
                        }

                        $db->query('DROP TABLE IF EXISTS '.$xml_casino_module);
                        $db->query('DROP TABLE IF EXISTS '.$xml_casino_fullzonacorp1_module);
                        $db->query('DROP TABLE IF EXISTS '.$xml_casino_fullzonacorp2_module);
                        $db->query('DROP TABLE IF EXISTS '.$xml_casino_fullzonacorp3_module);
                        $db->query('DROP TABLE IF EXISTS '.$xml_casino_fullzonacorpCumple_module);
                        $db->query('DROP TABLE IF EXISTS '.$xml_casino_fullzona_MinutaContig_module);
                        $db->query('DROP TABLE IF EXISTS '.$xml_casino_fullzona_Sugerencia_module);

                        $db->query('DELETE FROM config_alerta_sin_publicacion WHERE id_config_modulos = ' . $casinoId);
                        $db->query('DELETE FROM config_app_mobile WHERE id_config_modulos = ' . $casinoId);
                        $db->query('DELETE FROM config_app_web WHERE id_config_modulos = ' . $casinoId);
                        $db->query('DELETE FROM config_carpetas WHERE id_config_modulos = ' . $casinoId);
                        $db->query('DELETE FROM config_log WHERE id_config_modulos = ' . $casinoId);
                        $db->query('DELETE FROM config_pantallas WHERE id_config_modulos = ' . $casinoId);
                        $db->query('DELETE FROM config_publicacionmultiple WHERE id_config_modulo = ' . $casinoId);
                        $db->query('DELETE FROM config_modulos WHERE id = ' . $casinoId);

                        $db->query('DELETE FROM ' . $module . ' WHERE ' . $module . '.id = ' . $sourceID);

                    }
                    else{
                        $db->query('DELETE FROM ' . $module . ' WHERE ' . $module . '.id = ' . $sourceID);
                    }

                }

                CakeSession::write('moduleEdit.' . $this->moduleKey, $this->moduleKey);
                $this->Session->setFlash("Contenido eliminado con éxito. ", 'flash_custom');

                //$this->redirect('/'.$module);
                if (isset($this->request->data['s']) && !empty($this->request->data['s'])) {
                    $this->redirect('/' . $this->moduleKey . '/?s=' . $this->request->data['s']);
                } else {
                    $this->redirect('/' . $this->moduleKey);
                }
            }
        }

        if (isset($this->request->data['delete-action-motivo'])) {
            $hubo_codigos_no_encontrados = false;

            if ($this->request->data['id-row-delete']) {
                $db = ConnectionManager::getDataSource('default');

                $id_motivo = $this->request->data['select'];
                $txt_motivo = explode(':', $id_motivo)[1]; 
                $originIDs = explode(',', $this->request->data['id-row-delete']); 
                $originServices = explode(',', $this->request->data['id-row-servicios']);
                $originDishes = explode(',', $this->request->data['id-row-platos']);
                $tempDishes = explode(',', $this->request->data['id-row-platos-temporales']);
                $tipoPlatos = explode(',', $this->request->data['tipo-platos']);
                $fechaExhib = explode(',', $this->request->data['fecha_exhib']);

                $userLogged = parent::getLoggedUser();
                $segmento = $this->modules[$this->moduleKey]["gcdb"]["segmento"];
                $cliente = $this->modules[$this->moduleKey]["gcdb"]["cliente"];
                $sucursal = $this->modules[$this->moduleKey]["gcdb"]["sucursal"];

                $num_registers = count($originIDs);

                $hubo_codigos_no_encontrados = $this->Content->borrarPlatosConMotivos($module, $num_registers, $originIDs, $originServices, $fechaExhib, $originDishes, $tempDishes, $tipoPlatos, $txt_motivo, $userLogged['User']['email'], $segmento, $cliente, $sucursal);

                CakeSession::write('moduleEdit.' . $this->moduleKey, $this->moduleKey);

                if ($hubo_codigos_no_encontrados){
                    $this->Session->setFlash("Los platos con código no encontrado no se pudieron eliminar. ", 'flash_custom');
                }else{
                    $this->Session->setFlash("Contenido eliminado con éxito. ", 'flash_custom');
                }

                //$this->redirect('/'.$module);
                if (isset($this->request->data['s']) && !empty($this->request->data['s'])) {
                    $this->redirect('/' . $this->moduleKey . '/?s=' . $this->request->data['s']);
                } else {
                    $this->redirect('/' . $this->moduleKey);
                }
                die();
            }
        }

        if (isset($this->params->query['s']) && !empty($this->params->query['s'])) {
            $isSearch = true;
        }
        
        if ($this->moduleData['gcdb']['job_module']){

            // Produccion
            $server_db = LOCALSERVER;
            $user_db = LOCALUSERDB;
            $password_db = LOCALPASSDB;
            $db_db = LOCALDB;

            $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
            if (!$obj_conexion)
            {
                echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
                die();
            }

            //$select_qry = 'SELECT CronJob_Modulos.id, CronJob_Modulos.Modulo, CronJob_Modulos.Origen, CronJob_Carpetas.Servicio AS OrigenServicio, CronJob_Modulos.Destino, CronJob_Carpetas.LogoSodexo, CronJobFtp.Ip, CronJobFtp.`Port`, CronJobFtp.Usuario, CronJobFtp.Clave FROM CronJob_Carpetas INNER JOIN CronJob_Modulos ON CronJob_Carpetas.Id_CronJob_Modulos = CronJob_Modulos.id INNER JOIN CronJobFtp ON CronJob_Modulos.id_CronJobFtp = CronJobFtp.id WHERE CronJob_Modulos.Activado = 1';
            $select_qry = 'SELECT cronjob_modulos.id, cronjob_modulos.cliente_sucursal as Modulo, cronjob_modulos.cliente_sucursal as Origen, cronjob_carpetas.carpeta AS OrigenServicio, cronjob_modulos.destino AS Destino, cronjob_carpetas.logo_sodexo AS LogoSodexo, cronjob_ftp.Ip, cronjob_ftp.`Port`, cronjob_ftp.Usuario, cronjob_ftp.Clave FROM cronjob_carpetas INNER JOIN cronjob_modulos ON cronjob_carpetas.id_cronjob_modulos = cronjob_modulos.id INNER JOIN cronjob_ftp ON cronjob_modulos.id_cronjob_ftp = cronjob_ftp.id WHERE cronjob_modulos.activado = 1';
            
            if (!$resultado = $obj_conexion->query($select_qry))
            {
                //echo $select_qry;
                echo ".Lo sentimos, este sitio web está experimentando problemas.";
                die();
            }
            $jobs_modulos = Array();
            while ($registro = $resultado->fetch_assoc()) {
                $jobs_modulos[$registro['id']] = $registro['Modulo'] . ';' . $registro['Destino'];
                //echo $registro['id'] . ';' . $registro['Modulo'] . ';' . $registro['Destino'] . ';' . $registro['Activado'] . '<br>';
            }
            $job_module = true;
            $this->set('job_module', $job_module);
            $this->set('job_modules', $jobs_modulos);
            $this->set('job_module_result', $resultado);
        }

        $fieldsData     = $this->moduleData['gcdb']['fields']['field'];
        $orderBy        = isset($this->request->query['orderBy']) ? $this->request->query['orderBy'] : '';
        $orderDirection = isset($this->request->query['order']) ? $this->request->query['order'] : 'ASC';

        if (empty($orderBy)) {
            $orderParams = false;
        } else {
            $orderParams = $orderBy . ' ' . $orderDirection;
        }

        if ($isSearch) {
            $searchTerm  = trim($this->params->query['s']);

            $contentList = $this->Content->findAllRows($searchTerm, $this->moduleKey, $orderParams, $fieldsData, $page, ROWS_PER_PAGE, $pages);
        } else {
            $contentList = $this->Content->getAllRows($this->moduleKey, $orderParams, $fieldsData, $page, ROWS_PER_PAGE, $pages);
        }

        $availableDuplicateDestinations = array();

        foreach ($this->modules as $module_key => $module) {

            if ($this->Content->compareFields($this->moduleData['gcdb']['fields']['field'], $module['gcdb']['fields']['field'])) {
                $availableDuplicateDestinations[$module_key] = $module['gcdb']['content_name'];
            }
        }

        if ($isSearch || !empty($orderBy)) {

            $queryString = $isSearch ? '?s=' . $searchTerm : '';

            if ($queryString == '') {
                $queryString = '?';
            } else {
                $queryString = $queryString . '&';
            }

            $queryString = !empty($orderBy) ? $queryString . 'orderBy=' . $orderBy . '&order=' . $orderDirection : $queryString;
        } else {
            $queryString = '';
        }

        if ($this->moduleData['gcdb']['reports_app']) {
            $report = true;
        } else {
            $report = false;
        }

        if($this->moduleData['gcdb']['justificar_borrado']=='true'){
            // busca los motivos para eliminar un plato
            $motivosList = $this->Content->getAllMotivos();

            $this->set('motivosList', $motivosList);
        }

        // if($clientFolder == 'demo'){
        $this->set('sessionModuleEdit', CakeSession::read('moduleEdit'));
        $this->set('module', $this->moduleKey);
        // }

        $this->set('report', $report);
        $this->set('clientFolder', $clientFolder);
        $this->set('orderBy', $orderBy);
        $this->set('orderDirection', $orderDirection);
        $this->set('availableDuplicateDestinations', $availableDuplicateDestinations);
        $this->set('clientFolder', $clientFolder);
        $this->set('searchTerm', $searchTerm);
        $this->set('queryString', $queryString);
        $this->set('isSearch', $isSearch);
        $this->set('pages', $pages);
        $this->set('page', $page);
        $this->set('customFields', $fieldsData);
        $this->set('contentList', $contentList);
    }

    function downloadCSV()
    {
        //$this->layout = 'ajax';
        if ($this->moduleData['gcdb']['custom_csv'] == NULL) {
            $this->autoRender = false;
            $importMap = $this->moduleData['gcdb']['import_map']['map'];
            $namefileOri = $this->moduleData['gcdb']['content_name'];
            $nameFile = preg_replace('/\s+/', '_', $namefileOri);
            $nameFile2 = str_replace(':', '', $nameFile);
            $actual = '';
            $max = sizeof($importMap);
            $csvArr = $this->moduleData['gcdb']['tipos_platos']['tipo_plato'];
            $servicios = $this->moduleData['gcdb']['fields']['field'];
            $position = 0;
            $service = false;



            //
            // Analiza la estructura
            //
            // <import_map>
            //     <map column="Servicio" field="servicio" optional="no"></map>
            //     <map column="Tipo" field="tipo" optional="no"></map>
            //     <map column="Codigo" field="codigo" optional="no"></map>
            //     <map column="Calorias" field="calorias_temp" optional="si" />
            //     <map column="Fecha" field="fecha" optional="no" replace="/" replace-with="-"></map>
            // </import_map>
            foreach ($importMap as $index => $data) {

                if (strtolower($data['@column']) == 'tipo') {
                    $position = $index;
                }
                if (strtolower($data['@column']) == 'servicio') {

                    $service = true;
                }

                if ($max == ($index + 1)) {
                    $actual .= $data['@column'] . PHP_EOL;
                    //$actual .= $data['@column'] . '<br>';
                } else {
                    $actual .= $data['@column'] . ';';
                }

            }

            if ($service) {

                $tipos_platos = $this->moduleData['gcdb']['tipos_platos']['tipo_plato'];
                if(count($tipos_platos[0]) >1){
                   // var_dump(count($tipos_platos[0]));
                   // die();
                    foreach($tipos_platos as $value) {
                    
                        $tipoplato= $value['@tipoplato'];
                        $cantidad = $value['@cantidad'];
                        $servicio = $value['@servicio'];
                    
            
                    // var_dump($value) ;
                    //die;
                        // echo '<br>';
                        // echo '<pre>';
                        // print_r($name);
                        // echo '<br>';
                        // print_r($cant);
                        // echo '<br>';
                        // print_r($servicio);
                        // echo '</pre>';

                        for($rep = 0; $rep < $cantidad; $rep++){
                            // echo('-interno');
                            // echo '<br>';
                            // se crea un arreglo temporal
                            $auxArray = [];
                            for ($x = 0; $x < $max; $x++) {
                                if ($x == $max - 1) {
                                    $auxArray[$x] = '' . PHP_EOL;
                                    //$auxArray[$x] = '' . '<br>';
                                } else {
                                    
                                    $auxArray[$x] = ';';
                                }
                            }

                            $auxArray[0] = $servicio . ';';
                            $auxArray[1] = $tipoplato . ';';
                        
                            $linea_actual = implode("", $auxArray);

                            $actual.=$linea_actual;
                        }
                    }
                } else {
                    $tipoplato= $tipos_platos['@tipoplato'];
                    $cantidad = $tipos_platos['@cantidad'];
                    $servicio = $tipos_platos['@servicio'];

                    for($rep = 0; $rep < $cantidad; $rep++){
                        // echo('-interno');
                        // echo '<br>';
                        // se crea un arreglo temporal
                        $auxArray = [];
                        for ($x = 0; $x < $max; $x++) {
                            if ($x == $max - 1) {
                                $auxArray[$x] = '' . PHP_EOL;
                                //$auxArray[$x] = '' . '<br>';
                            } else {
                                
                                $auxArray[$x] = ';';
                            }
                        }

                        $auxArray[0] = $servicio . ';';
                        $auxArray[1] = $tipoplato . ';';
                    
                        $linea_actual = implode("", $auxArray);

                        $actual.=$linea_actual;
                    }
                }

            } else {

                $auxArray = [];

                for ($x = 0; $x < $max; $x++) {
                    if ($x == $max - 1) {
                        $auxArray[$x] = '' . PHP_EOL;
                    } else {
                        $auxArray[$x] = ';';
                    }
                }

                foreach ($csvArr as $csv) {
                    for ($i = 1; $i <= $csv['@cantidad']; $i++) {
                        $auxArray[$position] = $csv['@tipoplato'] . ';';
                        $actual .= implode("", $auxArray);
                    }
                }
            }

            ob_start();
            //header('Content-Type: text/html; charset=UTF-8');
            header('Content-type: application/vnd-ms-excel; charset=utf-8');
            header('Content-Encoding: UTF-8');
            header('Content-disposition: attachment; filename=' . $nameFile2 . '.csv');
            header('Expires: 0');
            header('Cache-Control: no-cache');
            echo utf8_decode(trim($actual));    // funciona en windows
            //echo trim($actual);               // funciona en mac
            header('Content-Length: ' . ob_get_length());

            ob_flush();
        }
        else{
            $file_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
            $file_link = $file_link . '/' . $this->moduleData['gcdb']['custom_csv'];
            $file_name = basename($file_link);
            $file_link = str_replace('/./','/',$file_link);
            $fn         =   file_put_contents($file_name,file_get_contents($file_link));
            header("Expires: 0");
            header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header("Content-type: application/file");
            header('Content-length: '.filesize($file_name));
            header('Content-disposition: attachment; filename="'.basename($file_name).'"');
            readfile($file_name);
        }
        die();
    }

    public function editCasino(){
        $id          = isset($this->request->params['id']) ? $this->request->params['id'] : false;
        $fieldsData  = $this->moduleData['gcdb']['fields']['field'];

        $content = $this->Content->getById($this->moduleKey, $id);
        $idCasino=$content['Content']['id'];
        $cliente = $content['Content']['cliente'];
        $sucursal = $content['Content']['sucursal'];
        $nombre_ficticio = $content['Content']['nombre'];
        $id_config_ftp = $content['Content']['id_config_ftp'];
        $segmento = $content['Content']['segmento'];
        $estado = $content['Content']['estado'];

        $xml = file_get_contents("../modules/".CLIENT."/{$cliente}_{$sucursal}_menu_diario.xml");
        
        $Array_from_xml = XML2Array::createArray($xml);

        // Produccion
        $server_db  = LOCALSERVER;
        $user_db    = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
        mysqli_set_charset($obj_conexion, 'utf8');
        if (!$obj_conexion)
        {
            echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
            die();
        }

        $select_qry = "SELECT * FROM config_branding";

        if (!$resultado = $obj_conexion->query($select_qry)) {
            die();
        }

        $tipos_platos_branding = array();

        if ($resultado->num_rows != 0) {
            while ($rows = $resultado->fetch_assoc()) {
                $row = $rows;
                $tipos_platos_branding[] = $rows['Descripcion'];
            }
        }
        //Cierra la conexion a la BD
        $obj_conexion->close();

        $tiene_varios_servicios = 0;
        if($Array_from_xml['gcdb']['servicios']['servicio']['@attributes'] == null){
            $tiene_varios_servicios = 1;
        }

        // parseando los servicios
        $service_data_items = [];
        if($tiene_varios_servicios == 1){
            //$service_data_items = [];
            foreach($Array_from_xml['gcdb']['servicios']['servicio'] as $servicio){
                $servicio_item = $servicio['@attributes']['servicio'];
                $paginas_item = $servicio['@attributes']['paginas'];
                $fondo_item = $servicio['@attributes']['fondo'];

                $service_data_items[] = $servicio_item;
                $service_data_items[] = $paginas_item;
                $service_data_items[] = $fondo_item;
            }
            //$data_services = implode(',',$service_data_items);
        }else{
            $servicio_item = $Array_from_xml['gcdb']['servicios']['servicio']['@attributes']['servicio'];
            $paginas_item = $Array_from_xml['gcdb']['servicios']['servicio']['@attributes']['paginas'];
            $fondo_item = $Array_from_xml['gcdb']['servicios']['servicio']['@attributes']['fondo'];

            $service_data_items[] = $servicio_item;
            $service_data_items[] = $paginas_item;
            $service_data_items[] = $fondo_item;
        }
        $data_services = implode(',',$service_data_items);

        //PANTALLAS

        $tiene_varias_pantallas = 0;
        if($Array_from_xml['gcdb']['pantallas']['pantalla']['@attributes'] == null){
            $tiene_varias_pantallas = 1;
        }

        // parseando pantallas
        $pantallas_data_items = [];
        if($tiene_varias_pantallas == 1){
            //$service_data_items = [];
            foreach($Array_from_xml['gcdb']['pantallas']['pantalla'] as $pantalla){
                $mac_item = $pantalla['@attributes']['macAddress'];
                $ip_item = $pantalla['@attributes']['ip'];
                $macaModelo_item = $pantalla['@attributes']['marcaModelo'];

                $pantallas_data_items[] = $mac_item;
                $pantallas_data_items[] = $ip_item;
                $pantallas_data_items[] = $macaModelo_item;
            }
            //$data_services = implode(',',$service_data_items);
        }else{
            $mac_item = $Array_from_xml['gcdb']['pantallas']['pantalla']['@attributes']['macAddress'];
            $ip_item = $Array_from_xml['gcdb']['pantallas']['pantalla']['@attributes']['ip'];
            $macaModelo_item = $Array_from_xml['gcdb']['pantallas']['pantalla']['@attributes']['marcaModelo'];

            $pantallas_data_items[] = $mac_item;
            $pantallas_data_items[] = $ip_item;
            $pantallas_data_items[] = $macaModelo_item;
        }
        $data_pantallas = implode(',',$pantallas_data_items);

        // parseando los platos
        $service_data_platos_items = [];
        $service_data_branding_items = [];
        foreach($Array_from_xml['gcdb']['tipos_platos']['tipo_plato'] as $plato){
            $servicio_item = $plato['@attributes']['servicio'];
            $tipoplato_item = $plato['@attributes']['tipoplato'];
            $cantidad_item = $plato['@attributes']['cantidad'];
            $pagina_item = $plato['@attributes']['pagina'];
            $indice_item = $plato['@attributes']['indice'];
            $ingles_item = $plato['@attributes']['ingles'];

            if (in_array($tipoplato_item, $tipos_platos_branding)){
                $service_data_branding_items[] = $servicio_item;
                $service_data_branding_items[] = $tipoplato_item;
                $service_data_branding_items[] = $cantidad_item;
                $service_data_branding_items[] = $pagina_item;
                $service_data_branding_items[] = $indice_item;
                $service_data_branding_items[] = $ingles_item;
            }else{
                $service_data_platos_items[] = $servicio_item;
                $service_data_platos_items[] = $tipoplato_item;
                $service_data_platos_items[] = $cantidad_item;
                $service_data_platos_items[] = $pagina_item;
                $service_data_platos_items[] = $indice_item;
                $service_data_platos_items[] = $ingles_item;
            }
        }
        $data_platos = implode(',',$service_data_platos_items);
        $data_platos_branding = implode(',',$service_data_branding_items);

        //echo '<pre>';
        //print_r($service_data_branding_items);
        //print_r($data_platos);
        //print_r($Array_from_xml['gcdb']['tipos_platos']['tipo_plato']);
        //print_r($content);
        //echo '</pre>';

        switch($segmento){
            case "Corporativo":
                $segmento_mini = 'Corp';
                break;
            case "Mineria":
                $segmento_mini = 'Mineria';
                break;
            case "Salud":
                $segmento_mini = 'Salud';
                break;
        }

        $Chk_fulzona1_visible = true;
        $Chk_fulzona2_visible = true;
        $Chk_fulzona3_visible = true;
        $Chk_cumpleanos_visible = true;
        $Chk_menu_contingencia_visible = true;
        $Chk_sugerencia_chef_visible = true;

        $FullZona_1_file = MODULES_CONFIG_PATH . CLIENT . '/' . $cliente.'_'.$sucursal.'_FullZona'.$segmento_mini.'_1.xml';
        $FullZona_2_file = MODULES_CONFIG_PATH . CLIENT . '/' . $cliente.'_'.$sucursal.'_FullZona'.$segmento_mini.'_2.xml';
        $FullZona_3_file = MODULES_CONFIG_PATH . CLIENT . '/' . $cliente.'_'.$sucursal.'_FullZona'.$segmento_mini.'_3.xml';
        $Cumpleanos_file = MODULES_CONFIG_PATH . CLIENT . '/' . $cliente.'_'.$sucursal.'_FullZona'.$segmento_mini.'_Cumple.xml';
        $Contingencia_file = MODULES_CONFIG_PATH . CLIENT . '/' . $cliente.'_'.$sucursal.'_minuta_contingencia.xml';
        $Sugerencia_file = MODULES_CONFIG_PATH . CLIENT . '/' . $cliente.'_'.$sucursal.'_sugerencia_chef.xml';

        if (!file_exists($FullZona_1_file)){
            $Chk_fulzona1_visible = false;
        }
        if (!file_exists($FullZona_2_file)){
            $Chk_fulzona2_visible = false;
        }
        if (!file_exists($FullZona_3_file)){
            $Chk_fulzona3_visible = false;
        }
        if (!file_exists($Cumpleanos_file)){
            $Chk_cumpleanos_visible = false;
        }
        if (!file_exists($Contingencia_file)){
            $Chk_menu_contingencia_visible = false;
        }
        if (!file_exists($Sugerencia_file)){
            $Chk_sugerencia_chef_visible = false;
        }

        $this->set('customFields', $fieldsData);
        $this->set('content', $content);
        $this->set('contentID', $id);
        $this->set('fields', $this->moduleData['gcdb']['fields']);

        $this->set('cliente', $cliente);
        $this->set('sucursal', $sucursal);
        $this->set('nombre_ficticio', $nombre_ficticio);
        $this->set('id_config_ftp', $id_config_ftp);
        $this->set('segmento', $segmento); 
        $this->set('data_services', $data_services);
        $this->set('data_pantallas', $data_pantallas);
        $this->set('data_platos', $data_platos);
        $this->set('data_platos_branding', $data_platos_branding);

        $this->set('Chk_fulzona1_visible', $Chk_fulzona1_visible);
        $this->set('Chk_fulzona2_visible', $Chk_fulzona2_visible);
        $this->set('Chk_fulzona3_visible', $Chk_fulzona3_visible);
        $this->set('Chk_cumpleanos_visible', $Chk_cumpleanos_visible);
        $this->set('Chk_menu_contingencia_visible', $Chk_menu_contingencia_visible);
        $this->set('Chk_sugerencia_chef_visible', $Chk_sugerencia_chef_visible);


        if(count($this->request->data) == 0){
            $this->set('modulesCreated', 0);
            return;
        }
        
        $cliente = $this->request->data['Cliente'];	
        $sucursal = $this->request->data['Sucursal'];
        $nombre_ficticio = $this->request->data['NombreFicticio'];
		$segmento = $this->request->data['Segmento'];
        $cliente_sucursal = $cliente . '_' . $sucursal;
        $ftp = $this->request->data['Ftp'];
        $chk_create_ftp_folders = $this->request->data['Chk_create_ftp_folders'];
        $chk_create_MI_folders = $this->request->data['Chk_create_MI_folders'];
        $chk_fulzona1 = $this->request->data['Chk_fulzona1'];
		$fullzona1_name = $this->request->data['Fullzona1_name'];
		$chk_fulzona2 = $this->request->data['Chk_fulzona2'];
		$fullzona2_name = $this->request->data['Fullzona2_name'];
		$chk_fulzona3 = $this->request->data['Chk_fulzona3'];
		$fullzona3_name = $this->request->data['Fullzona3_name'];
        $chk_cumpleano = $this->request->data['Chk_cumpleanos'];
        $chk_pantallas = $this->request->data['Chk_pantallas'];
        $chk_menu_contingencia = $this->request->data['Chk_menu_contingencia'];
        $tbl_servicios = $this->request->data['tbl-servicios'];
        $tbl_tipos_platos = $this->request->data['tbl-tipos-platos'];
        $tbl_branding = $this->request->data['tbl-branding'];
        $tbl_pantallas = $this->request->data['tbl-pantallas'];

        $array_tbl_servicios = explode(',', $tbl_servicios);
        $tbl_servicios = array_chunk($array_tbl_servicios, 3);

        $array_tbl_tipos_platos = explode(',', $tbl_tipos_platos);
        $tbl_tipos_platos = array_chunk($array_tbl_tipos_platos, 6);

        $array_tbl_branding = explode(',', $tbl_branding);
        $tbl_branding = array_chunk($array_tbl_branding, 5);

        $array_tbl_pantallas = explode(',', $tbl_pantallas);
        $tbl_pantallas = array_chunk($array_tbl_pantallas, 3);

     
        // ##################### Inserciones de base de datos #############################

        //------------------------ Acceso a la base de datos ------------------------------

        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
        if (!$obj_conexion)
        {
            echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
            die();
        }

        // -------------  Obtener id del casino a modificar ------------------

        $select_qry = "SELECT config_modulos.id FROM config_modulos 
                        WHERE config_modulos.cliente_sucursal = '".$cliente."_".$sucursal."'";

        if (!$resultado = $obj_conexion->query($select_qry))
        {
            echo $select_qry;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
        }
        $id = $resultado->fetch_assoc();
        $casinoId =$id['id'];

        
        

        // -------------  Obtener el nombre de las carpetas por Servicio ------------------

        $select_qry = "SELECT id, nombre, carpeta FROM config_casinos_servicios";

        if (!$resultado = $obj_conexion->query($select_qry))
        {
            echo $select_qry;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
        }

        $servicios_carpetas = array();

        if ($resultado->num_rows != 0) {
            while ($rows = $resultado->fetch_assoc()) {
                $row = $rows;

                $id = $rows['id'];
                $nombre = $rows['nombre'];
                $carpeta = $rows['carpeta'];

                $casino_servicio = array();
                $casino_servicio['id'] = $id;
                $casino_servicio['nombre'] = $nombre;
                $casino_servicio['carpeta'] = $carpeta;
                $casinos_servicios[] = $casino_servicio;

                $servicios_carpetas[$nombre] = $carpeta;
            }
        }
    
        $qryupdate = "UPDATE config_casinos SET nombre = '$nombre_ficticio' WHERE id = $idCasino ";
        if (!$resultado = $obj_conexion->query($qryupdate))
        {
            //echo $qryinsert;
            //echo ".Lo sentimos, este sitio web está experimentando problemas.";
            $obj_conexion->close();
        }
        $modulo_publicacion_multiple = '';

        // ------------------------- Update Pantallas --------------------------------

        if ($chk_pantallas === 'on'){

            //Borra las Pantallas Antiguas
            $qrydel = "DELETE FROM config_pantallas WHERE id_config_modulos = '$casinoId'";
    
            if (!$resultado = $obj_conexion->query($qrydel)) {
                echo $qrydel;
                echo ".Lo sentimos, este sitio web está experimentando problemas.";
                die();
            }

            //crea Pantallas
            foreach($tbl_pantallas as $pantalla) {
                $mac_address = $pantalla[0];
                $ip = $pantalla[1];
                $marca_modelo = $pantalla[2];

                $qryinsert = "INSERT INTO config_pantallas(`id_config_modulos`, `macadress`, `ip`, `marca_modelo`, `created`, `modified`) 
                                VALUES ($casinoId, '$mac_address', '$ip', '$marca_modelo', NOW(), NOW())";
    
                if (!$resultado = $obj_conexion->query($qryinsert)) {
                    echo $qryinsert;
                    echo ".Lo sentimos, este sitio web está experimentando problemas.";
                    die();
                }
            }
        }

        // --------------  Agregar casino a Publicacion Multiple --------------------
        if ($chk_fulzona1 === 'on'){

            $result = XMLModuleGenerator::generateFullzona($this->request->data, 1, $servicios_carpetas);

            switch($segmento){
                case 'Corporativo': 
                    $modulo_publicacion_multiple = 'FullZonaCorp_1';
                    break;
                case 'Mineria': 
                    $modulo_publicacion_multiple = 'FullZonaMineria_1';
                    break;
                case 'Salud': 
                    $modulo_publicacion_multiple = 'FullZonaSalud_1';
                    break;
            }
        }

        // --------------  Agregar casino a Publicacion Multiple --------------------
        if ($chk_fulzona2 === 'on'){

            $result = XMLModuleGenerator::generateFullzona($this->request->data, 2, $servicios_carpetas);

            switch($segmento){
                case 'Corporativo': 
                    $modulo_publicacion_multiple = 'FullZonaCorp_2';
                    break;
                case 'Mineria': 
                    $modulo_publicacion_multiple = 'FullZonaMineria_2';
                    break;
                case 'Salud': 
                    $modulo_publicacion_multiple = 'FullZonaSalud_2';
                    break;
            }
        }
  
        // --------------  Agregar casino a Publicacion Multiple --------------------
        if ($chk_fulzona3 === 'on'){

            $result = XMLModuleGenerator::generateFullzona($this->request->data, 3, $servicios_carpetas);

            switch($segmento){
                case 'Corporativo': 
                    $modulo_publicacion_multiple = 'FullZonaCorp_3';
                    break;
                case 'Mineria': 
                    $modulo_publicacion_multiple = 'FullZonaMineria_3';
                    break;
                case 'Salud': 
                    $modulo_publicacion_multiple = 'FullZonaSalud_3';
                    break;
            }
        }
  
        // -------------------   casino a Cumpleaños -------------------------
        if ($chk_cumpleano === 'on'){

            $result = XMLModuleGenerator::generateCumpleano($this->request->data, $servicios_carpetas);
        }
        // --------------------   sugerencias del chef -------------------------
        if ($chk_menu_contingencia === 'on'){

            $result = XMLModuleGenerator::generateSugerenciaChef($this->request->data, $servicios_carpetas);
        }
   

        // se vuelven a crear el modulo de MenuDiario
        $result = XMLModuleGenerator::generateMenuDiario($this->request->data, $servicios_carpetas);
        $result = XMLModuleGenerator::generateContigencia($this->request->data, $servicios_carpetas);
        
        // al procesar se regresa al modulo de config_casinos
        header('Location: https://'.$_SERVER['HTTP_HOST'].'/config_casinos');
        die();
    }

    public function edit()
    {
        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {
                $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
            }
        } else {
            $clientFolder = '';
        }

        $id          = isset($this->request->params['id']) ? $this->request->params['id'] : false;
        $fieldsData  = $this->moduleData['gcdb']['fields']['field'];

        if($this->moduleData['gcdb']['justificar_borrado']=='true'){
            // busca los motivos para eliminar un plato
            $motivosList = $this->Content->getAllMotivos();

            $this->set('motivosList', $motivosList);
        }

        if (isset($this->request->data['edit-content-id'])) {
            $saveData = array();

            $es_casino = true;
            if($this->request->data['orig_tipo'] == '' && $this->request->data['orig_s2id_codigo'] == '' && $this->request->data['orig_calorias_temp'] == '' && $this->request->data['orig_fecha'] == '' && $this->request->data['orig_tmpCode'] == ''){
                $es_casino = false;
            }

            // modificacion para sistema de monitoreo
            $date_value = ''; 

            if ($es_casino ){
                $userLogged = parent::getLoggedUser();

                $accion = 'plato modificado';
                $codigo_temporal = $this->request->data['codigo_temp'];
                $cod_modificado = $this->request->data['orig_s2id_codigo'];

                if (($this->request->data['orig_s2id_codigo'] != $this->request->data['codigo']) && ($this->request->data['orig_s2id_codigo'] != '')){
                    $accion = 'reemplazo por plato del mismo maestro';
                    $cod_plato = $this->request->data['codigo'];
                    $nombre_plato = $this->buscarNombrePlato($this->request->data['codigo']);
                    $codigo_temporal = $this->request->data['codigo']; // se ocupa codigo_temporal para pasar el nuevo codigo del mismo maestro
                }
                else{
                    if ($this->request->data['codigo_temp'] != ''){
                        $accion = 'modificacion por plato temporal';
                        $cod_plato = $this->request->data['codigo_temp'];
                        $nombre_plato = $this->buscarNombrePlatoTemporal($this->request->data['codigo_temp']);
                        $codigo_temporal = 'T'.$this->request->data['codigo_temp']; 
                        
                        if ($cod_modificado == NULL){
                            $cod_modificado = $this->request->data['codigo'];
                        }
                    }
                }

                $fullname = $userLogged['User']['fullname'];
                $usuario = $userLogged['User']['email'];
                $segmento = $this->moduleData['gcdb']['segmento'];
                $fecha_exhib = $this->request->data['fecha'];
                $tipo_plato = $this->request->data['tipo'];
                $servicio = $this->request->data['servicio'];
                $sucursal = $this->moduleData['gcdb']['sucursal'];
                $cliente = $this->moduleData['gcdb']['cliente'];
                $nombre_platotemporal = $this->request->data['temporal'];
                $cod_motivo = explode(':', $this->request->data['motivo_cambio'])[0];
                $txt_motivo = explode(':', $this->request->data['motivo_cambio'])[1];

                // echo '<pre>';
                // print_r($this->request->data);
                // echo '</pre>';
                // echo '<br>';
                // echo '<br>';
                // echo $segmento;
                // echo '<br>';
                // echo $cod_modificado;
                // echo '<br>';
                // echo $accion;
                // echo '<br>';
                // echo $fecha_exhib;
                // echo '<br>';
                // echo $servicio;
                // echo '<br>';
                // echo $cod_plato;
                // echo '<br>';
                // echo $nombre_plato;
                // echo '<br>';
                // echo $tipo_plato;
                // echo '<br>';
                // echo $cliente;
                // echo '<br>';
                // echo $sucursal;
                // echo '<br>';
                // echo $usuario;
                // echo '<br>';
                // echo $codigo_temporal;
                // echo '<br>';
                // echo $txt_motivo;
                
                $this->Content->updateDish($segmento,$cod_modificado,$accion,$fecha_exhib,$servicio,$cod_plato,$nombre_plato,$tipo_plato,$cliente,$sucursal,$usuario,$codigo_temporal,$txt_motivo);
            }

            foreach ($fieldsData as $field) {

                if ($field['@type'] == 'image') {
                    $value = empty($this->request->data[$field['@key']]) ? 0 : $this->request->data[$field['@key']];
                } 
                elseif ($field['@type'] == 'date') {

                    if (empty($this->request->data[$field['@key']])) {
                        $value = '0000-00-00';
                    } else {

                        $dateParts = explode('/', $this->request->data[$field['@key']]);

                        if (isset($field['@no-transform-to-mysql']) && $field['no-transform-to-mysql'] = true) {
                            $value =  count($dateParts) == 3 ? $dateParts[0] . '-' . $dateParts[1] . '-' . $dateParts[2] : '0000-00-00';
                        } else {
                            $value = count($dateParts) == 3 ? $dateParts[2] . '-' . $dateParts[1] . '-' . $dateParts[0] : '0000-00-00';
                        }
                        // modificacion para sistema de monitoreo
                        if (count($dateParts) == 3)
                            $date_value = $dateParts[2].'-'.$dateParts[1].'-'.$dateParts[0];
                    }
                } 
                elseif ($field['@type'] == 'select') {

                    if (isset($field['@multiple']) && $field['@multiple'] === 'true') {

                        if (isset($field['@rendermodules']) && $field['@rendermodules'] === 'true')
                        {
                            $value = implode('|#|', $this->request->data['publish']);
                        }
                        else
                        {
                            $value = implode('|#|', $this->request->data[$field['@key']]);
                        }

                    } else {
                        $value = $this->request->data[$field['@key']];
                    }
                } else 
                {
                    $value = $this->request->data[$field['@key']];
                }

                $saveData[$field['@key']] = $value;
            }

            if ($this->request->data['edit-content-id'] == 0) {
                $this->Content->customCreate($this->moduleKey);
            } else {
                $saveData['id'] = $this->request->data['edit-content-id'];
            }

            $this->Content->customSave($saveData, $this->moduleKey);

            $this->Session->setFlash('Guardado con Exíto!', 'flash_custom');

            if($clientFolder == 'demo'){
                // modificacion para sistema de monitoreo
                file_get_contents("http://api.digitalboard.cl/sodexo/modules/{$this->moduleKey}/upload/{$date_value}");
                CakeSession::write('moduleEdit.' . $this->moduleKey, $this->moduleKey);
            }

            //$response_updated = file_get_contents("http://api.digitalboard.cl/sodexo/modules/{$this->moduleKey}/updated");

            if (isset($this->request->data['s']) && !empty($this->request->data['s'])) {
                $this->redirect('/' . $this->moduleKey . '/?s=' . $this->request->data['s']);
            } else {
                $this->redirect('/' . $this->moduleKey);
            }
        }

        if ($id == 'new') {
            if (isset($this->moduleData['gcdb']['hide_layout'])){
                if($this->moduleData['gcdb']['hide_layout'])
                    $this->layout = 'ajax';
            }

            $id = 0;

            foreach ($fieldsData as $field) {
                $content['Content'][$field['@key']] = '';
            }

            //$response_updated = file_get_contents("http://api.digitalboard.cl/sodexo/modules/{$this->moduleKey}/load");

        } else {
            $content = $this->Content->getById($this->moduleKey, $id);

            if (substr($this->moduleKey, -11) == 'menu_diario'){
                $codigo_plato_maestro = $content['Content']['codigo'];
                $plato_maestro = $this->buscarNombrePlatoMaestro($codigo_plato_maestro); 
                $plato_temporal = $content['Content']['temporal'];
            }
        }

        $this->set('customFields', $fieldsData);
        $this->set('content', $content);
        $this->set('contentID', $id);
        $this->set('fields', $this->moduleData['gcdb']['fields']);
        $this->set('platoMaestro', $plato_maestro);
        $this->set('platoTemporal', $plato_temporal);
    }

    public function createCasino(){

        if(count($this->request->data) == 0){
            $this->set('modulesCreated', 0);
            return;
        }

        $tbl_branding = $this->request->data['tbl-branding'];
        $array_tbl_branding = explode(',', $tbl_branding);
        $tbl_branding = array_chunk($array_tbl_branding, 5);

        $array_tipo_branding = array();
        foreach($tbl_branding as $tbl_branding_item){
            $array_tipo_branding[] = $tbl_branding_item[0];
        } 

        $cliente = $this->request->data['Cliente'];
        $sucursal = $this->request->data['Sucursal'];

        $this->set('modulesCreated', 1);

        $sessionID = session_id();

        // se arma al url que va a llamar el ajax desde la vista
        $url = 'https://';
        $url.= $_SERVER['HTTP_HOST'];
        $url.= '/'.$cliente.'_'.$sucursal.'_menu_diario';
        //$url = 'https://sodexo.dev.digitalboard.app/Sika_Lampa_menu_diario';

        $module = explode("/", $_SERVER['REQUEST_URI'])[1];

        $this->set('url', $url);
        $this->set('cliente', $cliente);
        $this->set('sucursal', $sucursal);
        $this->set('module', $module);
        $this->set('contentType', 'text/html; charset=UTF-8');
        $this->set('host', 'digitalboard.app');
        $this->set('userAgent', 'Mozilla/5.0');
        $this->set('connection', 'keep-alive');
        $this->set('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9');
        $this->set('referer', 'https://digitalboard.app/');
        $this->set('cookie', 'CAKEPHP=' . $sessionID);

		$id = 0;
		$this->set('contentID', $id);

        $chk_create_ftp_folders = 'off';
        $chk_create_MI_folders = 'off';
        $chk_fulzona1 = 'off';
        $chk_fulzona2 = 'off';
        $chk_fulzona3 = 'off';
        $chk_cumpleano = 'off';
        $chk_pantallas = 'off';
        $chk_menu_contingencia = 'off';

        $cliente = $this->request->data['Cliente'];	
        $sucursal = $this->request->data['Sucursal'];
        $nombre_ficticio = $this->request->data['NombreFicticio'];
		$segmento = $this->request->data['Segmento'];
        $cliente_sucursal = $cliente . '_' . $sucursal;
        $ftp = $this->request->data['Ftp'];
        $chk_create_ftp_folders = $this->request->data['Chk_create_ftp_folders'];
        $chk_create_MI_folders = $this->request->data['Chk_create_MI_folders'];
        $chk_fulzona1 = $this->request->data['Chk_fulzona1'];
		$fullzona1_name = $this->request->data['Fullzona1_name'];
		$chk_fulzona2 = $this->request->data['Chk_fulzona2'];
		$fullzona2_name = $this->request->data['Fullzona2_name'];
		$chk_fulzona3 = $this->request->data['Chk_fulzona3'];
		$fullzona3_name = $this->request->data['Fullzona3_name'];
        $chk_cumpleano = $this->request->data['Chk_cumpleanos'];
        $chk_pantallas = $this->request->data['Chk_pantallas'];
        $chk_menu_contingencia = $this->request->data['Chk_menu_contingencia'];
        $tbl_servicios = $this->request->data['tbl-servicios'];
        $tbl_tipos_platos = $this->request->data['tbl-tipos-platos'];
        $tbl_branding = $this->request->data['tbl-branding'];
        $tbl_pantallas = $this->request->data['tbl-pantallas'];

        $array_tbl_servicios = explode(',', $tbl_servicios);
        $tbl_servicios = array_chunk($array_tbl_servicios, 3);

        // $server_db = LOCALSERVER;
        // $user_db = LOCALUSERDB;
        // $password_db = LOCALPASSDB;
        // $db_db = LOCALDB;

        // $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
        // if (!$obj_conexion)
        // {
        //     echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
        //     die();
        // }

        // $select_qry = "SELECT id, nombre, carpeta FROM config_casinos_servicios";

        // if (!$resultado = $obj_conexion->query($select_qry))
        // {
        //     echo $select_qry;
        //     echo ".Lo sentimos, este sitio web está experimentando problemas.";
        //     die();
        // }

        // $servicios_carpetas = array();

        // if ($resultado->num_rows != 0) {
        //     while ($rows = $resultado->fetch_assoc()) {
        //         $row = $rows;

        //         $id = $rows['id'];
        //         $nombre = $rows['nombre'];
        //         $carpeta = $rows['carpeta'];

        //         $casino_servicio = array();
        //         $casino_servicio['id'] = $id;
        //         $casino_servicio['nombre'] = $nombre;
        //         $casino_servicio['carpeta'] = $carpeta;
        //         $casinos_servicios[] = $casino_servicio;

        //         $servicios_carpetas[$nombre] = $carpeta;
        //     }
        // }
        
        // foreach($tbl_servicios as $nombre_servicio) {
        //     $nombre_servicio = $nombre_servicio[0];
        //     $carpeta = $servicios_carpetas[$nombre_servicio];

        //     for($dia = 1; $dia < 8; $dia++){
        //         echo $cliente . '-' . $sucursal . '-' . $carpeta . '-' . $dia . '-' . $folder_parent;
        //         echo '<br>';
        //         //XMLModuleGenerator::createCarpetaServicioDia($cliente, $sucursal, $carpeta, $dia, $folder_parent);
        //     }
        // }

        // echo 'listo';
        // die();

        $array_tbl_tipos_platos = explode(',', $tbl_tipos_platos);
        $tbl_tipos_platos = array_chunk($array_tbl_tipos_platos, 6);

        $array_tbl_branding = explode(',', $tbl_branding);
        $tbl_branding = array_chunk($array_tbl_branding, 5);

        $array_tbl_pantallas = explode(',', $tbl_pantallas);
        $tbl_pantallas = array_chunk($array_tbl_pantallas, 3);

        // ##################### Inserciones de base de datos #############################

        //------------------------ Acceso a la base de datos ------------------------------

        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
        if (!$obj_conexion)
        {
            echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
            die();
        }

        // -------------  Obtener el nombre de las carpetas por Servicio ------------------

        $select_qry = "SELECT id, nombre, carpeta FROM config_casinos_servicios";

        if (!$resultado = $obj_conexion->query($select_qry))
        {
            echo $select_qry;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
        }

        $servicios_carpetas = array();

        if ($resultado->num_rows != 0) {
            while ($rows = $resultado->fetch_assoc()) {
                $row = $rows;

                $id = $rows['id'];
                $nombre = $rows['nombre'];
                $carpeta = $rows['carpeta'];

                $casino_servicio = array();
                $casino_servicio['id'] = $id;
                $casino_servicio['nombre'] = $nombre;
                $casino_servicio['carpeta'] = $carpeta;
                $casinos_servicios[] = $casino_servicio;

                $servicios_carpetas[$nombre] = $carpeta;
            }
        }

        //----------------------- Se crea modulo menu diario -------------------------------
        
        $result = XMLModuleGenerator::generateMenuDiario($this->request->data, $servicios_carpetas);

        $result = XMLModuleGenerator::generateContigencia($this->request->data, $servicios_carpetas);

        //------------------------ Obtiene las cuentas FTP ---------------------------------
        $select_qry = "SELECT config_ftp.id, config_ftp.Ip, config_ftp.`Port`, config_ftp.Usuario, config_ftp.Clave FROM config_ftp";

        if (!$resultado = $obj_conexion->query($select_qry)) {
            //$comentario = $Tag.": Error en la query";
            //Envia email de alerta Error
            //$mensajeSendMail = $comentario;
            //file_get_contents($serverApiSendMail."?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
            die();
        }
       
        $cuentas_ftp = array();
        $cuentas_ftp_array = array();

        if ($resultado->num_rows != 0) {
            while ($rows = $resultado->fetch_assoc()) {
                $row = $rows;

                $id = $rows['id'];
                $ip = $rows['Ip'];
                $port = $rows['Port'];
                $usuario = $rows['Usuario'];
                $clave = $rows['Clave'];

                $cuenta_ftp = array();
                $cuenta_ftp['id'] = $id;
                $cuenta_ftp['data'] = array();
                $cuenta_ftp['data']['user'] = $usuario;
                $cuenta_ftp['data']['pass'] = $clave;
                $cuenta_ftp['data']['ip'] = $ip;
                //print("Ip de conexión: " + $ip);
                $cuenta_ftp['data']['port'] = $port;
                //print("\n port de conexión: " + $port);

                $cuentas_ftp_array[] = $cuenta_ftp;
            }
        }

        $res = array();
        foreach ($cuentas_ftp_array as $key=>$value) {
            //print_r($value);
            $cuentas_ftp[$value['id']] = $value['data'];
        }


        // -----------  Crear las carpetas en el servidor FTP -------------
        echo "Valor variable CHK: " + $chk_create_ftp_folders;
        if ($chk_create_ftp_folders){
            
            //Conecta al FTP
            $Ip = $cuentas_ftp['1']['ip'];
            $Port = $cuentas_ftp['1']['port'];
            $Usuario = $cuentas_ftp['1']['user'];
            $Clave = $cuentas_ftp['1']['pass'];

            $ftp_conn = @ftp_connect($Ip, $Port);
            //print("******* Esta es la ip que necesitamos : " + $Ip + "***************");
            $login_res = ftp_login($ftp_conn, $Usuario, $Clave);
            if ((!$ftp_conn) || (!$login_res)) {
                die("La conexión FTP ha fallado!");
            }

            $folder = '/'.strtoupper(CLIENT);
            $ftp_result = ftp_chdir($ftp_conn, $folder);
            $folder = $folder.'/'.$cliente;
            //Implementación crear casino con logos en carpetas FFREZ----------
       
            $ftp_server = 'digitalboard.app';
            $ftp_user_name = 'appnovedades@digitalboard.app';
            $ftp_user_pass = 'qazplm_01';
            $_imagenPorDefectoSodexo = IMAGENSODEXOPORDEFECTO;
            $rutaLogoLocal = RUTAORIGENLOGOS . $_imagenPorDefectoSodexo;
            //echo 'Ruta del logo local es: '.$rutaLogoLocal;
           
            if(ftp_chdir($ftp_conn, $folder)){
                $folder_sucursal = $folder .'/'.$sucursal;
                if (ftp_mkdir($ftp_conn, $folder_sucursal)) {
                    for ($i = 1; $i < 8; $i++) {
                        $folder_sucursal_day = $folder_sucursal .'/'.$i;
                        if(ftp_mkdir($ftp_conn, $folder_sucursal_day)){
                            $folder_sucursal_day_folder = $folder_sucursal_day . '/';
                            foreach($tbl_servicios as $servicio){
                                $nombre_servicio = $servicio[0];
                                $nombre_carpeta = $servicios_carpetas[$nombre_servicio];
                                ftp_mkdir($ftp_conn,$folder_sucursal_day_folder . $nombre_carpeta);
                                $file_name = $folder_sucursal_day_folder . $nombre_carpeta.'/'.$_imagenPorDefectoSodexo;
                                //echo 'Ruta del archivo a subir 1: '.$file_name;
                                $temp_file_location = $rutaLogoLocal;
                                //echo 'Ruta del tempFile a subir 1: '.$temp_file_location; 
                                if (ftp_put($ftp_conn, $file_name, $temp_file_location, FTP_BINARY)) {
                                   //print ('Funcionó 1');
                                   
                                   //echo "\n Ip" + $Ip;
                                   //echo "\n tempFIleLocation" + $temp_file_location;
                                   //echo "\n fileName" + $file_name;
                                } else {
                                   //print ('Error 1');
                                }
                            }
                        }
                    }
                }
            }else {
                if (ftp_mkdir($ftp_conn, $folder)){
                    $folder_sucursal = $folder .'/'.$sucursal;
                    if (ftp_mkdir($ftp_conn, $folder_sucursal)) {
                        for ($i = 1; $i < 8; $i++) {
                            $folder_sucursal_day = $folder_sucursal .'/'.$i;
                            if(ftp_mkdir($ftp_conn, $folder_sucursal_day)){
                                $folder_sucursal_day_folder = $folder_sucursal_day . '/';
                                foreach($tbl_servicios as $servicio){
                                    $nombre_servicio = $servicio[0];
                                    $nombre_carpeta = $servicios_carpetas[$nombre_servicio];
                                    ftp_mkdir($ftp_conn,$folder_sucursal_day_folder . $nombre_carpeta);
                                    $file_name = $folder_sucursal_day_folder . $nombre_carpeta.'/'.$_imagenPorDefectoSodexo;
                                    //echo 'Ruta del archivo a subir 2: '.$file_name;
                                    $temp_file_location = $rutaLogoLocal;
                                    //echo 'Ruta del tempFile a subir 2: '.$temp_file_location; 
                                    if (ftp_put($ftp_conn, $file_name, $temp_file_location, FTP_BINARY)) {
                                        //print ('Funcionó 2');
                                         
                                        //echo "\n Ip" + $Ip;
                                        //echo "\n tempFIleLocation" + $temp_file_location;
                                        //echo "\n fileName" + $file_name;
                                    } else {
                                        //print ('Error 2');
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ftp_close($ftp_conn);
        }
        //-------------- Fin Implementación carpetas FFREZ ------


        // -----------  Crear las carpetas remotas -------------

        $mi_sodexo_server = MISODEXOSERVER;
        $mi_sodexo_port = MISODEXOSERVERPORT;
        $mi_sodexo_base_id = MISODEXOFOLDERID;

        // Se crea la carpeta raiz
        $curl = curl_init();
        $curl_url = "http://$mi_sodexo_server:$mi_sodexo_port/contents/folder";
        $curl_fields = "{\"groupName\": \"$cliente\",\"parentGroupId\": $mi_sodexo_base_id}";

        curl_setopt_array($curl, array(
        CURLOPT_URL => $curl_url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'PUT',
        CURLOPT_POSTFIELDS => $curl_fields,
        CURLOPT_HTTPHEADER => array('Content-Type: application/json')
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $obj_created = json_decode($response);
        $folder_parent = $obj_created->id;
        //echo $folder_parent.'/cliente';
        
        if($obj_created->code == '0'){//CLIENTE CREADO 
            //se crea la subcarpeta
            $curl = curl_init();
            $curl_url = "http://$mi_sodexo_server:$mi_sodexo_port/contents/folder";
            $curl_fields = "{\"groupName\": \"$sucursal\",\"parentGroupId\": $folder_parent}";

            curl_setopt_array($curl, array(
                CURLOPT_URL => $curl_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'PUT',
                CURLOPT_POSTFIELDS => $curl_fields,
                CURLOPT_HTTPHEADER => array('Content-Type: application/json')
            ));

            $response = curl_exec($curl);
            curl_close($curl);

            $obj_created = json_decode($response);
            $folder_parent = $obj_created->id;
            //echo $folder_parent.'/id'.$sucursal;
          //  die();
        }else{ //CLIENTE YA EXISTE
            //se crea la subcarpeta
            $curl = curl_init();
            $curl_url = "http://$mi_sodexo_server:$mi_sodexo_port/contents/folder";
            $curl_fields = "{\"groupName\": \"$sucursal\",\"parentGroupId\": $folder_parent}";

            curl_setopt_array($curl, array(
                CURLOPT_URL => $curl_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'PUT',
                CURLOPT_POSTFIELDS => $curl_fields,
                CURLOPT_HTTPHEADER => array('Content-Type: application/json')
            ));

            $response = curl_exec($curl);
            curl_close($curl);

            $obj_created = json_decode($response);
            $folder_parent = $obj_created->id;
        }
        foreach($tbl_servicios as $nombre_servicio) {
            $nombre_servicio = $nombre_servicio[0];
            $carpeta = $servicios_carpetas[$nombre_servicio];

            for($dia = 1; $dia < 8; $dia++){
                XMLModuleGenerator::createCarpetaServicioDia($cliente, $sucursal, $carpeta, $dia, $folder_parent);
            }
        }

        // ---------------  Crear el Modulo en la base de datos ----------------------
        $carp_destino= '/'. strtoupper(CLIENT) .'/'.$cliente.'/'.$sucursal;
        $qryinsert = "INSERT INTO config_modulos(`nombre`, `cliente_sucursal`, `origen`, `destino`, `id_config_ftp`, `segmento`, `estado`, `created`, `modified`) 
                        VALUES ('$cliente_sucursal', '$cliente_sucursal', '/$cliente/$sucursal', '$carp_destino', '$ftp', '$segmento', '1', NOW(), NOW())";

        if (!$resultado = $obj_conexion->query($qryinsert))
        {
            echo $qryinsert;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
            //$obj_conexion->close();
        }
        $module_id = $obj_conexion->insert_id;

        // ----------  Obtener el nombre de las carpetas por Servicio ---------------

        $select_qry = "SELECT id, nombre, carpeta FROM config_casinos_servicios";

        if (!$resultado = $obj_conexion->query($select_qry))
        {
            echo $select_qry;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
        }

        $servicios_carpetas = array();

        if ($resultado->num_rows != 0) {
            while ($rows = $resultado->fetch_assoc()) {
                $row = $rows;

                $id = $rows['id'];
                $nombre = $rows['nombre'];
                $carpeta = $rows['carpeta'];

                $casino_servicio = array();
                $casino_servicio['id'] = $id;
                $casino_servicio['nombre'] = $nombre;
                $casino_servicio['carpeta'] = $carpeta;
                $casinos_servicios[] = $casino_servicio;

                $servicios_carpetas[$nombre] = $carpeta;
            }
        }

        // ---------------  Crear el Modulo en la base de datos ----------------------

        foreach($tbl_servicios as $nombre_servicio) {
            $nombre_servicio = $nombre_servicio[0];
            $qryinsert = "INSERT INTO config_carpetas(`id_config_modulos`, `carpeta`, `logo`, `created`, `modified`) 
                            VALUES ($module_id, '/$servicios_carpetas[$nombre_servicio]', 'sodexo_logo_1080x1920.jpg', NOW(), NOW())";

            if (!$resultado = $obj_conexion->query($qryinsert)) {
                echo $qryinsert;
                echo ".Lo sentimos, este sitio web está experimentando problemas.";
                die();
            }
        }

        $modulo_publicacion_multiple = '';

        // ------------------------- Agregar Pantallas --------------------------------
        
        if ($chk_pantallas === 'on'){
            foreach($tbl_pantallas as $pantalla) {
                $mac_address = $pantalla[0];
                $ip = $pantalla[1];
                $marca_modelo = $pantalla[2];
                $qryinsert = "INSERT INTO config_pantallas(`id_config_modulos`, `macadress`, `ip`, `marca_modelo`, `created`, `modified`) 
                                VALUES ($module_id, '$mac_address', '$ip', '$marca_modelo', NOW(), NOW())";
    
                if (!$resultado = $obj_conexion->query($qryinsert)) {
                    echo $qryinsert;
                    echo ".Lo sentimos, este sitio web está experimentando problemas.";
                    die();
                }
            }
        }
        
        // --------------  Agregar casino a Publicacion Multiple --------------------
        if ($chk_fulzona1 === 'on'){

            $result = XMLModuleGenerator::generateFullzona($this->request->data, 1, $servicios_carpetas);

            switch($segmento){
                case 'Corporativo': 
                    $modulo_publicacion_multiple = 'FullZonaCorp_1';
                    break;
                case 'Mineria': 
                    $modulo_publicacion_multiple = 'FullZonaMineria_1';
                    break;
                case 'Salud': 
                    $modulo_publicacion_multiple = 'FullZonaSalud_1';
                    break;
            }
        }
        $qryinsert = "INSERT INTO config_publicacionmultiple(`id_config_modulo`, `publicacion_multiple`, `created`, `modified`) 
                        VALUES ($module_id, '$modulo_publicacion_multiple', NOW(), NOW())";

        if (!$resultado = $obj_conexion->query($qryinsert))
        {
            echo $qryinsert;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            $obj_conexion->close();
            die();
        }
        // --------------  Agregar casino a Publicacion Multiple --------------------
        if ($chk_fulzona2 === 'on'){

            $result = XMLModuleGenerator::generateFullzona($this->request->data, 2, $servicios_carpetas);

            switch($segmento){
                case 'Corporativo': 
                    $modulo_publicacion_multiple = 'FullZonaCorp_2';
                    break;
                case 'Mineria': 
                    $modulo_publicacion_multiple = 'FullZonaMineria_2';
                    break;
                case 'Salud': 
                    $modulo_publicacion_multiple = 'FullZonaSalud_2';
                    break;
            }
        }
        $qryinsert = "INSERT INTO config_publicacionmultiple(`id_config_modulo`, `publicacion_multiple`, `created`, `modified`) 
                        VALUES ($module_id, '$modulo_publicacion_multiple', NOW(), NOW())";

        if (!$resultado = $obj_conexion->query($qryinsert))
        {
            echo $qryinsert;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            $obj_conexion->close();
            die();
        }
        // --------------  Agregar casino a Publicacion Multiple --------------------
        if ($chk_fulzona3 === 'on'){

            $result = XMLModuleGenerator::generateFullzona($this->request->data, 3, $servicios_carpetas);

            switch($segmento){
                case 'Corporativo': 
                    $modulo_publicacion_multiple = 'FullZonaCorp_3';
                    break;
                case 'Mineria': 
                    $modulo_publicacion_multiple = 'FullZonaMineria_3';
                    break;
                case 'Salud': 
                    $modulo_publicacion_multiple = 'FullZonaSalud_3';
                    break;
            }
        }
        $qryinsert = "INSERT INTO config_publicacionmultiple(`id_config_modulo`, `publicacion_multiple`, `created`, `modified`) 
                        VALUES ($module_id, '$modulo_publicacion_multiple', NOW(), NOW())";

        if (!$resultado = $obj_conexion->query($qryinsert))
        {
            echo $qryinsert;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
            //$obj_conexion->close(); 
        }
        // -------------------  Agregar casino a Cumpleaños -------------------------
        if ($chk_cumpleano === 'on'){

            $result = XMLModuleGenerator::generateCumpleano($this->request->data, $servicios_carpetas);
        }
        // --------------------  Crear sugerencias del chef -------------------------
        if ($chk_menu_contingencia === 'on'){

            $result = XMLModuleGenerator::generateSugerenciaChef($this->request->data, $servicios_carpetas);
        }
        // --------------------------------------------------------------------------

        $qryinsert = "INSERT INTO config_casinos(`cliente`, `sucursal`, `nombre`, `id_config_ftp`, `segmento`, `estado`, `created`, `modified`) 
                        VALUES ('$cliente', '$sucursal', '$nombre_ficticio', $ftp, '$segmento', 1, NOW(), NOW())";

        if (!$resultado = $obj_conexion->query($qryinsert))
        {
            echo $qryinsert;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
            //$obj_conexion->close();
        }
        $module_id = $obj_conexion->insert_id;

        $obj_conexion->close();

        // llama a la funcion para hacer el Core2
        $this->setCore2();
        // Limpia el cache de Persistence y el de Modules
        parent::clear_cache();
        // llama a la funcion para hacer el Core0
        $this->setCore0();

        return 1;
   
	}

    public function createCasinoPost(){
        // if (isset($_GET['casino'])){
        //     $casino = $_GET['casino'];

        $module = explode("/", $_SERVER['REQUEST_URI'])[1];

        $url = 'https://';
        $url.= $_SERVER['HTTP_HOST'].'/'.$module; //.$_SERVER['REQUEST_URI'];

        $this->setCore2();
        parent::clear_cache();
        $this->setCore0();
     
        header("Location: $url");
        //CakeSession::write('moduleEdit.' . $this->moduleKey, $this->moduleKey);
        //$this->Session->setFlash("Casino Creado. ", 'flash_custom');
        die();
    }

    public function updateCasino(){
        $id          = isset($this->request->params['id']) ? $this->request->params['id'] : false;
        $fieldsData  = $this->moduleData['gcdb']['fields']['field'];
        $content = $this->Content->getById($this->moduleKey, $id);

        echo '<pre>';
        print_r($content);
        echo '</pre>';

        $this->set('contentID', $id);
        $this->set('content', $content);
        die();

        if(count($this->request->data) == 0){
            $this->set('modulesCreated', 0);
            return;
        }

        $cliente = $this->request->data['Cliente'];
        $sucursal = $this->request->data['Sucursal'];

        $this->set('modulesCreated', 1);

        $sessionID = session_id();

        // se arma al url que va a llamar el ajax desde la vista
        $url = 'https://';
        $url.= $_SERVER['HTTP_HOST'];
        $url.= '/'.$cliente.'_'.$sucursal.'_menu_diario';
        //$url = 'https://sodexo.dev.digitalboard.app/Sika_Lampa_menu_diario';

        $module = explode("/", $_SERVER['REQUEST_URI'])[1];

        $this->set('url', $url);
        $this->set('cliente', $cliente);
        $this->set('sucursal', $sucursal);
        $this->set('module', $module);
        $this->set('contentType', 'text/html; charset=UTF-8');
        $this->set('host', 'digitalboard.app');
        $this->set('userAgent', 'Mozilla/5.0');
        $this->set('connection', 'keep-alive');
        $this->set('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9');
        $this->set('referer', 'https://digitalboard.app/');
        $this->set('cookie', 'CAKEPHP=' . $sessionID);

		$id = 0;
		$this->set('contentID', $id);

        $chk_create_ftp_folders = 'off';
        $chk_create_MI_folders = 'off';
        $chk_fulzona1 = 'off';
        $chk_fulzona2 = 'off';
        $chk_fulzona3 = 'off';
        $chk_cumpleano = 'off';
        $chk_menu_contingencia = 'off';

        // --------------------- Modo Pruebas --------------------------
        // $this->request->data['Cliente']  = 'Cprueba';
        // $this->request->data['Sucursal'] = 'Sucursal';
        // $this->request->data['NombreFicticio'] = 'Casino prueba sucursal nuevo';
		// $this->request->data['Segmento'] = 'Mineria';
        
        // $this->request->data['Ftp'] = 1;
        // $this->request->data['Chk_create_ftp_folders'] = 'on';
        // $this->request->data['Chk_create_MI_folders'] = 'on';
        // $this->request->data['Chk_fulzona1'] = 'on';
		// $this->request->data['Fullzona1_name'] = 'Equilibrate';
		// $this->request->data['Chk_fulzona2'] = 'on';
		// $this->request->data['Fullzona2_name'] = 'Campaña';
		// $this->request->data['Chk_fulzona3'] = 'on';
		// $this->request->data['Fullzona3_name'] = 'Informativo';
        // $this->request->data['Chk_cumpleanos'] = 'on';
        // $this->request->data['Chk_menu_contingencia'] = 'on';
        // $this->request->data['tbl-servicios'] = 'Desayuno,2,Almuerzo,2,Cena,2';
        // $this->request->data['tbl-tipos-platos'] = 'Almuerzo,Sopa-crema,2,1,1,soup,Almuerzo,Salad-bar,2,2,2,,Desayuno,Entrada,1,1,1,,Desayuno,Bebestible,2,2,2,,Cena,Entrada,2,1,1,';
        // --------------------------------------------------------------

        $cliente = $this->request->data['Cliente'];	
        $sucursal = $this->request->data['Sucursal'];
        $nombre_ficticio = $this->request->data['NombreFicticio'];
		$segmento = $this->request->data['Segmento'];
        $cliente_sucursal = $cliente . '_' . $sucursal;
        $ftp = $this->request->data['Ftp'];
        $chk_create_ftp_folders = $this->request->data['Chk_create_ftp_folders'];
        $chk_create_MI_folders = $this->request->data['Chk_create_MI_folders'];
        $chk_fulzona1 = $this->request->data['Chk_fulzona1'];
		$fullzona1_name = $this->request->data['Fullzona1_name'];
		$chk_fulzona2 = $this->request->data['Chk_fulzona2'];
		$fullzona2_name = $this->request->data['Fullzona2_name'];
		$chk_fulzona3 = $this->request->data['Chk_fulzona3'];
		$fullzona3_name = $this->request->data['Fullzona3_name'];
        $chk_cumpleano = $this->request->data['Chk_cumpleanos'];
        $chk_menu_contingencia = $this->request->data['Chk_menu_contingencia'];
        $tbl_servicios = $this->request->data['tbl-servicios'];
        $tbl_tipos_platos = $this->request->data['tbl-tipos-platos'];

        $array_tbl_servicios = explode(',', $tbl_servicios);
        $tbl_servicios = array_chunk($array_tbl_servicios, 3);

        $array_tbl_tipos_platos = explode(',', $tbl_tipos_platos);
        $tbl_tipos_platos = array_chunk($array_tbl_tipos_platos, 6);

        // ##################### Inserciones de base de datos #############################

        //------------------------ Acceso a la base de datos ------------------------------

        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
        if (!$obj_conexion)
        {
            echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
            die();
        }

        // -------------  Obtener el nombre de las carpetas por Servicio ------------------

        $select_qry = "SELECT id, nombre, carpeta FROM config_casinos_servicios";

        if (!$resultado = $obj_conexion->query($select_qry))
        {
            echo $select_qry;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
        }

        $servicios_carpetas = array();

        if ($resultado->num_rows != 0) {
            while ($rows = $resultado->fetch_assoc()) {
                $row = $rows;

                $id = $rows['id'];
                $nombre = $rows['nombre'];
                $carpeta = $rows['carpeta'];

                $casino_servicio = array();
                $casino_servicio['id'] = $id;
                $casino_servicio['nombre'] = $nombre;
                $casino_servicio['carpeta'] = $carpeta;
                $casinos_servicios[] = $casino_servicio;

                $servicios_carpetas[$nombre] = $carpeta;
            }
        }

        //---------------------- Prueba de creacion de modulos ----------------------------

        // $result = XMLModuleGenerator::generateMenuDiario($this->request->data, $servicios_carpetas);
        // $result = XMLModuleGenerator::generateFullzona($this->request->data, 1, $servicios_carpetas);
        // $result = XMLModuleGenerator::generateFullzona($this->request->data, 2, $servicios_carpetas);
        // $result = XMLModuleGenerator::generateFullzona($this->request->data, 3, $servicios_carpetas);
        // $result = XMLModuleGenerator::generateContigencia($this->request->data, $servicios_carpetas);
        // $result = XMLModuleGenerator::generateSugerenciaChef($this->request->data, $servicios_carpetas);
        // $result = XMLModuleGenerator::generateCumpleano($this->request->data, $servicios_carpetas);

        // $obj_conexion->close();

        // // llama a la funcion para hacer el Core2
        // $this->setCore2();
        // // Limpia el cache de Persistence y el de Modules
        // parent::clear_cache();
        // // llama a la funcion para hacer el Core0
        // $this->setCore0();

        // return 1;
        // die();


        //----------------------- Se crea modulo menu diario -------------------------------
        
        $result = XMLModuleGenerator::generateMenuDiario($this->request->data, $servicios_carpetas);

        $result = XMLModuleGenerator::generateContigencia($this->request->data, $servicios_carpetas);

        //------------------------ Obtiene las cuentas FTP ---------------------------------
        $select_qry = "SELECT config_ftp.id, config_ftp.Ip, config_ftp.`Port`, config_ftp.Usuario, config_ftp.Clave FROM config_ftp";

        if (!$resultado = $obj_conexion->query($select_qry)) {
            //$comentario = $Tag.": Error en la query";
            //Envia email de alerta Error
            //$mensajeSendMail = $comentario;
            //file_get_contents($serverApiSendMail."?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
            die();
        }

        $cuentas_ftp = array();
        $cuentas_ftp_array = array();

        if ($resultado->num_rows != 0) {
            while ($rows = $resultado->fetch_assoc()) {
                $row = $rows;

                $id = $rows['id'];
                $ip = $rows['Ip'];
                $port = $rows['Port'];
                $usuario = $rows['Usuario'];
                $clave = $rows['Clave'];

                $cuenta_ftp = array();
                $cuenta_ftp['id'] = $id;
                $cuenta_ftp['data'] = array();
                $cuenta_ftp['data']['user'] = $usuario;
                $cuenta_ftp['data']['pass'] = $clave;
                $cuenta_ftp['data']['ip'] = $ip;
                $cuenta_ftp['data']['port'] = $port;

                $cuentas_ftp_array[] = $cuenta_ftp;
            }
        }

        $res = array();
        foreach ($cuentas_ftp_array as $key=>$value) {
            //print_r($value);
            $cuentas_ftp[$value['id']] = $value['data'];
        }

        // -----------  Crear las carpetas en el servidor FTP -------------
        if ($chk_create_ftp_folders){
            //Conecta al FTP
            $Ip = $cuentas_ftp['1']['ip'];
            $Port = $cuentas_ftp['1']['port'];
            $Usuario = $cuentas_ftp['1']['user'];
            $Clave = $cuentas_ftp['1']['pass'];

            $ftp_conn = @ftp_connect($Ip, $Port);
            $login_res = ftp_login($ftp_conn, $Usuario, $Clave);

            $folder = '/SODEXO';

            $ftp_result = ftp_chdir($ftp_conn, $folder);
            $folder = $folder .'/'.$cliente;
            
            if (ftp_mkdir($ftp_conn, $folder)) {
                $folder_sucursal = $folder .'/'.$sucursal;
                if (ftp_mkdir($ftp_conn, $folder_sucursal )) {
                    for ($i = 1; $i < 8; $i++) {
                        $folder_sucursal_day = $folder_sucursal .'/'.$i;
                        if(ftp_mkdir($ftp_conn, $folder_sucursal_day)){
                            $folder_sucursal_day_folder = $folder_sucursal_day . '/';

                            foreach($tbl_servicios as $servicio){
                                $nombre_servicio = $servicio[0];
                                $nombre_carpeta = $servicios_carpetas[$nombre_servicio];

                                ftp_mkdir($ftp_conn,$folder_sucursal_day_folder . $nombre_carpeta);
                            }
                        }
                    }
                }
            }

            ftp_close($ftp_conn);
        }

        // ---------------  Crear el Modulo en la base de datos ----------------------
        $qryinsert = "INSERT INTO config_modulos(`nombre`, `cliente_sucursal`, `origen`, `destino`, `id_config_ftp`, `segmento`, `estado`, `created`, `modified`) 
                        VALUES ('$cliente_sucursal', '$cliente_sucursal', '/$cliente/$sucursal', '/SODEXO/$cliente/$sucursal', '$ftp', '$segmento', '1', NOW(), NOW())";

        if (!$resultado = $obj_conexion->query($qryinsert))
        {
            echo $qryinsert;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
            //$obj_conexion->close();
        }
        $module_id = $obj_conexion->insert_id;

        // ----------  Obtener el nombre de las carpetas por Servicio ---------------
        $select_qry = "SELECT id, nombre, carpeta FROM config_casinos_servicios";

        if (!$resultado = $obj_conexion->query($select_qry))
        {
            echo $select_qry;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
        }

        $servicios_carpetas = array();

        if ($resultado->num_rows != 0) {
            while ($rows = $resultado->fetch_assoc()) {
                $row = $rows;

                $id = $rows['id'];
                $nombre = $rows['nombre'];
                $carpeta = $rows['carpeta'];

                $casino_servicio = array();
                $casino_servicio['id'] = $id;
                $casino_servicio['nombre'] = $nombre;
                $casino_servicio['carpeta'] = $carpeta;
                $casinos_servicios[] = $casino_servicio;

                $servicios_carpetas[$nombre] = $carpeta;
            }
        }

        // ---------------  Crear el Modulo en la base de datos ----------------------

        // echo '<pre>';
        // foreach($tbl_servicios as $nombre_servicio) {
        //     $nombre_servicio = $nombre_servicio[0];
        //     print_r($servicios_carpetas[$nombre_servicio]);
        // }
        // echo '</pre>';
        // die();

        foreach($tbl_servicios as $nombre_servicio) {
            $nombre_servicio = $nombre_servicio[0];
            $qryinsert = "INSERT INTO config_carpetas(`id_config_modulos`, `carpeta`, `logo`, `created`, `modified`) 
                            VALUES ($module_id, '/$servicios_carpetas[$nombre_servicio]', 'sodexo_logo_1080x1920.jpg', NOW(), NOW())";

            if (!$resultado = $obj_conexion->query($qryinsert)) {
                echo $qryinsert;
                echo ".Lo sentimos, este sitio web está experimentando problemas.";
                die();
            }
        }

        $modulo_publicacion_multiple = '';
        // --------------  Agregar casino a Publicacion Multiple --------------------
        if ($chk_fulzona1 === 'on'){

            $result = XMLModuleGenerator::generateFullzona($this->request->data, 1, $servicios_carpetas);

            switch($segmento){
                case 'Corporativo': 
                    $modulo_publicacion_multiple = 'FullZonaCorp_1';
                    break;
                case 'Mineria': 
                    $modulo_publicacion_multiple = 'FullZonaMineria_1';
                    break;
                case 'Salud': 
                    $modulo_publicacion_multiple = 'FullZonaSalud_1';
                    break;
            }
        }
        $qryinsert = "INSERT INTO config_publicacionmultiple(`id_config_modulo`, `publicacion_multiple`, `created`, `modified`) 
                        VALUES ($module_id, '$modulo_publicacion_multiple', NOW(), NOW())";

        if (!$resultado = $obj_conexion->query($qryinsert))
        {
            echo $qryinsert;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
            //$obj_conexion->close();
        }
        // --------------  Agregar casino a Publicacion Multiple --------------------
        if ($chk_fulzona2 === 'on'){

            $result = XMLModuleGenerator::generateFullzona($this->request->data, 2, $servicios_carpetas);

            switch($segmento){
                case 'Corporativo': 
                    $modulo_publicacion_multiple = 'FullZonaCorp_2';
                    break;
                case 'Mineria': 
                    $modulo_publicacion_multiple = 'FullZonaMineria_2';
                    break;
                case 'Salud': 
                    $modulo_publicacion_multiple = 'FullZonaSalud_2';
                    break;
            }
        }
        $qryinsert = "INSERT INTO config_publicacionmultiple(`id_config_modulo`, `publicacion_multiple`, `created`, `modified`) 
                        VALUES ($module_id, '$modulo_publicacion_multiple', NOW(), NOW())";

        if (!$resultado = $obj_conexion->query($qryinsert))
        {
            echo $qryinsert;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
            //$obj_conexion->close();
        }
        // --------------  Agregar casino a Publicacion Multiple --------------------
        if ($chk_fulzona3 === 'on'){

            $result = XMLModuleGenerator::generateFullzona($this->request->data, 3, $servicios_carpetas);

            switch($segmento){
                case 'Corporativo': 
                    $modulo_publicacion_multiple = 'FullZonaCorp_3';
                    break;
                case 'Mineria': 
                    $modulo_publicacion_multiple = 'FullZonaMineria_3';
                    break;
                case 'Salud': 
                    $modulo_publicacion_multiple = 'FullZonaSalud_3';
                    break;
            }
        }
        $qryinsert = "INSERT INTO config_publicacionmultiple(`id_config_modulo`, `publicacion_multiple`, `created`, `modified`) 
                        VALUES ($module_id, '$modulo_publicacion_multiple', NOW(), NOW())";

        if (!$resultado = $obj_conexion->query($qryinsert))
        {
            echo $qryinsert;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
            //$obj_conexion->close(); 
        }
        // -------------------  Agregar casino a Cumpleaños -------------------------
        if ($chk_cumpleano === 'on'){

            $result = XMLModuleGenerator::generateCumpleano($this->request->data, $servicios_carpetas);
        }
        // --------------------  Crear sugerencias del chef -------------------------
        if ($chk_menu_contingencia === 'on'){

            $result = XMLModuleGenerator::generateSugerenciaChef($this->request->data, $servicios_carpetas);
        }
        // --------------------------------------------------------------------------

        $qryinsert = "INSERT INTO config_casinos(`cliente`, `sucursal`, `nombre`, `id_config_ftp`, `segmento`, `estado`, `created`, `modified`) 
                        VALUES ('$cliente', '$sucursal', '$nombre_ficticio', $ftp, '$segmento', 0, NOW(), NOW())";

        if (!$resultado = $obj_conexion->query($qryinsert))
        {
            echo $qryinsert;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            $obj_conexion->close();
            die();
        }
        $module_id = $obj_conexion->insert_id;

        $obj_conexion->close();

        // llama a la funcion para hacer el Core2
        $this->setCore2();
        // Limpia el cache de Persistence y el de Modules
        parent::clear_cache();
        // llama a la funcion para hacer el Core0
        $this->setCore0();

        return 1;
	}

    public function setCore2(){
        $config_dir = dirname(__FILE__).'/../Config/';
        $source_file = $config_dir.'core2.php';
        $dest_file = $config_dir.'core.php';
        copy($source_file, $dest_file);
    }

    public function setCore0(){
        $config_dir = dirname(__FILE__).'/../Config/';
        $source_file = $config_dir.'core0.php';
        $dest_file = $config_dir.'core.php';
        copy($source_file, $dest_file);
    }

    public function buscarNombrePlato($codigo)
    {
        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
        if (!$obj_conexion)
        {
            echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
            die();
        }

        $qry = "SELECT nombre_espanol FROM maestro_productos WHERE codigo = '$codigo'";
        
        if (!$resultado = $obj_conexion->query($qry))
        {
            echo $qryinsert;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            $obj_conexion->close();
            die();
        }

        $registro = $resultado->fetch_assoc();
        $nombre = $registro["nombre_espanol"];

        $obj_conexion->close();

        return $nombre;
    }

    public function registerPublication($cliente, $sucursal, $segmento){
        $userLogged = parent::getLoggedUser();
        $fullname = $userLogged['User']['fullname'];

        $tablename = '';
        switch(strtolower($segmento)){
            case 'corporativo':
                $tablename = 'reportes_cambios_corporativos';
                break;
            case 'mineria':
                $tablename = 'reportes_cambios_mineria';
                break;
            case 'salud':
                $tablename = 'reportes_cambios_salud';
                break;
        }

        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
        if (!$obj_conexion)
        {
            echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
            die();
        }

        $qryinsert = "INSERT INTO $tablename (accion, Segmento, cliente, sucursal, usuario, created, modified) VALUES ('publicacion', '$segmento', '$cliente', '$sucursal', '$fullname', NOW(), NOW())";
        
        if ($tablename != ''){
            if (!$resultado = $obj_conexion->query($qryinsert))
            {
                echo $qryinsert;
                echo ".Lo sentimos, este sitio web está experimentando problemas.";
                $obj_conexion->close();
                die();
            }
        }
        

        $obj_conexion->close();
    }

    public function buscarNombrePlatoTemporal($codigo)
    {
        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
        if (!$obj_conexion)
        {
            echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
            die();
        }

        $qry = "SELECT nombre_espanol FROM maestro_temporal WHERE codigo_temporal = '$codigo'";
        
        if (!$resultado = $obj_conexion->query($qry))
        {
            echo $qryinsert;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
        }

        $registro = $resultado->fetch_assoc();
        $nombre = $registro["nombre_espanol"];

        $obj_conexion->close();

        return $nombre;
    }

    public function buscarNombrePlatoMaestro($codigo)
    {
        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
        if (!$obj_conexion)
        {
            echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
            die();
        }

        $qry = "SELECT nombre_espanol FROM maestro_productos WHERE codigo = '$codigo'";
        
        if (!$resultado = $obj_conexion->query($qry))
        {
            echo $qryinsert;
            echo ".Lo sentimos, este sitio web está experimentando problemas.";
            die();
        }

        $registro = $resultado->fetch_assoc();
        $nombre = $registro["nombre_espanol"];

        $obj_conexion->close();

        return $nombre;
    }

    public function createTmpDish()
    {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $postdata = $this->request->query;

        $table = 'reportes_creacion_de_codigos';
        

        $id = $postdata['id'];
        $fecha = $postdata['fecha'];
        $servicio = $postdata['servicio'];
        $nombre = $postdata['nombre'];
        $segmento = $postdata['segmento'];
        $cliente = $postdata['cliente'];
        $sucursal = $postdata['sucursal'];
        $motivo = $postdata['motivo'];
        $nombreingles = $postdata['nombreingles'];
        $calorias = $postdata['calorias'];
        $lactosa = $postdata['lactosa'];
        $soya = $postdata['soya'];
        $frutosecos = $postdata['frutosecos'];
        $gluten = $postdata['gluten'];
        $vegano = $postdata['vegano'];
        $vegetariano = $postdata['vegetariano'];
        $altoenazucar = $postdata['altoenazucar'];
        $altoensodio = $postdata['altoensodio'];
        $user = $postdata['user'];

        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
        if (!$obj_conexion)
        {
            echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
            die();
        }

        
    
        // agregar maestro temporal
        $table = 'maestro_temporal';
        $qryinsert = 'INSERT INTO '.$table.'(codigo,fecha,servicio,motivo,nombre_espanol,nombre_ingles,calorias,lactosa,soya,frutos_secos,gluten,vegano,vegetariano,altoazucar,altosodio,segmento,cliente,sucursal,usuario) ';
        $qryinsert .="VALUES ('$id','$fecha', '$servicio', '$motivo', '$nombre', '$nombreingles', '$calorias', '$lactosa', '$soya', '$frutosecos', '$gluten', '$vegano', '$vegetariano', '$altoenazucar', '$altoensodio', '$segmento', '$cliente', '$sucursal', '$user')";
        
        if (!$resultado = $obj_conexion->query($qryinsert))
        {
            //echo $qryinsert;
            //echo ".Lo sentimos, este sitio web está experimentando problemas.";
            $obj_conexion->close();
        }

        // $LastID <--- Codigo temporal generado
        $LastID = $obj_conexion->insert_id;

        $qryupdate = "UPDATE $table SET codigo_temporal = '$LastID' WHERE id = $LastID";
        if (!$resultado = $obj_conexion->query($qryupdate))
        {
            //echo $qryinsert;
            //echo ".Lo sentimos, este sitio web está experimentando problemas.";
            $obj_conexion->close();
        }

        $table = 'reportes_creacion_de_codigos';

        $cod_local = $id;
        $cod_modificado = $LastID;
        $nombre_fantasia = $nombre;

        $qryinsert = 'INSERT INTO '.$table.'(cod_local,cod_modificado,fecha,servicio,nombre_fantasia, Segmento, cliente, usuario, motivo) ';
        $qryinsert .="VALUES ('$cod_local','$cod_modificado', '$fecha', '$servicio', '$nombre_fantasia', '$segmento', '$cliente', '$user', '$motivo')";

        if (!$resultado = $obj_conexion->query($qryinsert))
        {
            //echo $qryinsert;
            //echo ".Lo sentimos, este sitio web está experimentando problemas.";
            $obj_conexion->close();
        }

        $obj_conexion->close();

        echo $LastID;
    }

    public function checkcode()
    {
        $this->layout = 'ajax';
        $this->autoRender = false;

        $data = $this->params['data'];
        $data = $data["values"];

        $num_elements = count($data);

        $tabla_maestro = $this->modules[$this->moduleKey]["gcdb"]["publish_url"]["@related-module"];

        $server_db = LOCALSERVER;
        $user_db = LOCALUSERDB;
        $password_db = LOCALPASSDB;
        $db_db = LOCALDB;

        $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
        if (!$obj_conexion)
        {
            echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
            die();
        }

        $no_encontrados = array();

        for($i = 0; $i < $num_elements; $i++){
            $sql = "SELECT COUNT(*) as cuenta FROM maestro_productos WHERE codigo = $data[$i]";


            if (!$resultado = $obj_conexion->query($sql))
            {
                echo '0';
                $obj_conexion->close();
                die();
            }

            $registro = $resultado->fetch_assoc();
            if ($registro["cuenta"] == 0){
                $no_encontrados[] = $data[$i];
            }
        }

        $obj_conexion->close();

        echo json_encode($no_encontrados);
    }

    public function delete()
    {
        if (isset($this->params['url']['id'])) {
            $id = $this->params['url']['id'];
        } else {
            return false;
        }

        $this->layout = 'ajax';
        $this->autoRender = false;
        $this->Content->customDeleteAll(array('id_row' => $id), $this->moduleKey);

        return true;
    }

    private function makePreviewImageMode()
    {
        $userLogged = parent::getLoggedUser();
        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {

                $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
            }
        } else {
            $clientFolder = '';
        }
        

        define('AMBIENTE', 'test');
        include(WWW_ROOT . 'files' . DS . $clientFolder . DS . 'templates_gd' . DS . $this->moduleKey . '.php');
    }

    public function makePreview($urlIndex = false)
    {
        $this->layout = 'ajax';

        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {
                $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
            }
        } else {
            $clientFolder = '';
        }

        if (!isset($status)) {
            
            $status = array('success' => true);

            if (isset($this->moduleData['gcdb']['publish_url']['@related-module'])) {

                $this->Content->generatePreview($this->moduleKey, $clientFolder, $this->moduleData['gcdb']);

                $relatedModules = explode('|', $this->moduleData['gcdb']['publish_url']['@related-module']);
                
                foreach ($relatedModules as $relatedModule) {

                    $this->Content->setSource($relatedModule);
                    $this->Content->generatePreview($relatedModule, $clientFolder, $this->modules[$relatedModule]['gcdb']);
                }
            } else {
                $this->Content->generatePreview($this->moduleKey, $clientFolder, $this->moduleData['gcdb']);
            }
        }

        $urlIndex = isset($_GET['urlIndex']) ? $_GET['urlIndex'] : false;

        define('URLINDEX', $urlIndex);

        if ($clientFolder == 'sodexo' or $clientFolder == 'sodexochile') {
            $modulesExcluded = array('beneficios_maipu', 'agenda_maipu', 'sabias_que_maipu', 'cumpleanos_maipu', 'datos_avisos_maipu', 'noticias_maipu', 'seguridad_maipu', 'agenda_corporativo', 'sabias_que_corporativo', 'beneficios_corporativo', 'cumpleanos_corporativo', 'datos_avisos_corporativo', 'noticias_corporativo', 'seguridad_corporativo');

            if (!in_array($this->moduleKey, $modulesExcluded)) {
                $this->makePreviewImageMode();
            }
        }
        $this->set('urlIndex', $urlIndex);
        $this->set('status', $status);
        $this->set('clientFolder', $clientFolder);
    }

    public function makePublishImageMode()
    {
        define('URLINDEX', 0);
        define('AMBIENTE', 'prod');
        $userLogged = parent::getLoggedUser();

        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {
                $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
            }
        } else {
            $clientFolder = '';
        }

        include(WWW_ROOT . 'files' . DS . $clientFolder . DS . 'templates_gd' . DS . $this->moduleKey . '.php');

        if ($this->moduleKey == 'demo_unidad1_menu_diario') {

            $userLogged = parent::getLoggedUser();

            $alertsConfigured = $this->PublishAlerts->find('all', array('conditions' => array('module' => $this->moduleKey)));

            foreach ($alertsConfigured as $alert) {

                $startService = $alert['PublishAlerts']['start_time'];

                $time1 = strtotime(date('H:m:s'));
                $time2 = strtotime($startService);
                $diff  = (($time1 - $time2) / 60);

                if ($diff < 0) {

                    $diff = $diff * -1;

                    if ($diff < $alert['PublishAlerts']['gap_time_minutes']) {

                        $this->Content->setSource($this->moduleKey);

                        $availableColumns = $this->Content->getColumnTypes();

                        //echo 'ALERTAR -> ' . $alert['PublishAlerts']['service'].' -> '. $diff .'<br>';

                        if (in_array($availableColumns, "servicio")) {
                            $conditions = array('servicio' => $alert['PublishAlerts']['service'], 'fecha' => date('d-m-Y'));
                        } else {
                            $conditions = array('fecha' => date('d-m-Y'));
                        }

                        $haveRows = $this->Content->find('all', array('conditions' => $conditions));

                        if (!empty($haveRows)) {

                            $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
                            $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                            $cabeceras .= 'To: ' . $alert['PublishAlerts']['recipients'] . ' <' . $mail . '>' . "\r\n";
                            $cabeceras .= 'From: Digitalboard Alertas <no-responder@digitalboard.cl>' . "\r\n";

                            $date = date('d-m-Y');
                            $time = date('H:m');

                            $msg .= '<b>Unidad:</b> ' . $alert['PublishAlerts']['unit'] . '<br>';
                            $msg .= '<b>Publicado Por:</b> ' . $userLogged['User']['fullname'] . ' - ' . $userLogged['User']['email'] . '<br>';
                            $msg .= '<b>Fecha de publicación: </b>' . $date . '<br>';
                            $msg .= '<b>Hora de publicación: </b>' . $time . 'hrs. <br>';
                            $msg .= '<b>Servicio:</b> ' . $alert['PublishAlerts']['service'] . '<br>';
                            $msg .= '<b>Comienzo del Servicio:</b> ' . $alert['PublishAlerts']['start_time'] . ' hrs.<br><br>';

                            mail($alert['PublishAlerts']['recipients'], '[' . $alert['PublishAlerts']['unit'] . '] Publicacion Tardia [' . $date . ']', $msg, $cabeceras);

                            $save['PublishAlertsLogs']['unit']  = $alert['PublishAlerts']['unit'];
                            $save['PublishAlertsLogs']['user']  = $userLogged['User']['fullname'];
                            $save['PublishAlertsLogs']['email']  = $userLogged['User']['email'];
                            $save['PublishAlertsLogs']['date']  = $date . ' ' . $time;
                            $save['PublishAlertsLogs']['service']  = $alert['PublishAlerts']['service'];
                            $save['PublishAlertsLogs']['start_service']  = $alert['PublishAlerts']['start_time'] . ' hrs.';
                            $save['PublishAlertsLogs']['gat_time_minutes']  = $diff;

                            $this->PublishAlertsLogs->create();
                            $this->PublishAlertsLogs->save($save);
                        }
                    }
                }
            }
        }
    }

    public function makeSync()
    {
        $clientFolder = $this->getClientSubdomain();

        switch ($clientFolder) {
            case 'cruzverde':
                $url = "http://190.151.100.10:9500/process";
                $result = file_get_contents($url);

                switch ($result) {
                    case 'initiated':
                        echo '<div class="modal-content">';
                        echo '  <div class="modal-body with-padding">';
                        echo '    <div class="alert alert-block alert-info fade in block-inner">';
                        echo '      <h6><i class="icon-screen"></i> Estado</h6>';
                        echo '      <hr>';
                        echo '      <p>Se inició el proceso de sincronización</p>';
                        echo '      <div class="text-left">';
                        echo '        <a class="btn btn-default" href="#" data-dismiss="modal">Cerrar</a>';
                        echo '      </div>';
                        echo '    </div>';
                        echo '  </div>';
                        echo '</div>';
                        break;
                    case 'busy':
                        echo '<div class="modal-content">';
                        echo '  <div class="modal-body with-padding">';
                        echo '    <div class="alert alert-block alert-info fade in block-inner">';
                        echo '      <h6><i class="icon-screen"></i> Alerta</h6>';
                        echo '      <hr>';
                        echo '      <p>Se está ejecutando un proceso previo de sincronización.</p>';
                        echo '      <div class="text-left">';
                        echo '        <a class="btn btn-default" href="#" data-dismiss="modal">Cerrar</a>';
                        echo '      </div>';
                        echo '    </div>';
                        echo '  </div>';
                        echo '</div>';
                        break;
                    default:
                        echo '<div class="modal-content">';
                        echo '  <div class="modal-body with-padding">';
                        echo '    <div class="alert alert-block alert-info fade in block-inner">';
                        echo '      <h6><i class="icon-screen"></i> Alerta</h6>';
                        echo '      <hr>';
                        echo '      <p>Hay un error en la sincronización.</p>';
                        echo '      <div class="text-left">';
                        echo '        <a class="btn btn-default" href="#" data-dismiss="modal">Cerrar</a>';
                        echo '      </div>';
                        echo '    </div>';
                        echo '  </div>';
                        echo '</div>';
                        break;
                }


                break;

            default:
                echo '<div class="modal-content">';
                echo '  <div class="modal-body with-padding">';
                echo '    <div class="alert alert-block alert-info fade in block-inner">';
                echo '      <h6><i class="icon-screen"></i> Error</h6>';
                echo '      <hr>';
                echo '      <p>Operación no permitida</p>';
                echo '      <div class="text-left">';
                echo '        <a class="btn btn-default" href="#" data-dismiss="modal">Cerrar</a>';
                echo '      </div>';
                echo '    </div>';
                echo '  </div>';
                echo '</div>';
                break;
        }

        die();
    }

    // Funcion que borra la marca de la ultima publicacion
    function deletePublishFile($directorio){
        $files = glob($directorio . '/*');
        foreach ($files as $file) {
            if (is_file(($file))) {
                if (strpos($file, 'Publish_') !== false) {
                    unlink($file);
                }
                //if (strpos($file, 'Cronjob_') !== false) {
                //    unlink($file);
                //}
            }
        }
    }

    public function makePublish()
    {
        if(isset($_GET['u'])) {
            $manual_publish = true;
        } else {
            $manual_publish = false;
        }

        $this->layout = 'ajax';

        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {
                $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
            }
        } else {
            $clientFolder = '';
        }

        $userLogged = parent::getLoggedUser();

        $userLogged['User']['id'] = isset($_GET['fromJOB']) ? $_GET['fromJOB'] : $userLogged['User']['id'];

        if (!isset($this->moduleData['gcdb']['publish_url']) && !isset($status)) {
            $status = array('success' => false, 'message' => 'No se definio URL de publicación');
        }

        if (isset($this->moduleData['gcdb']['notification_button'])) {
            $this->set('showSendNotificationsButton', true);
        } else {

            $this->set('showSendNotificationsButton', false);
        }

        if (!isset($status)) {

            date_default_timezone_set('America/Santiago');

            if (isset($this->moduleData['gcdb']['onelan']['players']['player'])) {

                $this->PublishHistory->create();
                $this->PublishHistory->save(array('user_id' => $userLogged['User']['id'], 'module' => $this->moduleKey, 'send_date' =>  date('Y-m-d H:i:s'), 'status' =>  'PENDING'));

                $publishID = $this->PublishHistory->getLastInsertId();

                if (isset($this->moduleData['gcdb']['publish_url']['@related-module'])) {

                    $this->Content->generatePublish($this->moduleKey, $clientFolder, $publishID, $this->moduleData['gcdb']);
                    $relatedModules = explode('|', $this->moduleData['gcdb']['publish_url']['@related-module']);


                    foreach ($relatedModules as $relatedModule) {

                        $this->Content->setSource($relatedModule);
                        $this->Content->generatePublish($relatedModule, $clientFolder, $publishID, $this->modules[$relatedModule]['gcdb']);
                    }
                } else {
                    $this->Content->generatePublish($this->moduleKey, $clientFolder, $publishID, $this->moduleData['gcdb']);
                }

                if (is_array($this->moduleData['gcdb']['onelan']['players']['player'])) {

                    foreach ($this->moduleData['gcdb']['onelan']['players']['player'] as $player) {
                        $this->PublishPlayerStatus->create();
                        $this->PublishPlayerStatus->save(array('publish_id' => $publishID, 'serial' => $player, 'status' => 'pending', 'send_date' =>  date('Y-m-d H:i:s'), 'retry_count' =>  0));
                    }
                } else {

                    $this->PublishPlayerStatus->create();
                    $this->PublishPlayerStatus->save(array('publish_id' => $publishID, 'serial' => $this->moduleData['gcdb']['onelan']['players']['player'], 'status' => 'pending', 'send_date' =>  date('Y-m-d H:i:s'), 'retry_count' =>  0));
                }
            } else {
                $Cliente = $this->moduleData['gcdb']['cliente'];
                $Sucursal =  $this->moduleData['gcdb']['sucursal'];
                
                if ($Cliente !== NULL && $Sucursal !== NULL){
                    // Borrar marca vieja de publicacion
                    $this->deletePublishFile(WWW_ROOT . 'files' . DS . $clientFolder . DS . 'templates_images' . DS . $Cliente . DS . $Sucursal);

                    $marca_hoy = WWW_ROOT . 'files' . DS . $clientFolder . DS . 'templates_images' . DS . $Cliente . DS . $Sucursal . DS .  'Publish_' . date("Ymd") . '.txt';
                    $fileHandler = fopen($marca_hoy, 'w+');
                    fclose($fileHandler);
                }

                $this->PublishHistory->create();
                $this->PublishHistory->save(array('user_id' => $userLogged['User']['id'], 'module' => $this->moduleKey, 'send_date' =>  date('Y-m-d H:i:s'), 'status' =>  'PUBLISHED'));

                $publishID = $this->PublishHistory->getLastInsertId();

                if (isset($this->moduleData['gcdb']['publish_url']['@related-module'])) {

                    $this->Content->generatePublish($this->moduleKey, $clientFolder, $publishID, $this->moduleData['gcdb']);

                    $relatedModules = explode('|', $this->moduleData['gcdb']['publish_url']['@related-module']);

                    foreach ($relatedModules as $relatedModule) {
                        $this->Content->setSource($relatedModule);
                        $this->Content->generatePublish($relatedModule, $clientFolder, $publishID,  $this->modules[$relatedModule]['gcdb']);
                    }

                } else {
                    $this->Content->generatePublish($this->moduleKey, $clientFolder, $publishID, $this->moduleData['gcdb']);
                }
            }

            $CURL = curl_init();

            curl_setopt_array($CURL, array(
                CURLOPT_TIMEOUT => 2,
                CURLOPT_RETURNTRANSFER => false,
                CURLOPT_URL => $this->moduleData['gcdb']['publish_url'] . '?module=' . $this->moduleKey . '&client=' . $clientFolder . '&publishID=' . $publishID
            ));

            $output = @curl_exec($CURL);

            curl_close($CURL);

            if (isset($this->moduleData['gcdb']['raspberry']['serial'])) {

                foreach ($this->moduleData['gcdb']['raspberry'] as $raspberry) {

                    foreach ($raspberry as $raspberryItem) {
                        //pr($raspberryItem);
                        if ($raspberryItem['@with_update_file']) {
                            $this->createUpdateFile($raspberryItem['@serial']);
                        }
                    }
                }

            }

            // registrar publicacion
            if ($manual_publish && $this->endsWith($this->moduleKey, 'menu_diario'))
                $this->registerPublication($this->moduleData['gcdb']['cliente'], $this->moduleData['gcdb']['sucursal'], $this->moduleData['gcdb']['segmento']);

            if ($clientFolder == 'sodexo' or $clientFolder == 'sodexochile') {
                $modulesExcluded = array('beneficios_maipu', 'cumpleanos_maipu', 'datos_avisos_maipu', 'noticias_maipu', 'seguridad_maipu', 'beneficios_corporativo', 'cumpleanos_corporativo', 'datos_avisos_corporativo', 'noticias_corporativo', 'seguridad_corporativo');

                if (!in_array($this->moduleKey, $modulesExcluded)) {
                    $this->makePublishImageMode();
                }
            }
            if ($clientFolder == 'enjoy') {
                $modulesIncludes = array('demo_promociones', 'demo_premios');

                if (in_array($this->moduleKey, $modulesIncludes)) {
                    $this->makePublishImageMode();
                }
            }
            if ($clientFolder == 'sodexo') {
            }

            $status = array('success' => true);
            CakeSession::delete('moduleEdit.' . $this->moduleKey);
        }

        $this->set('status', $status);
        $this->set('clientFolder', $clientFolder);
        $this->set('moduleKey', $this->moduleKey);
    }

    public function makeProcess()
    {
        $this->layout = 'ajax';

        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {
                $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
            }
        } else {
            $clientFolder = '';
        }

        $userLogged = parent::getLoggedUser();
        $userLogged['User']['id'] = isset($_GET['fromJOB']) ? $_GET['fromJOB'] : $userLogged['User']['id'];


        date_default_timezone_set('America/Santiago');
        $this->Content->procesarReportesDeModificacionCorporativo();
        $this->Content->procesarReportesDeModificacionMineria();
        $this->Content->procesarReportesDeModificacionSalud();

        $status = array('success' => true, 'message' => 'Se ha procesado Reporte Corporativo, Mineria y Salud');
        CakeSession::delete('moduleEdit.' . $this->moduleKey);

        $this->set('status', $status);
        $this->set('clientFolder', $clientFolder);
        $this->set('moduleKey', $this->moduleKey);
    }

    public function makeProcessCorporativo()
    {
        $this->layout = 'ajax';

        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {
                $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
            }
        } else {
            $clientFolder = '';
        }

        $userLogged = parent::getLoggedUser();
        $userLogged['User']['id'] = isset($_GET['fromJOB']) ? $_GET['fromJOB'] : $userLogged['User']['id'];


        date_default_timezone_set('America/Santiago');
        $this->Content->procesarReportesDeModificacionCorporativo();

        $status = array('success' => true, 'message' => 'Se ha procesado Reporte Corporativo, Mineria y Salud');
        CakeSession::delete('moduleEdit.' . $this->moduleKey);

        $this->set('status', $status);
        $this->set('clientFolder', $clientFolder);
        $this->set('moduleKey', $this->moduleKey);
    }

    public function makeProcessMineria()
    {
        $this->layout = 'ajax';

        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {
                $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
            }
        } else {
            $clientFolder = '';
        }

        $userLogged = parent::getLoggedUser();
        $userLogged['User']['id'] = isset($_GET['fromJOB']) ? $_GET['fromJOB'] : $userLogged['User']['id'];


        date_default_timezone_set('America/Santiago');
        $this->Content->procesarReportesDeModificacionMineria();

        $status = array('success' => true, 'message' => 'Se ha procesado Reporte Corporativo, Mineria y Salud');
        CakeSession::delete('moduleEdit.' . $this->moduleKey);

        $this->set('status', $status);
        $this->set('clientFolder', $clientFolder);
        $this->set('moduleKey', $this->moduleKey);
    }

    public function makeProcessSalud()
    {
        $this->layout = 'ajax';

        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {
                $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
            }
        } else {
            $clientFolder = '';
        }

        $userLogged = parent::getLoggedUser();
        $userLogged['User']['id'] = isset($_GET['fromJOB']) ? $_GET['fromJOB'] : $userLogged['User']['id'];


        date_default_timezone_set('America/Santiago');
        $this->Content->procesarReportesDeModificacionSalud();

        $status = array('success' => true, 'message' => 'Se ha procesado Reporte Corporativo, Mineria y Salud');
        CakeSession::delete('moduleEdit.' . $this->moduleKey);

        $this->set('status', $status);
        $this->set('clientFolder', $clientFolder);
        $this->set('moduleKey', $this->moduleKey);
    }

    function endsWith( $haystack, $needle ) {
        $length = strlen( $needle );
        if( !$length ) {
            return true;
        }
        return substr( $haystack, -$length ) === $needle;
    }

    public function sendNotifications()
    {
        $this->layout = 'ajax';

        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {

                $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
            }
        } else {
            $clientFolder = '';
        }

        $userLogged = parent::getLoggedUser();


        $CURL = curl_init();

        curl_setopt_array($CURL, array(
            CURLOPT_TIMEOUT => 2,
            CURLOPT_RETURNTRANSFER => false,
            CURLOPT_URL => 'http://sodexo.digitalboard.cl/files/sodexo/mobile/send_notifications.php'
        ));

        $output = @curl_exec($CURL);

        curl_close($CURL);


        $this->set('output', $output);
        $this->set('clientFolder', $clientFolder);
        $this->set('moduleKey', $this->moduleKey);
    }

    private function createUpdateFile($serial = false)
    {
        date_default_timezone_set('America/Santiago');

        $this->layout = 'ajax';

        if (STANDALONE_MODE === false) {

            $client_folder = $this->getClientSubdomain();

            if ($client_folder === false) {
                die('No se pudo determinar la carpeta de imagenes del cliente');
            }
        } else {
            $client_folder = '';
        }

        if ($serial) {

            @mkdir(WWW_ROOT . 'files' . DS . $client_folder . DS . 'templates' . DS . 'updates' . DS . $serial);
            $final_path = WWW_ROOT . 'files' . DS . $client_folder . DS . 'templates' . DS . 'updates' . DS . $serial . DS . 'update.csv';
        } else {
            $final_path = WWW_ROOT . 'files' . DS . $client_folder . DS . 'templates' . DS . 'updates' . DS . 'update.csv';
        }

        $fileData = fopen($final_path, 'w+');

        $txt = date('d-m-Y') . ';' . date("H:i:s");
        fwrite($fileData, $txt);
    }

    function getLiveRelatedContent()
    {
        $this->layout = 'ajax';
        $this->autoRender = false;

        $displayFields =  $this->request->params['displayFields'];

        $displayFieldsParts = explode(':', $displayFields);

        $term = $_GET['term'];

        $conditions[] = $displayFieldsParts[0] . ' LIKE "%' . $term . '%" OR ' . $displayFieldsParts[1] . ' LIKE "%' . $term . '%"';

        return json_encode($this->Content->getLiveRelatedContent($this->moduleKey, $conditions, $displayFieldsParts[0], $displayFieldsParts[1]));
    }

    function validateUnique()
    {
        $this->layout = 'ajax';
        $this->autoRender = false;

        $returnStatus = $this->Content->validateUnique($this->moduleKey, $_GET['id'], $_GET['field'], $_GET[$_GET['field']]);

        $returnMessage = (!empty($returnStatus)) ? 'Este identificador debe ser unico' : 'true';

        return json_encode($returnMessage);
    }

    // curl en php
    static public function curl_to_host($method, $url, $headers, $data, &$resp_headers)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        $rtn = curl_exec($ch);
        $error_msg = curl_error($ch);
        curl_close($ch);

        $rtn = explode("\r\n\r\nHTTP/", $rtn, 2);
        $rtn = (count($rtn) > 1 ? 'HTTP/' : '') . array_pop($rtn);

        list($str_resp_headers, $rtn) = explode("\r\n\r\n", $rtn, 2);

        $str_resp_headers = explode("\r\n", $str_resp_headers);
        array_shift($str_resp_headers);
        $resp_headers = array();
        foreach ($str_resp_headers as $k => $v) {
            $v = explode(': ', $v, 2);
            $resp_headers[$v[0]] = $v[1];
        }

        return $rtn;
    }

    static function recursiveDelete($handle, $directory)
    {   
        if( !(@ftp_rmdir($handle, $directory) || @ftp_delete($handle, $directory)) )
        {
            $filelist = @ftp_nlist($handle, $directory);
            foreach($filelist as $file) {
                $filename = explode('/',$file);
                $filename = $filename[count($filename)-1];

                if ($filename != "." && $filename != "..") {
                    ContentController::recursiveDelete($handle, $file);
                }
            }
            ContentController::recursiveDelete($handle, $directory);
        }
    }
    
}
