<?php
 
App::uses('AppController', 'Controller');

class ToolsController extends AppController {

    public function beforeFilter() {        

        parent::beforeFilter();        
        $userLogged = parent::checkLogged();
        if(isset($userLogged)){
			
			$this->Modules = parent::initModules();                               
		   
			if(!$this->Modules) {
				die('Error al cargar el XML de configuracion de modulos');
			}
			
			$modulesMenu = parent::getModulesMenu($this->Modules);
			
			$this->set('modulesMenu', $modulesMenu);                
			$this->set('userLogged', $userLogged);                        
			$this->set('activeMenu', 'tools');           
			$this->set('viewTitle', 'Herramientas');  
		}
        
    }      
	
	public function index(){
        
        $this->templatesURLs();
        
	}

    public function reset(){
        
        // llama a la funcion para hacer el Core2
        $this->setCore2();
        // Limpia el cache de Persistence y el de Modules
        parent::clear_cache();
        // llama a la funcion para hacer el Core0
        $this->setCore0();
       CakeSession::write('moduleEdit.' . $this->moduleKey, $this->moduleKey);
       $this->Session->setFlash("Modulo Reseteado. ", 'flash_custom'); 
       header("Location: /config_modulos");
        die();
    }

    public function setCore2(){
        $config_dir = dirname(__FILE__).'/../Config/';
        $source_file = $config_dir.'core2.php';
        $dest_file = $config_dir.'core.php';
        copy($source_file, $dest_file);
    }

    public function setCore0(){
        $config_dir = dirname(__FILE__).'/../Config/';
        $source_file = $config_dir.'core0.php';
        $dest_file = $config_dir.'core.php';
        copy($source_file, $dest_file);
    }
	
    public function offlineTools(){             
        
        $players          = false;
        $redirects        = false;
        $availablePlayers = $this->GCDS->getPlayers($this->Modules);
        
        if(STANDALONE_MODE === false) {
            
            $clientFolder = $this->getClientSubdomain();
            
            if($clientFolder === false) {
                die('No se pudo determinar la carpeta de imagenes del cliente');
            }
            
        } else {
            $clientFolder = '';
        }
        
        if(isset($_GET['d'])) {
           
            header("Content-disposition: attachment; filename=redirect_". $_GET['d']. ".html");
            header("Content-type: text/html");
            echo "<!DOCTYPE html><html><head><title></title></head><body><script>location.href= '../../ad_hoc_media/files/".$clientFolder."/templates/html/prod/" . $_GET['d']. ".html';</script></body></html>";
            die();
            
        }
        
        if($availablePlayers) {
            foreach($availablePlayers as $playerSerial) {                 
                if(!isset($players[$playerSerial])) {
                    $players[$playerSerial] = $playerSerial;
                }
            }
        }
        
        $root = WWW_ROOT .'files' . DS . $clientFolder . DS . 'templates';

        $iter = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($root, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::SELF_FIRST,
            RecursiveIteratorIterator::CATCH_GET_CHILD 
        );

        $paths = array($root);
        
        foreach ($iter as $path => $dir) {
            if ($dir->isDir()) {
                $paths[] = str_replace($root, '', $path);
            }
        }

        foreach($this->Modules as $k => $v) {            
            $redirects[$k] = $this->Modules[$k]['gcdb']['content_name'];
        }
        
        unset($paths[0]);     
        
        $this->set('paths', $paths);        
        $this->set('players', $players);  
        $this->set('redirects', $redirects);  
        $this->set('clientFolder', $clientFolder);  
        $this->set('activeMenu', 'offlineTools'); 
        
    }
    
//    <!DOCTYPE html> 
//<html>
//    <head>
//        <title></title>        
//    </head>
//    <body>        
//                
//        <script>                    	
//			location.href= '../../../ad_hoc_media/files/enjoy/templates/html/prod/conoce_a_edificios_corporativos.html';
//        </script></body></html>
    
    public function templatesUrls(){

        if(STANDALONE_MODE === false) {
            
            $clientFolder = $this->getClientSubdomain();
            
            if($clientFolder === false) {
                die('No se pudo determinar el subdominio del cliente');
            }
            
        } else {
            $clientFolder = '';
        }
        
        $test = false;
        $prod = false;
        
        foreach($this->Modules as $k => $v) {
            $test[] = '<tr><td><b>'.$this->Modules[$k]['gcdb']['content_name'].'</b></td><td>http://'.$clientFolder.'.digitalboard.cl/files/'.$clientFolder.'/templates/html/test/'.$k.'.html</td></tr>'; 
            $prod[] = '<tr><td><b>'.$this->Modules[$k]['gcdb']['content_name'].'</b></td><td>http://'.$clientFolder.'.digitalboard.cl/files/'.$clientFolder.'/templates/html/prod/'.$k.'.html</td></tr>'; 
        }       
        
        $this->set('activeMenu', 'templatesURLs'); 
        $this->set('testURLs', $test); 
        $this->set('prodURLs', $prod); 
	}
    
	
}