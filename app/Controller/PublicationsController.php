<?php
 
App::uses('AppController', 'Controller');

class PublicationsController extends AppController {

    public  $uses      = array('GCDS', 'PublishHistory', 'Permission', 'PublishPlayerStatus', 'DSM', 'Content');

    public function beforeFilter() {        
        
        parent::beforeFilter();        
          
        $this->Modules = parent::initModules();    
        $client = $this->GCDS->getClientSubdomain();
         
        if(!$this->Modules) {
            die('Error al cargar el XML de configuracion de modulos');
        }
        
        $modulesMenu = parent::getModulesMenu($this->Modules);
                    
        $this->set('modulesMenu', parent::getModulesMenu($this->Modules));    
        $this->set('client', $client);
        $this->set('activeMenu', 'publish');           
        $this->set('viewTitle', 'Publicador');      
        
    }      

   public function index() {  
       
       $userLogged = parent::getLoggedUser();

       $userPermission = $this->Permission->find('all', array(
           'conditions'=>array('Permission.user_id' => $userLogged['User']['id'])
       ));
       
       $userPermission = Set::extract('{n}.Permission.module', $userPermission);

       if($userLogged['User']['rol'] == 'PLATFORM_ADMIN'){
           $condition = array();
       }else{
           $condition = array('PublishHistory.module in ("' . implode('","', $userPermission) . '")');
       }
       
       $publications = $this->PublishHistory->find('all', 
              // array('fields' => array('DATE_FORMAT(PublishHistory.send_date, "%d/%m/%Y a las %H:%i") as formatDate', '*','User.*'), 'order' => 'send_date DESC', 'limit' => 50,
                   array('fields' => array('DATE_FORMAT(DATE_ADD(PublishHistory.send_date, INTERVAL 0 HOUR), "%d/%m/%Y a las %H:%i") as formatDate', '*','User.*'), 'order' => 'send_date DESC', 'limit' => 50,
                   'joins' => array(
                array(
                    'table' => 'user',
                    'alias' => 'User',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'User.id = PublishHistory.user_id'
                    )
                )),
                   'conditions' => $condition
                   ));
         
        $playerNames = array();

    //     foreach($publications as &$publication) {
           
    //         $playerStatus = $this->PublishPlayerStatus->findAllByPublishId($publication['PublishHistory']['id']);
           
    //         $publication['PublishHistory']['displayName'] = $this->GCDS->getModuleNameByKey($publication['PublishHistory']['module'], $this->Modules);
              
            
            
    //         foreach ($playerStatus as &$player) {
                
    //             if(!array_key_exists($player['PublishPlayerStatus']['serial'], $playerNames)) {
    //                 $playerNames[$player['PublishPlayerStatus']['serial']] = $this->DSM->getPlayerName($player['PublishPlayerStatus']['serial']);
    //             }

    //             $player['PublishPlayerStatus']['player_name'] = isset($playerNames[$player['PublishPlayerStatus']['serial']]) ? $playerNames[$player['PublishPlayerStatus']['serial']] : '';

    //         }
            

    //        $publication['playerStatus'] = $playerStatus;
    //    }
       
       $this->set('publications', $publications); 
       
   
   }
   
   public function reports($page = 1){
       
        $userLogged = parent::getLoggedUser();
        $current_time = date('d-m-Y');
        
        $userPermission = $this->Permission->find('all', array(
            'conditions'=>array('Permission.user_id' => $userLogged['User']['id'])
        ));
       
        $userPermission = Set::extract('{n}.Permission.module', $userPermission);
        
        //$publications = $this->PublishHistory->getPublishWeek($page, $rowsPerPage, $pages);
        
        $condition = array( 'PublishHistory.send_date BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY)  AND NOW()', 'NOT' => array('module LIKE "%maestro%"' , 'module LIKE "%bloque%"'));
        $publications = $this->PublishHistory->getPublishWeek($condition);
        $arrayModulePulication = $this->publicationsWeekHystory($publications);

        $start = 2 * ($page - 1);
        $pages= ceil(count($arrayModulePulication)/2);
        
        $arrayModulePulication2 = array_slice($arrayModulePulication, $start, 2);
        
        $this->set('pages', $pages);
        $this->set('page', $page);
        $this->set('publications', $arrayModulePulication2); 
        
   }

   public function download(){
       
        $this->layout = 'ajax';
        $this->autoRender = false;
      
        $condition = array( 'PublishHistory.send_date BETWEEN DATE_SUB(NOW(), INTERVAL 1 MONTH)  AND NOW()', 'NOT' => array('module LIKE "%maestro%"' , 'module LIKE "%bloque%"', 'module LIKE "%pelambre%"'));
        
        //$publications = $this->PublishHistory->getPublishWeek($page, $rowsPerPage, $pages);
        $publications = $this->PublishHistory->getPublishWeek($condition);
 
        $arrayModulePulicationdownload = $this->publicationsWeekHystory($publications);
        $client = $this->GCDS->getClientSubdomain();
        
        
        $actual = 'fecha; cantidad de publicaciones; sucursal; modulo; ediciones platos día; ediciones platos futuros'.PHP_EOL;
            
        foreach($arrayModulePulicationdownload as $date => $publication) {
            foreach($publication as $modulePublication) {
                
                $clientModule = explode(':', $modulePublication['displayName']);
                
                
                
                $actual .= $date.'; ' .count($modulePublication['publish']) . '; ' . utf8_decode($clientModule[0]) . '; ' . utf8_decode($clientModule[1]) . '; ' . count($modulePublication['today']) . '; ' . count($modulePublication['future']).PHP_EOL;
            }                
        }
          
        ob_start();
        header('Content-disposition: attachment; filename=historial_publicaciones_' . $client . '.csv');
        header('Content-Type: text/csv; charset=utf-8');
        header('Expires: 0');  
        header('Cache-Control: no-cache');             
        echo $actual;
        header('Content-Length: '. ob_get_length());   
        ob_flush();
        die();
       
   }
   
   public function publicationsWeekHystory($publications){

        foreach ($publications as $publication){
            $sendDateExplode = explode(" ", $publication['PublishHistory']['send_date']);
            $sendDateArray = explode("-", $sendDateExplode[0]);
            $sendDateFormat = $sendDateArray[2] . '-' . $sendDateArray[1] . '-' . $sendDateArray[0];
            
            $arrayModulePulication[$sendDateFormat][$publication['PublishHistory']['module']]['displayName'] = $this->GCDS->getModuleNameByKey($publication['PublishHistory']['module'], $this->Modules);
            $arrayModulePulication[$sendDateFormat][$publication['PublishHistory']['module']]['publish'][] = $publication;
            
            $conditionToday = array();
            $conditionToday[] = array("fecha BETWEEN '". $sendDateFormat . "' AND '" . $sendDateFormat . "'", "TIMEDIFF(created, modified)");
            $arrayModulePulication[$sendDateFormat][$publication['PublishHistory']['module']]['today'] = $this->PublishHistory->getModifModule($publication['PublishHistory']['module'], $conditionToday);
            
            $conditionFuture = array();
            $conditionFuture[] = array("STR_TO_DATE(fecha,'%d-%m-%Y')  > STR_TO_DATE('" . $sendDateFormat . "','%d-%m-%Y')", "TIMEDIFF(created, modified)");
            $arrayModulePulication[$sendDateFormat][$publication['PublishHistory']['module']]['future'] = $this->PublishHistory->getModifModule($publication['PublishHistory']['module'], $conditionFuture);
  
        }   
        
        return $arrayModulePulication;
   }
   
   
    public function publicationsReviewImages(){

        $modulesMenu = parent::getModulesMenu($this->Modules);
        foreach ($modulesMenu as $module){
            if(stristr($module['url'], 'maestro') === FALSE){
                $nameExplode = explode(': ', $module['name']);
                $module['url'] = str_replace('_menu_diario','', $module['url']); 
                $module['url'] = str_replace('_sugerencia_chef','', $module['url']);
                $module['url'] = str_replace('_bloque_inferior_1','', $module['url']); 
                $module['url'] = str_replace('_bloque_inferior_2','', $module['url']);
                $module['url'] = str_replace('_bloque_inferior','', $module['url']); 
                $module['url'] = str_replace('_bloque_inferior','', $module['url']);

                
                $modules[$module['url'].'|'.$module['ftp_connection']] = $nameExplode[0];
            }
        }

        $modulesFilter = array_unique($modules);
        $this->set('modules', $modulesFilter); 
    }
    
    /*
    
    public function generateFolderYears(){
        
         if($this->request->is('post')){
             
            $client = $this->GCDS->getClientSubdomain();
            
            if(!file_exists(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $this->request->data['client'])){
                mkdir(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $this->request->data['client']);
            }
            
            if(!file_exists(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $this->request->data['client']) . DS . $this->request->data['branch']){
                mkdir(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $this->request->data['client'] . DS . $this->request->data['branch']);
            }
            
            mkdir(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $this->request->data['client'] . DS . $this->request->data['branch'] . DS . $this->request->data['ano']);

            for($m = 1; $m <= 12; $m++){
                $meses[] = ($m < 10)? '0' . $m : $m;
            }
            $mesComplete = 31;
             $countServices = count($this->request->data['services']);
             
            foreach ($meses as $mes){
                mkdir(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $this->request->data['client'] . DS . $this->request->data['branch'] . DS . $this->request->data['ano'] . DS . $mes);
                
                for($dia = 1; $dia <= $mesComplete; $dia++){
                    
                    mkdir(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $this->request->data['client'] . DS . $this->request->data['branch'] . DS . $this->request->data['ano'] . DS . $mes . DS . $dia);
                    
                    if($countServices <= 1 && $this->request->data['services'][0] == 'Alm'){
                            foreach ($_POST['orientacion'] as $orientacion){
                                mkdir(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $this->request->data['client'] . DS . $this->request->data['branch'] . DS . $this->request->data['ano'] . DS . $mes . DS . $dia . DS . $orientacion);
                            }
                        }else{

                           if($countServices > 1){
                                foreach ($this->request->data['services'] as $service){
                                    mkdir(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $this->request->data['client'] . DS . $this->request->data['branch'] . DS . $this->request->data['ano'] . DS . $mes . DS . $dia . DS . $service); 
                                    foreach ($this->request->data['orientacion'] as $orientacion){
                                        mkdir(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $this->request->data['client'] . DS . $this->request->data['branch'] . DS . $this->request->data['ano'] . DS . $mes . DS . $dia . DS . $service . DS . $orientacion);
                                    }
                                }
                           }else{
                               echo 'ERROR';
                           }

                        }
                    
                }   
            }
        echo 'OK';
        die();
        }
        
    }*/
    
    
    
    public function generateFolder(){
        
        $nameClient = '';
        $branch = '';
        $servicio = [];
        
        $ano = date("Y");
        $mesActual = date("n");
        $mesActual = $mesActual - 1;
        
        for($m = $mesActual; $m <= 12; $m++){
            $meses[] = ($m < 10)? '0' . $m : $m;
        }

        $totalDias = 31;
        $client = $this->GCDS->getClientSubdomain();

       
        if($_POST){
             
            if(isset($_POST['client']) && !empty($_POST['client']) || isset($_POST['branch']) && !empty($_POST['branch'])){
         
                
                $countServices = count($_POST['services']);
                
                if(!file_exists(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $_POST['client'])){
                    mkdir(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $_POST['client']);
                }

                mkdir(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $_POST['client'] . DS . $_POST['branch']);
                mkdir(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $_POST['client'] . DS . $_POST['branch'] . DS . $ano);
                foreach ($meses as $mes){

                    mkdir(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $_POST['client'] . DS . $_POST['branch'] . DS . $ano . DS . $mes);
                    for($dia = 1; $dia <= $totalDias; $dia++){

                        mkdir(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $_POST['client'] . DS . $_POST['branch'] . DS . $ano . DS . $mes . DS . $dia);
                        if($countServices <= 1 && $_POST['services'][0] == 'Alm'){
                            foreach ($_POST['orientacion'] as $orientacion){
                                mkdir(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $_POST['client'] . DS . $_POST['branch'] . DS . $ano . DS . $mes . DS . $dia . DS . $orientacion);
                            }
                        }else{

                           if($countServices > 1){
                                foreach ($_POST['services'] as $service){
                                    mkdir(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $_POST['client'] . DS . $_POST['branch'] . DS . $ano . DS . $mes . DS . $dia . DS . $service); 
                                    foreach ($_POST['orientacion'] as $orientacion){
                                        mkdir(WWW_ROOT .'files' . DS . $client . DS . 'templates_images' . DS . $_POST['client'] . DS . $_POST['branch'] . DS . $ano . DS . $mes . DS . $dia . DS . $service . DS . $orientacion);
                                    }
                                }
                           }else{
                               echo 'ERROR';
                           }

                        }
                    }
                }

                $dataConnection =  'ftp://' . $_POST['client'] . ':qazplm@190.151.100.14/';
                $ftpConnection = $this->connectFTP($dataConnection);
                $urlLogo = WWW_ROOT .'files' . DS . $client . DS . 'templates' . DS . 'images';
                 
                if($ftpConnection){
                   
                    $rute = '/' . $_POST['branch'];

                    ftp_mkdir($ftpConnection, $_POST['branch']);
                    
                    for($diasFtp = 1; $diasFtp <= 7; $diasFtp++){
                        
                        ftp_mkdir($ftpConnection, $rute . DS . $diasFtp);
               
                        if($countServices == 1 && $_POST['services'][0] == 'Alm'){

                            foreach ($_POST['orientacion'] as $orientacion){

                                ftp_mkdir($ftpConnection, $rute . DS . $diasFtp . DS . $orientacion);

                                $logo = 'sodexo_logo_1080x1152.jpg';
                                if($orientacion == 'Izq' && $orientacion == 'Der'){
                                    $logo = 'sodexo_logo_960x1080.jpg'; 
                                }else if($orientacion == 'Aba'){
                                    $logo = 'sodexo_logo_1080x768.jpg';
                                }
                                if(ftp_chdir($ftpConnection, $rute . DS . $diasFtp . DS . $orientacion)){
                                    ftp_put($ftpConnection, $rute . DS . $diasFtp . DS . $orientacion . DS . $logo, $urlLogo . DS . $logo, FTP_BINARY);
                                }
                            }
                        }else{
                            
                            foreach ($_POST['services'] as $service){

                                ftp_mkdir($ftpConnection, $rute . DS . $diasFtp . DS . $service);

                                foreach ($_POST['orientacion'] as $orientacion){
                                    ftp_mkdir($ftpConnection, $rute . DS . $diasFtp . DS . $service . DS . $orientacion);

                                    $logo = 'sodexo_logo_1080x1152.jpg';
                                    if($orientacion == 'Izq' && $orientacion == 'Der'){
                                        $logo = 'sodexo_logo_960x1080.jpg'; 
                                    }else if($orientacion == 'Aba'){
                                        $logo = 'sodexo_logo_1080x768.jpg';
                                    }
                                    if(ftp_chdir($ftpConnection, $rute . DS . $diasFtp . DS . $service . DS . $orientacion)){
                                        ftp_put($ftpConnection, $rute . DS . $diasFtp . DS . $service . DS . $orientacion . DS . $logo, $urlLogo . DS . $logo, FTP_BINARY);
                                    }

                                }
                            }
                        }  
                    }
                    
                    ftp_close($ftpConnection);
                     
                    $groupExist = false;
                    $groupID = 0;
                    $serverMI = $_POST['serverMI'];
                    $createGroups = false;
                    $subGroupID = false;
                    $portMI =  $_POST['portMI'];
                    
                    $responseToken = $this->curlApiMI('http://'.$serverMI.':' . $portMI . '/MagicInfo/openapi/auth?cmd=getAuthToken&id=admin&pw=qazplm_01');
                    $token = $responseToken['responseClass'];
                    
                    if($token){
                       
                        $responseListGroup = $this->curlApiMI('http://'.$serverMI.':' . $portMI . '/MagicInfo/openapi/open?service=CommonContentService.getContentGroupList&token=' . $token);
                        $listGroup = $responseListGroup['responseClass']['resultList']['Group'];
                    
     
                        
                        
                        foreach ($listGroup as $group){
                                
                            if($group['group_name'] == $_POST['client']){
                               
                                $groupExist = true;
                                $groupID = $group['group_id'];
                                break;
                            }
                        }
                        
                       
                        if($groupExist){

                            if($_POST['branch'] != 'U1'){

                                foreach ($listGroup as $group2){
                                
                                    if($group2['group_name'] == $_POST['branch']){

                                        $subGroupExist = true;
                                        $groupID = $group2['group_id'];
                                    }
                                }
                                
                                if(!$subGroupExist){
                                    $responseCreateGroup = $this->curlApiMI('http://' . $serverMI . ':' . $portMI . '/MagicInfo/openapi/open?service=CommonContentService.addContentGroup&token=' . $token . '&group=%20%3CGroup%3E%20%3Cgroup_name%3E'.$_POST['branch'].'%3C/group_name%3E%3Cp_group_id%3E'.$groupID.'%3C/p_group_id%3E%3C/Group%3E');
                                }
                                $createGroups = true;
                            }else{
                                $createGroups = true;
                            }

                        }else{
                    
                            $responseCreateGroup = $this->curlApiMI('http://' . $serverMI . ':' . $portMI . '/MagicInfo/openapi/open?service=CommonContentService.addContentGroup&token=' . $token . '&group=%20%3CGroup%3E%20%3Cgroup_name%3E' . $_POST['client'] . '%3C/group_name%3E%3Cp_group_id%3E' . $groupID . '%3C/p_group_id%3E%3C/Group%3E');

                            if($_POST['branch'] != 'U1'){
                                if($responseCreateGroup['responseClass']){
                                
                                    $responseListGroup2 = $this->curlApiMI('http://' . $serverMI . ':' . $portMI . '/MagicInfo/openapi/open?service=CommonContentService.getContentGroupList&token=' . $token);
                                    $listGroup2 = $responseListGroup2['responseClass']['resultList']['Group'];

                                    foreach ($listGroup2 as $group){

                                        if($group['group_name'] == $_POST['client']){

                                            $responseCreateSubGroup = $this->curlApiMI('http://' . $serverMI . ':' . $portMI . '/MagicInfo/openapi/open?service=CommonContentService.addContentGroup&token=' . $token . '&group=%20%3CGroup%3E%20%3Cgroup_name%3E' . $_POST['branch'] . '%3C/group_name%3E%3Cp_group_id%3E' . $group['group_id'] . '%3C/p_group_id%3E%3C/Group%3E');
                                            $createSubGroup = $responseCreateSubGroup['responseClass'];

                                            if($createSubGroup){
                                                $createGroups = true;   
                                            }

                                        }
                                    }
                                } 
                            }else{
                                $createGroups = true;   
                            }
                        }

                        if($createGroups){
                            
                            $responseListGroup = $this->curlApiMI('http://' . $serverMI . ':' . $portMI . '/MagicInfo/openapi/open?service=CommonContentService.getContentGroupList&token=' . $token);
                            $listGroup = $responseListGroup['responseClass']['resultList']['Group'];
                            
                            $branchDistinct = $_POST['branch'] != 'U1'? $_POST['branch']: $_POST['client'];
                            
                            
                            foreach ($listGroup as $group){
                                
                                if($group['group_name'] == $branchDistinct){

                                    $subGroupID = $group['group_id'];
                                    $createRemote = true;
                                    break;
                                }
                            } 
                            
                            if($createRemote){
                                
                                $weeek = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];
                                $services = $_POST['services'];
                                $ipFtp = $_POST['ipFtp'];
                                $ori = $_POST['orientacion'];
                                $passFTP ='qazplm';
                                $resfresh = $_POST['refresh'];
                                $puerto = $_POST['port'];
                                
                                for($ii=0; $ii<7; $ii++){
                                
                                    foreach ($services as $service){

                                        $dayFTP = $ii + 1;

                                        if(strtolower($service) == strtolower('alm') && count($services) == 1){
                                             
                                            foreach ($ori as $orient){
                                                
                                                $ftpDir ='/' . $_POST['branch']. '/' . $dayFTP . '/' . $orient . '/' ;
                                                
                                                $contentName = $weeek[$ii].'_'.  strtoupper($service). '_' . $_POST['client']. '_' . $_POST['branch'] .'_'. strtoupper($orient);
                                                $addContent = $this->curlApiMI("http://".$serverMI.":" . $portMI . "/MagicInfo/openapi/open?service=CommonContentService.addFtpContent&token=" . $token . "&ftpContentName=" . $contentName . "&ftpAddress=" . $ipFtp . "&ftpDirectory=" . $ftpDir . "&ftpPort=" . $puerto . "&group_id=" . $subGroupID . "&loginId=" . $_POST['client'] . "&password=" . $passFTP . "&refreshInterval=" . $resfresh);
                                               
                                            }
                                        }else{
                                            
                                            foreach ($ori as $orient){
 
                                                $ftpDir ='/' . $_POST['branch']. '/' . $dayFTP . '/' . $service . '/' . $orient . '/' ;

                                                $contentName = $weeek[$ii].'_'.  strtoupper($service). '_' . $_POST['client']. '_' . $_POST['branch'] .'_'. strtoupper($orient);
                                                $addContent = $this->curlApiMI("http://" . $serverMI . ":" . $portMI . "/MagicInfo/openapi/open?service=CommonContentService.addFtpContent&token=" . $token . "&ftpContentName=" . $contentName . "&ftpAddress=" . $ipFtp . "&ftpDirectory=".$ftpDir."&ftpPort=" . $puerto . "&group_id=" . $subGroupID . "&loginId=" . $_POST['client'] . "&password=" . $passFTP . "&refreshInterval=" . $resfresh);

                                            }
                                        } 

                                    }

                                }
                                echo 'OK';
                                
                            }
                   
                        }else{
                            echo 'ERROR_CARPETAS';
                        }
                        
                    }

                }else{
                    echo "ERROR FTP";
                }
            }
            die();
        }
   
    }
    
    function curlApiMI($url){
        
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = simplexml_load_string(curl_exec($curl));
        curl_close($curl);

        $json = json_encode($response);
        $array = json_decode($json, true);
        
        return $array;
    }
    
    
    /*
    function ftp_isdir($connect_id,$dir){
            
            if(ftp_chdir($connect_id,$dir))
            {
                ftp_cdup($connect_id);
                return true;

            }
            else
            {
                return false;
            }
    } */
    
    public function getHH() {
                
        /***************************************************************/
        
        $hh['bci_agustinas']['Almuerzo']['init'] = '10:30:00';
        $hh['bci_agustinas']['Almuerzo']['end'] = '16:30:00';
        
        
        $hh['bci_alcantara']['Almuerzo']['init'] = '10:30:00';
        $hh['bci_alcantara']['Almuerzo']['end'] = '16:30:00';
        
        $hh['bci_el_golf']['Almuerzo']['init'] = '10:30:00';
        $hh['bci_el_golf']['Almuerzo']['end'] = '16:30:00'; 
        
        $hh['bci_huerfanos']['Almuerzo']['init'] = '10:30:00';
        $hh['bci_huerfanos']['Almuerzo']['end'] = '16:30:00';         
                
        $hh['bci_nova']['Almuerzo']['init'] = '10:30:00';
        $hh['bci_nova']['Almuerzo']['end'] = '16:30:00';        
        
        $hh['bci_telecanal']['Almuerzo']['init'] = '10:30:00';
        $hh['bci_telecanal']['Almuerzo']['end'] = '16:30:00';         
        /***************************************************************/
        $hh['banco_chile_agustinas_menu_diario']['Almuerzo']['init'] = '10:30:00';
        $hh['banco_chile_agustinas_menu_diario']['Almuerzo']['end'] = '16:30:00';         
        /***************************************************************/
        $hh['bice_vida_unidad1']['Almuerzo']['init'] = '12:00:00';
        $hh['bice_vida_unidad1']['Almuerzo']['end'] = '15:30:00';                 
        /***************************************************************/        
        $hh['hospital_militar_unidad1']['Almuerzo']['init'] = '10:30:00';
        $hh['hospital_militar_unidad1']['Almuerzo']['end'] = '15:45:00';
        $hh['hospital_militar_unidad1']['Cena']['init'] = '19:00:00';
        $hh['hospital_militar_unidad1']['Cena']['end'] = '23:45:00';                     
        /***************************************************************/       
        $hh['caserones_unidad1']['Desayuno']['init'] = '05:00:00';
        $hh['caserones_unidad1']['Desayuno']['end'] = '10:30:00'; 
        $hh['caserones_unidad1']['Almuerzo']['init'] = '12:00:00';
        $hh['caserones_unidad1']['Almuerzo']['end'] = '15:00:00';
        $hh['caserones_unidad1']['Cena']['init'] = '16:45:00';
        $hh['caserones_unidad1']['Cena']['end'] = '23:00:00';                       
        /***************************************************************/          
        $hh['lomas_bayas_unidad1']['Desayuno']['init'] = '06:00:00';
        $hh['lomas_bayas_unidad1']['Desayuno']['end'] = '09:00:00'; 
        $hh['lomas_bayas_unidad1']['Almuerzo']['init'] = '12:00:00';
        $hh['lomas_bayas_unidad1']['Almuerzo']['end'] = '15:30:00';
        $hh['lomas_bayas_unidad1']['Cena']['init'] = '18:00:00';
        $hh['lomas_bayas_unidad1']['Cena']['end'] = '21:30:00';
        $hh['lomas_bayas_unidad1']['Cena Nochera']['init'] = '00:00:00';
        $hh['lomas_bayas_unidad1']['Cena Nochera']['end'] = '03:00:00';        
        /***************************************************************/                 
        $hh['sura_unidad1']['Almuerzo']['init'] = '12:00:00';
        $hh['sura_unidad1']['Almuerzo']['end'] = '15:30:00'; 
        /***************************************************************/                 
        $hh['colbun_nehuenco']['Almuerzo']['init'] = '10:30:00';
        $hh['colbun_nehuenco']['Almuerzo']['end'] = '15:30:00';         
        /***************************************************************/                 
        $hh['oficina_regional_talcahuano']['Almuerzo']['init'] = '10:30:00';
        $hh['oficina_regional_talcahuano']['Almuerzo']['end'] = '15:00:00';                  
        /***************************************************************/                 
        $hh['el_mercurio_unidad1']['Almuerzo']['init'] = '11:00:00';
        $hh['el_mercurio_unidad1']['Almuerzo']['end'] = '16:30:00';             
        /***************************************************************/                 
        $hh['papeles_biobio_u1']['Almuerzo']['init'] = '13:00:00';
        $hh['papeles_biobio_u1']['Almuerzo']['end'] = '14:00:00';                           
        /***************************************************************/                 
        $hh['orafti_unidad1']['Almuerzo']['init'] = '10:30:00';
        $hh['orafti_unidad1']['Almuerzo']['end'] = '15:30:00';        
        /***************************************************************/                 
        $hh['colbun_los_andes']['Almuerzo']['init'] = '10:30:00';
        $hh['colbun_los_andes']['Almuerzo']['end'] = '15:30:00';        
        /***************************************************************/                 
        $hh['agricola_garces_molina']['Almuerzo']['init'] = '10:30:00';
        $hh['agricola_garces_molina']['Almuerzo']['end'] = '14:30:00';  
        $hh['agricola_garces_molina']['Cena Tarde']['init'] = '19:00:00';
        $hh['agricola_garces_molina']['Cena Tarde']['end'] = '21:00:00';  
        $hh['agricola_garces_molina']['Cena Madrugada']['init'] = '02:00:00';
        $hh['agricola_garces_molina']['Cena Madrugada']['end'] = '04:00:00';              
        /***************************************************************/                 
        $hh['clinica_alemana_unidad1']['Almuerzo']['init'] = '11:00:00';
        $hh['clinica_alemana_unidad1']['Almuerzo']['end'] = '15:00:00';                    
        /***************************************************************/                 
        $hh['collahuasi_pionero']['Desayuno']['init'] = '04:30:00';
        $hh['collahuasi_pionero']['Desayuno']['end'] = '10:30:00';  
        $hh['collahuasi_pionero']['Almuerzo']['init'] = '10:30:00';
        $hh['collahuasi_pionero']['Almuerzo']['end'] = '15:00:00';  
        $hh['collahuasi_pionero']['Cena']['init'] = '16:00:00';
        $hh['collahuasi_pionero']['Cena']['end'] = '23:00:00';          
            
        $hh['collahuasi_1000']['Desayuno']['init'] = '04:30:00';
        $hh['collahuasi_1000']['Desayuno']['end'] = '10:30:00';  
        $hh['collahuasi_1000']['Almuerzo']['init'] = '10:30:00';
        $hh['collahuasi_1000']['Almuerzo']['end'] = '15:00:00';  
        $hh['collahuasi_1000']['Cena']['init'] = '16:00:00';
        $hh['collahuasi_1000']['Cena']['end'] = '23:00:00';
        
        $hh['collahuasi_460']['Desayuno']['init'] = '04:30:00';
        $hh['collahuasi_460']['Desayuno']['end'] = '10:30:00';  
        $hh['collahuasi_460']['Almuerzo']['init'] = '10:30:00';
        $hh['collahuasi_460']['Almuerzo']['end'] = '15:00:00';  
        $hh['collahuasi_460']['Cena']['init'] = '16:00:00';
        $hh['collahuasi_460']['Cena']['end'] = '23:00:00';
        
        $hh['collahuasi_coposa']['Desayuno']['init'] = '04:30:00';
        $hh['collahuasi_coposa']['Desayuno']['end'] = '10:30:00';  
        $hh['collahuasi_coposa']['Almuerzo']['init'] = '10:30:00';
        $hh['collahuasi_coposa']['Almuerzo']['end'] = '15:00:00';  
        $hh['collahuasi_coposa']['Cena']['init'] = '16:00:00';
        $hh['collahuasi_coposa']['Cena']['end'] = '23:00:00';
        /***************************************************************/                 
        $hh['metlife_unidad1']['Almuerzo']['init'] = '12:00:00';
        $hh['metlife_unidad1']['Almuerzo']['end'] = '15:30:00';  
        /***************************************************************/                 
        $hh['fresnius_kabi_food_unidad1']['Almuerzo']['init'] = '10:30:00';
        $hh['fresnius_kabi_food_unidad1']['Almuerzo']['end'] = '15:00:00';          
        /***************************************************************/    
        $hh['minera_anglo_tortolas_planta']['Desayuno']['init'] = '06:30:00';
        $hh['minera_anglo_tortolas_planta']['Desayuno']['end'] = '09:30:00';  
        $hh['minera_anglo_tortolas_planta']['Almuerzo']['init'] = '12:00:00';
        $hh['minera_anglo_tortolas_planta']['Almuerzo']['end'] = '15:30:00';  
        $hh['minera_anglo_tortolas_planta']['Cena']['init'] = '18:00:00';
        $hh['minera_anglo_tortolas_planta']['Cena']['end'] = '22:30:00'; 
        $hh['minera_anglo_tortolas_planta']['Cena Nochera']['init'] = '00:00:00';
        $hh['minera_anglo_tortolas_planta']['Cena Nochera']['end'] = '03:30:00';         
       
        $hh['minera_anglo_tortolas_central']['Desayuno']['init'] = '06:30:00';
        $hh['minera_anglo_tortolas_central']['Desayuno']['end'] = '09:30:00';  
        $hh['minera_anglo_tortolas_central']['Almuerzo']['init'] = '12:00:00';
        $hh['minera_anglo_tortolas_central']['Almuerzo']['end'] = '15:30:00';  
        $hh['minera_anglo_tortolas_central']['Cena']['init'] = '18:00:00';
        $hh['minera_anglo_tortolas_central']['Cena']['end'] = '22:30:00'; 
        $hh['minera_anglo_tortolas_central']['Colación Nochera']['init'] = '00:00:00';
        $hh['minera_anglo_tortolas_central']['Colación Nochera']['end'] = '03:30:00';                
       
        $hh['minera_anglo_changres']['Desayuno']['init'] = '06:45:00';
        $hh['minera_anglo_changres']['Desayuno']['end'] = '08:45:00';  
        $hh['minera_anglo_changres']['Almuerzo']['init'] = '11:15:00';
        $hh['minera_anglo_changres']['Almuerzo']['end'] = '15:00:00';  
        $hh['minera_anglo_changres']['Choco B']['init'] = '18:15:00';
        $hh['minera_anglo_changres']['Choco B']['end'] = '22:00:00'; 
        $hh['minera_anglo_changres']['Choco C']['init'] = '01:45:00';
        $hh['minera_anglo_changres']['Choco C']['end'] = '05:45:00';
        
        $hh['minera_anglo_soldado']['Desayuno']['init'] = '06:30:00';
        $hh['minera_anglo_soldado']['Desayuno']['end'] = '08:30:00';  
        $hh['minera_anglo_soldado']['Almuerzo']['init'] = '11:30:00';
        $hh['minera_anglo_soldado']['Almuerzo']['end'] = '15:30:00';  
        $hh['minera_anglo_soldado']['Cena']['init'] = '19:00:00';
        $hh['minera_anglo_soldado']['Cena']['end'] = '03:00:00';          
        /***************************************************************/        
        $hh['clariant_unidad1']['Almuerzo']['init'] = '10:30:00';
        $hh['clariant_unidad1']['Almuerzo']['end'] = '15:00:00';          
        /***************************************************************/     
        $hh['cartones_san_fernando_unidad1']['Almuerzo']['init'] = '10:30:00';
        $hh['cartones_san_fernando_unidad1']['Almuerzo']['end'] = '15:00:00';          
        /***************************************************************/       
        $hh['banco_bice_teatinos']['Almuerzo']['init'] = '12:00:00';
        $hh['banco_bice_teatinos']['Almuerzo']['end'] = '15:30:00';          
        /***************************************************************/                
        $hh['nestle_maipu']['Almuerzo']['init'] = '10:00:00';
        $hh['nestle_maipu']['Almuerzo']['end'] = '15:30:00';  
        $hh['nestle_maipu']['Cena']['init'] = '01:30:00';
        $hh['nestle_maipu']['Cena']['end'] = '02:30:00';  
        $hh['nestle_maipu']['Comida']['init'] = '17:00:00';
        $hh['nestle_maipu']['Comida']['end'] = '20:50:00';
        
        return $hh;
    }
    
    
    public function planification($selectedModule = 'agricola_garces_molina', $dateFrom =  'false', $dateTo = 'false'){

     
        //Configure::write('debug', 2);
        
        $hh = $this->getHH();
                
        $dateFrom = $dateFrom ==  'false' ? date('Y-m-d', strtotime('-7 days')) : $dateFrom;
        $dateTo   = $dateTo == 'false' ? date('Y-m-d') : $dateTo;
        
        $modulesMenu = parent::getModulesMenu($this->Modules);
        
        foreach ($modulesMenu as $module) {
            if(stristr($module['url'], 'maestro') === FALSE) {
                
                $nameExplode = explode(': ', $module['name']);
                
                $module['url'] = str_replace('_menu_diario','', $module['url']); 
                $module['url'] = str_replace('_sugerencia_chef','', $module['url']);
                $module['url'] = str_replace('_bloque_inferior_1','', $module['url']); 
                $module['url'] = str_replace('_bloque_inferior_2','', $module['url']);
                $module['url'] = str_replace('_bloque_inferior','', $module['url']); 
                $module['url'] = str_replace('_bloque_inferior','', $module['url']);

                
                $modules[$module['url']] = $nameExplode[0];
            }
        }

        $modulesFilter = array_unique($modules);
              
        if(isset($hh[$selectedModule])) {
            
            $fields = $this->Content->getSchema($selectedModule . '_menu_diario_reportes');       
            
            if(isset($fields['servicio'])) {  
                $withService = true;
                $services = array_keys($hh[$selectedModule]);
            } else {
                $withService = false;
                $services = array('Almuerzo');
            }           
            
            $date = strtotime($dateFrom);
            $end  = strtotime($dateTo);                
            
            do {

                foreach($services as $service) {
                    
                    $firstUploadMenuItem = $this->Content->getFirstUploadMenuItem($service, $selectedModule . '_menu_diario_reportes', date('d-m-Y' , $date), $withService);
                                     
                    //echo 'servicio:'. $service.' <br>primer item encontrado:<br>';
                   
                    
                    if(!empty($firstUploadMenuItem)) {
                        
                        // print_r($firstUploadMenuItem);
                         
                        $firstPublication = $this->PublishHistory->getFirstAfterHour($selectedModule . '_menu_diario', $firstUploadMenuItem['Content']['created'], date('Y-m-d' , $date).' '.$hh[$selectedModule][$service]['init'], $withService);
                        
                        if(isset($firstPublication[0]['diff'])) {
                            $firstPublication[0]['diffHuman'] = $this->toHours($firstPublication[0]['diff']);
                        } else {
                            $firstPublication[0]['diffHuman'] = '-';
                        }            
                        
                        $planification[date('d/m' , $date)][$service] = $firstPublication;
                    } else {
                        
                       // echo 'nada <br>';
                        $planification[date('d/m' , $date)][$service] = false;
                    }
                    
      
                }
                 
                $date = strtotime('+1 day', $date);  

            } while ($date <= $end);
            
            
        } else {
            // no set casino
        }
              
        $datePart = explode('-', $dateFrom);
        $dateFrom = $datePart[2] . '/' . $datePart[1] . '/'. $datePart[0];
        
        $datePart = explode('-', $dateTo);
        $dateTo = $datePart[2] . '/' . $datePart[1]. '/' . $datePart[0];
        
        $this->set('hh', $hh);
        $this->set('services', $services);
        $this->set('planification', $planification);
        $this->set('selectedModule', $selectedModule);
        $this->set('dateFrom', $dateFrom);
        $this->set('dateTo', $dateTo);           
        $this->set('modules', $modulesFilter); 
        
    }
    
    public function noPublication($selectedModule = 'agricola_garces_molina', $dateFrom =  'false', $dateTo = 'false'){

        //Configure::write('debug', 2);
        
        $hh = $this->getHH();
                
        $dateFrom = $dateFrom ==  'false' ? date('Y-m-d', strtotime('-7 days')) : $dateFrom;
        $dateTo   = $dateTo == 'false' ? date('Y-m-d') : $dateTo;
        
        $modulesMenu = parent::getModulesMenu($this->Modules);
        
        foreach ($modulesMenu as $module) {
            if(stristr($module['url'], 'maestro') === FALSE) {
                
                $nameExplode = explode(': ', $module['name']);
                
                $module['url'] = str_replace('_menu_diario','', $module['url']); 
                $module['url'] = str_replace('_sugerencia_chef','', $module['url']);
                $module['url'] = str_replace('_bloque_inferior_1','', $module['url']); 
                $module['url'] = str_replace('_bloque_inferior_2','', $module['url']);
                $module['url'] = str_replace('_bloque_inferior','', $module['url']); 
                $module['url'] = str_replace('_bloque_inferior','', $module['url']);
                
                $modules[$module['url']] = $nameExplode[0];
            }
        }

        $modulesFilter = array_unique($modules);
              
        if(isset($hh[$selectedModule])) {
            
            $fields = $this->Content->getSchema($selectedModule . '_menu_diario_reportes');

            if(isset($fields['servicio'])) {  
                $withService = true;
                $services = array_keys($hh[$selectedModule]);
            } else {
                $withService = false;
                $services = array('Almuerzo');
            }
            
            $date = strtotime($dateFrom);
            $end  = strtotime($dateTo);                
            
            do {

                foreach($services as $service) {
                    
                    $no = $this->Content->getNoPublication($selectedModule, date('Y-m-d' , $date).' '.$hh[$selectedModule][$service]['init'], $service, $withService);
                    $planification[date('d/m' , $date)][$service] = $no;
                }
                 
                $date = strtotime('+1 day', $date);  

            } while ($date <= $end);
            
            
            
        } else {
            // no set casino
        }
          
        
        $datePart = explode('-', $dateFrom);
        $dateFrom = $datePart[2] . '/' . $datePart[1] . '/'. $datePart[0];
        
        $datePart = explode('-', $dateTo);
        $dateTo = $datePart[2] . '/' . $datePart[1]. '/' . $datePart[0];
     
        $this->set('hh', $hh);
        $this->set('services', $services);
        $this->set('planification', $planification);
        $this->set('selectedModule', $selectedModule);
        $this->set('dateFrom', $dateFrom);
        $this->set('dateTo', $dateTo);           
        $this->set('modules', $modulesFilter); 
        
    }
    
    
    public function changes($selectedModule = 'agricola_garces_molina', $dateFrom =  'false', $dateTo = 'false'){

       // Configure::write('debug', 2);
        
        $hh = $this->getHH();

        $dateFrom = $dateFrom ==  'false' ? date('Y-m-d', strtotime('-7 days')) : $dateFrom;
        $dateTo   = $dateTo == 'false' ? date('Y-m-d') : $dateTo;
        
        $modulesMenu = parent::getModulesMenu($this->Modules);
        
        foreach ($modulesMenu as $module) {
            
            if(stristr($module['url'], 'maestro') === FALSE) {
                
                $nameExplode = explode(': ', $module['name']);
                
                $module['url'] = str_replace('_menu_diario','', $module['url']); 
                $module['url'] = str_replace('_sugerencia_chef','', $module['url']);
                $module['url'] = str_replace('_bloque_inferior_1','', $module['url']); 
                $module['url'] = str_replace('_bloque_inferior_2','', $module['url']);
                $module['url'] = str_replace('_bloque_inferior','', $module['url']); 
                $module['url'] = str_replace('_bloque_inferior','', $module['url']);
                
                $modules[$module['url']] = $nameExplode[0];
            }
        }

        $modulesFilter = array_unique($modules);
              
        if(isset($hh[$selectedModule])) {
            
            $fields = $this->Content->getSchema($selectedModule . '_menu_diario_reportes');

            if(isset($fields['servicio'])) {  
                $withService = true;
                $services = array_keys($hh[$selectedModule]);
            } else {
                $withService = false;
                $services = array('Almuerzo');
            }
            
            $date = strtotime($dateFrom);
            $end  = strtotime($dateTo);                  
            
            do {

                foreach($services as $service) {                    
                    
                    $hb = strtotime('-1 hour', strtotime(date('Y-m-d' , $date).' '.$hh[$selectedModule][$service]['init']));      
                    $hservice_end = strtotime(date('Y-m-d' , $date).' '.$hh[$selectedModule][$service]['end']);      
                      
                    $hourBefore = $this->Content->getItemModifications($selectedModule,date('Y-m-d H:i:00' , $hb), $service, $withService);
                    $justBefore = $this->Content->getItemModifications($selectedModule, date('Y-m-d' , $date).' '.$hh[$selectedModule][$service]['init'], $service, $withService,  date('Y-m-d H:i:00' , $hb));
                    $during = $this->Content->getItemModifications($selectedModule, date('Y-m-d H:i:00' , $hservice_end), $service,$withService ,date('Y-m-d' , $date).' '.$hh[$selectedModule][$service]['init'] );
                  
                    $planification[date('d/m' , $date)][$service]['hour-before']      = count($hourBefore);
                    $planification[date('d/m' , $date)][$service]['just-hour-before'] = count($justBefore);
                    $planification[date('d/m' , $date)][$service]['during'] = count($during);
                                 
                }
                 
                $date = strtotime('+1 day', $date);  

            } while ($date <= $end);
            
            
            
        } else {
            // no set casino
        }     
        
        $datePart = explode('-', $dateFrom);
        $dateFrom = $datePart[2] . '/' . $datePart[1] . '/'. $datePart[0];
        
        $datePart = explode('-', $dateTo);
        $dateTo = $datePart[2] . '/' . $datePart[1]. '/' . $datePart[0];

        
        $this->set('hh', $hh);
        $this->set('services', $services);
        $this->set('planification', $planification);
        $this->set('selectedModule', $selectedModule);
        $this->set('dateFrom', $dateFrom);
        $this->set('dateTo', $dateTo);           
        $this->set('modules', $modulesFilter); 
        
    }
    
    
    public function tablePublicationsReviewAjax($moduleFTP){
        
        $this->layout = 'ajax';
        $client = $this->GCDS->getClientSubdomain();
        $current_time = date('j-m-Y');
        $day_of_week = date('N', strtotime($current_time));
        $jpg = array();
        $jpgBit = array();
        
        if($moduleFTP != ''){
            
            $explodeModuleFPT = explode('|', $moduleFTP);
            $dateExplode = explode('-', $current_time);
            $moduleUrl = explode('_',$explodeModuleFPT[1]);
            
            $publish = $this->PublishHistory->publishTodayForModule($explodeModuleFPT[0]);

            if(isset($publish) && !empty($publish)){
                
                foreach ($publish as &$publishEdit){
                    $publishEdit['PublishHistory']['module_edit'] = str_replace($explodeModuleFPT[0] . '_', '', $publishEdit['PublishHistory']['module']);
                }

                $moduleUrlDigital = ($moduleUrl[0] == 'Escondida')? 'Escond'  : $moduleUrl[0];
                $moduleUrlDigital = ($moduleUrl[0] == 'Agricola')? 'AgricolaGarces' : $moduleUrl[0];
                $branchUrlDigital = ($moduleUrl[1] == 'VistaAlegre')? 'VisAleCer' : $moduleUrl[1];

                $moduleUrlImage = 'templates_images' . DS . $moduleUrlDigital . DS . $branchUrlDigital . DS . $dateExplode[2] . DS . $dateExplode[1] . DS . $dateExplode[0] . DS;
                $directoryDigital = WWW_ROOT . 'files'.DS . $client . DS . $moduleUrlImage;

                $folders = array();

                $this->scanDir($directoryDigital, $jpg);

                foreach ($jpg as &$urls){
                    $explodeUrl = explode('/webroot', $urls);
                    $folderReplace[] = str_replace($directoryDigital, '', $urls);
                    $urls = $explodeUrl[1];
                }

                foreach ($folderReplace as $folder){
                    $folderExplode = explode('/', $folder);
                    $foldersad[] = array_pop($folderExplode);
                    $folders[] = implode('/', $folderExplode);
                }

                $dataConnection =  'ftp://' . $moduleUrl[0] . ':qazplm@190.151.100.14/';
                $urlShowImage =  'ftp://' . $moduleUrl[0] . ':qazplm@190.151.100.14:1221/';
                $ftpConnection = $this->connectFTP($dataConnection);

                $foldersClear = array_unique($folders);
                $listFiles = array();

                foreach ($foldersClear as $path){

                    $listFiles = ftp_nlist($ftpConnection, DS . $moduleUrl[1] . DS . $day_of_week . DS . $path . DS );
                    foreach ($listFiles as $files){

                        if(strpos($files, 'Gnr_') === false){
                            $jpgBit[] = file_get_contents($urlShowImage . '' . $moduleUrl[1] . DS . $day_of_week . DS . $path . DS . $files );  
                        }

                    }
                }
            }
        }
        $this->set('moduleFTP', $moduleFTP);
        $this->set('publishList', $publish);
        $this->set('imagesDigital', $jpg);
        $this->set('imagesFTP', $jpgBit);

    }
   
    
    function scanDir($target, &$jpg) { 
       
        if(is_dir($target)){ 
            $files = glob( $target . '*', GLOB_MARK); //GLOB_MARK adds a slash to directories returned 
           
            foreach( $files as $file ) { 
       
                if(pathinfo($file, PATHINFO_EXTENSION) == 'jpg' ){
                    
                    $jpg[] = $file;
                }
                $this->scanDir( $file, $jpg);    
            } 
            
        }
    }

    function connectFTP($uri, $pasv = false) { 
         
        preg_match("/ftp:\/\/(.*?):(.*?)@(.*?)(\/.*)/i", $uri, $uriParts); 
        
        $ftpConnection = ftp_connect($uriParts[3],1221 ); 
      

        if (ftp_login($ftpConnection, $uriParts[1], $uriParts[2])){  
             
            if($pasv) {
                
                ftp_pasv($ftpConnection, true);
            }
        
            return $ftpConnection; 
        } 

        return null; 
    } 
    

     public function toHours($min, $type = '') { //obtener segundos
        
        if($min <= 0) {
            $time = ' de Anteleción';
        } else {
            $time = ' Posterior';
        }
                    
        $minutos= abs($min);

        
        $segundos   = $minutos * 60;
        $dias       = floor($segundos/86400);
        $mod_hora   = $segundos % 86400;
        $horas      = floor($mod_hora/3600);        
        $mod_minuto = $mod_hora%3600;
        $minutos    = floor($mod_minuto/60);

        if($horas <= 0) {
            return $minutos.' Minutos. <br> <small>'.$time.'</small>';
        } elseif($dias <= 0) {
            return $horas.' Horas y '.$minutos.' Min. <br> <small>'.$time.'</small>';
        } else {
            return $dias.' Días '.$horas.' Horas y '.$minutos.' Min.<br> <small>'.$time.'</small>';
        }
        
    }
   
}