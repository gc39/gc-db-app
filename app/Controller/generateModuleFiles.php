<?php
	include_once 'Array2XML.php';

	class XMLModuleGenerator
	{
		public static function generateMenuDiario($data, $servicios_carpetas){
			$chk_create_ftp_folders = 'off';
			$chk_create_MI_folders = 'off';
			$chk_fulzona1 = 'off';
			$chk_fulzona2 = 'off';
			$chk_fulzona3 = 'off';
			$chk_cumpleano = 'off';
			$chk_menu_contingencia = 'off';

			$cliente = $data['Cliente'];	
			$sucursal = $data['Sucursal'];
			$nombre_ficticio = $data['NombreFicticio'];
			$segmento = $data['Segmento'];
			$cliente_sucursal = $cliente . '_' . $sucursal;
			$ftp = $data['Ftp'];
			$chk_create_ftp_folders = $data['Chk_create_ftp_folders'];
			$chk_create_MI_folders = $data['Chk_create_MI_folders'];
			$chk_fulzona1 = $data['Chk_fulzona1'];
			$fullzona1_name = $data['Fullzona1_name'];
			$chk_fulzona2 = $data['Chk_fulzona2'];
			$fullzona2_name = $data['Fullzona2_name'];
			$chk_fulzona3 = $data['Chk_fulzona3'];
			$fullzona3_name = $data['Fullzona3_name'];
			$chk_cumpleano = $data['Chk_cumpleanos'];
			$chk_pantallas = $data['Chk_pantallas'];
			$chk_menu_contingencia = $data['Chk_menu_contingencia'];
			$tbl_pantallas = $data['tbl-pantallas'];
			$tbl_servicios = $data['tbl-servicios'];
			$tbl_branding = $data['tbl-branding'];
			$tbl_tipos_platos = $data['tbl-tipos-platos'];

			$array_tbl_pantallas = explode(',', $tbl_pantallas);
			$tbl_pantallas = array_chunk($array_tbl_pantallas, 3);

			$array_tbl_servicios = explode(',', $tbl_servicios);
			$tbl_servicios = array_chunk($array_tbl_servicios, 3);

		

			$array_tbl_branding = explode(',', $tbl_branding);
			$tbl_branding = array_chunk($array_tbl_branding, 6);
			$array_tipo_branding = array();
			foreach($tbl_branding as $tbl_branding_item){
				if(!empty($tbl_branding_item[1])){
					$array_tipo_branding[] = $tbl_branding_item[1];
				}
				
			}
			$array_tipo_branding = array_unique($array_tipo_branding);

			$array_tbl_tipos_platos = explode(',', $tbl_tipos_platos);
			$tbl_tipos_platos = array_chunk($array_tbl_tipos_platos, 6);
		

			// pantallas
			$pantallas = array();
			$tmp_pantallas = array();
			foreach($tbl_pantallas as $tbl_pantallas_item){
				if(!empty( $tbl_pantallas_item[0])){
					$macAddress = $tbl_pantallas_item[0];
					$ip = $tbl_pantallas_item[1];
					$marcaModelo = $tbl_pantallas_item[2];

					$attribute_array = array();
					$attribute_array['@attributes'] = array();
					$attribute_array['@attributes']['macAddress'] = $macAddress;
					$attribute_array['@attributes']['ip'] = $ip;
					$attribute_array['@attributes']['marcaModelo'] = $marcaModelo;

					$tmp_pantallas[] = $attribute_array;
				}
			}
			$pantallas['pantalla'] = $tmp_pantallas;

			// Servicios
			$servicios = array();
			$tmp_servicios = array();
			foreach($tbl_servicios as $tbl_servicios_item){
				if(!empty( $tbl_servicios_item[0])){
					$servicio = $tbl_servicios_item[0];
					$paginas = $tbl_servicios_item[1];
					$fondo = $tbl_servicios_item[2];
	
					$attribute_array = array();
					$attribute_array['@attributes'] = array();
					$attribute_array['@attributes']['servicio'] = $servicio;
					$attribute_array['@attributes']['paginas'] = $paginas;
					$attribute_array['@attributes']['fondo'] = $fondo ? $fondo : 'default';
	
					$tmp_servicios[] = $attribute_array;
				}
			}

			$servicios['servicio'] = $tmp_servicios;
			// Preview
		   	$tbls_servicios=  sizeof($servicios['servicio']);// Elimina las posiciones vacias en el array
			if ($tbls_servicios >1 ){			
				$preview_url = array();
				$tmp_preview_url = array();
				foreach($tbl_servicios as $tbl_servicios_item){
					if(!empty($tbl_servicios_item[0])){
						$nombre_carpeta = $tbl_servicios_item[0];
						$servicio = 'Menú del Día ' . $tbl_servicios_item[0];
						$paginas = $tbl_servicios_item[1];
	
						//../files/sodexo/templates/html/test/index.php?cliente=Casino&amp;sucursal=Corporativo&amp;paginas=5&amp;servicio=Des#
						
						$attribute_array = array();
					
						$attribute_array['@value'] = "../files/".CLIENT."/templates/html/test/index.php?cliente=$cliente&sucursal=$sucursal&paginas=$paginas&servicio=$servicios_carpetas[$nombre_carpeta]#";
						$attribute_array['@attributes'] = array();
						$attribute_array['@attributes']['label'] = $servicio;
	
						$tmp_preview_url[] = $attribute_array;
					}		
				}

				$preview_url['url'] = $tmp_preview_url;
			}else{	
				$tbl_servicios_item  = $tbl_servicios[0];
				$nombre_carpeta = $tbl_servicios_item[0];
				$preview_url = "../files/".CLIENT."/templates/html/test/index.php?cliente=$cliente&sucursal=$sucursal&paginas=$paginas&servicio=$servicios_carpetas[$nombre_carpeta]#";
				
			}

			$attribute_publish_url_array = array();
			$attribute_publish_url_array['@attributes'] = array();
			$attribute_publish_url_array['@attributes']['related-module'] = 'maestro_productos';

			// Tipos Platos
			$tipo_plato = array();
			$tmp_tipo_plato = array();
			foreach($tbl_tipos_platos as $tbl_tipos_platos_item){
				if(!empty($tbl_tipos_platos_item[0])){
				//	var_dump($tbl_tipos_platos_item[1]);
					//die();
					$servicio = $tbl_tipos_platos_item[0];
					$tipo_plato_item = $tbl_tipos_platos_item[1];
					$cantidad = $tbl_tipos_platos_item[2];
					$pagina = $tbl_tipos_platos_item[3];
					$indice = $tbl_tipos_platos_item[4];
					$ingles = $tbl_tipos_platos_item[5];
					if($ingles == 'SI'){
						$ingles=true;
					}else{
						$ingles=false;
					}

					$attribute_array = array();
					$attribute_array['@attributes'] = array();
					$attribute_array['@attributes']['servicio'] = $servicio;
					$attribute_array['@attributes']['tipoplato'] = $tipo_plato_item;
					$attribute_array['@attributes']['cantidad'] = $cantidad;
					$attribute_array['@attributes']['pagina'] = $pagina;
					$attribute_array['@attributes']['indice'] = $indice;
					$attribute_array['@attributes']['ingles'] = $ingles;

					$tmp_tipo_plato[] = $attribute_array;
			
				}
			}
			

			foreach($tbl_branding as $tbl_branding_item){
				if(!empty($tbl_tipos_platos_item[0])){
					$servicio = $tbl_branding_item[0];
					$tipo_plato_item = $tbl_branding_item[1];
					$cantidad = $tbl_branding_item[2];
					$pagina = $tbl_branding_item[3];
					$indice = $tbl_branding_item[4];
					$ingles = $tbl_branding_item[5];
					if($ingles == 'SI'){
						$ingles=true;
					}else{
						$ingles=false;
					}
	
					$attribute_array = array();
					$attribute_array['@attributes'] = array();
					$attribute_array['@attributes']['servicio'] = $servicio;
					$attribute_array['@attributes']['tipoplato'] = $tipo_plato_item;
					$attribute_array['@attributes']['cantidad'] = $cantidad;
					$attribute_array['@attributes']['pagina'] = $pagina;
					$attribute_array['@attributes']['indice'] = $indice;
					$attribute_array['@attributes']['ingles'] = $ingles;
	
					$tmp_tipo_plato[] = $attribute_array;
				}

			}

			// Ordena segun el orden de la definicion de los servicios
			$tmp_tipo_plato_sorted = array();
			foreach($tmp_servicios as $tmp_servicios_item){
				$item_servicio = $tmp_servicios_item['@attributes']['servicio'];

				foreach($tmp_tipo_plato as $tmp_tipo_plato_item){
					if($tmp_tipo_plato_item['@attributes']['servicio'] == $item_servicio){
						$tmp_tipo_plato_sorted[] = $tmp_tipo_plato_item;
					}
				}
			}
			$ing=false;
			foreach($tmp_tipo_plato_sorted as $tipos){
				foreach($tipos as $tipo){
					if(!empty($tipo)){
						$ing = $tipo['ingles'];
						//
					}	
				}
			}

			$tmp_tipo_plato = $tmp_tipo_plato_sorted;
			$tipo_plato['tipo_plato'] = $tmp_tipo_plato;

			//var_dump($ing);
			//die();
			// datos fijos
			$import_map_array = array(
				'map' =>
					array (
						0 =>
							array (
								'@value' => '',
								'@attributes' =>
									array (
										'column' => 'Servicio',
										'field' => 'servicio',
										'optional' => 'no',
									),
							),
						1 =>
							array (
								'@value' => '',
								'@attributes' =>
									array (
										'column' => 'Tipo',
										'field' => 'tipo',
										'optional' => 'no',
									),
							),
						2 =>
							array (
								'@value' => '',
								'@attributes' =>
									array (
										'column' => 'Codigo',
										'field' => 'codigo',
										'optional' => 'no',
									),
							),
						3 =>
							array (
								'@value' => '',
								'@attributes' =>
									array (
										'column' => 'Calorias',
										'field' => 'calorias_temp',
										'optional' => 'si',
									),
							),
						4 =>
							array (
								'@value' => '',
								'@attributes' =>
									array (
										'column' => 'Fecha',
										'field' => 'fecha',
										'optional' => 'no',
										'replace' => '/',
										'replace-with' => '-',
									),
							),
					)
			);

			$fields_array = array (
				'field' =>
					array (
						0 =>
							array (
								'@value' => '',
								'@attributes' =>
									array (
										'key' => 'servicio',
										'label' => 'Servicio',
										'column-width' => '220',
										'db-type' => 'varchar(45)',
										'type' => 'text',
										'visible-in-list' => 'true',
										'max-length' => '45',
										'logical-show-behavior' => 'true',
										'validations' => 'required',
										'readonly' => 'true',
									),
							),
						1 =>
							array (
								'@value' => '',
								'@attributes' =>
									array (
										'key' => 'tipo',
										'label' => 'Tipo',
										'column-width' => '220',
										'db-type' => 'varchar(45)',
										'type' => 'text',
										'visible-in-list' => 'true',
										'max-length' => '45',
										'editable' => 'true',
										'validations' => 'required',
										'readonly' => 'true',
									),
							),
						2 =>
							array (
								'@value' => '',
								'@attributes' =>
									array (
										'key' => 'codigo',
										'label' => 'Codigo / Plato',
										'column-width' => '240',
										'db-type' => 'varchar(45)',
										'type' => 'text-related',
										'visible-in-list' => 'true',
										'max-length' => '45',
										'editable' => 'true',
										'validations' => 'required',
										'related-to-id' => 'maestro_productos.codigo',
										'related-to-show' => 'maestro_productos.nombre_espanol',
									),
							),
						3 =>
							array (
								'@value' => '',
								'@attributes' =>
									array (
										'key' => 'calorias_temp',
										'label' => 'Calorías',
										'column-width' => '330',
										'db-type' => 'varchar(4)',
										'type' => 'text',
										'visible-in-list' => 'true',
										'max-length' => '4',
										'editable' => 'true',
										'placeholder' => 'Solo agregar si la porción es distinta al maestro de productos',
									),
							),
						4 =>
							array (
								'@value' => '',
								'@attributes' =>
									array (
										'key' => 'fecha',
										'label' => 'Fecha',
										'column-width' => '130',
										'db-type' => 'varchar(10)',
										'type' => 'date',
										'visible-in-list' => 'true',
										'no-transform-to-mysql' => 'true',
										'editable' => 'false',
										'validations' => 'required',
									),
							),
						5 =>
							array (
								'@value' => '',
								'@attributes' =>
									array (
										'key' => 'temporal',
										'label' => 'Plato Temporal',
										'column-width' => '330',
										'db-type' => 'varchar(100)',
										'type' => 'tmp_code',
										'visible-in-list' => 'true',
										'max-length' => '100',
										'editable' => 'true',
										'related-to-field' => 'codigo',
										'placeholder' => '',
									),
							),
						6 =>
							array (
								'@value' => '',
								'@attributes' =>
									array (
										'key' => 'codigo_temp',
										'label' => 'Codigo Temporal',
										'column-width' => '330',
										'db-type' => 'varchar(50)',
										'type' => 'text',
										'visible-in-list' => 'false',
										'max-length' => '50',
										'editable' => 'true',
										'edit-visibity' => 'no-show',
									),
							),
					),
			);
			
			// final
			$module_menu_diario = array(
				"cliente" => $cliente,
				"sucursal" => $sucursal,
				"segmento" => $segmento,
				"ingles" => $ing,
				"justificar_borrado" => true,

				"button_add" => false,
				"download_csv" => true,

				"pantallas" => $pantallas,
				"servicios" => $servicios,

				"preview_url" => $preview_url,
				"publish_url" => $attribute_publish_url_array,

				"tipos_platos" => $tipo_plato,

				"preview_width" => 1080,
				"preview_height" => 1920,

				//"content_name" => "$cliente $sucursal: Menu Diario",
				"content_name" => "$nombre_ficticio: Menu Diario",
				"icon" => 'icon-food2',
				"ftp_connection" => $cliente.'_'.$sucursal,
				"allow_import" => true,

				"import_map" => $import_map_array,
				"fields" => $fields_array
			);

			$xml = Array2XML::createXML('gcdb', $module_menu_diario);
			file_put_contents(RUTAMODULOS.'/'.$cliente.'_'.$sucursal.'_menu_diario.xml', $xml->saveXML());

			// crear el archivo gd
			$config_dir = dirname(__FILE__).'/../webroot/files/'.CLIENT.'/templates_gd/';
			$source_file = $config_dir.'Template_menu_diario.php';
			$dest_file = $config_dir.$cliente.'_'.$sucursal.'_menu_diario.php';
			copy($source_file, $dest_file);

			// agrega configuracion de branding
			$fp = fopen($dest_file, 'a'); 

			$server_db = LOCALSERVER;
			$user_db = LOCALUSERDB;
			$password_db = LOCALPASSDB;
			$db_db = LOCALDB;
			$obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
			// se carga de la base de datos de branding
			$sql = 'SELECT * FROM config_branding ORDER BY TipoBranding';
			mysqli_set_charset($obj_conexion, 'utf8');
			if (!$resultado = $obj_conexion->query($sql)) die();
			$branding_nombres = array();
			while ($rows = $resultado->fetch_assoc()) {
				$tipo = $rows['TipoBranding'];
				$descripcion = $rows['Descripcion'];

				$branding_nombres[$descripcion] = $tipo;
			}

			$last_include_branding = '';
			$counter_branding = 0;
			foreach($array_tipo_branding as $array_tipo_branding_item){
				if($last_include_branding != $branding_nombres[$array_tipo_branding_item]){
					fwrite($fp, "\n");
					fwrite($fp, "\n");
					if ($counter_branding>0)
						fwrite($fp, '$prevent_clean_branding = true;');
					else
						fwrite($fp, '$prevent_clean_branding = false;');

					fwrite($fp, "\n");
					fwrite($fp, '$current_branding = "');
					fwrite($fp, "$branding_nombres[$array_tipo_branding_item]");
					fwrite($fp, '";');
					fwrite($fp, "\n");
					fwrite($fp, "include __DIR__.'/_include_branding.php';");
					fwrite($fp, "\n");

					$last_include_branding = $branding_nombres[$array_tipo_branding_item];
				}
				$counter_branding++;
			}
			fwrite($fp, "\n");
			fwrite($fp, "\n");
			fwrite($fp, 'mysqli_close($obj_conexion);');
			
			// fin de agregar configuracion de branding
			
			fclose($fp);  

		}
	
		public static function generateFullzona($data, $num_fullzona, $servicios_carpetas){



			$cliente = $data['Cliente'];
			$sucursal = $data['Sucursal'];
			$segmento = $data['Segmento'];
			$ftp = $data['Ftp'];
			$chk_fulzona1 = $data['Chk_fulzona1'];
			$fullzona1_name = $data['Fullzona1_name'];
			$chk_fulzona2 = $data['Chk_fulzona2'];
			$fullzona2_name = $data['Fullzona2_name'];
			$chk_fulzona3 = $data['Chk_fulzona3'];
			$fullzona3_name = $data['Fullzona3_name'];
			$chk_cumpleano = $data['Chk_cumpleanos'];
			$chk_menu_contingencia = $data['Chk_menu_contingencia'];
			$chk_sugerencia_chef = $data['Chk_sugerencia_chef'];
			$nombre_ficticio = $data['NombreFicticio'];

			switch($segmento){
                case 'Corporativo': 
					$preview_filename = $num_fullzona."40_FullZonaCorp_$num_fullzona.jpg";
                    $modulo_publicacion_multiple = 'FullZonaCorp_'.$num_fullzona;
                    break;
                case 'Mineria': 
					$preview_filename = $num_fullzona."40_FullZonaMineria_$num_fullzona.jpg";
                    $modulo_publicacion_multiple = 'FullZonaMineria_'.$num_fullzona;
                    break;
                case 'Salud': 
					$preview_filename = $num_fullzona."40_FullZonaSalud_$num_fullzona.jpg";
                    $modulo_publicacion_multiple = 'FullZonaSalud_'.$num_fullzona;
                    break;
            }


			$tbl_servicios = $data['tbl-servicios'];
			$array_tbl_servicios = explode(',', $tbl_servicios);
			$tbl_servicios = array_chunk($array_tbl_servicios, 3);

			// Servicios
			$servicios = array();
			$tmp_servicios = array();
			foreach($tbl_servicios as $tbl_servicios_item){
				$servicio = $tbl_servicios_item[0];
				$paginas = $tbl_servicios_item[1];

				$attribute_array = array();
				$attribute_array['@attributes'] = array();
				$attribute_array['@attributes']['servicio'] = $servicio;
				$attribute_array['@attributes']['paginas'] = $paginas;

				$tmp_servicios[] = $attribute_array;
			}
			$servicios['servicio'] = $tmp_servicios;

			//datos fijos
			$fields_array = array (
				'field' =>array (
					0 => 
					array (
						'@value' => '',
						'@attributes' => 
						array (
							'key' => 'imagen',
							'label' => 'Imagen',
							'type' => 'image',
							'db-type' => 'int(11)',
							'column-width' => '80',
							'visible-in-list' => 'true',
							'expected-width' => '1080',
							'expected-height' => '1920',
							'editable' => 'true',
							'validations' => 'required',
						),
					),
					1 => 
					array (
						'@value' => '',
						'@attributes' => 
						array (
							'key' => 'fecha_inicio',
							'label' => 'Fecha Inicio',
							'type' => 'date',
							'db-type' => 'date',
							'column-width' => '130',
							'visible-in-list' => 'true',
							'editable' => 'true',
							'validations' => 'required',
						),
					),
					2 => 
					array (
						'@value' => '',
						'@attributes' => 
						array (
							'key' => 'fecha_termino',
							'label' => 'Fecha Termino',
							'type' => 'date',
							'db-type' => 'date',
							'column-width' => '130',
							'visible-in-list' => 'true',
							'editable' => 'true',
							'validations' => 'required',
						),
					),
				),
			);

			switch($num_fullzona){
                case 1: 
                    $publicacion_fullzona_name = $fullzona1_name;
                    break;
                case 2: 
                    $publicacion_fullzona_name = $fullzona2_name;
                    break;
                case 3: 
                    $publicacion_fullzona_name = $fullzona3_name;
                    break;
            }


			// final
			$module = array(
				"cliente" => $cliente,
				"sucursal" => $sucursal,
				"segmento" => $segmento,
				"nombre_FullZona" => $modulo_publicacion_multiple,
				"indice_FullZona" => $num_fullzona.'40',
				"allow_import" => false,
				"preview_url" => "../files/".CLIENT."/templates/html/test/index.html?cliente=$cliente&sucursal=$sucursal&fullzona=$preview_filename#",
				"publish_url" => '',
				"servicios" => $servicios,
				"preview_width" => 1080,
				"preview_height" => 1920,
				//"content_name" => "$cliente $sucursal: $publicacion_fullzona_name",
				"content_name" => "$nombre_ficticio: $publicacion_fullzona_name",
				"ftp_connection" => $cliente.'_'.$sucursal,
				"gallery_tag" => $cliente.'_'.$sucursal.'_'.$segmento.'_'.$num_fullzona,
				"icon" => 'icon-food2',
				"fields" => $fields_array
			);

			$xml = Array2XML::createXML('gcdb', $module);
			file_put_contents(RUTAMODULOS.'/' . $cliente.'_'.$sucursal.'_'.$modulo_publicacion_multiple.'.xml', $xml->saveXML());

			// crear el archivo gd
			$config_dir = dirname(__FILE__).'/../webroot/files/'.CLIENT.'/templates_gd/';
			$source_file = $config_dir.'Template_FullZona.php';
			$dest_file = $config_dir.$cliente.'_'.$sucursal.'_'.$modulo_publicacion_multiple.'.php';
			copy($source_file, $dest_file);
		}
	
		public static function generateCumpleano($data, $servicios_carpetas){
			$cliente = $data['Cliente'];
			$sucursal = $data['Sucursal'];
			$segmento = $data['Segmento'];
			$ftp = $data['Ftp'];
			$chk_fulzona1 = $data['Chk_fulzona1'];
			$fullzona1_name = $data['Fullzona1_name'];
			$chk_fulzona2 = $data['Chk_fulzona2'];
			$fullzona2_name = $data['Fullzona2_name'];
			$chk_fulzona3 = $data['Chk_fulzona3'];
			$fullzona3_name = $data['Fullzona3_name'];
			$chk_cumpleanos = $data['Chk_cumpleanos'];
			$nombre_ficticio = $data['NombreFicticio'];

			$tbl_servicios = $data['tbl-servicios'];
			$array_tbl_servicios = explode(',', $tbl_servicios);
			$tbl_servicios = array_chunk($array_tbl_servicios, 3);


			// Servicios
			$servicios = array();
			$tmp_servicios = array();
			foreach($tbl_servicios as $tbl_servicios_item){
				$servicio = $tbl_servicios_item[0];
				$paginas = $tbl_servicios_item[1];

				$attribute_array = array();
				$attribute_array['@attributes'] = array();
				$attribute_array['@attributes']['servicio'] = $servicio;
				$attribute_array['@attributes']['paginas'] = $paginas;

				$tmp_servicios[] = $attribute_array;
			}
			$servicios['servicio'] = $tmp_servicios;

			//datos fijos

			$fields_array = array (
				'field' =>array (
					0 => 
					array (
						'@value' => '',
						'@attributes' => 
						array (
							'key' => 'Mes',
							'label' => 'Mes Cumpleaños',
							'type' => 'text',
							'db-type' => 'varchar(54)',
							'column-width' => '300',
							'visible-in-list' => 'true',
							'editable' => 'true',
							'validations' => 'required',
							'max-length' => '54'
						),
					),
					1 => 
					array (
						'@value' => '',
						'@attributes' => 
						array (
							'key' => 'fecha_inicio',
							'label' => 'Fecha Inicio',
							'type' => 'date',
							'db-type' => 'date',
							'column-width' => '130',
							'visible-in-list' => 'true',
							'editable' => 'true',
							'validations' => 'required',
						),
					),
					2 => 
					array (
						'@value' => '',
						'@attributes' => 
						array (
							'key' => 'fecha_termino',
							'label' => 'Fecha Termino',
							'type' => 'date',
							'db-type' => 'date',
							'column-width' => '130',
							'visible-in-list' => 'true',
							'editable' => 'true',
							'validations' => 'required',
						),
					),
				),
			);

			switch($segmento){
                case 'Corporativo': 
					$nombre_FullZona = 'FullZonaCorp_Cumple';
                    break;
                case 'Mineria': 
					$nombre_FullZona = 'FullZonaMineria_Cumple';
                    break;
                case 'Salud': 
					$nombre_FullZona = 'FullZonaSalud_Cumple';
                    break;
            }

			// final
			$module = array(
				"cliente" => $cliente,
				"sucursal" => $sucursal,
				"segmento" => $segmento,
				"nombre_FullZona" => $nombre_FullZona,
				"indice_FullZona" => '230',
				"allow_import" => false,
				"servicios" => $servicios,
				"preview_url" => "../files/".CLIENT."/templates/html/test/index.html?cliente=$cliente&sucursal=$sucursal&fullzona=230_$nombre_FullZona.jpg#",
				"publish_url" => '',
				"preview_width" => 1080,
				"preview_height" => 1920,
				//"content_name" => "$cliente $sucursal: Cumpleaños",
				"content_name" => "$nombre_ficticio: Cumpleaños",
				
				"ftp_connection" => $cliente.'_'.$sucursal,
				"gallery_tag" => $cliente.'_'.$sucursal.'_Cumpleaños',
				"icon" => 'icon-food2',
				"fields" => $fields_array
			);

			$xml = Array2XML::createXML('gcdb', $module);
			file_put_contents(RUTAMODULOS.'/' . $cliente.'_'.$sucursal."_$nombre_FullZona.xml", $xml->saveXML());

			// crear el archivo gd
			$config_dir = dirname(__FILE__).'/../webroot/files/'.CLIENT.'/templates_gd/';
			$source_file = $config_dir.'Template_FullZona_Cumple.php';
			$dest_file = $config_dir.$cliente.'_'.$sucursal."_$nombre_FullZona.php";
			copy($source_file, $dest_file);
		}

		public static function generateContigencia($data, $servicios_carpetas){
			$cliente = $data['Cliente'];
			$sucursal = $data['Sucursal'];
			$segmento = $data['Segmento'];
			$ftp = $data['Ftp'];
			$chk_fulzona1 = $data['Chk_fulzona1'];
			$fullzona1_name = $data['Fullzona1_name'];
			$chk_fulzona2 = $data['Chk_fulzona2'];
			$fullzona2_name = $data['Fullzona2_name'];
			$chk_fulzona3 = $data['Chk_fulzona3'];
			$fullzona3_name = $data['Fullzona3_name'];
			$chk_cumpleanos = $data['Chk_cumpleanos'];
			$chk_menu_contingencia = $data['Chk_menu_contingencia'];
			$chk_sugerencia_chef = $data['Chk_sugerencia_chef'];
			$tbl_servicios = $data['tbl-servicios'];
			$tbl_tipos_platos = $data['tbl-tipos-platos'];
			$nombre_ficticio = $data['NombreFicticio'];

			$array_tbl_servicios = explode(',', $tbl_servicios);
			$tbl_servicios = array_chunk($array_tbl_servicios, 3);

			$array_tbl_tipos_platos = explode(',', $tbl_tipos_platos);
			$tbl_tipos_platos = array_chunk($array_tbl_tipos_platos, 6);


			foreach($tbl_servicios as $tbl_servicios_item){
				if(!empty($tbl_servicios_item[0])){
					$nombre_carpeta = $tbl_servicios_item[0];
					$servCarpetas[] =$servicios_carpetas[$nombre_carpeta];

					//echo $servicios_carpetas[$nombre_carpeta] . "<br>";
					
				}
			}
			$servCarpetas = array_unique($servCarpetas);



			// Servicios
			$servicios = array();
			$tmp_servicios = array();
			foreach($servCarpetas as $servCarpeta){
				if(!empty( $servCarpeta)){
	
					$attribute_array = array();
					$attribute_array['@attributes'] = array();
					$attribute_array['@attributes']['servicio'] = $servCarpeta;
	
					$tmp_servicios[] = $attribute_array;
				}
			}

			$servicios['servicio'] = $tmp_servicios;


			// Servicios para el campo fields
			$fields_attribute_array = array();

			$fields_attribute_array[] = array (
				'@value' => '',
				'@attributes' => 
				array (
					'value' => ''
				));
			foreach($servCarpetas as $servCarpeta){
					$fields_attribute_array[] = array (
						'@value' => $servCarpeta,
						'@attributes' => 
						array (
							'value' => $servCarpeta
						));
				
	
			}

			// Preview
			$tbls_servicios=  sizeof($servCarpetas);// Elimina las posiciones vacias en el array


			if ($tbls_servicios >1){	
				$preview_url = array();
				$tmp_preview_url = array();
				foreach($servCarpetas as $servCarpeta){
					if(!empty($servCarpeta)){
						$nombre_carpeta = $servCarpeta;
						$nombre_archivo = "100_" . $servCarpeta . "_minuta_contingencia.jpg";
						$servicio = 'Menu Contingencia ' . $servCarpeta;
						$paginas = 1;
		
						$attribute_array = array();
						$attribute_array['@value'] = "../files/".CLIENT."/templates/html/test/index.html?cliente=$cliente&sucursal=$sucursal&fullzona=$nombre_archivo#";
						$attribute_array['@attributes'] = array();
						$attribute_array['@attributes']['label'] = $servicio;
		
						$tmp_preview_url[] = $attribute_array;
					}
				}

				$preview_url['url'] = $tmp_preview_url;
			}else{
				$tbl_servicios_item = $servCarpetas[0];
				$nombre_carpeta = $servCarpetas[0];
				$nombre_archivo = "100_" . $servCarpetas[0] . "_minuta_contingencia.jpg";
				$preview_url = "../files/".CLIENT."/templates/html/test/index.html?cliente=$cliente&sucursal=$sucursal&fullzona=$nombre_archivo#";
			}



			//datos fijos

			$fields_array = array (
				'field' =>array (
					0 => 
					array (
						'option' => $fields_attribute_array,
						'@attributes' => 
						array (
							'key' => 'servicio',
							'logical-show-behavior' => 'true',
							'column-width' => '220',
							'db-type' => 'varchar(45)',
							'label' => 'Servicio',
							'type' => 'select',
							'visible-in-list' => 'true',
							'max-length' => '45',
							'validations' => 'required'
						),
					),
					1 => 
					array (
						'@value' => '',
						'@attributes' => 
						array (
							'key' => 'platos',
							'column-width' => '300',
							'db-type' => 'text',
							'label' => 'Platos',
							'type' => 'textarea',
							'visible-in-list' => 'true',
							'editable' => 'true',
							'max-length' => '864',
							'validations' => 'required'
						),
					),
					2 => 
					array (
						'@value' => '',
						'@attributes' => 
						array (
							'key' => 'fecha',
							'column-width' => '130',
							'db-type' => 'varchar(10)',
							'label' => 'Fecha',
							'type' => 'date',
							'no-transform-to-mysql' => 'true',
							'visible-in-list' => 'true',
							'editable' => 'true',
							'validations' => 'required'
						),
					),
				),
			);


			// final
			$module = array(
				"cliente" => $cliente,
				"sucursal" => $sucursal,
				"segmento" => $segmento,
				"servicios" => $servicios,
				"preview_url" => $preview_url,
				"publish_url" => '',
				"preview_width" => 1080,
				"preview_height" => 1920,
				//"content_name" => "$cliente $sucursal: Minuta de Contingencia",
				"content_name" => "$nombre_ficticio: Minuta de Contingencia",
				"ftp_connection" => $cliente.'_'.$sucursal,
				"icon" => 'icon-food2',
				"allow_import" => "false",
				"fields" => $fields_array
			);

			$xml = Array2XML::createXML('gcdb', $module);
			file_put_contents(RUTAMODULOS.'/' . $cliente.'_'.$sucursal.'_minuta_contingencia.xml', $xml->saveXML());

			// crear el archivo gd
			$config_dir = dirname(__FILE__).'/../webroot/files/'.CLIENT.'/templates_gd/';
			$source_file = $config_dir.'Template_minuta_contingencia.php';
			$dest_file = $config_dir.$cliente.'_'.$sucursal.'_minuta_contingencia.php';
			copy($source_file, $dest_file);
		}

		public static function generateSugerenciaChef($data, $servicios_carpetas){
			$cliente = $data['Cliente'];
			$sucursal = $data['Sucursal'];
			$segmento = $data['Segmento'];
			$ftp = $data['Ftp'];
			$chk_fulzona1 = $data['Chk_fulzona1'];
			$fullzona1_name = $data['Fullzona1_name'];
			$chk_fulzona2 = $data['Chk_fulzona2'];
			$fullzona2_name = $data['Fullzona2_name'];
			$chk_fulzona3 = $data['Chk_fulzona3'];
			$fullzona3_name = $data['Fullzona3_name'];
			$chk_cumpleanos = $data['Chk_cumpleanos'];
			$chk_menu_contingencia = $data['Chk_menu_contingencia'];
			$chk_sugerencia_chef = $data['Chk_sugerencia_chef'];
			$tbl_servicios = $data['tbl-servicios'];
			$tbl_tipos_platos = $data['tbl-tipos-platos'];
			$nombre_ficticio = $data['NombreFicticio'];

			$array_tbl_servicios = explode(',', $tbl_servicios);
			$tbl_servicios = array_chunk($array_tbl_servicios, 3);

			$array_tbl_tipos_platos = explode(',', $tbl_tipos_platos);
			$tbl_tipos_platos = array_chunk($array_tbl_tipos_platos, 6);

			foreach($tbl_servicios as $tbl_servicios_item){
				if(!empty($tbl_servicios_item[0])){
					$nombre_carpeta = $tbl_servicios_item[0];
					$servCarpetas[] =$servicios_carpetas[$nombre_carpeta];

					//echo $servicios_carpetas[$nombre_carpeta] . "<br>";
					
				}
			}
			$servCarpetas = array_unique($servCarpetas);



			// Servicios
			$servicios = array();
			$tmp_servicios = array();
			foreach($servCarpetas as $servCarpeta){
				if(!empty( $servCarpeta)){
	
					$attribute_array = array();
					$attribute_array['@attributes'] = array();
					$attribute_array['@attributes']['servicio'] = $servCarpeta;
	
					$tmp_servicios[] = $attribute_array;
				}
			}

			$servicios['servicio'] = $tmp_servicios;

			// Servicios para el campo fields
			$fields_attribute_array = array();

			$fields_attribute_array[] = array (
				'@value' => '',
				'@attributes' => 
				array (
					'value' => ''
				));
			foreach($servCarpetas as $servCarpeta){
					$fields_attribute_array[] = array (
						'@value' => $servCarpeta,
						'@attributes' => 
						array (
							'value' => $servCarpeta
						));
				
	
			}

			// Preview
			$tbls_servicios=  sizeof($servCarpetas);// Elimina las posiciones vacias en el array
			if ($tbls_servicios>1){	
				$preview_url = array();
				$tmp_preview_url = array();
				foreach($servCarpetas as $servCarpeta){
					if(!empty($servCarpeta)){
						$nombre_carpeta = $servCarpeta;
						$nombre_archivo = "110_" . $servCarpeta . "_sugerencia_chef.jpg";
						$servicio = 'Sugerencia del Chef ' . $servCarpeta;
						$paginas = 1;
		
						$attribute_array = array();
						$attribute_array['@value'] = "../files/".CLIENT."/templates/html/test/index.html?cliente=$cliente&sucursal=$sucursal&fullzona=$nombre_archivo#";
						$attribute_array['@attributes'] = array();
						$attribute_array['@attributes']['label'] = $servicio;
		
						$tmp_preview_url[] = $attribute_array;
					}
				}

				$preview_url['url'] = $tmp_preview_url;
			}else{
				$tbl_servicios_item = $servCarpetas[0];
				$nombre_carpeta = $servCarpetas[0];
				$nombre_archivo = "110_" . $servCarpetas[0] . "_sugerencia_chef.jpg";
				$preview_url = "../files/".CLIENT."/templates/html/test/index.html?cliente=$cliente&sucursal=$sucursal&fullzona=$nombre_archivo#";
			}

			//datos fijos

			$fields_array = array (
				'field' =>array (
					0 => 
					array (
						'option' => $fields_attribute_array,
						'@attributes' => 
						array (
							'key' => 'servicio',
							'logical-show-behavior' => 'true',
							'column-width' => '220',
							'db-type' => 'varchar(45)',
							'label' => 'Servicio',
							'type' => 'select',
							'visible-in-list' => 'true',
							'max-length' => '45',
							'validations' => 'required'
						),
					),
					1 => 
					array (
						'@value' => '',
						'@attributes' => 
						array (
							'key' => 'plato',
							'column-width' => '330',
							'db-type' => 'varchar(44)',
							'label' => 'Plato',
							'type' => 'text',
							'visible-in-list' => 'true',
							'editable' => 'true',
							'max-length' => '44',
							'validations' => 'required'
						),
					),
					2 => 
					array (
						'@value' => '',
						'@attributes' => 
						array (
							'key' => 'calorias',
							'column-width' => '330',
							'db-type' => 'varchar(44)',
							'label' => 'Calorias',
							'type' => 'text',
							'visible-in-list' => 'true',
							'editable' => 'true',
							'max-length' => '4',
							'validations' => 'required'
						),
					),
					3 => 
					array (
						'@value' => '',
						'@attributes' => 
						array (
							'key' => 'fecha',
							'column-width' => '130',
							'db-type' => 'varchar(10)',
							'label' => 'Fecha',
							'type' => 'date',
							'no-transform-to-mysql' => 'true',
							'visible-in-list' => 'true',
							'editable' => 'true',
							'validations' => 'required'
						),
					),
				),
			);


			// final
			$module = array(
				"cliente" => $cliente,
				"sucursal" => $sucursal,
				"segmento" => $segmento,
				"servicios" => $servicios,
				"preview_url" => $preview_url,
				"publish_url" => '',
				"preview_width" => 1080,
				"preview_height" => 1920,
				//"content_name" => "$cliente $sucursal: Sugerencia del Chef",
				"content_name" => "$nombre_ficticio: Sugerencia del Chef",
				"ftp_connection" => $cliente.'_'.$sucursal,
				"icon" => 'icon-food2',
				"allow_import" => "false",
				"fields" => $fields_array
			);

			$xml = Array2XML::createXML('gcdb', $module);
			file_put_contents(RUTAMODULOS.'/' . $cliente.'_'.$sucursal.'_sugerencia_chef.xml', $xml->saveXML());

			// crear el archivo gd
			$config_dir = dirname(__FILE__).'/../webroot/files/'.CLIENT.'/templates_gd/';
			$source_file = $config_dir.'Template_sugerencia_chef.php';
			$dest_file = $config_dir.$cliente.'_'.$sucursal.'_sugerencia_chef.php';
			copy($source_file, $dest_file);
		}
	
		public static function createCarpetaServicioDia($cliente, $sucursal, $servicio, $dia, $parent_id){
			$texto_dia = '';
		
			switch($dia){
				case 1:
					$texto_dia = 'Lunes';
					break;
				case 2:
					$texto_dia = 'Martes';
					break;
				case 3:
					$texto_dia = 'Miercoles';
					break;
				case 4:
					$texto_dia = 'Jueves';
					break;
				case 5:
					$texto_dia = 'Viernes';
					break;
				case 6:
					$texto_dia = 'Sabado';
					break;
				case 7:
					$texto_dia = 'Domingo';
					break;
			}
		
			$servicioUpper = strtoupper($servicio);
		
			$mi_sodexo_server = MISODEXOSERVER;
			$mi_sodexo_port = MISODEXOSERVERPORT;
		
			$contentName = "{$texto_dia}_{$servicioUpper}_{$cliente}_{$sucursal}";
			$ftp_server = MISODEXOFTP;
			$ftp_port = MISODEXOPORT;
			$ftp_username = MISODEXOFTPUSERNAME;
			$ftp_password = MISODEXOFTPPASS;
			$ftp_directory = "/SODEXO/{$cliente}/{$sucursal}/{$dia}/{$servicio}";
			$groupId = $parent_id;
			$refreshInterval = 13;
			$loginRetry='N';
		
			$curl = curl_init();
		
			curl_setopt_array($curl, array(
				CURLOPT_URL => "http://$mi_sodexo_server:$mi_sodexo_port/contents/ftp",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'PUT',
				CURLOPT_POSTFIELDS =>"{
					\"contentName\": \"$contentName\",
					\"server\": \"$ftp_server\",
					\"port\": $ftp_port,
					\"username\": \"$ftp_username\",
					\"password\": \"$ftp_password\",
					\"directory\": \"$ftp_directory\",
					\"groupId\": $groupId,
					\"refreshInterval\": $refreshInterval
				}",
				CURLOPT_HTTPHEADER => array(
					'Content-Type: application/json'
				),
			));
			
			$response = curl_exec($curl);
			curl_close($curl);

			
		/*	#$dbconn1 = pg_connect("host=10.10.0.29 port=5432 dbname=magicinfo user=postgres password=qazplm_01");
			$conn_string = "host=10.10.0.29 port=5432 dbname=magicinfo user=postgres password=qazplm_01 options='--client_encoding=UTF8'";
			$query = "UPDATE mi_cms_info_ftp_setting SET can_login_retry='N' WHERE content_name='$contentName'";
			$dbconn1 = pg_connect($conn_string);

			// Revisamos el estado de la conexion en caso de errores. 
			if(!$dbconn1) {
			echo "Error: No se ha podido conectar a la base de datos\n";
			} else {
			echo "Conexión exitosa\n";
			}
			$result = pg_query($dbconn1, $query);
			// Close connection
			pg_close($dbconn1);*/
			
			
			#try {
			#	$dbconn1 = pg_connect($conn_string);
							
    		#	if ( $dbconn1 ) {		 
        	#		$result = pg_query($dbconn1, $query);
    		#	} else {
        	#		throw new Exception('Unable to connect a DataBase PostGres: 10.10.0.29');
    		#	}
			#} catch(Exception $e) {
    		#	echo $e->getMessage();
			#}
			#pg_close($dbconn1);
		}
	}

