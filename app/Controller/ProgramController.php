<?php
 
App::uses('AppController', 'Controller');

class ProgramController extends AppController {

    public  $uses    = array('GCDS', 'DSM', 'Playlist');
    private $Modules = false;
    
    public function beforeFilter() {        
        
        parent::beforeFilter();        
        
        $userLogged = parent::checkLogged();
        
        if(isset($userLogged)){
			
            $this->Modules = parent::initModules();     
            $client = $this->GCDS->getClientSubdomain();
		   
			if(!$this->Modules) {
				die('Error al cargar el XML de configuracion de modulos');
			}			
            
			$modulesMenu = parent::getModulesMenu($this->Modules);
			
			$this->set('modulesMenu', $modulesMenu);                
			$this->set('userLogged', $userLogged);   
            $this->set('client', $client);
			$this->set('viewTitle', 'Programación');  
		}
        
    }      

    public function index() {

        
       
        
    }
    
    
}