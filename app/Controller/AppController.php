<?php


/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

if (isset($_GET['phpinfo'])) {
    phpinfo();
    die();
}

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public $uses = array('GCDS');

    public function getClientSubdomain()
    {
        return $this->GCDS->getClientSubdomain();
    }

    public function initModules()
    {

        if (STANDALONE_MODE === false) {

            $client_folder = $this->getClientSubdomain();

            if ($client_folder === false) {
                die('No se pudo determinar la carpeta de modulos del cliente');
            }
        }

        $modules = false;

        $modules_config_path = STANDALONE_MODE === false ? MODULES_CONFIG_PATH . $client_folder . DS : MODULES_CONFIG_PATH;

        if ($handle = @opendir($modules_config_path)) {

            foreach (glob($modules_config_path . "*.xml") as $file) {

                $moduleKey = str_replace('gcds-', '', basename($file));
                $moduleKey = str_replace('.xml', '', $moduleKey);

                $modules[$moduleKey] = $this->GCDS->load($file);
            }

            closedir($handle);
        } else {
            die('No se encontro la carpeta de configuracion: ' . $modules_config_path);
        }

        App::uses('ConnectionManager', 'Model');

        $db     = ConnectionManager::getDataSource('default');
        $tables = $db->listSources();

        foreach ($modules as $moduleKey => $module) {

            if (!in_array($moduleKey, $tables)) {

                $sql_field = false;

                foreach ($module['gcdb']['fields']['field'] as $field) {

                    if (isset($field['@key']) && isset($field['@db-type']) && !empty($field['@key']) && !empty($field['@db-type'])) {
                        $sql_field[] = '`' . $field['@key'] . '` ' . $field['@db-type'] . ' NULL';
                    }
                };

                if ($sql_field) {

                    $sql = "
                        CREATE TABLE IF NOT EXISTS `" . $moduleKey . "` (
                            `id` int(11) NOT NULL AUTO_INCREMENT," .
                        implode(',', $sql_field)
                        . ", `created` datetime NOT NULL,
                            `modified` datetime NOT NULL,
                            PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;
                        ";

                    $db->rawQuery($sql);
                }
            }
        }

        $systemTables = array('image', 'user', 'permission', 'publish_history', 'publish_player_status', 'playlists', 'playlists_items', 'program', 'meds_turnos_1', 'publish_alerts', 'publish_alerts_logs');

        foreach ($tables as $table) {

            if (!array_key_exists($table, $modules) && !in_array($table, $systemTables) && strpos($table, '_reportes') === FALSE) {
                //die($table);
                //  $db->rawQuery("DROP TABLE IF EXISTS $table");
            }
        }

        return $modules;
    }

    public function clear_cache() {
        $cachePaths = array('persistent','models');
        foreach($cachePaths as $config) {
            clearCache(null, $config);
        }
    }

    public function getModulesMenu($modules)
    {

        $menu = false;

        foreach ($modules as $key => $module) {
            $menu[$key] = array(
                'url' => $key,
                'name' => $module['gcdb']['content_name'],
                'icon' => $module['gcdb']['icon'],
                'publish_url' => isset($module['gcdb']['publish_url']) ? $module['gcdb']['publish_url'] : true,
                'download_csv' => isset($module['gcdb']['download_csv']) ? $module['gcdb']['download_csv'] : false,
                'button_add' => isset($module['gcdb']['button_add']) ? $module['gcdb']['button_add'] : true,
                'preview_url' => isset($module['gcdb']['preview_url']) ? $module['gcdb']['preview_url'] : false,
                'download_mode' => isset($module['gcdb']['download_mode']) ? $module['gcdb']['download_mode'] : false,
                'preview_width' => isset($module['gcdb']['preview_width']) ? $module['gcdb']['preview_width'] : false,
                'preview_height' => isset($module['gcdb']['preview_height']) ? $module['gcdb']['preview_height'] : false,
                'allow_import' => isset($module['gcdb']['allow_import']) ? $module['gcdb']['allow_import'] : true,
                'allow_import_xls' => (isset($module['gcdb']['allow_import_xls']) && $module['gcdb']['allow_import_xls'] ? true : false),
                'advice' => (isset($module['gcdb']['advice']) && $module['gcdb']['advice'] ? $module['gcdb']['advice'] : ''),
                'import_map' => (isset($module['gcdb']['import_map']) ? $module['gcdb']['import_map'] : false),
                'import_map_xls' => (isset($module['gcdb']['import_map_xls']) ? $module['gcdb']['import_map_xls'] : false),
                'no_editable' => isset($module['gcdb']['no_editable']) ? true : false,
                'no_checkbox' => isset($module['gcdb']['no_checkbox']) ? $module['gcdb']['no_checkbox'] : false,
                'ftp_connection' => isset($module['gcdb']['ftp_connection']) ? $module['gcdb']['ftp_connection'] : false,
                'reports_app' => isset($module['gcdb']['reports_app']) ? $module['gcdb']['reports_app'] : false,
                'notification_button' => isset($module['gcdb']['notification_button']) ? $module['gcdb']['notification_button'] : false,
                'multiplepublication' => isset($module['gcdb']['multiplepublication']) ? $module['gcdb']['multiplepublication'] : false,
                'job_module' => isset($module['gcdb']['job_module']) ? $module['gcdb']['job_module'] : false,
                'custom_csv' => isset($module['gcdb']['custom_csv']) ? $module['gcdb']['custom_csv'] : false,
                'cronjob' => isset($module['gcdb']['cronjob']) ? $module['gcdb']['cronjob'] : false,
                'hide_preview' => isset($module['gcdb']['hide_preview']) ? $module['gcdb']['hide_preview'] : false,
                'data_csv' => isset($module['gcdb']['data_csv']) ? $module['gcdb']['data_csv'] : false,
                'cliente' => isset($module['gcdb']['cliente']) ? $module['gcdb']['cliente'] : false,
                'sucursal' => isset($module['gcdb']['sucursal']) ? $module['gcdb']['sucursal'] : false,
                'tipos_platos' => isset($module['gcdb']['tipos_platos']) ? $module['gcdb']['tipos_platos'] : false,
                'justificar_borrado' => isset($module['gcdb']['justificar_borrado']) ? $module['gcdb']['justificar_borrado'] : false,
                'refresh_on_publish' => isset($module['gcdb']['refresh_on_publish']) ? $module['gcdb']['refresh_on_publish'] : false,
                'hide_layout' => isset($module['gcdb']['hide_layout']) ? $module['gcdb']['hide_layout'] : false,
                'export_excel' => isset($module['gcdb']['export_excel']) ? $module['gcdb']['export_excel'] : false
            );
        }

        return $menu;
    }

    public function checkLogged()
    {

        if (!CakeSession::read('LoggedUserNew')) {
            return $this->redirect('/users/login');
        } else {
            return CakeSession::read('LoggedUserNew');
        }
    }

    public function getLoggedUser()
    {

        if (!CakeSession::read('LoggedUserNew')) {
            return false;
        } else {
            return CakeSession::read('LoggedUserNew');
        }
    }

    public function checkRol($roles)
    {

        if (!CakeSession::read('LoggedUserNew')) {
            return $this->redirect('/users/login');
        } else {

            $user = CakeSession::read('LoggedUserNew');

            if (!in_array($user['User']['rol'], $roles)) {
                return $this->redirect('/users/login');
            }
        }
    }
}
